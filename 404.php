<?php 
  session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/datatable-materialize.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript" src="assets/js/jquery.js"></script>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        <style>
          html, body, main{
            height: 100%;
            width: 100%!important;
          }
          /* main{
            padding: 100px;
          } */
          .notfound{
            height: 100%;
            width: 100%!important;
            padding-left: 10px;
            padding-right: 10px;
          }
          .backlink{
            font-size: 14px;
            border-radius: 25px;
          } 
          .message{
            font-size: 14px;
          }
        </style>
	</head>
  <body>

    <main class="grey darken-3 center-align">
      <div class="notfound valign-wrapper">
        <h1 class="yellow-text text-darken-3"> <strong>OOPS!</strong> <br> 
          <span class="error">404 PAGE NOT FOUND</span> <br> 
          <!-- <small class="message orange-text center-align">The page you are looking for might have been removed or had its name changed or temporarily unavailable.</small> <br> -->
          <small><a href="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>" class="backlink white-text btn orange darken-3">Back to Homepage </a></small>
        </h1>        
      </div>
    </main>

  </body>
</html>