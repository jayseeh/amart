-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2019 at 05:50 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amart`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `aID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `aType` varchar(15) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `mname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL,
  `phoneNumber` varchar(11) NOT NULL,
  `uAddress` varchar(1000) NOT NULL,
  `uBrgy` varchar(2000) NOT NULL,
  `uCity` varchar(2000) NOT NULL,
  `uCityProvince` varchar(500) NOT NULL,
  `uPic` varchar(500) NOT NULL,
  `uEmail` varchar(100) NOT NULL,
  `uGender` varchar(10) NOT NULL,
  `secretQuestion` varchar(100) NOT NULL,
  `secretAnswer` varchar(20) NOT NULL,
  `uDept` varchar(60) NOT NULL,
  `uDateReg` date NOT NULL,
  `aStatus` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`aID`, `aType`, `fname`, `mname`, `lname`, `username`, `password`, `phoneNumber`, `uAddress`, `uBrgy`, `uCity`, `uCityProvince`, `uPic`, `uEmail`, `uGender`, `secretQuestion`, `secretAnswer`, `uDept`, `uDateReg`, `aStatus`) VALUES
(0000000007, 'Owner', 'Gregory', 'Peralt', 'House', 'pakboy123', 'wVwRtm7RCHNZE8Hns1fE5CZ5jdAKgPVs3aYvO2TOgHrZP3M9JodztcjdOxq7WJfGBMMKIChzuCHMPibwqUYt/', '09774553289', '3 | Bacsil | City Of San Fernando | La Union', '013314003', '013314', '0133', '334899903162078858-erpipu-cfitlore-2.png', '123@gmail.com', 'Male', 'Are you gay?', 'No', '', '0000-00-00', 'Confirmed'),
(0000000010, 'Admin', 'Raphael', 'Jordan', 'Ada', 'ayhatelove', '2e/OLA6vL1aJ.xrZOxRdqICBzbVXCgNoaKJqtvr9K32WXsQtseqN/HQ1va6QPztROF3AR3j/XX5onkJykbTc/', '09727171717', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000011, 'User', 'Amart ', 'Market', 'Digital ', 'ianace', 'lzlF.gmkvxBBEzb5xDENPO92duhDA9ftcoCM0T9PJ4BznE9VASSLzRM0ZXBC4cPBoZco8ckvj.2esTLsZALn/', '09829191919', '123 | Pagdaraoan | City Of San Fernando | La Union', '013314041', '013314', '0133', '94901657627075496-eiftp-culoerpri-ioam.png', 'ian.ace@gmail.com', 'Male', 'When you were young, what did you want to be when you grew up?', 'surgeon', '', '0000-00-00', 'Confirmed'),
(0000000014, 'User', 'Bunny', 'B', 'Bunny', 'bunny', 'mAWskE6e9/LLM3fpJN0hOJvuWJTfT6CBCJam2sZqCBrBVvzTuAldcVJrzCudmygIdHSqw8miqvzYNTDC1Hd2.', '09999999999', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000015, 'Owner', 'Jake', 'Q', 'Pilar', 'bunny', '.NAUen1XznX0rPp3skqXf/fiKItgcYtVdI/uEUr3UokXqKvBZ9OQ4u9MTI7zef0aI4SRuxlis2U02yUe.glF.', '00000000000', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000016, 'User', 'Gose', 'S', 'Sea', 'dog', 'vMgK.xZXPJYuxGodDN5.2yv.G0TOtDRw2UYprCQPZ15PFl3jVQVnx0JBoNsHlVQ2wGO3od9kNFoHKcRK09Gu/', '09999999999', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000017, 'User', 'Aaa', 'Aa', 'Aa', 'aaa', 'Pnq1gn/0.WQAZpc23x4UO/IDKQXOnS6ejA6AznY2DYWOR6zMiB49RlUp3HHx6IgyjgiqcXDmitSx/JgI/7qN1', '01111447521', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000018, 'Owner', 'Aa', 'Aa', 'Aa', 'aa', '7X3VfOUmk5WnhkDkS6wcUiSICwJx.2prSXUD3qiCTL13yiL3sYm1bNcrFKVW7Gw9dk6xNQtrcVlyJxP0CFpv.', '96987573442', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000019, 'User', 'Aa', 'Aa', 'Aa', 'aa', '7X3VfOUmk5WnhkDkS6wcUiSICwJx.2prSXUD3qiCTL13yiL3sYm1bNcrFKVW7Gw9dk6xNQtrcVlyJxP0CFpv.', '66668687687', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000020, 'Owner', 'Aa', 'Ss', 'Ss', 'ss', 'P15uQwqvXaE5eWBKLUrHaI/xyAJh2tX3PSQge/L.WIoLWUW/DvN1iavWIXFCN2vDYO9D0EfTcH85jorHmbHF.', '55576876876', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000021, 'User', 'Francist', 'B', 'Cara', 'franciscara', 'UmLPaS9JB5/ozA5/vCFIEqtB.Gx77q3KIC.1pp8P.psa318RnTqnrwQZ.zNTD6qhnuHwfsMoT.mQvPEwmZGP0', '09958732611', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000022, 'Owner', 'Aa', 'Aa', 'Aa', 'qqqqqq', 'lp.bZKhHMqxBZa.eOzDAGnQ63iQTZ3vu/YYPWXuBxcyqftEXNnZpAy/vWJe6jRkF1LdSrE13juLTtZNV6I3S0', '56666666632', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000023, 'User', 'B', 'B', 'B', 'Bookstores ', 'qmYyl7lnFRWBvUaqiFo0wd11qkgUBawnTddkUxHEgob6RiteF7IdR0mE8WYZprpJazmgz3925DIfbGd5Pn9S/', '12345678900', '44, Bacsil, Bacnotan, La Union', '', '', 'La Union', '380224253699483369-iepe-ptirfuoclr-partners.PNG', '', 'Male', '', '', '', '0000-00-00', 'Active'),
(0000000024, 'User', 'James', 'Y', 'Y', 'James12345', '18.roJXFRo/zKw1IXUPQWGNDIp5pKtbCJvHFZ..I5zgFItpMRs286FSYhpzv284g2VY5TBlSA29yEeE8knqy.', '00998876544', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000025, 'User', 'Daniel', 'A', 'A', 'qqqqqq', 'PKNZyaoL8grpi7loVOCHeMT/5iftimeQfQztBOYEuYVl2r6OHnYLjZFYIHs2EdoJNV39j4a2Z032XHfMKTPO/', '34252531125', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000026, 'User', 'Rizzel', 'Jayne', 'Marionne', 'Rjmga123', 'dO0N0TEcjXUW46HOjn4DgrY9DhY03PgQSXPfurOfvGcUNKzbG5y5lpYAJMYPVPPEiTFBTKHgbU7lHDSdwcUx/', '09277767453', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000027, '', '', '', '', '', 'Bxr0/yeUY.9TQdNDK6uNiWs.xyri8YNQop9oceOCgaKXTEnYDSycxXsLsUVuCAVnrA97etba84o07bd29m5w1', '', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000028, 'User', 'R', 'R', 'R', '12345r', 'mGh15lfnAMBZ6aG3/ur2nSZVGJL/7HUHMd/j2myTSXTfPFNLIfoZbk9/hndfb3BZ3Rf2JD5C.8oMdnhjHSW5.', '22222222222', '', '', '', '', 'default.png', '', '', '', '', '', '0000-00-00', 'Active'),
(0000000029, 'User', 'Gg', 'Mm', 'Mm', 'mmmmmm', 'ikc5ap.B0F.yiYNcQrNgRfDaoDM29KJN1CvpzUKP28ljI9sXa5pW2kMiShw415Ucd7HRcYmFsoMMfo7v7eVe.', '55555555555', '', '', '', '', 'default.png', '', '', '', '', '', '2019-06-23', 'Active'),
(0000000030, 'Owner', 'Jm', 'Jm', 'Jm', 'jjjjjj', 'mm6zcMGCUXnEzVxrQQ1wuglCs9l9.Va61ecrLoQoIc/S9BXYNiMJRZYOS5qKWai4VY4MK6habw5Y9ugagq0A/', '22222222222', '', '', '', '', 'default.png', '', '', '', '', '', '2019-06-23', 'Active'),
(0000000034, 'Admin', 'Admin', 'Admin', 'Admin', 'admin1', 'r8Saj5rI4Qlprw9vSVCgR.1O9u3Ceq1YXvN7cewUVcyt/KRveFDopt5q4HP6KHorAh1okyA6bAJO7Pc2lJGZ/', '09984363433', '', '', '', '', 'default.png', '', '', '', '', '', '2019-08-12', 'Active'),
(0000000031, 'User', 'Allen ', 'J.', 'Corpuz', 'a.collinnn', '39bZdYg2rWj88bamsDmZRd9rqTpoyjopwXyheDrsO4R/0SfhCi0N8m0nmv1e10.yfX02SLvYxXxT2oID1t2z.', '09496587229', 'Lubong, Tanqui, City Of San Fernando, La Union', '', '', 'La Union', 'default.png', '', 'Male', '', '', '', '2019-06-29', 'Active'),
(0000000032, 'User', 'Joseph', '', 'Ugay', 'Joseph1989', 'CAEkTAA9V7Ry35BuQgGHQSO/75z.C8VzgS/pYuYEE9QrOgDaaW0PXcipsowGcqWwGL.3inJEPmWnMszjDWF20', '09175642290', '', '', '', '', 'default.png', '', '', '', '', '', '2019-07-07', 'Active'),
(0000000033, 'Owner', 'Qwerty', 'Qwerty', 'Qwerty', 'qwerty', 'sX8T.19DtgUm1avnoAarVVaGPHnfBo8L/51.0fs8/tH4X.ChAqreFXBZHN83X6TSbnMfV49CeTlAWFmjchlI1', '11111111111', '', '', '', '', 'default.png', '', '', '', '', '', '2019-08-04', 'Active'),
(0000000035, 'Owner', 'Space', 'Jordan', 'Kamote', 'spacekamote', 'mVvUoXy/Xvi4nWRkvL4djyS7FGFw.xJv5oRdqLdE8t6E9ibgOX8jtGufLw03YVfmFz.iBzVnGUecMOomZTFC0', '09182737123', '', '', 'default.png', '', '', '', '', '', '', '', '2019-09-15', 'Active'),
(0000000036, 'User', 'Thom', 'De Villa', 'Patacsil', 'thommydp', 'QYQ9MIz9Npbjv7Ubpqt/zLXj/wBWEm5phWdR72oem6bjUiJJMRK53iVV6RFF8iQhA1CiJeYZL3wjDF7HZTlS/', '09123333333', '14 Colony Subdivision | San Francisco | City Of San Fernando | La Union', '013314052', '013314', '0133', '348416810566909051-uc-lprieerptofi-71200518_532141777553295_5931602603191304192_n.jpg', 'thom@goautodial.com', 'Male', 'Mother\'s maiden name', 'Vilma Cynthia Marque', '', '2019-09-15', 'Confirmed'),
(0000000037, 'Owner', 'Thom', 'Devilla', 'Owner', 'thomowner', 'eE0XAKfHFEI9Ywp0hx7m8duDoJAKYHaUBWRSHCq1K0zHUvLl4UFy1uZn64xoBTFXOosh.krDmtt7/3d/oU1K.', '09959886992', '14 Colony Subdivision | San Francisco | City Of San Fernando | La Union', '013314052', '013314', '0133', 'default.png', 'thom.patacsil@yahoo.com', 'Male', 'First beach visited', 'Acapulco', '', '2019-09-28', 'Confirmed');

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `businessID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `businessOwner` int(10) UNSIGNED ZEROFILL NOT NULL,
  `businessName` varchar(150) NOT NULL,
  `businessDescription` varchar(2000) NOT NULL,
  `businessCategory` varchar(50) NOT NULL,
  `businessPhone` varchar(2000) NOT NULL,
  `businessFB` varchar(2000) NOT NULL,
  `businessSubscriptionEnd` date NOT NULL,
  `businessStatus` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`businessID`, `businessOwner`, `businessName`, `businessDescription`, `businessCategory`, `businessPhone`, `businessFB`, `businessSubscriptionEnd`, `businessStatus`) VALUES
(0000000159, 0000000007, 'Thunderbird Resorts ', 'Thunderbird Resorts & Casinos -Poro Point is the only resort in the north with 5-star hotel amenities. It features luxurious rooms and villas, a myriad of dining options, recreational and wellness facilities, world-class casino, state of the art  event center and function rooms and 9-hole all weather golf course.', '0000000012', '888 8989', '555', '0000-00-00', 'Inactive'),
(0000000158, 0000000007, 'The Durable', 'Julios\' Glass Service supplies, installs, and assembles aluminum-framed windows and doors for residential and commercial buildings. Products and Services Offered o Sliding Doors & Windows o Casement & Awning Windows o Shower Enclosures o Swing Doors (ED, Frameless, Patch Fitting) o Store Fronts o Fixed Panels o Screen Doors & Windows o Steel Window Glasses o Jalousies / Louvers o Aluminum Showcases o Aquariums o Automobile and Boat Windshields o Glass Tints o Magnetic Cabinet Doors o Sliding Cabinet Doors o Table Glasses o Counter Glasses Other services offered: o Reconditioning of Aluminum Doors & Windows o Glass processing (Beveling, Etching, Holing)', '0000000049', '888 2222', '123', '0000-00-00', 'Active'),
(0000000157, 0000000007, 'Achile\'s Computer and CCTV', 'ACHILES COMPUTERS & CCTV started as a online seller of 2nd hand desktop computers and parts in the year 2006. Since then we add new products and improve our services to our customers to address the growing needs of computers and CCTV security systems.', '0000000073', '888 2011', '1111', '0000-00-00', 'Inactive'),
(0000000149, 0000000007, 'Computer Ben', '', '0000000020', '09984363433/', '123', '0000-00-00', 'Inactive'),
(0000000150, 0000000007, 'A\'s Store', '', '0000000060', '09984363433/', '123', '2019-03-03', 'Active'),
(0000000151, 0000000007, 'ABC Sari-Sari', '', '0000000001', '888 2145', '123', '0000-00-00', 'Active'),
(0000000152, 0000000007, 'ASA GraceCarinderia', 'ASA Grace Carinderia is a Filipino-inspired restaurant, based loosely on the affordable, roadside eateries, carinderias, of the Philippines. Our menu includes both traditional and modern Filipino dishes, as well as referencing other South East Asian flavors. Many of the dishes will be available in either rice bowls or as sandwiches, allowing for the experience of a new type of cuisine.', '0000000022', '888 1236 ', '123', '0000-00-00', 'Active'),
(0000000153, 0000000007, 'The Pet Shop', '', '0000000009', '888 9632', '123', '0000-00-00', 'Inactive'),
(0000000154, 0000000007, 'Mr. Barbershop', '', '0000000036', '8880023', '123', '0000-00-00', 'Inactive'),
(0000000155, 0000000007, 'Baker\'s Bakeshop', 'This establishment is located in the heart of Old Town just steps away from Old Town Square. It has become a household name in the area for its baked goods. Everything in the shop is made from scratch and 100% fresh as goods are baked daily. The diverse selection of goods is quite extensive ranging from bread to loaf cakes, savory pies, canapes, and even wedding cakes. Bakeshop even makes homemade sourdough bread, which is one of the most exciting things I discovered about this establishment in Prague! Stop by the bakery during your sightseeing adventures and warm up with a cup of soup or savor a piece of quiche; Bakeshop has a small yet comfortable seating area. The Bakeshop also serves warm and non-alcoholic beverages. You can also hire their catering service for a personal or business party, or commission a custom-made wedding cake. The Bakeshop recently underwent a major renovation; it is bigger, better and tastier than ever.', '0000000041', '888 2356', '123', '0000-00-00', 'Inactive'),
(0000000156, 0000000007, 'Luna Bookstore', '', '0000000029', '', '', '0000-00-00', 'Inactive'),
(0000000135, 0000000007, 'Small 1', '', '0000000008', '09984363433/', '123', '0000-00-00', 'Inactive'),
(0000000136, 0000000007, 'Small 2', '', '0000000007', '09984363433/', '123', '0000-00-00', 'Inactive'),
(0000000137, 0000000007, 'Small 3', '', '0000000009', '09984363433/', '123', '0000-00-00', 'Inactive'),
(0000000138, 0000000007, 'Small 4', '', '0000000019', '09984363433/', '123', '0000-00-00', 'Inactive'),
(0000000139, 0000000007, 'Free 1', '', '0000000002', '09984363433/', '123', '0000-00-00', 'Active'),
(0000000140, 0000000007, 'Free 2', '', '0000000001', '09984363433/', '123', '0000-00-00', 'Active'),
(0000000141, 0000000007, 'Free 3', '', '0000000003', '09984363433/', '123', '0000-00-00', 'Active'),
(0000000142, 0000000007, 'Medium 1', '', '0000000010', '09984363433/', '123', '0000-00-00', 'Inactive'),
(0000000143, 0000000007, 'Medium 2', '', '0000000012', '09984363433/', '123', '0000-00-00', 'Inactive'),
(0000000144, 0000000007, 'Medium 3', '', '0000000011', '09984363433/', '123', '0000-00-00', 'Inactive'),
(0000000145, 0000000007, 'Worship 1', '', '0000000013', '', '', '0000-00-00', 'Inactive'),
(0000000146, 0000000007, 'Worship 2', '', '0000000014', '', '', '0000-00-00', 'Inactive'),
(0000000147, 0000000007, 'Worship 3', '', '0000000015', '', '', '0000-00-00', 'Inactive'),
(0000000148, 0000000007, 'Worship 4', '', '0000000016', '', '', '0000-00-00', 'Inactive'),
(0000000134, 0000000007, 'Lorma', 'We envision Lorma Colleges as an educational institution with a global perspectiveâ€“ emphasizing quality, Christian values, and leadership skills relevant to national development.\r\nOur mission is to empower students for service anywhere in the world through Christian-inspired, quality-driven, and service oriented education and training.\r\n\r\nWe believe in a student-centered approach to education and management.\r\nWe believe in achieving a lot with limited resources, by knowing what other institutions are doing, adapting or improving and using that can be applied to the College.\r\nWe believe that a strong continuing faculty and staff development program in the college is a vital component of the total effort to attain the main goal of the College.\r\nWe believe in the stability and strength as a base for achieving quality education, relevant to national needs, and development of the total person.\r\nWe believe above all that giving honor and glory to God in everything we do, is most important in all official activities of the College.', '0000000011', '09984363433/', 'http://lorma.edu/', '0000-00-00', 'Inactive'),
(0000000133, 0000000007, 'Mormon', '', '0000000014', '', '', '0000-00-00', 'Inactive'),
(0000000132, 0000000007, 'Pet World', '', '0000000009', '09984363433/', '123', '0000-00-00', 'Inactive'),
(0000000131, 0000000007, 'Maris', '', '0000000010', '09984363433/', 'xz', '0000-00-00', 'Inactive'),
(0000000130, 0000000007, 'Romels Store', '', '0000000001', '09984363433/', 'erqr', '0000-00-00', 'Active'),
(0000000160, 0000000007, 'Sunset Bay Resort', 'â€œAt Sunset Bay we take pride in our friendly personal service and tranquil, relaxing surroundings. The resortâ€™s tropical gardens provide the perfect place to relax while, for the more active, there are walks along the beach or the opportunity to explore the varied delights of the coral reef.â€', '0000000012', '888 8888', 'xz', '0000-00-00', 'Inactive'),
(0000000161, 0000000007, 'Techno ', 'A DTI registered business that supplies computers, parts and accessories to companies, compter shops including pisonet business and other forms of \"piso\" servces.\r\nOur prices are just the same or even lower with the stores at Gilmore.\r\nWe can accommodate BULK orders.\r\nWe got ALL your computers, parts and accessories needs. Kindly browse our shop to see more. \r\nWe have a physical store located  at Angono Rizal.\r\nWe offers CASH ON DELIveRY and free shippin', '0000000020', '888 2323', '123', '0000-00-00', 'Inactive'),
(0000000162, 0000000007, 'Ramsy\'s', '', '0000000072', '121233', '2322', '0000-00-00', 'Inactive'),
(0000000163, 0000000007, 'Malayan', '', '0000000029', '09984363433/', 'xz', '0000-00-00', 'Inactive'),
(0000000164, 0000000007, 'j', '', '0000000037', '09984363433/', '123', '0000-00-00', 'Inactive'),
(0000000165, 0000000007, 'happy', '', '0000000009', '09984363433/', 'z', '0000-00-00', 'Inactive'),
(0000000166, 0000000011, 'Bakit', '', '0000000001', '35454', 'a', '0000-00-00', 'Active'),
(0000000167, 0000000007, 'Iglesia Ni Cristo', 'The Iglesia Ni Cristo (Church Of Christ) is a Christian religion whose primary purpose is to serve and worship the Almighty God based on His teachings recorded in the Bible. It is not a denomination or sect. It is neither affiliated to any federation or religious bodies nor itself an assembly of smaller churches or religious organizations.\r\n\r\nThis Church is for every one who heeds the call of God and embraces its faithâ€”regardless of oneâ€™s race, nationality, cultural background, social standing, economic status, and educational attainment.\r\n\r\n', '0000000025', '', '', '0000-00-00', 'Inactive'),
(0000000168, 0000000007, 'L', '', '0000000023', '22', '41', '0000-00-00', 'Active'),
(0000000169, 0000000007, 'Eliz', '', '0000000022', '35454', 'a', '0000-00-00', 'Active'),
(0000000170, 0000000007, 'sd', '', '0000000008', '', '', '0000-00-00', 'Inactive'),
(0000000171, 0000000007, 'Leadgenerationapp', '', '0000000074', '', '', '0000-00-00', 'Active'),
(0000000172, 0000000007, 'eustass\' place', '', '0000000001', '', '', '0000-00-00', 'Active'),
(0000000173, 0000000007, 'this new bisnes', '', '0000000021', '', '', '0000-00-00', 'Active'),
(0000000174, 0000000007, 'testng 1', '', '0000000001', '', '', '0000-00-00', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `businesscalendar`
--

CREATE TABLE `businesscalendar` (
  `calendarID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `bisCalendarIDkey` int(10) UNSIGNED ZEROFILL NOT NULL,
  `bisSchedDay` varchar(100) NOT NULL,
  `bisSchedTime` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `businesscalendar`
--

INSERT INTO `businesscalendar` (`calendarID`, `bisCalendarIDkey`, `bisSchedDay`, `bisSchedTime`) VALUES
(0000000042, 0000000167, ' Sunday', '07:30 AM - 09:30 AM'),
(0000000041, 0000000161, ' Monday Tuesday Wednesday', '05:00 AM - 04:31 PM'),
(0000000040, 0000000160, ' Monday Wednesday Friday', '11:17 AM - 04:17 PM'),
(0000000039, 0000000159, ' Monday Thursday Saturday Sunday', '04:13 AM - 02:13 PM'),
(0000000038, 0000000158, ' Tuesday Wednesday', '07:06 AM - 04:07 PM'),
(0000000037, 0000000152, ' Monday Wednesday Saturday Sunday', '09:00 AM - 03:00 PM'),
(0000000036, 0000000155, ' Monday Thursday Friday Saturday', '07:12 AM - 05:12 PM'),
(0000000035, 0000000151, ' Monday Thursday Sunday', '07:49 PM - 03:49 AM'),
(0000000034, 0000000150, ' Monday Tuesday Wednesday Thursday Friday Saturday Sunday', '06:30 AM - 02:44 PM'),
(0000000033, 0000000134, ' Monday Tuesday Wednesday Thursday Friday', '08:00 AM - ');

-- --------------------------------------------------------

--
-- Table structure for table `businesslocation`
--

CREATE TABLE `businesslocation` (
  `bisLocationID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `bisLocIDkey` int(10) UNSIGNED ZEROFILL NOT NULL,
  `bisNumber` varchar(100) NOT NULL,
  `bisStreet` varchar(100) NOT NULL,
  `bisBrgy` varchar(100) NOT NULL,
  `bisCityTown` varchar(100) NOT NULL,
  `bisProvince` varchar(100) NOT NULL,
  `bisLocLat` varchar(100) NOT NULL,
  `bisLocLong` varchar(100) NOT NULL,
  `provCode` varchar(60) NOT NULL,
  `munCode` varchar(60) NOT NULL,
  `brgyCode` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `businesslocation`
--

INSERT INTO `businesslocation` (`bisLocationID`, `bisLocIDkey`, `bisNumber`, `bisStreet`, `bisBrgy`, `bisCityTown`, `bisProvince`, `bisLocLat`, `bisLocLong`, `provCode`, `munCode`, `brgyCode`) VALUES
(0000000146, 0000000155, '', '', '', 'Santa Maria', 'Ilocos Sur', '16.6243719', '120.31612529999998', '', '', ''),
(0000000145, 0000000154, '', '', 'Cabugbugan', 'Tagudin', 'Ilocos Sur', '16.6243719', '120.31612529999998', '', '', ''),
(0000000143, 0000000152, '', '', 'Saytan', 'Pugo', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000144, 0000000153, '', '', 'Cabaroan Laud', 'City Of Vigan', 'Ilocos Sur', '16.6243719', '120.31612529999998', '', '', ''),
(0000000140, 0000000149, '', '', 'Biday', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000142, 0000000151, '', '', 'Banguian', 'Abulug', 'Cagayan', '16.6243719', '120.31612529999998', '', '', ''),
(0000000141, 0000000150, '', '', 'Cabangila', 'Altavas', 'Aklan', '16.6243719', '120.31612529999998', '', '', ''),
(0000000126, 0000000135, '', '', 'Malabago', 'Santa Cruz', 'Zambales', '16.6243719', '120.31612529999998', '', '', ''),
(0000000137, 0000000146, '', '', 'Biday', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000138, 0000000147, '', '', 'Bimmotobot', 'Naguilian', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000139, 0000000148, '', '', 'Damortis', 'Rosario', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000136, 0000000145, '', '', 'Adams (Pob.)', 'Adams', 'Ilocos Norte', '16.6243719', '120.31612529999998', '', '', ''),
(0000000135, 0000000144, '', '', 'Cabaroan (Pob.)', 'San Emilio', 'Ilocos Sur', '16.6243719', '120.31612529999998', '', '', ''),
(0000000131, 0000000140, '', '', 'Fabrica', 'Lal-Lo', 'Cagayan', '16.6243719', '120.31612529999998', '', '', ''),
(0000000132, 0000000141, '', '', 'Cabaroan (Pob.)', 'San Emilio', 'Ilocos Sur', '16.6243719', '120.31612529999998', '', '', ''),
(0000000133, 0000000142, '', '', 'San Rafael (Pob.)', 'San Narciso', 'Zambales', '16.6243719', '120.31612529999998', '', '', ''),
(0000000134, 0000000143, '', '', 'Sumangday', 'Panglima Sugala', 'Tawi-Tawi', '16.6243719', '120.31612529999998', '', '', ''),
(0000000129, 0000000138, '', '', 'Cabaroan (Pob.)', 'San Emilio', 'Ilocos Sur', '16.6243719', '120.31612529999998', '', '', ''),
(0000000130, 0000000139, '', '', 'Naulo', 'Santa Cruz', 'Zambales', '16.6243719', '120.31612529999998', '', '', ''),
(0000000127, 0000000136, '', '', 'Cabaroan (Pob.)', 'San Emilio', 'Ilocos Sur', '16.6243719', '120.31612529999998', '', '', ''),
(0000000128, 0000000137, '', '', 'Cabaroan (Pob.)', 'San Emilio', 'Ilocos Sur', '16.6243719', '120.31612529999998', '', '', ''),
(0000000125, 0000000134, '', '', 'Bungro', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000123, 0000000132, '', '', 'Bungro', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000124, 0000000133, '', '', 'Dilaguidi', 'Dilasag', 'Aurora', '16.6243719', '120.31612529999998', '', '', ''),
(0000000122, 0000000131, '', '', 'San Francisco', 'Santa Ignacia', 'Tarlac', '16.6243719', '120.31612529999998', '', '', ''),
(0000000121, 0000000130, '', '', 'San Rafael (Pob.)', 'San Narciso', 'Zambales', '16.6243719', '120.31612529999998', '', '', ''),
(0000000147, 0000000156, '', '', '', '', '', '16.6243719', '120.31612529999998', '', '', ''),
(0000000148, 0000000157, '', '', 'Magallanes (Pob.)', 'Luna', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000149, 0000000158, '', '', 'Poro', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000150, 0000000159, '', '', 'Poro', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000151, 0000000160, '', '', 'Bungro', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000152, 0000000161, '', '', 'San Vicente', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000153, 0000000162, '', '', 'Birunget', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000154, 0000000163, '', '', 'Bagacay', 'Alabel', 'Sarangani', '16.6243719', '120.31612529999998', '', '', ''),
(0000000155, 0000000164, '', '', 'Laug', 'Mexico', 'Pampanga', '16.6243719', '120.31612529999998', '', '', ''),
(0000000156, 0000000165, '', '', 'Garawon', 'Hernani', 'Eastern Samar', '16.6243719', '120.31612529999998', '', '', ''),
(0000000157, 0000000166, '', '', 'Amilongan', 'Alilem', 'Ilocos Sur', '16.6243719', '120.31612529999998', '', '', ''),
(0000000158, 0000000167, '', '', 'Apaleng', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000159, 0000000168, '', '', 'Abut', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000160, 0000000169, '', '', 'Bacsil', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000161, 0000000170, '', '', '', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000162, 0000000171, '', '', '', 'City Of San Fernando', 'La Union', '16.6243719', '120.31612529999998', '', '', ''),
(0000000163, 0000000172, '', '', '', '', '', '', '', '', '', ''),
(0000000164, 0000000173, '', '', '', '', '', '', '', '', '', ''),
(0000000165, 0000000174, '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `businessphotos`
--

CREATE TABLE `businessphotos` (
  `photoID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `bisPhotoIDkey` int(10) UNSIGNED ZEROFILL NOT NULL,
  `bisPhoto` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `businessphotos`
--

INSERT INTO `businessphotos` (`photoID`, `bisPhotoIDkey`, `bisPhoto`) VALUES
(0000000069, 0000000155, '292071133449603928-hrpetocoov-julie_s_bakeshop.png'),
(0000000068, 0000000151, '210868026269286439-ocrpootveh-Sari-Sari.jpg'),
(0000000067, 0000000151, '857041005974329473-eovocpotrh-sstore.jpg'),
(0000000066, 0000000151, '917634854192118159-perovohtoc-store.jpg'),
(0000000065, 0000000151, '368337735267337950-vhcoptooer-download (6).jpg'),
(0000000064, 0000000149, '3717480606089928-crvoopteho-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000063, 0000000148, '59854831976251721-vocroeothp-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000062, 0000000147, '360472127404102516-rtooeovhpc-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000061, 0000000146, '937152236797022458-optrvehcoo-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000060, 0000000145, '606683099735550200-hoovtecpor-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000059, 0000000135, '252367575163472707-opohvotcre-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000058, 0000000136, '114628500153026762-cterooopvh-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000057, 0000000137, '704121976112647591-htreopvcoo-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000056, 0000000138, '994354155262225010-ophvoctore-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000055, 0000000144, '1771925310142918-hptvreocoo-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000054, 0000000143, '407673687659454448-etcoovohrp-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000053, 0000000142, '205952730195579206-rotpcveoho-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000052, 0000000141, '686797863117881354-optevorohc-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000051, 0000000140, '296797062258510343-hvoepootrc-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000049, 0000000134, '321800566146317011-pvotorheoc-download (1).png'),
(0000000047, 0000000131, '185573345131675975-pcotrohove-15.png'),
(0000000050, 0000000139, '354470642557544620-rheocotvpo-57358066_2134387870007739_4439440576974159872_n.png'),
(0000000070, 0000000152, '546342587607015952-cetoovrpoh-eat.png'),
(0000000071, 0000000157, '685518324910368746-ovorcphtoe-aaa.jpg'),
(0000000072, 0000000158, '839364276934116288-ochrtvopeo-julio.jpg'),
(0000000073, 0000000159, '357787480710346214-ptcrhoooev-poro-main-page-header.jpg'),
(0000000074, 0000000160, '340553922659373294-hovooperct-sunset.jpg'),
(0000000075, 0000000161, '855270662573519116-cerpoothov-comp.jpg'),
(0000000076, 0000000162, '952372789309307770-cortoepvho-rest.jpg'),
(0000000077, 0000000132, '185829456772452946-orocotehvp-pet.jpg'),
(0000000078, 0000000167, '310345842954880599-oroohtpevc-INC-Ewa-Beach-house-of-worship-01.jpg'),
(0000000079, 0000000174, '108062745952453613-octovpreho-address.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `businessps`
--

CREATE TABLE `businessps` (
  `businessPSID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `businessPSIDkey` int(10) UNSIGNED ZEROFILL NOT NULL,
  `businessPSName` varchar(100) NOT NULL,
  `businessPSDesc` varchar(2000) NOT NULL,
  `businessPSPrice` decimal(10,2) NOT NULL,
  `qty` int(4) NOT NULL,
  `businessPSType` varchar(15) NOT NULL,
  `businessPSImage` varchar(500) NOT NULL,
  `psHomeService` varchar(20) NOT NULL,
  `businessPSStatus` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `businessps`
--

INSERT INTO `businessps` (`businessPSID`, `businessPSIDkey`, `businessPSName`, `businessPSDesc`, `businessPSPrice`, `qty`, `businessPSType`, `businessPSImage`, `psHomeService`, `businessPSStatus`) VALUES
(0000000075, 0000000130, 'Example Product 1', 'rwart', '212.00', 1, 'Product', '449492400551833889-cpvrcroduis-eet-download (2).jpg', 'Nope', 'Active'),
(0000000076, 0000000131, 'Example Services 1', 'aa', '212.00', 0, 'Service', '72387038998200778-pvctudi-esecorr-00.JPG', '', 'Active'),
(0000000077, 0000000131, 'Example Product 1', 'sfsa', '212.00', 1, 'Product', '544412554694600222-iescptocrde-rvu-5.jpg', 'Nope', 'Active'),
(0000000078, 0000000150, 'Ligo Sardines (Original)', 'Pwede sa sutanghon at isabay lang sa kanin', '18.00', 1, 'Product', '753777019936689348-peids-cceuovrtr-ligo.jpg', 'Nope', 'Active'),
(0000000079, 0000000151, 'Milo', 'Start your day with Milo', '8.00', 1, 'Product', '888844014165048991-peuocctvdr-sire-milo.jpg', 'Nope', 'Active'),
(0000000080, 0000000151, 'Joy Dishwashing Liquid', 'Out of Stock', '7.00', 1, 'Product', '642591247956491925--duesvctorrepic-joy.jpg', 'Nope', 'Active'),
(0000000081, 0000000155, 'Spanish Bread', '5 Pieces available', '5.00', 1, 'Product', '474113460433974528-tevecdrucripos--download (7).jpg', 'Nope', 'Active'),
(0000000082, 0000000155, 'Kababayan', '5 for 20 Pesos', '5.00', 1, 'Product', '33539809888763394-repts-viceuocrd-kabababyan.jpg', 'Nope', 'Active'),
(0000000083, 0000000155, 'Pandesal', 'Available only in mornings', '2.00', 1, 'Product', '811781719678064128-redicsrceou-ptv-pandesal.jpg', 'Nope', 'Active'),
(0000000085, 0000000152, 'Catering', 'Contact us for packages', '212.00', 0, 'Service', '786409722878430960-vt-ocsrcpiuedre-cater.jpg', '', 'Active'),
(0000000086, 0000000157, 'CPU', 'Brand New', '1500.00', 1, 'Product', '93584927123514538-crceueptirv-ods-cpu.jpg', 'Nope', 'Active'),
(0000000087, 0000000157, 'Monitor', 'Flat Screen 15\"', '5000.00', 1, 'Product', '112626966938783-evoru-ditcercsp-moni.jpg', 'Nope', 'Active'),
(0000000088, 0000000157, 'CCTV Camera', 'Limited Edition', '40000.00', 1, 'Product', '324795398762212510-ecrirts-edcopuv-home_product_02.png', 'Nope', 'Active'),
(0000000089, 0000000157, 'CCTV Installation', 'Contact us for more details', '7500.00', 0, 'Service', '685976515529051654-tecisd-rpecvuor-cc.jpg', '', 'Active'),
(0000000090, 0000000158, 'Window', 'Available in different colors', '4500.00', 1, 'Product', '183773763765327401-crvestpuride-co-Factory-price-grill-design-aluminum-glass-top.jpg_350x350.jpg', 'Nope', 'Active'),
(0000000091, 0000000158, 'Sliding Door', 'Limited Stocks', '15000.00', 1, 'Product', '598114832503552154-desite-coprvucr-249574.jpeg', 'Nope', 'Active'),
(0000000092, 0000000158, 'Installation', 'Prices may vary depending on the days of labor', '5000.00', 0, 'Service', '234742962829563508-verdeu-sptccior-install.jpg', '', 'Active'),
(0000000093, 0000000160, 'Entrance Fee', 'We offer Family and Barkada Packages', '300.00', 0, 'Service', '526322911818810270-sedpitcrv-eocur-ssss.jpg', '', 'Active'),
(0000000094, 0000000161, 'Unit Rent', '10 pesos per hour during midnights', '15.00', 0, 'Service', '13821182294242302-oidterrpsuc-cev-Main-Photo.jpg', '', 'Active'),
(0000000095, 0000000152, 'www', 'ww', '1.00', 1, 'Product', '574493643620148977-ieccvdrso-rteup-2.jpg', 'Nope', 'Active'),
(0000000096, 0000000169, 'www', '65', '1.00', 1, 'Product', '483875636927837019-voer-tucedicpsr-download (5).jpg', 'Nope', 'Active'),
(0000000097, 0000000169, 'www', '3+', '1.00', 0, 'Service', '868975004311801313-ieoscvtrr-epucd-000.jpg', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cartID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `productID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `userID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `cartBisIDkey` int(10) UNSIGNED ZEROFILL NOT NULL,
  `cartRefCode` varchar(15) NOT NULL,
  `cartItemType` varchar(20) NOT NULL,
  `serviceType` varchar(20) NOT NULL,
  `itemName` varchar(50) NOT NULL,
  `itemQty` int(4) NOT NULL,
  `itemPrice` decimal(10,2) NOT NULL,
  `cartTotal` decimal(10,2) NOT NULL,
  `cartStatus` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cartID`, `productID`, `userID`, `cartBisIDkey`, `cartRefCode`, `cartItemType`, `serviceType`, `itemName`, `itemQty`, `itemPrice`, `cartTotal`, `cartStatus`) VALUES
(0000000034, 0000000059, 0000000007, 0000000111, '74Tf32E1q9NLy8c', 'Product', '', 'Example Product 1', 1, '212.00', '212.00', 'Approved'),
(0000000035, 0000000060, 0000000011, 0000000111, '0RhbB4laPD8388p', 'Product', '---', 'Example Product 2', 1, '212.00', '212.00', 'Declined'),
(0000000036, 0000000059, 0000000011, 0000000111, '0RhbB4laPD8388p', 'Product', '---', 'Example Product 1', 3, '212.00', '636.00', 'Declined'),
(0000000039, 0000000059, 0000000011, 0000000111, 'zQa32lZI5pe3Aq7', 'Product', '---', 'Example Product 1', 1, '212.00', '212.00', 'Declined'),
(0000000040, 0000000059, 0000000007, 0000000111, 'PF3jxdmpTto3rbZ', 'Product', '---', 'Example Product 1', 1, '212.00', '212.00', 'Declined'),
(0000000041, 0000000060, 0000000007, 0000000111, 'PF3jxdmpTto3rbZ', 'Product', '---', 'Example Product 2', 1, '212.00', '212.00', 'Declined'),
(0000000042, 0000000063, 0000000007, 0000000110, '', 'Product', '---', 'Example Product 1', 1, '212.00', '212.00', 'Declined'),
(0000000043, 0000000059, 0000000007, 0000000111, 'O02ljKq6hF172bQ', 'Product', '---', 'Example Product 1', 14, '212.00', '2968.00', 'Declined'),
(0000000044, 0000000060, 0000000007, 0000000111, 'O02ljKq6hF172bQ', 'Product', '---', 'Example Product 2', 12, '212.00', '2544.00', 'Declined'),
(0000000045, 0000000059, 0000000011, 0000000111, 'H0B7k11cqFZN5I4', 'Product', '---', 'Example Product 1', 1, '212.00', '212.00', 'Declined'),
(0000000046, 0000000055, 0000000007, 0000000099, 'Yin7NLD8zITxygK', 'Product', '---', 'Example Product 1', 1, '212.00', '212.00', 'Declined'),
(0000000047, 0000000059, 0000000007, 0000000111, '', 'Product', '---', 'Example Product 1', 4, '212.00', '848.00', 'Declined'),
(0000000051, 0000000073, 0000000007, 0000000122, 'Wtko1A76P0jEdNR', 'Product', '---', 'Example Product 1', 1, '212.00', '212.00', 'Declined'),
(0000000052, 0000000067, 0000000007, 0000000115, 'z29A3qoYE8ts2TG', 'Product', '---', 'Example Product 1', 1, '212.00', '212.00', 'Pending'),
(0000000053, 0000000055, 0000000007, 0000000099, 'mdWDXreEOTNoC0M', 'Product', '---', 'Example Product 1', 1, '212.00', '212.00', 'Approved'),
(0000000055, 0000000059, 0000000007, 0000000111, '', 'Product', '---', 'Example Product 1', 1, '212.00', '212.00', 'Declined'),
(0000000056, 0000000090, 0000000007, 0000000158, 'nb7rI2pFe27WgAz', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Approved'),
(0000000057, 0000000090, 0000000011, 0000000158, 'whY3mAarM5IxFL1', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Pending'),
(0000000060, 0000000077, 0000000007, 0000000131, '', 'Product', '', 'Example Product 1', 1, '212.00', '212.00', 'Declined'),
(0000000061, 0000000077, 0000000007, 0000000131, '', 'Product', '', 'Example Product 1', 2, '212.00', '424.00', 'Declined'),
(0000000062, 0000000090, 0000000007, 0000000158, 'nb7rI2pFe27WgAz', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Approved'),
(0000000063, 0000000090, 0000000007, 0000000158, 'OExNXwp0444225T', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Pending'),
(0000000064, 0000000090, 0000000007, 0000000158, 'OExNXwp0444225T', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Pending'),
(0000000065, 0000000090, 0000000007, 0000000158, 'OExNXwp0444225T', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Pending'),
(0000000066, 0000000090, 0000000007, 0000000158, 'c3dD52qr2EL7zb8', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Approved'),
(0000000067, 0000000091, 0000000007, 0000000158, 'c3dD52qr2EL7zb8', 'Product', '', 'Sliding Door', 1, '15000.00', '15000.00', 'Approved'),
(0000000068, 0000000090, 0000000007, 0000000158, '', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Declined'),
(0000000069, 0000000090, 0000000011, 0000000158, 'whY3mAarM5IxFL1', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Pending'),
(0000000070, 0000000090, 0000000011, 0000000158, 'whY3mAarM5IxFL1', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Pending'),
(0000000072, 0000000090, 0000000011, 0000000158, '2onr33SdPGHqbYE', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Approved'),
(0000000073, 0000000090, 0000000011, 0000000158, '2onr33SdPGHqbYE', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Approved'),
(0000000074, 0000000090, 0000000011, 0000000158, 'G77B3yYFIWM78rx', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Pending'),
(0000000075, 0000000090, 0000000011, 0000000158, 'G77B3yYFIWM78rx', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Pending'),
(0000000076, 0000000091, 0000000011, 0000000158, 'G77B3yYFIWM78rx', 'Product', '', 'Sliding Door', 1, '15000.00', '15000.00', 'Pending'),
(0000000077, 0000000090, 0000000011, 0000000158, 'etj3F2HlKq2w44P', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Pending'),
(0000000078, 0000000090, 0000000011, 0000000158, 'M4ct3197OZD8wbE', 'Product', '', 'Window', 1, '4500.00', '4500.00', 'Approved'),
(0000000079, 0000000091, 0000000011, 0000000158, 'M4ct3197OZD8wbE', 'Product', '', 'Sliding Door', 1, '15000.00', '15000.00', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `cartprocess`
--

CREATE TABLE `cartprocess` (
  `cartprocessID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `businessIDkey` int(10) UNSIGNED ZEROFILL NOT NULL,
  `cartRefCodekey` varchar(20) NOT NULL,
  `cartTotal` varchar(50) NOT NULL,
  `cartReqDate` date NOT NULL,
  `cartpickupDateTime` varchar(50) NOT NULL,
  `msg` varchar(2000) NOT NULL,
  `cartprocessStatus` varchar(20) NOT NULL,
  `reply` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cartprocess`
--

INSERT INTO `cartprocess` (`cartprocessID`, `businessIDkey`, `cartRefCodekey`, `cartTotal`, `cartReqDate`, `cartpickupDateTime`, `msg`, `cartprocessStatus`, `reply`) VALUES
(0000000012, 0000000008, 'lmN8ToIj7AJ5qaZ', '40000.00', '2019-05-13', 'May 14, 2019 | 12:00 PM', '55', 'Approved', ''),
(0000000013, 0000000008, 'Tn0fi09JyZ486FL', '500.00', '2019-05-13', 'May 24, 2019 | 12:30 AM', '99', 'Pending', ''),
(0000000014, 0000000008, '1nem1yx5ZQHldaT', '69.00', '2019-05-13', 'May 29, 2019 | 11:00 AM', 'fefe', 'Pending', ''),
(0000000015, 0000000008, 'TbHqCw679S5I9Xm', '10755.00', '2019-05-13', 'May 25, 2019 | 12:00 PM', '33', 'Approved', 'Reason 1'),
(0000000016, 0000000008, 'Dxnet7aOZ7mdC9l', '10755.00', '2019-05-25', 'May 26, 2019 | 06:30 PM', 'WHEEEEEEEEEEEEEEEEEEEEEEEEEEEEE', 'Pending', ''),
(0000000017, 0000000111, '74Tf32E1q9NLy8c', '212.00', '2019-05-26', 'May 29, 2019 | 05:25 PM', 'WHEEEEEEEEEEEEEEEEEEEEEEEEEEEEE', 'Approved', ''),
(0000000018, 0000000111, 'Om7dRz2Fk9Xq1HN', '', '2019-05-26', 'May 27, 2019 | 06:31 PM', 'bnn', 'Pending', ''),
(0000000019, 0000000111, '0RhbB4laPD8388p', '848.00', '2019-06-01', 'Jun 24, 2019 | 06:30 PM', 'bnn', 'Declined', 'Reason 1'),
(0000000020, 0000000111, 'zQa32lZI5pe3Aq7', '212.00', '2019-06-01', 'Jun 19, 2019 | 04:20 PM', 'BAka hindi ko makuha Agad Dahil MAy DADAanan Pa ako SIr', 'Declined', 'Reason 2'),
(0000000021, 0000000111, 'PF3jxdmpTto3rbZ', '424.00', '2019-06-02', 'Jun 25, 2019 | 06:30 AM', 'BAka hindi ko makuha Agad Dahil MAy DADAanan Pa ako SIr', 'Declined', 'Reason 1'),
(0000000022, 0000000111, 'O02ljKq6hF172bQ', '5512.00', '2019-06-02', 'Jun 20, 2019 | 06:30 AM', 'BAka hindi ko makuha Agad Dahil MAy DADAanan Pa ako SIr', 'Declined', 'Reason 1'),
(0000000023, 0000000111, 'H0B7k11cqFZN5I4', '212.00', '2019-06-02', 'Jun 11, 2019 | 06:30 AM', 'BAka hindi ko makuha Agad Dahil MAy DADAanan Pa ako SIr', 'Declined', 'Reason 1'),
(0000000024, 0000000099, 'Yin7NLD8zITxygK', '212.00', '2019-06-02', 'Jun 03, 2019 | 06:30 PM', 'BAka hindi ko makuha Agad Dahil MAy DADAanan Pa ako SIr', 'Declined', 'Reason 1'),
(0000000025, 0000000122, 'Wtko1A76P0jEdNR', '212.00', '2019-06-10', 'Jun 11, 2019 | 05:15 PM', 'BAka hindi ko makuha Agad Dahil MAy DADAanan Pa ako SIr', 'Declined', 'Reason4 '),
(0000000026, 0000000099, '5lchXN05Mb4SW12', '', '2019-06-13', 'Jun 10, 2019 | 05:31 PM', 'WHEEEEEEEEEEEEEEEEEEEEEEEEEEEEE', 'Pending', ''),
(0000000027, 0000000115, 'z29A3qoYE8ts2TG', '212.00', '2019-06-23', 'Jun 11, 2019 | 04:20 AM', 'BAka hindi ko makuha Agad Dahil MAy DADAanan Pa ako SIr', 'Pending', ''),
(0000000028, 0000000099, 'mdWDXreEOTNoC0M', '212.00', '2019-06-23', 'Jun 24, 2019 | 05:25 AM', 'ggggggg', 'Approved', 'Reason 1'),
(0000000029, 0000000158, 'nb7rI2pFe27WgAz', '9000.00', '2019-07-28', 'Jul 29, 2019 | 06:30 PM', 'saey', 'Approved', ''),
(0000000030, 0000000158, 'OExNXwp0444225T', '13500.00', '2019-07-31', 'Jul 31, 2019 | 06:30 PM', 'bnn', 'Pending', ''),
(0000000031, 0000000158, 'c3dD52qr2EL7zb8', '19500.00', '2019-08-06', 'Aug 07, 2019 | 04:20 AM', 'bbbbbb', 'Approved', ''),
(0000000032, 0000000158, 'whY3mAarM5IxFL1', '13500.00', '2019-08-12', 'Aug 12, 2019 | 04:29 PM', 'bbbbbb', 'Pending', ''),
(0000000033, 0000000158, '2onr33SdPGHqbYE', '9000.00', '2019-08-17', 'Aug 21, 2019 | 06:30 PM', 'hjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjllussursyuyi6.iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii6tjgc                 fhsssssssssssssssssssssssssssssssssssssssssssssssssssst3q', 'Approved', ''),
(0000000034, 0000000158, 'G77B3yYFIWM78rx', '24000.00', '2019-08-20', 'Aug 21, 2019 | 06:30 PM', 'bbbbbb', 'Pending', ''),
(0000000035, 0000000158, 'etj3F2HlKq2w44P', '4500.00', '2019-08-31', 'Aug 30, 2019 | 05:50 PM', 'fgrw', 'Pending', ''),
(0000000036, 0000000158, 'M4ct3197OZD8wbE', '19500.00', '2019-09-12', 'Sep 27, 2019 | 06:30 PM', 'bbbbbb', 'Approved', '');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categoryID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `categoryName` varchar(30) NOT NULL,
  `categoryType` varchar(20) NOT NULL,
  `categoryFee` decimal(10,2) NOT NULL,
  `categoryStatus` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categoryID`, `categoryName`, `categoryType`, `categoryFee`, `categoryStatus`) VALUES
(0000000001, 'Sari Sari Store', 'Free', '0.00', 'Active'),
(0000000002, 'Market Stalls', 'Free', '0.00', 'Active'),
(0000000003, 'Street Food', 'Free', '0.00', 'Active'),
(0000000004, 'Store', 'Micro', '1300.00', 'Active'),
(0000000005, 'One-stop', 'Micro', '1300.00', 'Active'),
(0000000006, 'Food trip', 'Micro', '1300.00', 'Active'),
(0000000007, 'Parlor', 'Small', '200.00', 'Active'),
(0000000008, 'Car Wash', 'Small', '200.00', 'Active'),
(0000000009, 'Pet shop', 'Small', '200.00', 'Active'),
(0000000010, 'Hotel', 'Medium', '6000.00', 'Active'),
(0000000011, 'School', 'Medium', '6000.00', 'Active'),
(0000000012, 'Resorts', 'Medium', '6000.00', 'Active'),
(0000000019, 'Vulcanizing', 'Small', '200.00', 'Active'),
(0000000020, 'Computer Shop', 'Small', '200.00', 'Active'),
(0000000021, 'Ukay -ukay ', 'Free', '0.00', 'Active'),
(0000000022, 'Karenderya', 'Free', '0.00', 'Active'),
(0000000023, 'Food Cart', 'Free', '0.00', 'Active'),
(0000000024, 'Buddhism', 'Worship', '0.00', 'Active'),
(0000000025, 'Christianity', 'Worship', '0.00', 'Active'),
(0000000026, 'Hinduism', 'Worship', '0.00', 'Active'),
(0000000027, 'Islam', 'Worship', '0.00', 'Active'),
(0000000028, 'Boarding House', 'Small', '200.00', 'Active'),
(0000000029, 'Bookstores', 'Small', '200.00', 'Active'),
(0000000030, 'Tarpaulin Printing ', 'Small', '200.00', 'Active'),
(0000000031, 'Gown Design', 'Small', '200.00', 'Active'),
(0000000032, 'Cellphone Repair Center', 'Small', '200.00', 'Active'),
(0000000033, 'Appliances ', 'Small', '200.00', 'Active'),
(0000000034, 'Rent House', 'Small', '200.00', 'Active'),
(0000000035, 'Furniture', 'Small', '200.00', 'Active'),
(0000000036, 'Barbershop ', 'Small', '200.00', 'Active'),
(0000000037, 'Water Refilling Station', 'Small', '200.00', 'Active'),
(0000000038, 'Massage Center', 'Small', '200.00', 'Active'),
(0000000039, 'Derma', 'Small', '200.00', 'Active'),
(0000000040, 'Drugs Store', 'Small', '200.00', 'Active'),
(0000000041, 'Bakeshop', 'Small', '200.00', 'Active'),
(0000000042, 'Auto Shop', 'Small', '200.00', 'Active'),
(0000000043, 'Upholstery', 'Small', '200.00', 'Active'),
(0000000044, 'Tiles', 'Small', '200.00', 'Active'),
(0000000045, 'Construction Materials', 'Small', '200.00', 'Active'),
(0000000046, 'Electronic Materials', 'Small', '200.00', 'Active'),
(0000000047, 'Photography Studio', 'Small', '200.00', 'Active'),
(0000000048, 'Radiator Service Center', 'Small', '200.00', 'Active'),
(0000000049, 'Aluminium Glass', 'Small', '200.00', 'Archived'),
(0000000050, 'Dental Clinic', 'Small', '200.00', 'Active'),
(0000000051, 'Restobar', 'Small', '200.00', 'Active'),
(0000000052, 'Tutorial', 'Small', '200.00', 'Active'),
(0000000053, 'Pasalubong', 'Small', '200.00', 'Active'),
(0000000054, 'Coffee Shop', 'Small', '200.00', 'Active'),
(0000000055, 'Optical Clinic', 'Small', '200.00', 'Active'),
(0000000056, 'Music Materials Shop', 'Small', '200.00', 'Active'),
(0000000057, 'Garment', 'Small', '200.00', 'Active'),
(0000000058, 'T-Shirt Printing', 'Small', '200.00', 'Active'),
(0000000059, 'Motorcycle Parts', 'Small', '200.00', 'Active'),
(0000000060, 'Club', 'Small', '200.00', 'Active'),
(0000000062, 'Flower Shop', 'Small', '200.00', 'Active'),
(0000000063, 'Space For Rent', 'Small', '200.00', 'Active'),
(0000000064, 'Lot For Sale', 'Small', '200.00', 'Active'),
(0000000065, 'House For Sale', 'Small', '200.00', 'Active'),
(0000000066, 'Bed Spacer', 'Small', '200.00', 'Active'),
(0000000067, 'Boutique', 'Small', '200.00', 'Active'),
(0000000068, 'Land Survey Service', 'Small', '200.00', 'Active'),
(0000000069, 'Gym', 'Small', '200.00', 'Active'),
(0000000070, 'Apartment', 'Small', '200.00', 'Active'),
(0000000071, 'House And Lot ', 'Medium', '6000.00', 'Active'),
(0000000072, 'Restaurant', 'Medium', '6000.00', 'Active'),
(0000000073, 'Computer Parts ', 'Small', '200.00', 'Active'),
(0000000074, 'System Provider', 'Free', '0.00', 'Active'),
(0000000076, 'Pang Kasjay', 'Small', '200.00', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `categoryrequests`
--

CREATE TABLE `categoryrequests` (
  `catRequestID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `catRequesterID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `catRequestText` varchar(255) NOT NULL,
  `catRequestReason` text NOT NULL,
  `catDeclineReason` varchar(255) NOT NULL,
  `catDateRequested` date NOT NULL,
  `catDateUpdated` date NOT NULL,
  `catRequestStatus` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categoryrequests`
--

INSERT INTO `categoryrequests` (`catRequestID`, `catRequesterID`, `catRequestText`, `catRequestReason`, `catDeclineReason`, `catDateRequested`, `catDateUpdated`, `catRequestStatus`) VALUES
(0000000002, 0000000007, 'Pang Kasjay', 'tapno maka maysa nga jords', '', '2019-11-04', '2019-11-04', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `declinestatements`
--

CREATE TABLE `declinestatements` (
  `statementID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `statementContent` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `declinestatements`
--

INSERT INTO `declinestatements` (`statementID`, `statementContent`) VALUES
(0000000001, 'Out of stock'),
(0000000002, 'No Personel available');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `eventID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `eventBisIDkey` varchar(10) NOT NULL,
  `eventTitle` varchar(500) NOT NULL,
  `eventDesc` varchar(2000) NOT NULL,
  `eventPoster` varchar(500) NOT NULL,
  `eventStatus` varchar(20) NOT NULL,
  `datePosted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`eventID`, `startDate`, `endDate`, `eventBisIDkey`, `eventTitle`, `eventDesc`, `eventPoster`, `eventStatus`, `datePosted`) VALUES
(0000000004, '2019-05-06', '2019-05-10', '0000000008', 'Unli fork uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu', 'weh weh weh  wekrjfhwefkjwlefkj wlefj lwej flwe fjlwejf lwkef jwlke fj\r\nweh weh weh  wekrjfhwefkjwlefkj wlefj lwej flwe fjlwejf lwkef jwlke fjweh weh weh  wekrjfhwefkjwlefkj wlefj lwej flwe fjlwejf lwkef jwlke fj\r\nweh weh weh  wekrjfhwefkjwlefkj wlefj lwej flwe fjlwejf lwkef jwlke fj\r\nweh weh weh  wekrjfhwefkjwlefkj wlefj lwej flwe fjlwejf lwkef jwlke fjweh weh weh  wekrjfhwefkjwlefkj wlefj lwej flwe fjlwejf lwkef jwlke fj\r\n\r\n\r\nweh weh weh  wekrjfhwefkjwlefkj wlefj lwej flwe fjlwejf lwkef jwlke fj', '504943848134368897-ocvctsriudpe-er-fat rat.png', 'Active', '0000-00-00 00:00:00'),
(0000000005, '2019-02-14', '2019-02-19', '0000000006', '20% free', 'qqeeeeeeeeegg', '560852051603363037-rcutdesivcepo-r-3_poster-a5.jpg', 'Active', '0000-00-00 00:00:00'),
(0000000006, '2019-02-21', '2019-02-28', '0000000049', 'huehue', 'ertgergerg', '48980713-52695030_1727603917345028_5770334836153647104_n.jpg', 'Active', '0000-00-00 00:00:00'),
(0000000007, '2019-02-19', '2019-02-28', '0000000049', 'testing ng date', 'erfgertgergf', '941375732-321111.png', 'Active', '2019-02-16 16:23:53'),
(0000000008, '2019-04-17', '2019-04-25', '0000000056', 'gre', '2322', '634704590-7bd302a02567a8ceb8006956a0933a51bde63aa4_hq.jpg', 'Active', '2019-04-14 10:49:13'),
(0000000009, '2019-05-22', '2019-05-17', '0000000057', 'bnjjj', '4556665', '709479388-4.jpg', 'Active', '2019-05-21 17:18:55'),
(0000000010, '2019-05-22', '2019-05-24', '0000000057', '62', 'kjhoh', '243039142-1.jpg', 'Active', '2019-05-21 17:19:51'),
(0000000011, '2019-05-22', '2019-05-31', '0000000060', '3aaaaaaaa', '665665', '172255806-4.jpg', 'Active', '2019-05-21 18:03:38'),
(0000000012, '2019-05-25', '2019-05-27', '0000000008', 'sale 30%', 'sdrys\r\ntyrty\r\netyrty\r\nt\r\nydrtydrty\r\ntrydrty\r\ntrydrty', '167639664-000.jpg', 'Active', '2019-05-23 19:13:06'),
(0000000013, '2019-05-30', '2019-05-31', '0000000073', 'free taste', 'yjftty', '386633387-0.jpg', 'Active', '2019-05-23 19:15:26'),
(0000000014, '2019-05-25', '2019-05-26', '0000000075', 'YHUUY6', 'DDFSSF', '725783685-0.jpg', 'Active', '2019-05-24 21:54:29'),
(0000000015, '2019-05-27', '2019-05-29', '0000000093', '30%', ',o,ko', '966578912-55.jpg', 'Active', '2019-05-25 16:35:51'),
(0000000016, '2019-05-27', '2019-05-28', '0000000099', '20% free', 'qqqq', '198452672830447319-tedscvupo-ericr-3.jpg', 'Active', '2019-05-26 11:09:43'),
(0000000017, '2019-05-29', '2019-05-31', '0000000110', '5Pesos', 'qqqq', '530013564-download (2).jpg', 'Active', '2019-05-26 11:10:39'),
(0000000018, '2019-05-31', '2019-06-06', '0000000110', '10 pesos', 'qqqq', '114399815-888.jpg', 'Active', '2019-05-26 11:11:46'),
(0000000019, '2019-05-29', '2019-05-29', '0000000111', '10% Free', 'aaaaa', '211627020-download (4).jpg', 'Active', '2019-05-26 11:19:37'),
(0000000020, '2019-05-29', '2019-05-31', '0000000111', 'Free Beer', 'aaaaaa', '534490534-download (3).jpg', 'Active', '2019-05-26 11:20:16'),
(0000000021, '2019-05-30', '2019-05-31', '0000000109', 'Dota Tournament ', 'Prize Champion 50,000', '180050254-VlbJEbK.jpg', 'Active', '2019-05-26 12:39:39'),
(0000000022, '2019-05-29', '2019-05-29', '0000000109', 'Promo  7hrs/100 Pesos', 'aaaaaaaaa', '365949958-images (7).jpg', 'Active', '2019-05-26 12:41:34'),
(0000000023, '2019-05-29', '2019-05-30', '0000000101', 'Branch Out', 'aaaaaaaa', '687524771-images (8).jpg', 'Active', '2019-05-26 12:43:27'),
(0000000024, '2019-05-30', '2019-05-31', '0000000102', 'Free Entrance', 'aaaaaaaa', '886630563-images (9).jpg', 'Active', '2019-05-26 12:46:36'),
(0000000025, '2019-05-29', '2019-05-30', '0000000104', 'Math', 'aaa', '60963017-37331098_1775023772586986_4094355571241648128_n.jpg', 'Active', '2019-05-26 18:55:34'),
(0000000026, '2019-06-03', '2019-06-05', '0000000109', 'Free 1hr  ', 'hhhaahaahaahhaahha', '819872818-download (11).jpg', 'Active', '2019-05-30 02:12:18'),
(0000000027, '2019-06-04', '2019-06-07', '0000000104', 'English', 'hashhashhash', '770065536-download (12).jpg', 'Active', '2019-05-30 02:18:58'),
(0000000029, '2019-06-06', '2019-06-08', '0000000119', 'free drinks', 'aaaaaaaaaa', '346771224-download (4).jpg', 'Active', '2019-06-01 12:53:40'),
(0000000030, '2019-06-06', '2019-06-07', '0000000122', 'P.E', 'aljkghapo', '105916788441888784-c-sievpdrortuce-35240540_1663948053652621_2139684876448694272_n.jpg', 'Active', '2019-06-01 17:29:16'),
(0000000031, '2019-06-24', '2019-06-25', '0000000103', '3aaaaaaaa', 'ihh', '108555207-1234.jpg', 'Active', '2019-06-23 10:44:53'),
(0000000032, '2019-07-10', '2019-07-26', '0000000130', 'Free Drinks', '5yhs', '411564535-download (3).jpg', 'Active', '2019-07-07 14:20:32'),
(0000000033, '2019-07-09', '2019-07-10', '0000000135', 'sale 30%', 'wsd', '189109201-images (1).jpg', 'Active', '2019-07-08 07:20:46'),
(0000000034, '2019-07-09', '2019-07-09', '0000000136', '5Pesos', 'rfta', '788326106-888.jpg', 'Active', '2019-07-08 07:21:47'),
(0000000035, '2019-07-16', '2019-07-26', '0000000137', '10 pesos Each', 'fgs', '163004106-download (2).jpg', 'Active', '2019-07-08 07:22:23'),
(0000000036, '2019-07-15', '2019-07-16', '0000000139', '20', 'ghz', '337575277-download.jpg', 'Active', '2019-07-08 07:23:01'),
(0000000037, '2019-07-15', '2019-07-17', '0000000140', 'free taste', 'gsa', '466275699902265072-orteciu-pedcrvs-download (1).jpg', 'Active', '2019-07-08 07:24:06'),
(0000000038, '2019-07-15', '2019-07-26', '0000000142', '30%', 'dgf', '151866112-download (4).jpg', 'Active', '2019-07-08 07:24:34'),
(0000000039, '2019-07-29', '2019-07-26', '0000000143', 'Free Drinks', 'dhy', '124852281-download (3).jpg', 'Active', '2019-07-08 07:25:10'),
(0000000040, '2019-07-09', '2019-07-27', '0000000149', 'Tournaments', 'wgya', '198167066-download (11).jpg', 'Active', '2019-07-08 07:32:21'),
(0000000041, '2019-07-23', '2019-07-26', '0000000149', 'Dota Tournaments', 'ts', '67284801-VlbJEbK.jpg', 'Active', '2019-07-08 07:33:03'),
(0000000042, '2019-07-16', '2019-07-26', '0000000149', 'Free 1hr  ', 'awg', '791583550-images (7).jpg', 'Active', '2019-07-08 07:33:40'),
(0000000043, '2019-07-23', '2019-07-26', '0000000144', 'Free Entrance', 'awgty', '529276263-k22.jpg', 'Active', '2019-07-08 07:34:18'),
(0000000044, '2019-07-16', '2019-07-16', '0000000143', 'free Party', 'ayg3', '743998137-images (9).jpg', 'Active', '2019-07-08 07:35:08'),
(0000000045, '2019-07-16', '2019-07-27', '0000000141', 'Opening', 'aGY', '190724732-images (8).jpg', 'Active', '2019-07-08 07:35:43'),
(0000000046, '2019-08-10', '2019-07-17', '0000000162', '10% Discount', 'Bring your family!', '327887869-Dining-room-at-Robert-Restaurant-in-Paris-600x400.jpg', 'Active', '2019-07-11 15:53:53'),
(0000000047, '2019-07-09', '2019-07-25', '0000000162', 'Launching of new Dishes', 'Come and taste!', '961869334-kkk.jpg', 'Active', '2019-07-11 15:57:52'),
(0000000048, '2019-07-18', '2019-07-25', '0000000160', 'Parokya ni Edgar Concert', 'Rakrakan na!', '724881018-parokya.jpg', 'Active', '2019-07-11 15:58:49'),
(0000000049, '2019-07-07', '2019-07-25', '0000000161', 'Free 1 hour promo', 'Extend na!', '270897025-com.jpg', 'Active', '2019-07-11 16:07:03'),
(0000000050, '2019-07-18', '2019-07-19', '0000000160', 'No Entrance Fee', 'Experience now!', '875539791-sunset.jpg', 'Active', '2019-07-11 16:09:28'),
(0000000051, '2019-07-23', '2019-07-25', '0000000161', 'Free Open Hour', 'First 5 Customers', '211459371-lol.jpg', 'Active', '2019-07-11 16:10:19'),
(0000000052, '2019-07-25', '2019-07-26', '0000000132', 'Launching', 'Open Now!', '79047950-makati.jpg', 'Active', '2019-07-11 16:11:28'),
(0000000053, '2019-07-10', '2019-07-18', '0000000155', '5 Pesos Each', 'Limited Edition', '185773242-ube.jpg', 'Active', '2019-07-11 16:14:50'),
(0000000054, '2019-07-17', '2019-07-25', '0000000155', 'Buy 1 take 1', 'Tikman na!', '277992754-monggo.jpg', 'Active', '2019-07-11 16:18:58'),
(0000000055, '2019-07-23', '2019-07-26', '0000000158', '10% Discount', 'Budget Friendly!', '23481265-alu.jpg', 'Active', '2019-07-11 16:21:21'),
(0000000056, '2019-07-25', '2019-07-27', '0000000158', 'Free Installation', 'Limited offer', '212701202-num.jpg', 'Active', '2019-07-11 16:22:40'),
(0000000057, '2019-11-03', '2019-11-08', '0000000150', 'hehe', 'hehe', '5920411-address.jpg', 'Active', '2019-11-03 21:08:19');

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `expID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `businessID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `uploaderID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `expDesc` varchar(2000) NOT NULL,
  `expPhoto` varchar(2000) NOT NULL,
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`expID`, `businessID`, `uploaderID`, `expDesc`, `expPhoto`, `dateTime`) VALUES
(0000000008, 0000000008, 0000000009, '13t13g13g', '962768554881011963-nxiepceeer-0.jpg', '2019-01-18 07:28:12'),
(0000000017, 0000000008, 0000000011, 'ethretgerverf', '889495849784912109-enepxerice-255676_235691713110939_100000103352805_1208484_758351_n.jpg', '2019-02-15 22:17:00'),
(0000000018, 0000000008, 0000000011, 'dgbfsizdhgfiskdjgfbksjd bfgbslkjdfgbskldfjbgvskdfbjcvslkdjfbgd fgdgbfsizdhgfiskdjgfbksjdfgbslkjdfgbskldfj bgvskdfbjcvslkdjfbgdfgdgbfsizdhgfiskdjgfbk sjdfgbslkjdfgbskldfjbgvskdfbjcvslkdjfbgdfgd gbfsizdhgfiskdjgfbksjdfgbslkjdfgbskldfjbg vskdfbjcvslkdjfbgdfgdgbfsizdhgfiskdjgfbksjd fgbslkjdfgbskldfjbgvskdfbjcvslkdjfbgdfg', '278747559791473388-ecxeeierpn-download.jpg', '2019-04-13 20:51:00'),
(0000000020, 0000000007, 0000000011, 'gerger', '335906983381225586-eecerxenip-3.png', '2019-04-13 21:53:00'),
(0000000021, 0000000049, 0000000011, '52855252\r\n6526262\r\n52855252\r\n6526262\r\n52855252\r\n6526262\r\n52855252\r\n6526262\r\n\r\n52855252\r\n6526262\r\n52855252\r\n6526262\r\n\r\n', '965270996122528077-pcxneeerie-3.png', '2019-04-20 18:22:00'),
(0000000022, 0000000006, 0000000011, 'regwerf', '663055420540405273-pxreeceine-30710610_1861929344105643_9068369768108523520_n.jpg', '2019-05-04 21:01:00'),
(0000000023, 0000000006, 0000000011, 'rwgererf', '316070557125305176-rnecepeiex-52001266_1284651661673158_3026809430839656448_n.jpg', '2019-05-04 21:04:00'),
(0000000024, 0000000005, 0000000011, 'regherger ', '908966064688415527-eexpiecnre-52001266_1284651661673158_3026809430839656448_n.jpg', '2019-05-05 03:27:00'),
(0000000025, 0000000049, 0000000011, 'gretgretg', '823028564321228028-nexipeerce-Untitled.png', '2019-05-05 03:28:00'),
(0000000026, 0000000057, 0000000011, 'hjvkh', '68777589573643297-xnpciereee-00.JPG', '2019-05-21 04:07:00'),
(0000000027, 0000000057, 0000000011, 'lhuj', '495582595636048394-repeixcene-55.jpg', '2019-05-21 04:17:00'),
(0000000028, 0000000060, 0000000007, 'jhin', '610960878850114962-xpceeeinre-220px-EvanjelickÃ½_Kostol_Å trba.jpg', '2019-05-20 17:02:00'),
(0000000029, 0000000062, 0000000007, 'ljkhujg', '915353467371668266-cineeepxer-388988ca8473929ba2336a2ce4537589.jpg', '2019-05-20 17:06:00'),
(0000000031, 0000000072, 0000000007, 'wt', '926197210623104155-pnecexeier-11.png', '2019-05-22 03:48:00'),
(0000000032, 0000000068, 0000000007, 'df', '499868874563401337-execepirne-5.jpg', '2019-05-22 03:48:00'),
(0000000033, 0000000069, 0000000007, 'dfd', '54159286328970161-inrexeceep-5.png', '2019-05-22 03:49:00'),
(0000000034, 0000000070, 0000000007, 'dfdf', '361085221666493804-receipexen-3.jpg', '2019-05-22 03:49:00'),
(0000000035, 0000000071, 0000000007, 'yrtd', '56846673395979636-cnxpieeere-4.jpg', '2019-05-22 03:50:00'),
(0000000036, 0000000008, 0000000007, 'kgfdutkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', '380020174846107910-eepenrciex-0.jpg', '2019-05-22 18:23:00'),
(0000000038, 0000000008, 0000000007, 'ojujh', '521061576304100584-eepxcneeir-4.jpg', '2019-05-25 02:59:00'),
(0000000039, 0000000008, 0000000007, 'ojuhn8', '503847120409826114-eeprxiceen-37331098_1775023772586986_4094355571241648128_n.jpg', '2019-05-25 03:05:00'),
(0000000040, 0000000093, 0000000007, 'ki9ok0', '500659230879779611-eicepnexre-000.jpg', '2019-05-25 03:37:00'),
(0000000041, 0000000008, 0000000011, '6wy', '975620771340190674-eecrxnipee-13895519_1053143784774992_1523506620221152668_n.jpg', '2019-05-25 03:40:00'),
(0000000042, 0000000092, 0000000007, 'tyhwsh', '43838020138987342-pceerxieen-5.jpg', '2019-05-25 04:01:00'),
(0000000043, 0000000111, 0000000007, 'nnnnnn', '459021588307572111-pciexnreee-1234.jpg', '2019-05-25 22:41:00'),
(0000000044, 0000000109, 0000000007, 'aaaaaaaaaa', '883168353130183398-eixeperecn-download (8).jpg', '2019-05-25 23:35:00'),
(0000000045, 0000000109, 0000000007, 'aaaaaaaaaaaaa', '567038887404379632-ecenpirxee-download (9).jpg', '2019-05-25 23:35:00'),
(0000000046, 0000000109, 0000000007, 'aaaaaaaaaa', '232673261103044867-irneepexce-download (10).jpg', '2019-05-25 23:35:00'),
(0000000047, 0000000111, 0000000007, 'aaaaaa', '628220506259172647-iereepncxe-images (6).jpg', '2019-05-25 23:36:00'),
(0000000048, 0000000111, 0000000007, 'aaaaaaa', '309124874921926841-repnxeeeic-images (5).jpg', '2019-05-25 23:37:00'),
(0000000049, 0000000111, 0000000007, 'aaaaaaaaaa', '79110353664787494-expicreene-images (3).jpg', '2019-05-25 23:37:00'),
(0000000050, 0000000111, 0000000007, 'aaaaaaaa', '78084502566843571-expecrinee-images (4).jpg', '2019-05-25 23:38:00'),
(0000000051, 0000000109, 0000000011, 'aaaaa', '733488381165888271-cnrpeeexei-images (10).jpg', '2019-05-26 00:25:00'),
(0000000052, 0000000109, 0000000021, 'rrrrrrrr\r\nr\r\nr\r\nr\r\nr\r\n\r\nr\r\nr\r\nr\r\nr\r\n\r\nr\r\nr', '503170417900507936-peerceiexn-000.jpg', '2019-05-25 17:28:00'),
(0000000054, 0000000108, 0000000021, 'sdfsgsdgh', '9741667566811628-penreeicxe-13895519_1053143784774992_1523506620221152668_n.jpg', '2019-05-25 17:59:00'),
(0000000056, 0000000108, 0000000021, 'tfagsgewaea', '164101739106781892-eipxecrnee-48392198_2305973349466273_9112485322825924608_o.jpg', '2019-05-25 18:04:00'),
(0000000057, 0000000113, 0000000007, 'hfxdjdgf', '30560037461197310-ncirpeeeex-1234.jpg', '2019-05-29 04:29:00'),
(0000000058, 0000000103, 0000000007, 'fffff', '286526997784017860-epxneecrei-1234.jpg', '2019-05-30 22:57:00'),
(0000000059, 0000000105, 0000000011, 'gag', '18829537470877618-encexerepi-4.png', '2019-05-31 22:11:00'),
(0000000060, 0000000119, 0000000011, 'aaaaa', '186671293536089753-cerpexeien-1234.jpg', '2019-05-31 23:41:00'),
(0000000061, 0000000122, 0000000007, 'arfasfa', '377715859344361568-pexnirceee-44633238_1909473509142011_1690882303212912640_n.jpg', '2019-06-01 04:30:00'),
(0000000062, 0000000122, 0000000007, 'yg5ewyh', '524942984400659905-eeerenipxc-13895519_1053143784774992_1523506620221152668_n.jpg', '2019-06-01 04:30:00'),
(0000000063, 0000000110, 0000000007, 'k', '84511114755895049-neexerpeci-000.jpg', '2019-06-22 21:43:00'),
(0000000064, 0000000131, 0000000007, 'hgfuj', '347800712905740310-eienepxecr-000.jpg', '2019-07-07 01:26:00'),
(0000000065, 0000000143, 0000000007, 'whya', '709813524580943683-irxneepcee-000.jpg', '2019-07-07 18:37:00'),
(0000000066, 0000000143, 0000000007, 'yayr', '326813712248194570-neeeperixc-00.JPG', '2019-07-07 18:37:00'),
(0000000067, 0000000143, 0000000007, 'yh', '879162774815537393-ecpnereexi-1234.jpg', '2019-07-07 18:38:00'),
(0000000068, 0000000143, 0000000007, 'hyuhyte54', '27389062436543631-nerixceeep-download (9).jpg', '2019-07-07 18:38:00'),
(0000000069, 0000000143, 0000000007, 'yrhe3', '316646805822411596-pxnrceieee-images (3).jpg', '2019-07-07 18:38:00'),
(0000000070, 0000000143, 0000000007, 'huy4w', '657457782373259702-xeienpceer-images (4).jpg', '2019-07-07 18:38:00'),
(0000000071, 0000000143, 0000000007, 'uyh', '735969020424182583-riepcneeex-images (5).jpg', '2019-07-07 18:39:00'),
(0000000072, 0000000142, 0000000007, 'gra3wy', '959271586288102469-ercepeienx-download (6).jpg', '2019-07-07 18:41:00'),
(0000000073, 0000000142, 0000000007, 'ga3rd', '217324112144258293-epxieeenrc-toscacafe_interior.0.0.1505759163.0.jpg', '2019-07-07 18:41:00'),
(0000000074, 0000000144, 0000000007, 'gy3', '478650896202856520-eeeexnpirc-48392198_2305973349466273_9112485322825924608_o.jpg', '2019-07-07 18:41:00'),
(0000000078, 0000000144, 0000000007, 'hujwd', '39629444393368234-eeincrepex-37331098_1775023772586986_4094355571241648128_n.jpg', '2019-07-07 18:43:00'),
(0000000079, 0000000144, 0000000007, 'uhjw4', '883738879301258236-reeiexencp-13895519_1053143784774992_1523506620221152668_n.jpg', '2019-07-07 18:43:00'),
(0000000080, 0000000134, 0000000007, 'pklmnhuv', '733609793605133958-rpeenixece-Nursing-min.jpg', '2019-07-11 02:40:00'),
(0000000081, 0000000134, 0000000007, 'asdsadsa', '818187560319720799-eeieneprcx-qqq.jpg', '2019-07-11 02:41:00'),
(0000000082, 0000000159, 0000000007, 'asdsadsads', '934302629517309472-cepeernxei-vvv.jpg', '2019-07-11 02:42:00'),
(0000000083, 0000000159, 0000000007, 'asfasfsaf', '963228027373461496-recexepeni-ggg.jpg', '2019-07-11 02:43:00'),
(0000000084, 0000000160, 0000000007, 'asacacc', '952297842666856565-rcniexpeee-eee.jpg', '2019-07-11 02:43:00'),
(0000000085, 0000000160, 0000000007, 'accaac', '747912301633503509-eeeiepnrcx-www.jpg', '2019-07-11 02:44:00'),
(0000000086, 0000000161, 0000000007, 'fsaffsaf', '931273358644213397-exceerepni-cafe-13.jpg', '2019-07-11 02:45:00'),
(0000000087, 0000000161, 0000000007, 'ascas', '454879447354931682-neepiceexr-cafe-12.jpg', '2019-07-11 02:46:00'),
(0000000088, 0000000162, 0000000007, 'zxvzxvx', '104928016870152312-xcneeeepri-1.jpg', '2019-07-11 02:51:00'),
(0000000089, 0000000162, 0000000007, 'asdasas', '678215185790220-erxneipeec-2.jpg', '2019-07-11 02:52:00'),
(0000000090, 0000000162, 0000000007, 'ascasc', '401522361619094773-ceeieexrpn-3.jpg', '2019-07-11 02:52:00'),
(0000000091, 0000000162, 0000000007, 'cxzc', '83597883246893820-nxpeeicree-4.jpg', '2019-07-11 02:53:00'),
(0000000092, 0000000162, 0000000007, 'gbfbbb', '635675320987068033-pecerenxie-5.jpg', '2019-07-11 02:53:00'),
(0000000093, 0000000158, 0000000007, 'klholhbpo', '54636238673487894-rexcnpeeei-1234.jpg', '2019-07-30 23:54:00'),
(0000000094, 0000000158, 0000000011, 'v', '44744306198772937-xcpireenee-0.jpg', '2019-09-11 23:03:00');

-- --------------------------------------------------------

--
-- Table structure for table `expimagelogs`
--

CREATE TABLE `expimagelogs` (
  `explogID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `thisBusiness` int(10) UNSIGNED ZEROFILL NOT NULL,
  `thisUploader` int(10) UNSIGNED ZEROFILL NOT NULL,
  `expImage` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expimagelogs`
--

INSERT INTO `expimagelogs` (`explogID`, `thisBusiness`, `thisUploader`, `expImage`) VALUES
(0000000001, 0000000169, 0000000011, '420670413165416258-eicxrnpeee-761688232A-791381836-experience-0.jpg'),
(0000000008, 0000000008, 0000000009, '962768554881011963-nxiepceeer-0.jpg'),
(0000000009, 0000000008, 0000000011, '627624512194061280-crneeeexpi-14wu-yongning-extreme-selfie.jpg'),
(0000000010, 0000000008, 0000000011, '373840332969085693-ceieeprexn-samsung-galaxy-s9-quick-camera-look-selfie-focus-1.jpg'),
(0000000011, 0000000008, 0000000011, '316070557125305176-rnecepeiex-samsung-galaxy-s9-quick-camera-look-selfie-focus-1.jpg'),
(0000000012, 0000000008, 0000000011, '899688720427185059-nicpreexee-50272801_358950504937269_7665350754487500800_n.png'),
(0000000013, 0000000008, 0000000011, '952789306527648926-nexepcerie-50272801_358950504937269_7665350754487500800_n.png'),
(0000000014, 0000000008, 0000000011, '548278809170379639-irpeneecxe-6a0120a8551282970b0147e13c129b970b.jpg'),
(0000000015, 0000000008, 0000000011, '344146729810607910-ecxpirenee-Call-Center-Employee-Experience-HR-WFM.png'),
(0000000016, 0000000008, 0000000011, '156555176535003662-cnxriepeee-J4o.gif'),
(0000000017, 0000000008, 0000000011, '907318115602539062-eerneexpci-blackx_thumbnail.jpg'),
(0000000018, 0000000008, 0000000011, '923126220981872558-exepnceeir-255676_235691713110939_100000103352805_1208484_758351_n.jpg'),
(0000000019, 0000000008, 0000000011, '889495849784912109-enepxerice-255676_235691713110939_100000103352805_1208484_758351_n.jpg'),
(0000000020, 0000000008, 0000000011, '278747559791473388-ecxeeierpn-download.jpg'),
(0000000021, 0000000008, 0000000011, '379791260445861816-xecepnreie-2222.png'),
(0000000022, 0000000007, 0000000011, '335906983381225586-eecerxenip-3.png'),
(0000000023, 0000000049, 0000000011, '965270996122528077-pcxneeerie-3.png'),
(0000000024, 0000000006, 0000000011, '663055420540405273-pxreeceine-30710610_1861929344105643_9068369768108523520_n.jpg'),
(0000000025, 0000000006, 0000000011, '316070557125305176-rnecepeiex-52001266_1284651661673158_3026809430839656448_n.jpg'),
(0000000026, 0000000005, 0000000011, '908966064688415527-eexpiecnre-52001266_1284651661673158_3026809430839656448_n.jpg'),
(0000000027, 0000000049, 0000000011, '823028564321228028-nexipeerce-Untitled.png'),
(0000000028, 0000000057, 0000000011, '68777589573643297-xnpciereee-00.JPG'),
(0000000029, 0000000057, 0000000011, '495582595636048394-repeixcene-55.jpg'),
(0000000030, 0000000060, 0000000007, '610960878850114962-xpceeeinre-220px-EvanjelickÃ½_Kostol_Å trba.jpg'),
(0000000031, 0000000062, 0000000007, '915353467371668266-cineeepxer-388988ca8473929ba2336a2ce4537589.jpg'),
(0000000032, 0000000008, 0000000007, '736745531735093503-rexpineece-37331098_1775023772586986_4094355571241648128_n.jpg'),
(0000000033, 0000000072, 0000000007, '926197210623104155-pnecexeier-11.png'),
(0000000034, 0000000068, 0000000007, '499868874563401337-execepirne-5.jpg'),
(0000000035, 0000000069, 0000000007, '54159286328970161-inrexeceep-5.png'),
(0000000036, 0000000070, 0000000007, '361085221666493804-receipexen-3.jpg'),
(0000000037, 0000000071, 0000000007, '56846673395979636-cnxpieeere-4.jpg'),
(0000000038, 0000000008, 0000000007, '380020174846107910-eepenrciex-0.jpg'),
(0000000039, 0000000008, 0000000007, '95311320923347928-reeinxcpee-000.jpg'),
(0000000040, 0000000008, 0000000007, '521061576304100584-eepxcneeir-4.jpg'),
(0000000041, 0000000008, 0000000007, '503847120409826114-eeprxiceen-37331098_1775023772586986_4094355571241648128_n.jpg'),
(0000000042, 0000000093, 0000000007, '500659230879779611-eicepnexre-000.jpg'),
(0000000043, 0000000008, 0000000011, '975620771340190674-eecrxnipee-13895519_1053143784774992_1523506620221152668_n.jpg'),
(0000000044, 0000000092, 0000000007, '43838020138987342-pceerxieen-5.jpg'),
(0000000045, 0000000111, 0000000007, '459021588307572111-pciexnreee-1234.jpg'),
(0000000046, 0000000109, 0000000007, '883168353130183398-eixeperecn-download (8).jpg'),
(0000000047, 0000000109, 0000000007, '567038887404379632-ecenpirxee-download (9).jpg'),
(0000000048, 0000000109, 0000000007, '232673261103044867-irneepexce-download (10).jpg'),
(0000000049, 0000000111, 0000000007, '628220506259172647-iereepncxe-images (6).jpg'),
(0000000050, 0000000111, 0000000007, '309124874921926841-repnxeeeic-images (5).jpg'),
(0000000051, 0000000111, 0000000007, '79110353664787494-expicreene-images (3).jpg'),
(0000000052, 0000000111, 0000000007, '78084502566843571-expecrinee-images (4).jpg'),
(0000000053, 0000000109, 0000000011, '733488381165888271-cnrpeeexei-images (10).jpg'),
(0000000054, 0000000109, 0000000021, '503170417900507936-peerceiexn-000.jpg'),
(0000000055, 0000000108, 0000000021, '154835346172263318-pxreeeinec-44633238_1909473509142011_1690882303212912640_n.jpg'),
(0000000056, 0000000108, 0000000021, '9741667566811628-penreeicxe-13895519_1053143784774992_1523506620221152668_n.jpg'),
(0000000057, 0000000108, 0000000021, '62634853268844736-enexcipere-44633238_1909473509142011_1690882303212912640_n.jpg'),
(0000000058, 0000000108, 0000000021, '164101739106781892-eipxecrnee-48392198_2305973349466273_9112485322825924608_o.jpg'),
(0000000059, 0000000113, 0000000007, '30560037461197310-ncirpeeeex-1234.jpg'),
(0000000060, 0000000103, 0000000007, '286526997784017860-epxneecrei-1234.jpg'),
(0000000061, 0000000105, 0000000011, '18829537470877618-encexerepi-4.png'),
(0000000062, 0000000119, 0000000011, '186671293536089753-cerpexeien-1234.jpg'),
(0000000063, 0000000122, 0000000007, '377715859344361568-pexnirceee-44633238_1909473509142011_1690882303212912640_n.jpg'),
(0000000064, 0000000122, 0000000007, '524942984400659905-eeerenipxc-13895519_1053143784774992_1523506620221152668_n.jpg'),
(0000000065, 0000000110, 0000000007, '84511114755895049-neexerpeci-000.jpg'),
(0000000066, 0000000131, 0000000007, '347800712905740310-eienepxecr-000.jpg'),
(0000000067, 0000000143, 0000000007, '709813524580943683-irxneepcee-000.jpg'),
(0000000068, 0000000143, 0000000007, '326813712248194570-neeeperixc-00.JPG'),
(0000000069, 0000000143, 0000000007, '879162774815537393-ecpnereexi-1234.jpg'),
(0000000070, 0000000143, 0000000007, '27389062436543631-nerixceeep-download (9).jpg'),
(0000000071, 0000000143, 0000000007, '316646805822411596-pxnrceieee-images (3).jpg'),
(0000000072, 0000000143, 0000000007, '657457782373259702-xeienpceer-images (4).jpg'),
(0000000073, 0000000143, 0000000007, '735969020424182583-riepcneeex-images (5).jpg'),
(0000000074, 0000000142, 0000000007, '959271586288102469-ercepeienx-download (6).jpg'),
(0000000075, 0000000142, 0000000007, '217324112144258293-epxieeenrc-toscacafe_interior.0.0.1505759163.0.jpg'),
(0000000076, 0000000144, 0000000007, '478650896202856520-eeeexnpirc-48392198_2305973349466273_9112485322825924608_o.jpg'),
(0000000077, 0000000144, 0000000007, '601763000782524601-eprceieexn-44633238_1909473509142011_1690882303212912640_n.jpg'),
(0000000078, 0000000144, 0000000007, '663824842236690717-recixpeeen-44535821_1907209019368460_6136802014446747648_n.jpg'),
(0000000079, 0000000144, 0000000007, '686001606427810579-icernxepee-46714324_1957156377707057_507166299705573376_n.jpg'),
(0000000080, 0000000144, 0000000007, '39629444393368234-eeincrepex-37331098_1775023772586986_4094355571241648128_n.jpg'),
(0000000081, 0000000144, 0000000007, '883738879301258236-reeiexencp-13895519_1053143784774992_1523506620221152668_n.jpg'),
(0000000082, 0000000134, 0000000007, '733609793605133958-rpeenixece-Nursing-min.jpg'),
(0000000083, 0000000134, 0000000007, '818187560319720799-eeieneprcx-qqq.jpg'),
(0000000084, 0000000159, 0000000007, '934302629517309472-cepeernxei-vvv.jpg'),
(0000000085, 0000000159, 0000000007, '963228027373461496-recexepeni-ggg.jpg'),
(0000000086, 0000000160, 0000000007, '952297842666856565-rcniexpeee-eee.jpg'),
(0000000087, 0000000160, 0000000007, '747912301633503509-eeeiepnrcx-www.jpg'),
(0000000088, 0000000161, 0000000007, '931273358644213397-exceerepni-cafe-13.jpg'),
(0000000089, 0000000161, 0000000007, '454879447354931682-neepiceexr-cafe-12.jpg'),
(0000000090, 0000000162, 0000000007, '104928016870152312-xcneeeepri-1.jpg'),
(0000000091, 0000000162, 0000000007, '678215185790220-erxneipeec-2.jpg'),
(0000000092, 0000000162, 0000000007, '401522361619094773-ceeieexrpn-3.jpg'),
(0000000093, 0000000162, 0000000007, '83597883246893820-nxpeeicree-4.jpg'),
(0000000094, 0000000162, 0000000007, '635675320987068033-pecerenxie-5.jpg'),
(0000000095, 0000000158, 0000000007, '54636238673487894-rexcnpeeei-1234.jpg'),
(0000000096, 0000000158, 0000000011, '44744306198772937-xcpireenee-0.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `favID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `noteUserID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `noteBusinessIDkey` int(10) UNSIGNED ZEROFILL NOT NULL,
  `favNote` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`favID`, `noteUserID`, `noteBusinessIDkey`, `favNote`) VALUES
(0000000001, 0000000009, 0000000008, 'rty;lmjhrotfgjlrfmgrt'),
(0000000002, 0000000009, 0000000008, 'ukinnas na talga'),
(0000000003, 0000000009, 0000000008, 'Fuck it'),
(0000000008, 0000000011, 0000000005, 'iol'),
(0000000017, 0000000007, 0000000008, 'hwerhe'),
(0000000018, 0000000007, 0000000073, 'ouhgi'),
(0000000019, 0000000007, 0000000075, 'sdfgdf'),
(0000000030, 0000000011, 0000000101, 'gsga'),
(0000000031, 0000000011, 0000000171, 'hehe'),
(0000000032, 0000000011, 0000000150, 'f');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `feedbackID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `userID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `feedbackContent` varchar(2000) NOT NULL,
  `feedbackDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`feedbackID`, `userID`, `feedbackContent`, `feedbackDate`) VALUES
(0000000001, 0000000007, 'fwefew', '2019-04-21'),
(0000000002, 0000000007, '4t32r32 4f354 34 34f \r\n34 \r\nf34\r\nf \r\n34 f', '2019-04-21'),
(0000000003, 0000000007, '568', '2019-04-21'),
(0000000004, 0000000007, 'hyrfhjt', '2019-05-29'),
(0000000005, 0000000007, 'hfg\r\njfdd\r\njkhgj\r\ngjd\r\nhfdghd\r\nfcjg', '2019-05-29'),
(0000000006, 0000000007, 'kiyoui\r\nuyjr\r\nuti\r\nyury\r\nyru', '2019-05-29'),
(0000000007, 0000000007, 'hmnn', '2019-06-23'),
(0000000008, 0000000007, 'p0;8p', '2019-06-29');

-- --------------------------------------------------------

--
-- Table structure for table `imagelogs`
--

CREATE TABLE `imagelogs` (
  `logID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `logType` varchar(30) NOT NULL,
  `pixLogIDKey` int(10) UNSIGNED ZEROFILL NOT NULL,
  `picName` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imagelogs`
--

INSERT INTO `imagelogs` (`logID`, `logType`, `pixLogIDKey`, `picName`) VALUES
(0000000004, 'Product', 0000000013, '416672120-shutterstock_chemicals_antoine2k.jpg'),
(0000000005, 'Product', 0000000013, '409052922-chemical1-770x433.jpg'),
(0000000006, 'Product', 0000000003, '695986005-download.jpg'),
(0000000007, 'Product', 0000000003, '93950347-4Snz7siSeS8e8dbUc7FyUR-480-80.jpg'),
(0000000008, 'Product', 0000000006, '11901856330291748-rpvscetri-ocude-61pz6N01NJL._SY679_.jpg'),
(0000000010, 'Product', 0000000008, '91461181638177491-ect-ecdrvporsui-477783-razer-blackwidow-tournament-edition-chroma-2.jpg'),
(0000000011, 'Product', 0000000018, '45593262752655029-redvecors-pcuit-1-deathadder-chroma-gaming-mouse-black.jpg'),
(0000000012, 'Product', 0000000019, '521465807774764901-rcevucs-eporitd-wallpaper.jpg'),
(0000000017, 'Product', 0000000030, '62776151683875176-edcic-vuersotrp-1500000.bmp'),
(0000000018, 'Product', 0000000031, '729335058758399491-seircpe-ouvrcdt-1501154.bmp'),
(0000000019, 'Product', 0000000032, '910491943323486328-eotersccivur-dp-11901856330291748-rpvscetri-ocude-61pz6N01NJL._SY679_.jpg'),
(0000000020, 'Product', 0000000013, '175628662473388672--iecrsovcutpdre-Chemical-products-ADI.jpg'),
(0000000021, 'Product', 0000000001, '17272950165740967-uceidptvcso-rre-Chemical-products-ADI.jpg'),
(0000000022, 'Product', 0000000001, '788269043454315186-ptiroccudse-rve-6a0120a8551282970b0147e13c129b970b.jpg'),
(0000000023, 'Product', 0000000033, '260437012170623780-ed-uorctpsvcire-6a0120a8551282970b0147e13c129b970b.jpg'),
(0000000024, 'Product', 0000000034, '999328613897308349-pvdsr-ctocreuie-s-l300.jpg'),
(0000000025, 'Product', 0000000034, '434387207568695068--ctueivrospdcer-download.jpg'),
(0000000026, '', 0000000000, '692871094149688721-vretoei-pcucsdr-3_poster-a5.jpg'),
(0000000027, 'Product', 0000000001, '306793213864074707-e-rercudipcsvot-fat rat.png'),
(0000000028, 'Service', 0000000035, '873291015440460205-copuerdcesivtr--fat rat.png'),
(0000000029, 'Service', 0000000036, '370697022860839843-dreocpiver-csut-1280x720_cmsv2_5c258ef7-4be0-54f5-8596-e4819690d11e-3391350.jpg'),
(0000000030, 'Service', 0000000037, '77331543962127685-teuccrio-vsderp-1280x720_cmsv2_5c258ef7-4be0-54f5-8596-e4819690d11e-3391350.jpg'),
(0000000031, 'Product', 0000000038, '200195313698028564-dvoprctesceiu-r-spear_lv35.jpg'),
(0000000032, 'Product', 0000000039, '98419190338806153-rptreccidu-oevs-spear_lv35.jpg'),
(0000000033, 'Product', 0000000040, '276245117549957275-dvur-eirpsceotc-spear_lv35.jpg'),
(0000000034, 'Service', 0000000041, '653198242637847900--ducpoersivcret-1280x720_cmsv2_5c258ef7-4be0-54f5-8596-e4819690d11e-3391350.jpg'),
(0000000035, 'Product', 0000000030, '11892700274157715-siorvepucetcr-d-3.png'),
(0000000036, '', 0000000007, '235137940241149903-cteuvidorrc-pse-3.png'),
(0000000037, '', 0000000007, '663024902927703857-diustrv-rpoeecc-3.png'),
(0000000038, '', 0000000007, '59936524722320556-ricecudsoep-vrt-52001266_1284651661673158_3026809430839656448_n.jpg'),
(0000000039, 'Product', 0000000006, '277801514797729492--oridupeecvtrsc-30710610_1861929344105643_9068369768108523520_n.jpg'),
(0000000040, '', 0000000007, '113250733111694336-e-eodpcrcvstrui-52001266_1284651661673158_3026809430839656448_n.jpg'),
(0000000041, '', 0000000007, '427185059971160888-putsdevcr-eiroc-30710610_1861929344105643_9068369768108523520_n.jpg'),
(0000000042, 'Product', 0000000006, '167449952269409180-tr-prvisecdcoeu-52001266_1284651661673158_3026809430839656448_n.jpg'),
(0000000043, '', 0000000041, '24807739326550293--cdesoucpvrriet-a.png'),
(0000000044, 'Service', 0000000007, '356048584764160156-cevdpociurs-ret-message_avatar2.png'),
(0000000045, 'Service', 0000000007, '582794189135681153-cucrovpersdei-t-message_avatar1.png'),
(0000000046, 'Product', 0000000042, '256461766393257449-orcucsteripvd-e-4.jpg'),
(0000000047, 'Product', 0000000043, '877888939641948948-rudseot-ccirpev-888.jpg'),
(0000000048, 'Product', 0000000044, '693633678968983836-i-ptrdouvrsecec-4.jpg'),
(0000000049, 'Product', 0000000045, '38868542027042230-ci-rpeevrtsuodc-1.jpg'),
(0000000050, 'Product', 0000000046, '712023008923265748-tuepvsdriecr-co-2.jpg'),
(0000000051, 'Product', 0000000002, '184686603382998831-s-rvuctcodpeire-2.jpg'),
(0000000052, 'Product', 0000000002, '382698264537414717-drvropiscctu-ee-4.jpg'),
(0000000053, 'Product', 0000000047, '848949646835764995-pvi-osucrdcetre-download (4).jpg'),
(0000000054, 'Product', 0000000048, '660808781489612699-pidtcvecrs-roue-download (3).jpg'),
(0000000055, 'Service', 0000000049, '22764657319686268-sveortdreucp-ci-1234.jpg'),
(0000000056, 'Product', 0000000050, '141619586147024362--ccrrieoupdetvs-download (4).jpg'),
(0000000057, 'Product', 0000000051, '74270016605812377-pserucriteocd-v-download (3).jpg'),
(0000000058, 'Service', 0000000052, '58859544590690115-tscrr-cdoeipveu-1234.jpg'),
(0000000059, 'Product', 0000000053, '585372135651722963-dr-veciecsotrup-download (3).jpg'),
(0000000060, 'Product', 0000000054, '102092455610871720-tcdruro-ipvcsee-download (3).jpg'),
(0000000061, 'Product', 0000000055, '106895042112640557-isorc-rvpetduec-download (4).jpg'),
(0000000062, 'Product', 0000000056, '756354859732582009-turcircevdep-so-download (3).jpg'),
(0000000063, 'Service', 0000000057, '563717029639490957--ceoervpduicstr-13895519_1053143784774992_1523506620221152668_n.jpg'),
(0000000064, 'Service', 0000000058, '340658341380841599-ptcerdesriovc-u-000.jpg'),
(0000000065, '', 0000000057, '342255162141227209-veccturods-reip-000.jpg'),
(0000000066, '', 0000000057, '243185743230169084-cpdstru-ceiover-00.JPG'),
(0000000067, 'Product', 0000000059, '47069087295204152-potrsviudecre-c-download (4).jpg'),
(0000000068, 'Product', 0000000060, '113593781827145781-irsdrpvoccue-et-download (3).jpg'),
(0000000069, 'Service', 0000000061, '607316817673413576-potidvrcceres-u-00.JPG'),
(0000000070, 'Service', 0000000062, '322262151613722832--teedcruroispcv-000.jpg'),
(0000000071, 'Product', 0000000055, '845666103703034688-treucr-iosevpcd-5.jpg'),
(0000000072, 'Product', 0000000056, '992332228141527591-rcsveuoeci-ptrd-2.jpg'),
(0000000073, '', 0000000057, '893204786625200906-edtor-pviescucr-4.jpg'),
(0000000074, '', 0000000058, '250946799864190134-vitrec-pcesrodu-1.jpg'),
(0000000075, 'Product', 0000000063, '987795589193995280-reuspo-icvdetcr-44535821_1907209019368460_6136802014446747648_n.jpg'),
(0000000076, 'Service', 0000000064, '92621475497081896-us-deiptrcrovce-5.jpg'),
(0000000077, 'Product', 0000000065, '410091422351694632-t-repsdccvoieur-download.jpg'),
(0000000078, 'Product', 0000000066, '442523476831128298-rietpvosdr-uecc-download (4).jpg'),
(0000000079, 'Product', 0000000067, '596473387775316284-dscirrveu-tpeoc-4.jpg'),
(0000000080, 'Product', 0000000068, '6295523039139211-up-roceestdicvr-2.jpg'),
(0000000081, 'Product', 0000000069, '55244784021522549-uroecpcised-trv-3.jpg'),
(0000000082, 'Product', 0000000070, '76806294287313583-udcireretos-cpv-5.jpg'),
(0000000083, 'Product', 0000000071, '299010024860126775-ccorrpvute-edsi-2.jpg'),
(0000000084, 'Product', 0000000071, '61836740847580626--eccpitroeurvds-download (2).jpg'),
(0000000085, 'Product', 0000000072, '296774038328541571-cot-devruerispc-f45aa257e928f4be492e1457481b3fd7.jpg'),
(0000000086, 'Product', 0000000073, '826485706943908108-sce-dcpeovutirr-13895519_1053143784774992_1523506620221152668_n.jpg'),
(0000000087, 'Service', 0000000074, '467861033773397026-csetdoc-pirrvue-44535821_1907209019368460_6136802014446747648_n.jpg'),
(0000000088, 'Product', 0000000075, '449492400551833889-cpvrcroduis-eet-download (2).jpg'),
(0000000089, 'Service', 0000000076, '72387038998200778-pvctudi-esecorr-00.JPG'),
(0000000090, 'Product', 0000000077, '544412554694600222-iescptocrde-rvu-5.jpg'),
(0000000091, 'Product', 0000000078, '753777019936689348-peids-cceuovrtr-ligo.jpg'),
(0000000092, 'Product', 0000000000, '944887197554296017-ieucsovrtp-cder-powder.jpg'),
(0000000093, 'Product', 0000000079, '888844014165048991-peuocctvdr-sire-milo.jpg'),
(0000000094, 'Product', 0000000080, '642591247956491925--duesvctorrepic-joy.jpg'),
(0000000095, 'Product', 0000000081, '474113460433974528-tevecdrucripos--download (7).jpg'),
(0000000096, 'Product', 0000000082, '33539809888763394-repts-viceuocrd-kabababyan.jpg'),
(0000000097, 'Product', 0000000083, '811781719678064128-redicsrceou-ptv-pandesal.jpg'),
(0000000098, 'Service', 0000000084, '696675643650595394-dsivcorp-euecrt-cater.jpg'),
(0000000099, 'Service', 0000000085, '786409722878430960-vt-ocsrcpiuedre-cater.jpg'),
(0000000100, 'Product', 0000000086, '93584927123514538-crceueptirv-ods-cpu.jpg'),
(0000000101, 'Product', 0000000087, '112626966938783-evoru-ditcercsp-moni.jpg'),
(0000000102, 'Product', 0000000088, '324795398762212510-ecrirts-edcopuv-home_product_02.png'),
(0000000103, 'Service', 0000000089, '685976515529051654-tecisd-rpecvuor-cc.jpg'),
(0000000104, 'Product', 0000000090, '183773763765327401-crvestpuride-co-Factory-price-grill-design-aluminum-glass-top.jpg_350x350.jpg'),
(0000000105, 'Product', 0000000091, '598114832503552154-desite-coprvucr-249574.jpeg'),
(0000000106, 'Service', 0000000092, '234742962829563508-verdeu-sptccior-install.jpg'),
(0000000107, 'Service', 0000000093, '526322911818810270-sedpitcrv-eocur-ssss.jpg'),
(0000000108, 'Service', 0000000094, '13821182294242302-oidterrpsuc-cev-Main-Photo.jpg'),
(0000000109, 'Product', 0000000095, '574493643620148977-ieccvdrso-rteup-2.jpg'),
(0000000110, 'Product', 0000000096, '483875636927837019-voer-tucedicpsr-download (5).jpg'),
(0000000111, 'Service', 0000000097, '868975004311801313-ieoscvtrr-epucd-000.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `marketplace`
--

CREATE TABLE `marketplace` (
  `marketpostID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `uploaderID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `marketpostType` varchar(20) NOT NULL,
  `marketpostDescription` varchar(2000) NOT NULL,
  `marketpostPrice` decimal(10,2) NOT NULL,
  `marketpostPic` varchar(2000) NOT NULL,
  `marketpostStatus` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marketplace`
--

INSERT INTO `marketplace` (`marketpostID`, `uploaderID`, `marketpostType`, `marketpostDescription`, `marketpostPrice`, `marketpostPic`, `marketpostStatus`) VALUES
(0000000005, 0000000011, 'Buying', 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', '212.00', 'mtakeepcra-1727259311083368200.jpg', 'Active'),
(0000000017, 0000000007, 'Selling', 'sdgsdfhdghdfgh', '500.00', 'apramcekte-7159789072151768555.jpg', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `rID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `bisnesID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `userID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `reportContent` varchar(2000) NOT NULL,
  `reportDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`rID`, `bisnesID`, `userID`, `reportContent`, `reportDate`) VALUES
(0000000002, 0000000008, 0000000011, 'rgheth erfger \r\nerger \r\nerg', '2019-04-14'),
(0000000003, 0000000007, 0000000011, 'tukinam ', '2019-04-14'),
(0000000004, 0000000008, 0000000011, '33', '2019-04-21'),
(0000000005, 0000000008, 0000000011, '6555', '2019-04-21'),
(0000000006, 0000000008, 0000000011, '652', '2019-04-21'),
(0000000007, 0000000111, 0000000011, 'Hbb', '2019-05-29'),
(0000000008, 0000000122, 0000000007, 'mk', '2019-06-23'),
(0000000009, 0000000122, 0000000007, 'hv ghf', '2019-06-23'),
(0000000010, 0000000101, 0000000007, 'hhh', '2019-06-23'),
(0000000011, 0000000101, 0000000011, 'hn', '2019-06-23'),
(0000000012, 0000000132, 0000000011, 'asdqwe', '2019-07-14'),
(0000000013, 0000000158, 0000000007, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\r\naaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\r\naatyrsdjtdk\r\nfklfylyf\r\n;lkg;', '2019-07-28');

-- --------------------------------------------------------

--
-- Table structure for table `secretquestions`
--

CREATE TABLE `secretquestions` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `secretquestions`
--

INSERT INTO `secretquestions` (`id`, `content`) VALUES
(1, 'Mother\'s maiden name'),
(2, 'First beach visited');

-- --------------------------------------------------------

--
-- Table structure for table `subdeclinestatements`
--

CREATE TABLE `subdeclinestatements` (
  `subdeclineID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `subdeclineStatement` varchar(255) NOT NULL,
  `subdeclineCategory` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subdeclinestatements`
--

INSERT INTO `subdeclinestatements` (`subdeclineID`, `subdeclineStatement`, `subdeclineCategory`) VALUES
(0000000001, 'Insufficient Amount', 'Product'),
(0000000002, 'Insufficient Credentials', 'Product'),
(0000000003, 'Insufficient Trust', 'Product'),
(0000000004, 'Insufficient Effort', 'Product'),
(0000000005, 'Reference Number Does not match', 'Subscription'),
(0000000006, 'Duplicate', 'Subscription'),
(0000000007, 'Bogus', 'Subscription'),
(0000000008, 'Awan ti kasjay', 'Category'),
(0000000009, 'Kasjay lang talga', 'Category');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `subscriptionID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `bisSubIDkey` int(10) UNSIGNED ZEROFILL NOT NULL,
  `bisOwner` int(10) UNSIGNED ZEROFILL NOT NULL,
  `subAmount` decimal(10,2) NOT NULL,
  `subNumOfMonth` int(2) NOT NULL,
  `subNewCategory` int(10) UNSIGNED ZEROFILL NOT NULL,
  `subDatePaid` date NOT NULL,
  `subDateApproved` date NOT NULL,
  `subDateEnd` date NOT NULL,
  `subRefNum` varchar(100) NOT NULL,
  `subRefPhoto` varchar(1000) NOT NULL,
  `subDecReason` varchar(255) NOT NULL,
  `subStatus` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`subscriptionID`, `bisSubIDkey`, `bisOwner`, `subAmount`, `subNumOfMonth`, `subNewCategory`, `subDatePaid`, `subDateApproved`, `subDateEnd`, `subRefNum`, `subRefPhoto`, `subDecReason`, `subStatus`) VALUES
(0000000007, 0000000150, 0000000007, '2400.00', 12, 0000000000, '2019-11-03', '2019-11-03', '2020-11-03', '8jskdlfj', '915771484-address.jpg', '', 'Approved'),
(0000000008, 0000000150, 0000000007, '800.00', 4, 0000000060, '2019-11-03', '2019-11-03', '2020-03-03', 'asd', '989990234-address.jpg', '', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `tourspot`
--

CREATE TABLE `tourspot` (
  `tourspotID` int(10) UNSIGNED ZEROFILL NOT NULL,
  `tourspotUploader` int(10) UNSIGNED ZEROFILL NOT NULL,
  `tourSpotTitle` varchar(2000) NOT NULL,
  `fullAddress` varchar(2000) NOT NULL,
  `municipality` varchar(50) NOT NULL,
  `tourspotComment` varchar(2000) NOT NULL,
  `tourspotPic` varchar(2000) NOT NULL,
  `tourspotLat` decimal(10,6) NOT NULL,
  `tourspotLong` decimal(10,6) NOT NULL,
  `tourspotStatus` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tourspot`
--

INSERT INTO `tourspot` (`tourspotID`, `tourspotUploader`, `tourSpotTitle`, `fullAddress`, `municipality`, `tourspotComment`, `tourspotPic`, `tourspotLat`, `tourspotLong`, `tourspotStatus`) VALUES
(0000000001, 0000000011, '123456', '169 P Burgos St. Abut City Of San Fernando La Union', 'City Of San Fernando', '123', '69827425886162008-stsoptu-itor-761688232A-791381836-experience-0.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000048, 0000000007, 'Macho Temple', 'Pagdaraoan Biday City Of San Fernando La Union', 'City Of San Fernando', 'In the Chinese hierarchy of folk deities, Ma-Cho (Matzu) is the powerful Queen of Heavens and protector of the sea. She is the patron saint of fishermen and overseas Chinese.\r\nLegend says she was born a mortal named Lin Mo-liang (silent girl) sometime during the Northern Sung Dynasty (960-1127 A.D.) near Meizhou Bay in Fujian Province, China.\r\nDuring her lifetime of only 28 years she was said to be exceptionally pure of spirit and compassionate. It is said that she had supernatural powers and performed miracles, subduing evil spirits and averting disasters at sea. She sacrificed her life while trying to save seafarers endangered by rough seas.\r\n', '379714638591957038-ot-sttpuosir-1234.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000049, 0000000007, 'Poro-Point Lighthouse', 'Pagdaraoan Poro City Of San Fernando La Union', 'City Of San Fernando', '\r\nKnown historically as San Fernando Point s a headland and peninsula located in the city of San Fernando, La Union, on the island of Luzon in the Philippines. It projects northwesterly about 3.2 kilometres (2.0 mi) into the South China Sea. It was named after its location within barangay Poro and is the site of Poro Point Freeport Zone, a special economic zone established since 1993\r\n', '52895636528860037-rtui-ottsspo-9ef601b426ff96beb1f2720308c73b57.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000050, 0000000007, 'Pagoda', 'Pagdaraoan Bato City Of San Fernando La Union', 'City Of San Fernando', 'Pagoda Hill is a popular place for sightseeing and strolling and has long been established as a place for people to meet, talk and relax amidst the wonderful backgound of the San Fernando landscape and cityscape.  It is also a popular place for couples, friends and old-timers alike.  People have always remembered seeing the beautiful La Union sunset from the sun deck.', '367433284654711177-poitotrus-st-1347700016pJ7RVOLM.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000051, 0000000007, 'Pindangan Ruins', 'Pagdaraoan Sevilla City Of San Fernando La Union', 'City Of San Fernando', 'Pindangan Ruins is the vestige of the oldest church in the area, built in May 6, 1786. It symbolizes the union of two sitios, San Vicente de Balanac, a coastal village, and San Guillermo de Dalangdang, a village at the foothills of mountains. Both sitios desired for mutual protection against their assailants, the pirates and the headhunters, through Fr. Jose Torres in 1759. The settlement was named as Pindangan which was derived from the Ilocano word Pindang, a traditional method of drying fish in the locality. On the same date, May 6, 1786, Pindangan was founded as Ministerio de San Fernando in honor of King Ferdinand of Spain. The church was damaged due by an earthquake that occurred in the year 1892.', '826390907847169538-otpstsuro-ti-pindangan-ruins4.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000052, 0000000007, 'Bacsil Ridge', 'Pagdaraoan Bacsil City Of San Fernando La Union', 'City Of San Fernando', 'Bacsil Ridge is one of the most famous battle sites in the region. The site can be found on a mountainous terrain about 10 kilometers east of the city proper. It was the border of the Japanese Line established in mid-January 1945 and the Joint 121st Infantry Regiment of USAPIF-NI. A monument stands in the ridge to commemorate the hard-fought and won battle in the city.', '348528835206371051-triu-spttoos-1512076_10200589904709560_55221333_o.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000053, 0000000007, 'La Union Science Centrum and Museum', 'Pagdaraoan Bungro City Of San Fernando La Union', 'City Of San Fernando', 'La Union Science Centrum and Museum (LUSCM), the first-ever interactive hands-on discovery museum in the North, provides a practical and exciting alternative to formal education through interactive exhibits and its portable Planetarium. LUSCM is a learning center for everyone. It is an environment where science is portrayed as a dynamic and active process more than just a collection of facts and figures. It encourages children and adults alike to explore, interact, and appreciate science and technology, culture and environment.', '619203910945566041-tttro-iospsu-Landmarks-in-La-Union-26.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000054, 0000000007, 'La Union Botanical and Zoological Garden', 'Pagdaraoan Birunget City Of San Fernando La Union', 'City Of San Fernando', 'Botanical garden is located at the top of a mountain; it will take 30 minutes of travel by jeepney/private car from the city plaza. If you are an adventure seeker, you might enjoy going there through bicycle and for those who donâ€™t have any kind of transportation it is better to hire a jeepney so that you can easily go back to city proper.', '4559016123467032--rosstpiuott-botanical-garden1.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000055, 0000000007, 'Heroes Hill and Freedom Park', 'Pagdaraoan Bato City Of San Fernando La Union', 'City Of San Fernando', 'Situated east of the Capitol Hill of San Fernando City, La Union along Guerrero Road is the Heroes Hill and Freedom Park Stairway. Overlooking the West Philippine Sea, this Park is a series of concrete 153 steps located at a hillside (Heroes Hill) with concrete statues of some of the freedom fighters placed strategically along the sides of the landings of the steps while the National hero Dr. Jose Rizal is placed at the central part of the steps signifying the peopleâ€™s reverence to their works and sacrifices in attaining the freedom of our nation. It was constructed during the tenure of Governor Juvenal Guerrero between 1968 and 1975.\r\n\r\nIt is nice to visit the park when the sun is lying low in the horizon by the early morning or in the late afternoon, sit down and watch the cars and pedestrians passing by at the road below or witness the sunset on the West Philippine Sea along the Poro Point.\r\n\r\n ', '196339454771307575-spu-ttosrito-img_8887.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000056, 0000000007, 'Sea Park Beach Resort', 'Pagdaraoan Bungro City Of San Fernando La Union', 'City Of San Fernando', 'Sea Park is famous for its fine sand beach and clean, calm, crystal clear and sparkling waters. The beachfront cottages and restaurant offer the best place for relaxation and superb dining experience overlooking the South China Sea. \r\n\r\nExperience the fun hot sun during summer and the ripples of water and cool breeze during rainy season. Squander and take the pleasure of serene and romantic sunset. Sea Park Beach Resort caters to all your leisure needs whatever the climate and weather conditions; we assure unique experience that only Sea Park can offer. Ideal place to unwind and get away from urban pressures and hassles. Enjoy the comfort of your home by the sea. \r\n', '221562718483644363-t-iststrupoo-Sea Park Beach Resort (5).jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000057, 0000000007, 'Baluarte Watch Tower', 'Pagdaraoan Darigayos Luna La Union', 'Luna', 'The Baluarte of Luna, one of the watchtowers built on the shores of La Union during the Spanish era, is considered a national treasure by the National Historical Institute.', '82969923488945473-upits-sottro-141239524.yUaCBdvF.JM3_2710xr.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000058, 0000000007, 'Bahay na Bato', 'Pagdaraoan Darigayos Luna La Union', 'Luna', 'Bahay na Bato of Luna, La Union showcases sculpted or carved arts and crafts made from stones and driftwoods. The house, aptly called \"Bahay na Bato,\" was made from the stones and pebbles found along the beaches of the municipality. After all, Luna is the Pebble Capital of the North and stone-picking is one of its booming industries.', '739204502901072354-ooiprttu-tss-BahaynaBato_09.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000059, 0000000007, 'Bahay na Bato', 'Pagdaraoan Cantoria Luna La Union', 'Luna', 'Bahay na Bato is a beautiful place that houses sculptures made of stones and rocks. There are also fabulous paintings on the walls and other artworks. It is a nice stopover in La Union and we dropped by this attraction while on our way to Abra. Spectacular place for selfies and for those who are in arts.', '30396317460115046--ostuiprstto-download.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000060, 0000000007, 'Coral Island Resort', 'Pagdaraoan Cantoria Luna La Union', 'Luna', 'Coral Island Resort situated 9 km. off the south east coast of Phuket, the island is surrounded by colorful reefs, hence the name Coral Island. Only 15 min. by speedboat from Phuket there is total seclusion in this lovely resort. We have shuttle boat every day from Chalong Pier to Resort and from Resort to Chalong Pier according schedule 3 times per day. \r\n\r\n', '39890023295641915-uorttspisto--download (1).jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000061, 0000000007, 'Darigayos Cove', 'Pagdaraoan Rimos Luna La Union', 'Luna', 'The 800-meter beachline of this cove is filled with white, fine sand, compared to the other coastlines of Luna where they are filled with pebbles. It is located in barangay Darigayos, 34 kilometers from the city of San Fernando. This is the first barangay to be passed when taking the Balaoan-Luna Road if coming from San Fernando.', '348992155356572824-ruisttoop-ts-darigayos_04.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000062, 0000000007, 'USAFIPNL', 'Pagdaraoan Santo Luna La Union', 'Luna', 'It is in 2008 when the flock visited here for the first time. During that time, a memorial hall and a monument with an inscription were still here. Weâ€™re dismayed on how it was seemingly abandoned. But after the years, it has been turned into a Military Shrine and Park. The park is developed through the Proclamation 590 signed by President Fidel Ramos on June 1, 1995. At its center is a monument, written on it are the words â€œWe Remained, at USAFIPNL (United States Armed Forces in the Philippines North Luzon)â€. It is flanked on both sides by two large black slabs, carved on it are the names of those who died during the war. The places of origin are also written on each of the respective names. Also inscribed at the back of the monument is a short story about the landing of the USS SS GAR.', '326300290180756852-iutsrtoot-ps-darigayos_09.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000063, 0000000007, 'Tapuacan River', 'Pagdaraoan Tavora Pugo La Union', 'Pugo', 'Tapuacan River, also known Tapuakan River or Pugo River, is a river located in Barangay Cares, municipality of Pugo, La Union, Philippines. Its headwaters are situated at the Santo Tomas mountain range, with the main tributary being the Najeng River located in Barangay Tabaan Norte, Tuba, Benguet.', '380320371107072236--ossrotuptti-La-Union-Tapuakan-River3.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000064, 0000000007, 'Kultura Splash Wave', 'Pagdaraoan Tavora Pugo La Union', 'Pugo', 'The resort is equipped with several facilities like kiddie pools and slides which is like a childrenâ€™s playground half-submerged in water. There are several dolphin shaped adult pools and a rectangular swimming pool, the deepest of which would be around five feet.\r\nOne of the two main attractions that the resort is boasting is their giant slides where the entry point of the two biggest slides is found 3-storeys high and those two slide found at the second floor has an inclination of about more than forty five degrees which guarantee the creation of a giant splash once you reach the pool or drop zone, just remember to cover your nose to prevent water from entering on impact.\r\n', '740883009860896316-t-itortsspou-images.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000065, 0000000007, 'Pugad Pugo Adventures', 'Pagdaraoan Cuenca Pugo La Union', 'Pugo', 'PUGAD adventureâ€™s asset, aside from the climate, is the nature of the place. It has the very first zipline adventure in the province. It has pools, for kids and adults, and cottages of different sizes. At the entrance, friendly smiles from the accommodating staffs welcome every visitor. Roving personnel are also deployed to ensure the security of everyone. There are three parts of the quite-wide adventure park which were divided by hanging bridges. The first part is the view at the entrance. There is a pool surrounded by cottages and this is the same as the second part while the third part is the main attraction of the place. It is where the king-size pool for adults and the mini zoological and botanical garden are located. There are also activities offered in the place such as ziplining, wall climbing, repelling and team building activities. Packages on the different activities are also offered at affordable prices. Indeed, there is a life at Pugad. It is very suitable for family bodings, barkada trips or even couple dates.', '592068564336451688-utsr-tosoitp-images (1).jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000066, 0000000007, 'Bust of Ferdinand Marcos', 'Pagdaraoan Cares Pugo La Union', 'Pugo', 'A 30 m (98 ft) concrete bust of President of the Philippines Ferdinand Marcos was built in Pugo, La Union using government funds. The monument was controversial as the land used was grabbed from the indigenous Ibaloi people, who were against the Marcos conjugal dictatorship. The monument was built upon the orders of then incumbent President Ferdinand Marcos in a park that he named after himself. The monument was destroyed in December 2002 by treasure hunters.The destroyed bust is considered \"a monument to evil, warning people never to become what this man was\".', '90065528188031566-ou-ssttrptoi-download (3).jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000067, 0000000007, 'Calle Crisologo', 'Pagdaraoan Cabaroan City Of Vigan Ilocos Sur', 'City Of Vigan', 'The major attraction of Vigan is its mestizo district which is filled with Spanish-style houses that evoke a bygone era when its people lived prosperously because of the Manila-Acapulco maritime trade. For visitors who want to experience being transported back to this period in Philippine Spanish colonial time, a walk or a calesa ride through Viganâ€™s Calle Crisologo or Mena Crisologo Street is a must. The tour will take visitors more than a couple of hours and will cost around half a thousand pesos. You will be guided through four blocks of cobblestone streets lined with heritage houses of the families of the Filipino-Chinese traders who rose to prominence during that time of vigorous trading in abel cloth, indigo, gold, tobacco and other goods that were transported to Vigan from all over the North.', '890077977351741915-tripsuto-tso-Calle.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000068, 0000000007, 'Palacio De Arzobispado', 'Pagdaraoan Barangay City Of Vigan Ilocos Sur', 'City Of Vigan', 'Standing majestic and really ancient, Palacio de Arzobispado is an historical landmark in Vigan. It was built in 1783 and served as the official residence of the Archbishop of Nueva Segovia. The palace is the only surviving 18th century archbishopâ€™s residence in the country. It also became the headquarters of General Emilio Aguinaldo in 1898, and of the invading American forces under Colonel James Parker in 1899.', '780335873893665455-utsrooits-tp-palacio.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000069, 0000000007, 'Paburnayan ', 'Pagdaraoan Barangay City Of Vigan Ilocos Sur', 'City Of Vigan', 'Pagburnayan means a place where you make pots, while burnay means pot or jar. Aside from jar making, they also sell all kinds of clay products from pots, jars, vases of different shapes and sizes.Pagburnayan shops can be found at Gomez St. cor. Liberation Boulevard, Vigan City.', '966177554717336569-sp-sroutiott-paburnayan.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000070, 0000000007, 'Baluarte Zoo', 'Pagdaraoan Camangaan City Of Vigan Ilocos Sur', 'City Of Vigan', 'Baluarte Zoo is a zoological park in Vigan, Ilocos Sur Philippines. It is owned by Chavit Singson.The Baluarte Zoo was established by local politician and businessman, Chavit Singson. Singson, who cites his hunting hobby as his motive in setting up Baluarte only considered opening a zoo when his hunting trophy collection grew too big. He thought that a zoo would serve to edify it patrons aside from promoting conservation and protection of endangered species.Described as a interactive wildlife sanctuary, the facility was designed and developed by Singson himself.', '212813121347090875-tt-oposriuts-Baluarte-Zoo.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000071, 0000000007, 'Syquia Mansion', 'Pagdaraoan San City Of Vigan Ilocos Sur', 'City Of Vigan', 'The Syquia Mansion which houses President Qurinoâ€™s extensive memorabilia is actually owned by his wife, Dona Alicia Quirino. Her ancestor was a Chinese who made his fortune in the robust trading in Vigan. Sy Kiaâ€™s pride in his Asian heritage can be seen by visitors to the Syquia mansion where they can view a portrait of him with his braided long hair and traditional Chinese robes. ', '376275386384295740-uosi-tpttosr-syquia.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000072, 0000000007, 'Plaza Salcedo', 'Pagdaraoan Cabaroan City Of Vigan Ilocos Sur', 'City Of Vigan', 'Right in front of the Saint Paul Cathedral is the Plaza Salcedo, named after the Spanish conqueror, Juan de Salcedo who subdued the early native settlement in Vigan and established upon it Villa Fernandina for the monarchs of Spain in 1572. With this, Juan de Salcedo founded the third of many Hispanic settlements in the Philippines.', '29673760729740126--troptutsosi-download (4).jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000073, 0000000007, 'Bio View Deck', 'Pagdaraoan Lacong Tagudin Ilocos Sur', 'Tagudin', 'The view deck is located in Brgy. Bio which is the southernmost barangay in Tagudin, also the first barangay you have to take in Ilocandia when you are traveling by land from the south. The viewing deck is located at the top of a hill. It will allow you to view the boundary of La Union and Ilocos Sur provinces, the beautiful Amburayan River and the Amburayan Bridge.', '98474406781096900-touiosps-rtt-bio.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000074, 0000000007, 'Amburayan River', 'Pagdaraoan Dacutan Tagudin Ilocos Sur', 'Tagudin', 'Amburayan River is a river in the island of Luzon in the Philippines. It originates from the Cordillera mountains and traverses the provinces of Benguet, La Union, and Ilocos Sur, where it empties into the South China Sea. The river serves as the boundary between the provinces of Ilocos Sur and La Union. ', '316715138340171377-soips-trttuo-ambu.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000076, 0000000007, 'Tagudin Sundials', 'Pagdaraoan Bario-an Tagudin Ilocos Sur', 'Tagudin', 'Built by the Spaniards in 1841 and 1948, these two sundials, one in front of the Tagudin Municipal Hall and the other at the courtyard of the church, are still in use.Tagudin Sundials are Spanish-era structures. The two sundials of Tagudin are the oldest sundials in the Philippines and the only sundial used during the Spanish regime.\r\n\r\n', '123947127825027165-orpsi-sttuto-sundias.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000077, 0000000007, 'Ambulayat Footbridge', 'Pagdaraoan -SELECT BARANGAY- Tagudin Ilocos Sur', 'Tagudin', 'Go for adventure and off-road travel in Tagudin, then Ambalayat hanging Bridge is the place for you. It is actually a footbridge but they are used of calling it as a hanging bridge. It is located in Brgy Ambalayat, the last barangay in the town at the east separated by the Amburayan River, thus, the foot bridge.\r\n\r\n', '491054248589635564-ttiproto-uss-download (5).jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000079, 0000000007, 'Pinsal Falls', 'Pagdaraoan Laslasong Santa Maria Ilocos Sur', 'Santa Maria', 'A 9 km long drive down Sta. Maria town proper leads to the legendary Pinsal falls that manifest a majestic Abra mythology. Pinsal Falls is located at the far end of Sta. Maria, bordering the province of Abra. Behind the largest waterfalls in Ilocos is a mythology of a giant named Angalo searching for his love, Aran. \r\n\r\n', '636019375368764590-otrsot-sutip-Pinsal-Falls.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000080, 0000000007, 'Twin Tower and Ruins', 'Pagdaraoan Cabaroan Santa Maria Ilocos Sur', 'Santa Maria', 'Located in Brgy. Baballasioan before Pinsal Falls. Similar to cable car the first in Ilocos Sur and the first in Ilocos Region. This facility is not functional, the machine used to rotate the steel rope up to the upper station was submerged by waters coming from the pinsal falls. The location of this facility is within the ground level of the river downstream from the Pinsal Falls.\r\n\r\n', '551161567246854792-spt-strotoiu-twin.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000081, 0000000007, 'Suso Beach', 'Pagdaraoan Bia-o Santa Maria Ilocos Sur', 'Santa Maria', 'Beautiful beaches along Barangay Suso, Nalvo, Lingsat, and Bia-o have clear waters. These places are very alluring for picnic and swimming for tourists and vacationists. They are very accessible to all kinds of vehicles. A road connecting these into one super beach is being planned on ground.\r\n\r\n', '1875713640076915--trsuooisttp-ss beach.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000082, 0000000007, 'Santa Maria Baroque Church', 'Pagdaraoan Laslasong Santa Maria Ilocos Sur', 'Santa Maria', 'A beautiful baroque church located in the town center of Sta. Maria in Ilocos Sur. Formally called as the Church of Our Lady of the Assumption, it was designated as a UNESCO World Heritage church in December 1993, as part of the Baroque Churches of the Philippines, a collection of four Baroque Spanish-era churches. Due to its unique structure and appealing location, it is a popular destination not only by the religious but also by the tourists.\r\n\r\n', '629733131505955486-ttorsis-upto-santa.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000083, 0000000007, 'example', ' Bato City Of San Fernando La Union', 'City Of San Fernando', 'teajhs', '11137510730172724-irsstoptot-u-images (11).jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000084, 0000000007, 'example', 'Pagdaraoan Adams Adams Ilocos Norte', 'Adams', 'ugiuktd', '884218702816065149-psoto-tursti-55.jpg', '0.000000', '0.000000', 'Confirmed'),
(0000000085, 0000000007, 'sd', 'sd Baraoas City Of San Fernando La Union', 'City Of San Fernando', 'sdsd', '207326557708538067-u-orttsiospt-3.jpg', '0.000000', '0.000000', 'Pending'),
(0000000086, 0000000007, 'mmmm', 'pkj Cabugbugan Santa Ignacia Tarlac', 'Santa Ignacia', 'mm', '316707455638737704-pts-orosutit-55.jpg', '0.000000', '0.000000', 'Declined');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`aID`),
  ADD UNIQUE KEY `username` (`username`,`phoneNumber`);

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`businessID`);

--
-- Indexes for table `businesscalendar`
--
ALTER TABLE `businesscalendar`
  ADD PRIMARY KEY (`calendarID`);

--
-- Indexes for table `businesslocation`
--
ALTER TABLE `businesslocation`
  ADD PRIMARY KEY (`bisLocationID`);

--
-- Indexes for table `businessphotos`
--
ALTER TABLE `businessphotos`
  ADD PRIMARY KEY (`photoID`);

--
-- Indexes for table `businessps`
--
ALTER TABLE `businessps`
  ADD PRIMARY KEY (`businessPSID`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cartID`);

--
-- Indexes for table `cartprocess`
--
ALTER TABLE `cartprocess`
  ADD PRIMARY KEY (`cartprocessID`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryID`);

--
-- Indexes for table `categoryrequests`
--
ALTER TABLE `categoryrequests`
  ADD PRIMARY KEY (`catRequestID`),
  ADD UNIQUE KEY `catRequestText` (`catRequestText`);

--
-- Indexes for table `declinestatements`
--
ALTER TABLE `declinestatements`
  ADD PRIMARY KEY (`statementID`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`eventID`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`expID`);

--
-- Indexes for table `expimagelogs`
--
ALTER TABLE `expimagelogs`
  ADD PRIMARY KEY (`explogID`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`favID`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`feedbackID`);

--
-- Indexes for table `imagelogs`
--
ALTER TABLE `imagelogs`
  ADD PRIMARY KEY (`logID`);

--
-- Indexes for table `marketplace`
--
ALTER TABLE `marketplace`
  ADD PRIMARY KEY (`marketpostID`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`rID`);

--
-- Indexes for table `secretquestions`
--
ALTER TABLE `secretquestions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subdeclinestatements`
--
ALTER TABLE `subdeclinestatements`
  ADD PRIMARY KEY (`subdeclineID`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`subscriptionID`);

--
-- Indexes for table `tourspot`
--
ALTER TABLE `tourspot`
  ADD PRIMARY KEY (`tourspotID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `aID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `businessID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;
--
-- AUTO_INCREMENT for table `businesscalendar`
--
ALTER TABLE `businesscalendar`
  MODIFY `calendarID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `businesslocation`
--
ALTER TABLE `businesslocation`
  MODIFY `bisLocationID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;
--
-- AUTO_INCREMENT for table `businessphotos`
--
ALTER TABLE `businessphotos`
  MODIFY `photoID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `businessps`
--
ALTER TABLE `businessps`
  MODIFY `businessPSID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cartID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `cartprocess`
--
ALTER TABLE `cartprocess`
  MODIFY `cartprocessID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `categoryrequests`
--
ALTER TABLE `categoryrequests`
  MODIFY `catRequestID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `declinestatements`
--
ALTER TABLE `declinestatements`
  MODIFY `statementID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `eventID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `expID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `expimagelogs`
--
ALTER TABLE `expimagelogs`
  MODIFY `explogID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `favID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `feedbackID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `imagelogs`
--
ALTER TABLE `imagelogs`
  MODIFY `logID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `marketplace`
--
ALTER TABLE `marketplace`
  MODIFY `marketpostID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `rID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `secretquestions`
--
ALTER TABLE `secretquestions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subdeclinestatements`
--
ALTER TABLE `subdeclinestatements`
  MODIFY `subdeclineID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `subscriptionID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tourspot`
--
ALTER TABLE `tourspot`
  MODIFY `tourspotID` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
