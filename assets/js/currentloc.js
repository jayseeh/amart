var options = {
    // enableHighAccuracy can slower response times or increase power consumption 
    enableHighAccuracy: true,
    // Maximum length of time (in milliseconds) the device is allowed to return a position
    timeout: 5000,
    // Maximum age in milliseconds of a possible cached position that is acceptable
    maximumAge: 1000 * 5 * 3
  };
  
  function startGeoLookup() {  
    navigator.geolocation.getCurrentPosition(success, error, options);  
     //$('.overlay, .loader').fadeIn('fast');
  } 
  startGeoLookup();
  
  function success(pos) {
    
    $('.overlay, .loader').fadeOut('fast');
    var crd = pos.coords;
  
    var roundAcc = Math.floor(crd.accuracy);
    
    // document.getElementById('mylat').value = crd.latitude.toFixed(6);
    // document.getElementById('mylng').value = crd.longitude.toFixed(6);
    // document.getElementById('txtSource').value = crd.latitude.toFixed(6) +' '+ crd.longitude.toFixed(6);
    
    // var  startlat = crd.latitude.toFixed(6);
        //   var  startlng = crd.longitude.toFixed(6);
        //   var sfc = new google.maps.LatLng(startlat, startlng);
          
          var geocoder = new google.maps.Geocoder;
          var infowindow = new google.maps.InfoWindow;
  
          var mapOptions = {
              center: sfc,
              streetViewControl: false,
              mapTypeControl: false,
              fullscreenControl:false,
              zoomControl:false
          }
          var image = '../images/origin.png';
          var map = new google.maps.Map(document.getElementById("dvmap"), mapOptions);
          var marker = new google.maps.Marker({
              position: sfc,
              animation: google.maps.Animation.DROP,
              icon: image,
              title:"You are here"
          });
          marker.addListener('click', toggleBounce);
  
          // To add the marker to the map, call setMap();
          marker.setMap(map);
          
          var mycurrentloc = document.getElementById("currentlocation");
          geocoder.geocode({'location': sfc}, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {
                map.setZoom(15);
            
                mycurrentloc.value = results[0].formatted_address;
                
                } else {
                window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
            });

          function toggleBounce() {
            if (marker.getAnimation() !== null) {
              marker.setAnimation(null);
            } else {
              marker.setAnimation(google.maps.Animation.BOUNCE);
            }
          }
          
  }
  var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    
  /* Error Codes: 1 PERMISSION_DENIED, 2 POSITION_UNAVAILABLE, 3 TIMEOUT */
  function error(err) {
    var geoerror = (err.code == (1, 2, 3) ? "Error loading" : err.message);
    //document.getElementById('errMsg').innerHTML = geoerror;
    $('.overlay, .loader').fadeOut('fast');
  }