<?php
    include 'db.sql/connect_db.php';
    if(isset($_POST['f-email']) && isset($_POST['answer'])){
        $email = $_POST['f-email'];
        $answer = $_POST['answer'];
        $fetchquery= mysqli_query($connect, "SELECT aID, username, secretAnswer FROM accounts WHERE uEmail='$email' and secretAnswer='$answer'"); 
        $count = mysqli_num_rows($fetchquery);

        if($count > 0) {
            while($fetchdata = mysqli_fetch_array($fetchquery)){
                $id = $fetchdata['aID']; 
                $username = $fetchdata['username'];
            }
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0"/>
        <link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="assets/js/jquery.js"></script>
        <script type="text/javascript" src="assets/js/materialize.min.js"></script>

    </head>
<body class="grey darken-2">
<main class="container">
<div class="card">
    <div class="card-content">
    <h4>
    <a href="index.php" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
    Change Password
    </h4>
    <form action="db.sql/updatepasswordrecover.php" id="form" method="post">
        <div class="row">
            <div class="col s12 m12 l12">
                <p>You're about to change your password</p>
            </div>
        </div>
        <div class='row'>
            <div class='input-field col s12 m12 l12'>
                <input class='validate' type='password' name="newpassword" id="newpassword" minlength=6 autofocus required/>
                <label for='newpassword'>Enter New Password</label>
            </div>
        </div>
        <div class='row'>
            <div class='input-field col s12 m12 l12'>
                <input class='validate' type='password' name="confirmnewpassword" id="confirmnewpassword" minlength=6 required/>
                <label for='confirmnewpassword'>Confirm New Password</label>
            </div>
            <div class="col s12">
				<span class="error" style="color:red"></span>
			</div>
        </div>
        <div class="row">
            <div class='col s12 l12 center-align'>
                <input type="hidden" name="uID" value="<?php echo $id; ?>">
                <input type="hidden" name="uname" value="<?php echo $username; ?>">
                <input type='submit' class="modal-trigger btn yellow darken-3" value='Change Password'>
            </div>
        </div>
        </form>
    </div>
</div>

</main>
    <script>
    $(document).ready(function(){
        M.updateTextFields();
        $('.modal').modal();

        $('#newpassword').keyup(function(e){
            var newpass = $(this).val();
            var confirmpass = $('#confirmnewpassword').val();
            if( (newpass.length >= 6 && confirmpass.length >=6 ) || confirmpass != ""){
                if(newpass == confirmpass){
                    $('.error').text('MATCHED').removeClass("red-text").addClass("green-text");
                    submit = true;
                }else{
                    $('.error').text('NOT MATCHED').removeClass("green-text").addClass("red-text");
                    submit = false;
                }
            }
        });

        $('#confirmnewpassword').keyup(function(e){
            var newpass = $('#newpassword').val();
            var confirmpass = $(this).val();
            if( (newpass.length >= 6 && confirmpass.length >=6) || newpass != ""){
                if(newpass == confirmpass){
                    $('.error').text('MATCHED').removeClass("red-text").addClass("green-text");
                    submit = true;
                }else{
                    $('.error').text('NOT MATCHED').removeClass("green-text").addClass("red-text");
                    submit = false;
                }
            }
        });
        

        $('#form').submit(function(){
            var checkIfEmpty = $('#newpassword').val() != "" && $('#confirmnewpassword').val() != "" ;
            if(submit && checkIfEmpty){
                return true;
            }else{
                return false;
            }
        });
    });
    </script>
    <script type="text/javascript">
        function capitalize(obj) {
            str = obj.value;
            obj.value = str.replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
    </script>
</body>
</html>

<?php
        } else {
            header('location: index.php?message=incorrectanswer');
        }
    } else {
        header('location: index.php');
    }
?>