<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, X-CSRF-TOKEN');
    header('Content-type: application/json');

	include_once('salt.php');
    include_once('connect_db.php');
    
    $username = stripslashes($_REQUEST['username']);
	$password = stripslashes($_REQUEST['password']);

	$myusername = mysqli_real_escape_string($connect, $username);
	$mypassword = mysqli_real_escape_string($connect, $password);

	$Csalt = ucfirst($salt);
	//-----------------------------------------------
	$usernamehash = hash ("sha512", $myusername . $Csalt);
	//-----------------------------------------------
	$spass = crypt($mypassword,'$6$'.$usernamehash);
	//-----------------------------------------------
	$finalpass = substr($spass, 21);

        $query = "SELECT password 
				FROM accounts 
				WHERE username = '$myusername'";
                
        $result = mysqli_query($connect, $query);
        $num = $result->num_rows;
            if ($num > 0) {
                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    extract($row);
                    $user_item = array(
                        'password' => $password
                    );
				}
				if( $user_item['password'] == $finalpass ){
					echo json_encode(
						array('result' => 'true')
					);
				} else {
					echo json_encode(
						array('result' => 'false')
					);
				}
            } else {
                echo json_encode(
                    array(
                        'message' => 'No User Found'
                    )
                );
            }
        mysqli_close($connect);
?>