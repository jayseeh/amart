<?php
    header('Content-type: application/json');

    include_once('connect_db.php');
    
    if(isset($_POST['businessID'])){
        $businessID = $_POST['businessID'];
    }

        $query = "SELECT * FROM business 
                INNER JOIN categories ON
                business.businessCategory = categories.categoryID 
                WHERE businessID = '$businessID'";
                
        $result = mysqli_query($connect, $query);
        $num = $result->num_rows;
            if ($num > 0) {
                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    extract($row);
                    $business_item = array(
                        'businessID' => $businessID,
                        'businessName' => $businessName,
                        'categoryFee' => $categoryFee,
                        'businessCategory' => $businessCategory
                    );
                }
                echo json_encode($business_item);
            } else {
                echo json_encode(
                    array(
                        'message' => 'No business Found'
                    )
                );
            }
        mysqli_close($connect);
?>