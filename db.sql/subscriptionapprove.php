<?php
include 'connect_db.php';

if(isset($_POST['approve'])) {  

    $datenow = date('Y-m-d');
    $duration = $_POST['duration'];
    $subID = $_POST['subID'];
    $subDateEnd = $_POST['subDateEnd'];
    $newcategory = $_POST['newcategory'];
    
    if($subDateEnd == '0000-00-00' || $subDateEnd <= $datenow){
      $addsubtothisdate = $datenow;
    }else{
      $addsubtothisdate = $subDateEnd;
    }
    
    echo $effectiveDate = date('Y-m-d', strtotime("+".$duration." months", strtotime($addsubtothisdate)));
    $businessID = $_POST['businessID'];
    
    $sqlsubscription ="UPDATE subscription SET subDateApproved = '$datenow', subDateEnd ='$effectiveDate', subStatus= 'Approved' where subscriptionID ='$subID'";
    
    $sqlbusiness =  "UPDATE business SET businessStatus = 'Active', businessSubscriptionEnd = '$effectiveDate' , businessCategory = '$newcategory' WHERE businessID = '$businessID' ";
    
    if (!mysqli_query( $connect ,$sqlsubscription)) {
      die(header("location:../views/users/admin/subscription.php?message=error1"));
    }
    if (!mysqli_query( $connect ,$sqlbusiness)) {
      die(header("location:../views/users/admin/subscription.php?message=error2"));
    }
    header("location:../views/users/admin/subscription.php?message=success");

} else{
    header("location:../views/users/admin/subscription.php");
}

mysqli_close($connect);
?>
