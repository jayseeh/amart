<?php
include 'connect_db.php';
include 'salt.php';

if(isset($_POST['uID']) && isset($_POST['uname']) && isset($_POST['newpassword'])){
  $uname =  $_POST['uname'];
  $uID =  $_POST['uID'];

  $mypassword =  $_POST['newpassword'];
  $mypassword = stripslashes ($mypassword);
  $mypassword = mysqli_real_escape_string($connect, $mypassword);

  $Csalt = ucfirst($salt);
  //-----------------------------------------------
  $usernamehash = hash ("sha512", $uname . $Csalt);
  //-----------------------------------------------
  $spass = crypt($mypassword,'$6$'.$usernamehash);
  //-----------------------------------------------
  $newfinalPass = substr($spass, 21);

  $sql ="UPDATE accounts SET password ='$newfinalPass' where accounts.aID = '$uID'";

  if (!mysqli_query( $connect ,$sql)) {
    die(header("location:../index.php?$uname&msg=error"));
  }
  header("location:../index.php?$uname");
  mysqli_close($connect); 
} else {
  header("location: ../index.php");
}
?>