<?php 
include 'connect_db.php';

$error = '';

if(isset($_POST['submit'])) {     
    $thisID = $_POST['ID'];
	$type =  $_POST['type'];
	$oldpath = mysqli_real_escape_string($connect, $_POST['oldpath']);
	$thistype = lcfirst($type);
	
	if( !isset($thisID) ) {

		$error = 'User ID is not Set';
	
	} elseif( !isset($thistype) ) {
	
		$error = 'User Type is not Set';
	
	} else {
		$thisfile = rand(1,999999999).rand(1,999999999).'-'.str_shuffle('profile-picture')."-".$_FILES['profpic']['name'];
		$file = str_replace("'","",$thisfile);
		$file_loc = $_FILES['profpic']['tmp_name'];
		$file_size = $_FILES['profpic']['size'];
		$file_type = $_FILES['profpic']['type'];
		$folder="../assets/images/";
		$new_size = $file_size/1024; 
	
		if(move_uploaded_file($file_loc,$folder.$file)){
			$sql="UPDATE accounts SET uPic='$file' WHERE accounts.aID='$thisID'";
	
			if (!mysqli_query( $connect ,$sql)) {
				die(mysqli_error($connect));
			}	
				if( $oldpath != 'default.png' ) {
					unlink($folder.$oldpath);
				}
				header("location:../views/users/$thistype/profile.php");
				mysqli_close($connect);
		}else{
			$error = 'FileNotUploaded';
			header("location:../views/users/$thistype/profile.php?error=$error");
		}
	}
	
} else {
	header("location:../views/users/$thistype/profile.php?error=$error");
}

mysqli_close($connect); 

?>