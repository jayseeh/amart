<?php
    include 'db.sql/connect_db.php';
    if(isset($_POST['f-email'])){
        $email = $_POST['f-email'];
        $fetchquery= mysqli_query($connect, "SELECT aID, secretQuestion FROM accounts WHERE uEmail='$email'"); 
        $count = mysqli_num_rows($fetchquery);

        if($count > 0) {
            while($fetchdata = mysqli_fetch_array($fetchquery)){
                $question = $fetchdata['secretQuestion']; 
            }
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0"/>
        <link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="assets/js/jquery.js"></script>
        <script type="text/javascript" src="assets/js/materialize.min.js"></script>
    </head>
<body class="grey darken-2">
<main class="container">
<div class="card">
    <div class="card-content">
    <h4>
    <a href="index.php" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
    Answer Secret Question
    </h4>
    <form action="changepass.php" method="POST">
        <div class="row">
            <div class="col s12 m12 l12">
                <p><?php echo $question; ?></p>
                
            </div>
        </div>
        <div class='row'>
            <div class='input-field col s12 m12 l12'>
                <input class='validate' type='text'name="answer" autofocus required/>
                <label for='first'>Answer</label>
            </div>
        </div>
        <div class="row">
            <div class='col s12 l12 center-align'>
                 <input type="hidden" name="f-email" value="<?php echo $email; ?>">
                <input type='submit' class="modal-trigger btn yellow darken-3" value='Confirm'>
            </div>
        </div>
        </form>
    </div>
</div>

</main>
</body>
</html>

<?php 
        } else {
            header('location: index.php?message=incorrectemail');
        }
    } else {
        header('location: index.php');
    }
?>