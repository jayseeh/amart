<?php 
  session_start();

  if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 3600)) {
    // last request was more than 30 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
    header("Location: http://" . $_SERVER['SERVER_NAME']);
  }

  $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp

  $userID = "";
  if(isset($_GET['aID'])){
    $userID = $_GET['aID'];
  }
  if(isset($_SESSION['userID'])){
    $aType = $_SESSION['aType'];
  }
  if(isset($_GET['message'])){
    $message = $_GET['message'];
    if($message == 'incorrectemail'){
      $message = 'E-mail Address not Found';
    } elseif($message == 'incorrectlogin'){
      $message = 'Incorrect username or password';
    } elseif($message == 'incorrectanswer'){
      $message = 'Incorrect Answer';
    }
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="mlmi.png">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <title>AMART DIGITAL</title>

    <!-- Style Core CSS -->
    <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/>
	<link href="assets/css/style.css" rel="sstylesheet">
	<link href="assets/css/index.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- jQuery -->
    <script src="assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/materialize.min.js"></script>  
    <script>
      var d = new Date();
      // document.getElementById('date').innerHTML = datestring;
      function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        var suffix = "AM";
        if (h >= 12) {
          suffix = "PM";
          h = h - 12;
        }
        if (h == 0) {
        h = 12;
        }
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('time').innerHTML =
        h + ":" + m + ":" + s;
        document.getElementById('UTC').innerHTML = suffix;
        var t = setTimeout(startTime, 500);
      }
      function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
      }
    </script>
</head>

<style>
.modal
{

  max-height: 50%;
  overflow: hidden;
}

</style>


<!--*****TOP NAVIGATION BAR*****-->
<body class="grey darken-3" onload="startTime()">
<!-- error div -->
      <div id="error-div" class="col s12 red darken-3 center-align hide" style="padding: 10px; ">
        <span class="white-text"><?php echo $message; ?></span>
      </div>
  <form class="col s2 hide-on-small-only show-on-med-and-up" action="db.sql/checklogin.php" method="post">
    <nav id="nav">
      <div class='row col s12 '>
              <div class='col s4'>
          <a href="index.php"><img src="assets/images/logo.index.png" class="hide-on-small-only show-on-med-and-up" height="95px" width="200px" id="nav2"></a>
        <br>
        </div>
        <?php if(!isset($_SESSION['userID'])){ ?>
        <!-- Start of Login Field -->
        <div id="loginfields">
          <div class='col s3 '>
              <input class='validate' type='text' name='myusername' id='username' autocomplete='off ' value="<?php echo $userID; ?>" placeholder="Username" autofocus required/>
              <br>
          </div>
          <div class='col s3 '>
              <input class='validate' type='password' name='mypassword' id='mypassword' placeholder="Enter your password" autofocus required/>
          </div>

          <div class='col s1 hide-on-med-and-down show-on-large-only' id="div4">
            <center><button type='submit' name='submit' class='col s12 btn btn-medium waves-effect yellow darken-3' id='nav3'>Login</button></center>
          </div>
          <div class='col s2 hide-on-large-only hide-on-small-only show-on-med-only black-text' id="div4">
            <center><button type='submit' name='submit' class='col s12 btn btn-medium waves-effect black-text yellow darken-3' id='nav4'>Login</button></center>
          </div>
          
          <div class='col s1 hide-on-med-and-down show-on-large-only' id="div4">
          </div>
      
          <div class='row col s12 hide-on-small-only show-on-med-and-up' style="margin-top:-60px;">
            <div class='col s4 hide-on-small-only show-on-med-and-up'></div>
            <div class='col s3 hide-on-small-only show-on-med-and-up' style="text-align: center; float: center; ">
              <a href="#register" class="modal-trigger yellow-text text-darken-3" style="padding-top: -10%; text-align: center; float: center; border-radius: 20px; text-decoration: underline;"><h6><b>Create account</b></h6></a>
            </div>

            <div class='col s3 hide-on-small-only show-on-med-and-up' style="text-align: center; float: center;">
              <a href='#forgotten' class='yellow-text text-darken-3 modal-trigger'  style="padding-top: -10%; text-align: center; float: center; border-radius: 20px; text-decoration: underline;"><h6><b>Forgot Password?</b></h6></a></small></span>
            </div>
            <div class='col s2 hide-on-small-only show-on-med-and-up' style="text-align: center;">
              <span id="time" style="font-size: 40px; opacity: 0.6;"></span> <span id="UTC" style="font-size: 25px; opacity: 0.6;"></span>
              <br>
              <span id="date"></span>
            </div>
          </div>
        </div> 
        <!-- End of Login Field -->
        <?php } else { ?>
        <!-- Go to Dashboard -->
        <div id="gotodashboard" class="right grey darken-3" style="padding: 0px 20px;">
          <span><a href="views/users/<?php echo $aType; ?>/dashboard.php" class="yellow-text text-darken-3">Go to Dashboard >></a></span>
        </div>
        <!-- End of go to dashboard -->
        <?php } ?>

      </div>
    </nav>
  </form>
<!-- Mobile View -->
<div class="show-on-small hide-on-med-and-up">
  <center class="show-on-small  hide-on-med-and-up">
    <img class="responsive-img show-on-small  hide-on-med-up" style="width: 50%; height: 40%; padding-top: 10%; padding-bottom: 5%;" src="assets/images/logo.index.png" alt="logo" />

    <div class="container show-on-small  hide-on-med-and-up">
      <div class="card card-login">
        <div class="card-content">

        <?php if(!isset($_SESSION['userID'])){ ?>
          <form class="col s12" action="db.sql/checklogin.php" method="post">
            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='text' name='myusername' id='myusername' autocomplete='off' required autofocus/>
                <label for='user'>Username</label>
              </div>
            </div>

            <div class='row'>
                <div class='input-field col s12'>
                    <input class='validate' type='password' name='mypassword' id='password' required autofocus />
                    <label for='pw'>Enter your password</label>
                </div>
            </div>

            <div class="row">
              <div class="col s12">
                <span class="right"><small><a class='yellow-text text-darken-3 modal-trigger' href='#forgotten'><b>Forgot Password?</b></a></small></span>
              </div>
            </div>
            <center>
              <div class='row'>
                <center><input type='submit' name='submit' value="Login" class='col s12 btn btn-large yellow darken-3'></center>
              </div>
              <a href="#register" class="modal-trigger yellow-text text-darken-3">Create account</a>
            </center>
          </form>
          <!-- End of Login Field -->
          <?php } else { ?>
          <!-- Go to Dashboard -->
          <div class="grey darken-3" style="padding: 10px 20px;">
            <span><a href="views/users/<?php echo $aType; ?>/dashboard.php" class="yellow-text text-darken-3">Go to Dashboard >></a></span>
          </div>
          <!-- End of go to dashboard -->
          <?php } ?>
        </div> 
      </div>
    </div>
    <!-- End of Card -->
    
    <div id='mobile-footer'>
      <div class="row">
        <div class="col s6" style="border-right: 1px solid grey">
          <a href="#t&c" style="font-size: 13px; " class='modal-trigger amber-text text-darken-2'>Terms and Conditions</a>
        </div>
        <div class="col s6">
          <a href="#about" style="font-size: 13px;" class='modal-trigger amber-text text-darken-2'>Privacy and Regulations</a>
        </div>
      </div>
    </div> 
  </center>
</div>

<div class="carousel carousel-slider carousel-home show-on-med-and-up hide-on-small-only" width="100%">
    <a class="carousel-item" href="#one"><img src="assets/images/3.png">
      <!-- <div class="caption text-center">
        <h1 style="z-index: 1000; color: white; margin-top: -35%; text-align: center; font-size: 150px;">TRAVEL LOCAL</h1>
      </div> -->
    </a>
    <a class="carousel-item" href="#two"><img src="assets/images/4.png"></a>
    <a class="carousel-item" href="#three"><img src="assets/images/1.png"></a>
    <a class="carousel-item" href="#four"><img src="assets/images/2.png"></a>
</div>

<br>
<center><img src="assets/images/partners.png" class="hide-on-small-only show-on-med-and-up" id="logo" height="300px" width="300px"></center>
<!-- <center><img src="assets/images/partners.png" class="show-on-small-only hide-on-med-and-up" id="logo" height="100px" width="100px"></center> -->

<!--***INTRO***-->
<div id="" class="container hide-on-small-only show-on-med-and-up">
  <div class="row" id="intro1">
    <p class="hide-on-small-only show-on-med-and-up white-text"><strong>Centralize all business for individuals tired finding needs </strong><strong><b id="intro2">Amart Digital</b> will locate your destination come in!</strong></p>
    <!-- <p class="show-on-small-only hide-on-med-and-up white-text" id="mv3"><strong>Centralize all business for individuals tired finding needs </strong><strong><b id="intro2">Amart Digital</b> will locate your destination come in!</strong></p> -->
  </div>
</div>
<!-- ****END OF INTRO*** -->

<!--***ABOUT US*** -->
<div id="about1" class="cover black-text">
<div id="about1" class="container black-text hide-on-small-only show-on-med-and-up">
<br>
<div id="about2" class="row">
  <h3 class="black-text hide-on-small-only show-on-med-and-up"><center>ABOUT US<hr class="black-text" id="about3"></center></h3>
  <!-- <h5 class="black-text show-on-small-only hide-on-med-and-up"><center>ABOUT US<hr class="black-text" id="about3"></center></h5> -->
  <p class="black-text hide-on-small-only show-on-med-and-uphide-on-small-only show-on-med-and-up" id="intro1">We believe that e-commerce in this technologically advancing world would continue to develop. Amart would like to provide our clients an advantage online through our services by helping them showcase their products, services and location.</p>
  <br>
</div>
</div> <!--end of cover -->
</div> <!-- end of container -->
<!--***END OF ABOUT***-->
<br>

<div id="" class="container hide-on-small-only show-on-med-and-up">
<!--***MISSION / VISION***-->
<div class="row text-center hide-on-small-only show-on-med-and-up">
      <div class="col s6 m6 l6" id="div7">
        <div class="thumbnail" id="navbar"><br>
      <p id="div4"><strong><h3 class="amber-text">MISSION<hr id="hr"></h3></strong></p>
        <img src="assets/images/mission.png" width="100" height="100">
      </p>
      <p class="white-text">To provide easy access to travelling and small businesses with hidden potential</p>
    </div>
    </div>
    <div class="col s1 m1 l1" id="mv1">
    </div>
      <div class="col s5 m5 l5" id="div7">
    <div class="thumbnail" id="navbar"><br>
      <p class="text-center"><strong><h3 class="amber-text">VISION<hr id="hr"></h3></strong></p>
      <img src="assets/images/vision.png" width="100" height="100">
      <p class="white-text">Centralizing the businesses in the country and help them grow</p>
    </div>
      </div>
      
</div>
<div class='row'>
          <div class='input-field col s2'>
          </div>
          
          <div class='input-field col s2' >
		  <table>
			<tr style="border-style: none;">
				<td><i class="material-icons prefix black-text">phone</i></td>
				<td></td><td></td>
				<td><span class="black-text" for='user'>+6390909090909</span></td>
			</tr>
		  </table>
          </div>
		  <div class='input-field col s1'>
          </div>
          <div class='input-field col s2'>
		  <table>
			<tr style="border-style: none;">
				<td><i class="material-icons prefix fa fa-facebook black-text"></i></td>
				<td></td>
				<td><span class="black-text" for='user'>amart.digital@fb.com</span></td>
			</tr>
		  </table>
          </div>
		  <div class='input-field col s1'>
          </div>
          <div class='input-field col s2' >
		  <table>
			<tr style="border-style: none;">
				<td><i class="material-icons prefix black-text">email</i></td>
				<td></td><td></td>
				<td><span class="black-text" for='user'>amartdigital@gmail.com</span></td>
			</tr>
		  </table>
          </div>
          
          <div class='input-field col s2'>
          </div>
        </div>
        <div class='row'>
        <div id='div6' class="hide-on-small-only show-on-med-and-up">
          <div class='input-field col s12'>
            <a href="#t&c" style="font-size: 13px;" class='modal-trigger amber-text text-darken-2 hide-on-small-only show-on-med-and-up'>Terms and Conditions</a>
            &nbsp;&nbsp;&nbsp;<span style="color: black;" class="hide-on-small-only show-on-med-and-up">|</span>&nbsp;&nbsp;&nbsp;
            <a href="#about" style="font-size: 13px;" class='modal-trigger amber-text text-darken-2 hide-on-small-only show-on-med-and-up'>Privacy and Regulations</a>
          </div>
        </div>
    </div>

<!-- for mobile
<div class="row text-center show-on-small-only hide-on-med-and-up">
      <div class="col s1 m1 l1" id="mv1">
    </div>
    
    <div class="col s10 m10 l10" id="mv2">
        <div class="thumbnail" id="navbar"><br>
      <p id='div4'><strong><h5 class="amber-text">MISSION<hr id="hr"></h5></strong></p>
        <img src="assets/images/mission.png" width="70" height="70">
      </p>
      <p id="mv3" class="white-text">To provide easy access to travelling and small businesses with hidden potential</p>
    </div>
    </div>
    
    <div class="col s1 m1 l1" id="mv1">
    </div>
</div>
<div class="row text-center show-on-small-only hide-on-med-and-up">
      <div class="col s1 m1 l1" id="mv1">
    </div>

    <div class="col s10 m10 l10" id="mv2">
    <div class="thumbnail" id="navbar"><br>
      <p class="text-center white-text"><strong><h5 class="amber-text">VISION<hr id="hr"></h5></strong></p>
      <img src="assets/images/vision.png" width="70" height="70">
      <p id="mv3" class="white-text">Centralizing the businesses in the country and help them grow</p>
    </div>
      </div>
    
    <div class="col s1 m1 l1" id="mv1">
    </div>
</div>
</div> -->
<br>
<br>
<!--***END OF MISSION / VISION***-->
</div> <!--end of cover -->
</div> <!-- end of container -->


<!-- <div id="about1" class="cover black-text">
<div id="about1" class="container black-text hide-on-small-only show-on-med-and-up">
<br class="hide-on-small-only show-on-med-and-up"><br> -->
   
   
    
    <!-- <div style="float: center; font-size: 13px; text-align: center;">
      <a href="#t&c" class='modal-trigger amber-text text-darken-2 show-on-small-only hide-on-med-and-up'>Terms and Conditions</a>
      &nbsp;&nbsp;&nbsp;<span style="color: white;" class="show-on-small-only hide-on-med-and-up">|</span>&nbsp;&nbsp;&nbsp;
      <a href="#about" class='modal-trigger amber-text text-darken-2 show-on-small-only hide-on-med-and-up'>Privacy and Regulations</a>
    </div>   -->
</div>
</div>
<br>    
<!--****************************************END OF CONTACTS****************************************-->

<!-- Registration Modal BIG-->
<div id="register" class="modal" > 
    <div class="modal-content">
      <h4>
        <div class="row hide-on-small-only show-on-med-and-up">
          <div class="col s1 m1 l1">
            <!-- <a href="#" class="large modal-close yellow-text text-darken-3" id='div4'><i class="material-icons">arrow_back</i></a> -->
          </div>
          <div class="col s12 m12 l12 valign-center hide-on-small-only show-on-med-and-up">
            <center>Choose</center>
          </div>
        </div>
        <div class="row show-on-small-only hide-on-med-and-up">
          <div class="col s1 m1 l1">
            <!-- <a href="#" class="large modal-close yellow-text text-darken-3" id='div4'><i class="material-icons">arrow_back</i></a> -->
          </div><br>
          <div class="col s12 m12 l12 valign-center show-on-small-only hide-on-med-and-up">
            <center>Choose</center>
          </div>
        </div>
      </h4>
      
      <div class="divider show-on-large hide-on-med-and-down"></div><br>
        <div class="row">
          <div class="col s6 m6 l6  show-on-large hide-on-med-and-down "><br><br><br><br>
            <button onclick="window.location.href='registration.php?utype=User'" id='div4' class="btn btn-large yellow darken-3 registration-button valign-center">User</button>
          </div>

          <div class="col s6 m6 l6  hide-on-med-and-down show-on-large"><br><br><br><br>
            <button onclick="window.location.href='registration.php?utype=Owner'" class="btn btn-large yellow darken-3 registration-button valign-center">Owner</button>
          </div>

          <div class="col s6 m6 l6  show-on-med-and-down hide-on-large-only"><br>
            <button onclick="window.location.href='registration.php?utype=User'" id='div4' class="btn btn-large yellow darken-3 registration-button valign-center">User</button>
          </div>

          <div class="col s6 m6 l6  show-on-med-and-down hide-on-large-only"><br>
            <button onclick="window.location.href='registration.php?utype=Owner'" class="btn btn-large yellow darken-3 registration-button valign-center">Owner</button>
          </div>
        </div>     
    </div>
    
  </div>
<!--End of Registration-->


<!-- Forgot Password Modal -->
<div id="forgotten" class="modal">
    <div class="modal-content">
      <h4>
        <div class="row">
          <div class="col s1 m1 l1">
            <a href="#" class="large modal-close yellow-text text-darken-3" id='div4'><i class="material-icons">arrow_back</i></a>
          </div>
          <div class="col s11 m11 l11" id='rm1'>
            Forgot Password
          </div>
        </div>
      </h4>
    <div class="divider"></div>
      <form action="forgotten.php" method="POST">
        <div class="row">
          <div class="col s12 input-field">
            <label for="f-email">Email Address</label>
            <input type="email" name="f-email" class="validate" required />
          </div>
          <div class="row center-align">
            <div class="col s12">
              <input type="submit" name="submit" value="Proceed" class="btn yellow darken-3"/>
            </div>
          </div>
        </div>
      </form> 
    </div>
  </div>
<!--End of Forgot Password-->

<?php 
  include('views/partials/footer.php');
?>

<script>
  $(document).ready(function(){
    M.updateTextFields();
    $('.modal').modal();
  });
</script>

<?php include 'tc.php' ?>
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/materialize.min.js"></script>
<script>
  $(document).ready(function(){
    var message = "<?php if(isset($_GET['message'])) echo $_GET['message']; ?>";
    if(message != ""){
      if(message == "incorrectlogin" || message == "incorrectemail" || message == "incorrectanswer") {
        $('#error-div').removeClass('hide');
        $('#error-div').fadeOut(2000);
      }
    }

    setInterval( autoplay, 5000);

    function autoplay(){
      $('.carousel').carousel('next');
    }
  });
</script>

