var map;
var pos;
var lats,lngs;
function initMap() {

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      lats= position.coords.latitude;
      lngs= position.coords.longitude;
      document.getElementById('businessLat').value=lats;
      document.getElementById('businessLng').value=lngs;

    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  }

  setTimeout(function(){
    console.log("mapss");
    console.log("Lat: "+lats+ " Lng: "+lngs);
    var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png';
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: lats, lng: lngs},
      zoom: 13,
      mapTypeId: 'satellite',
      streetViewControl: false,
      mapTypeControl: true,
      fullscreenControl:false,
      zoomControl:true,
      draggable:true
    });
    
    var marker = new google.maps.Marker({
      position: {lat: lats, lng: lngs},
      map: map,
      icon: iconBase,
      draggable:true
    });
    google.maps.event.addListener(marker, 'dragend', function(evt){
      console.log( 'Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(6) + ' Current Lng: ' + evt.latLng.lng().toFixed(6) );
      //alert( 'Marker dropped: Current Lat: ' + evt.latLng.lat() + ' Current Lng: ' + evt.latLng.lng() );
      document.getElementById('businessLat').value=evt.latLng.lat().toFixed(6);
      document.getElementById('businessLng').value=evt.latLng.lng().toFixed(6);
    });
  },1000);

}