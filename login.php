<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0"/>
        <link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="assets/js/jquery.js"></script>
        <script type="text/javascript" src="assets/js/materialize.min.js"></script>
        <style>
          .registration-button{
            display: block;
            width: 100%;
            margin: 30% auto;
          }
          @media only screen and (min-width : 992px) {
            .card-login{
              width:50%;
            }
          }
        </style>
    </head>

<body class="grey darken-2">
  <main>
    <center>
      <img class="responsive-img" style="width: 250px; height: 100px;" src="assets/images/logo.png" alt="logo" />

      <div class="container">
        <div class="card card-login">
          <div class="card-content">

          <form class="col s12" action="db.sql/checklogin.php" method="post">
            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='text' name='myusername' id='myusername' autocomplete='off ' autofocus/>
                <label for='user'>Username</label>
              </div>
            </div>

            <div class='row'>
                <div class='input-field col s12'>
                    <input class='validate' type='password' name='mypassword' id='mypassword' />
                    <label for='pw'>Enter your password</label>
                </div>
            </div>

            <div class="row">
              <div class="col s12">
                <span class="right"><small><a class='yellow-text text-darken-3 modal-trigger' href='#forgotten'><b>Forgot Password?</b></a></small></span>
              </div>
            </div>
            <center>
              <div class='row'>
                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect yellow darken-3'>Login</button>
              </div>
              <a href="#register" class="modal-trigger yellow-text text-darken-3">Create account</a>
            </center>
          </form>
        </div>
        </div>
      </div>
    </center>

    <!-- Registration Modal -->
    <div id="register" class="modal" style="height: 100%;">
			<div class="modal-content">
        <h4>
        <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
        Choose
        </h4>
        <div class="divider"></div>

        <div class="row show-on-small-only hide-on-med-and-up">
          <div class="col s6 m6 l6 show-on-small-only hide-on-med-and-up">
            <button onclick="window.location.href='registration.php?utype=User'" class="btn btn-small yellow darken-3 registration-button valign-center show-on-small-only hide-on-med-and-up">User</button>
          </div>
          <div class="col s6 m6 l6 show-on-small-only hide-on-med-and-up">
            <button onclick="window.location.href='registration.php?utype=Owner'" class="btn btn-small yellow darken-3 registration-button valign-center show-on-small-only hide-on-med-and-up">Owner</button>
          </div>
        </div>  

		<div class="row hide-on-small-only show-on-med-and-up">
          <div class="col s12 m6 l6 hide-on-small-only show-on-med-and-up">
            <button onclick="window.location.href='registration.php?utype=User'" class="btn btn-small yellow darken-3 registration-button valign-center hide-on-small-only show-on-med-and-up">User</button>
          </div>
          <div class="col s12 m6 l6 hide-on-small-only show-on-med-and-up">
            <button onclick="window.location.href='registration.php?utype=Owner'" class="btn btn-small yellow darken-3 registration-button valign-center hide-on-small-only show-on-med-and-up">Owner</button>
          </div>
        </div> 
		
      </div>
    </div>

     <!-- Forgot Password Modal -->
     <div id="forgotten" class="modal" style="height: 100%;">
			<div class="modal-content">
        <h4>
        <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
        Forgot Password
        </h4>
        <div class="divider"></div>
        <form action="forgotten.php" method="POST">
          <div class="row">
            <div class="col s12 input-field">
              <label for="f-email">Email Address</label>
              <input type="email" name="f-email" class="validate" required />
            </div>
            <div class="row center-align">
              <div class="col s12">
                <input type="submit" name="submit" value="Proceed" class="btn yellow darken-3"/>
              </div>
            </div>
          </div>
        </form> 
      </div>
    </div>
  </main>
<script>
  $(document).ready(function(){
    M.updateTextFields();
    $('.modal').modal();
  });
</script>
</body>
</html>