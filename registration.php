<?php
    $regtype = $_GET['utype'];
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0"/>
        <link href="assets/css/styles.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="assets/js/jquery.js"></script>
        <script type="text/javascript" src="assets/js/materialize.min.js"></script>

    </head>

<body class="grey darken-2">
<main class="container">
<div class="card">
    <div class="card-content">
    <h4>
    <div class="row">
        <div class="col s1 m1 l1">
            <a href="index.php" class="large modal-close yellow-text text-darken-3" id='div4'><i class="material-icons">arrow_back</i></a>
        </div>
    <?php
    if($regtype == "Owner"){
        echo '<div class="col s11 m11 l11 valign-center" id="rm1">Owner Registration Form</div>';
    }else if($regtype == "User"){
        echo '<div class="col s11 m11 l11 valign-center" id="rm1">User Registration Form</div>';
    }
    ?>
    </h4>
    <form action="db.sql/insertusersql.php" method="post" id="registrationform">
        <div class='row'>
            <div class='input-field col s12 l4'>
                <input class='validate' type='text' minlength=3 onkeyup='javascript:capitalize(this);' name='first' id='first' autofocus required/>
                <label class="active" for='first'>First Name</label>
            </div>
            <div class='input-field col s12 l4'>
                <input class='validate' type='text' minlength=3 onkeyup='javascript:capitalize(this);' name='mid' id='mid' />
                <label class="active" for='mid'>Middle Name</label>
            </div>
            <div class='input-field col s12 l4'>
                <input class='validate' type='text' minlength=3 onkeyup='javascript:capitalize(this);' name='last' id='last' required/>
                <label class="active" for='last'>Last Name</label>
            </div>
        </div>
        <div class='row'>
            <div class='input-field col s12 l6'>
                <input class='validate' type='text' minlength=6 name='ruser' id='ruser' required/>
                <label class="active" for='ruser'>Username</label>
            </div>
            <div class='input-field col s12 l6'>
                <input class='validate' type='text' name='contact' maxlength='11' minlength='11' id='contact' required/>
                <label class="active" for='contact'>Contact</label>
            </div>
        </div>  
        <div class='row'>
            <div class='input-field col s12 l6'>
                <input class='validate' type='password' minlength=6 name='pass' id='pass' required/>
                <label class="active" for='pass'>Password</label>
            </div>
            <div class='input-field col s12 l6'>
                <input class='validate' type='password' minlength=6 name='confirmpass' id='confirmpass' required/>
                <label class="active" for='confirmpass'>Confirm Password</label>
            </div>
            <div class="col s12 center-align">
                <span class="error"></span>
            </div>
        </div>
        <div class="row">
            <div class="col s12 center align">
                <label>
                    <input type="checkbox" required />
                    <span>I have read and agree to the <a href="#t&c" class="modal-trigger"> Terms and Conditions </a> of Amart Digital</span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class='col s12 center-align'>
                <input type="hidden" name="utype" value="<?php echo $regtype; ?>" required/>
                <input type='submit' class="modal-trigger btn yellow darken-3" value='Create Account'>
            </div>
        </div>
        </form>
    </div>
</div>

<?php include 'tc.php'; ?>
</main>
<script>
  $(document).ready(function(){
    M.updateTextFields();
    $('.modal').modal();
    var submit = false;
    $('#pass').keyup(function(e){
        var pass = $(this).val();
        var confirmpass = $('#confirmpass').val();
        if( (pass.length >= 6 && confirmpass.length >=6 ) || confirmpass != ""){
            if(pass == confirmpass){
                $('.error').text('Password Matched').removeClass("red-text").addClass("green-text");
                submit = true;
            }else{
                $('.error').text('Password Not Matched').removeClass("green-text").addClass("red-text");
                submit = false;
            }
        }
    });

    $('#confirmpass').keyup(function(e){
        var pass = $('#pass').val();
        var confirmpass = $(this).val();
        if( (pass.length >= 6 && confirmpass.length >=6) || pass != ""){
            if(pass == confirmpass){
                $('.error').text('Password Matched').removeClass("red-text").addClass("green-text");
                submit = true;
            }else{
                $('.error').text('Password Not Matched').removeClass("green-text").addClass("red-text");
                submit = false;
            }
        }
    });

    $('#registrationform').submit(function(){
        if(submit){
            return true;
        }else{
            return false;
        }
    });
            
  });
    function capitalize(obj) {
        str = obj.value;
        obj.value = str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }
    </script>
        
</body>
</html>