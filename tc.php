<!-- Terms And Conditions Modal -->
<div id="t&c" class="modal" style="height: 100%;">
	<div class="modal-content">
	<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
        <h4 style="text-align: center;">TERMS AND CONDITIONS</h4>
        <div class="divider"></div>
        <!-- tabs -->
        <div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Terms of Use</b>
			</nav>
				<p style="text-align: justify;">
				By creating an account with "amartdigital.com" you are agreeing to be bound by the following Terms of Use. If you do not agree to our Terms of Use, then you may not access or use this website.
				</p>
				
				<p style="text-align: justify;">
				Any new features or tools which are added to the current Service shall be also subject to the Terms and Condition. Amartdigital reserves the right to update, change and replace the Terms of Use by posting updates and changes to the "amartdigital.com". You are advised to check the Terms of Service from time to time for any updates or changes.
				</p>
				
				<p style="text-align: justify;">
				Your continued use of or access to the Site following the posting of any changes to this Agreement constitutes acceptance of those changes.
				</p>
				
				<p style="text-align: justify;">
				You can review the most current version of the Terms of Service at any time in this website.
				</p>
        </div> 
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Overview</b>
			</nav>
				<p style="text-align: justify;">
				Amartdigital acts as a meeting place to allow users, who comply with our policies to offer, sell and buy certain goods and services. However, Amartdigital is not directly involved in the transaction between buyers and sellers. As a result, we has no control over the quality, safety, morality or legality of any aspect of the items listed, the truth or accuracy of the listings, the ability of sellers to sell items or the ability of buyers to pay for items.
				</p>
				
				<p style="text-align: justify;">
				We do not pre -screen users or the content or information provided by users and it is in their sole discretion to refuse or remove any Content that is available via the Service. We cannot ensure that a buyer or seller will actually complete a transaction.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Your Amartdigital Account</b>
			</nav>
				<p style="text-align: justify;">
				If you create an account to use the Services on the Site, (your “Account”) you are responsible for maintaining the security and accuracy of your Account, and you are fully responsible for all activities that occur under your Account and any other actions taken in connection with your Account.
				</p>
				
				<p style="text-align: justify;">
				You must immediately notify Amartdigital of any unauthorized uses of your Account or any other breaches of security. Amartdigital will not be liable for any acts or omissions by you, including any damages of any kind incurred as a result of such acts or omissions.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>General Conditions</b>
			</nav>
				<p style="text-align: justify;">
				The headings used in this agreement are included for convenience only and will not limit or otherwise affect these Terms.
				</p>
				
				<p style="text-align: justify;">
				We reserve the right to refuse service to anyone for any reason at any time.
				</p>
				
				<p style="text-align: justify;">
				We reserve the right to modify or terminate the Service for any reason, without notice at any time.
				</p>
				
				<p style="text-align: justify;">
				You expressly agree that your use of, or inability to use, the service is at your sole risk.
				</p>
				
				<p style="text-align: justify;">
				The Service is provided on an “as is“ and ”as available“ basis without any warranty or condition, express, implied or statutory.
				</p>
				
				<p style="text-align: justify;">
				Amartdigital does not warrant that the service will be uninterrupted, timely, secure, or error-free.
				</p>
				
				<p style="text-align: justify;">
				Amartdigital does not warrant that the results that may be obtained from the use of the service will be accurate or reliable.
				</p>
				
				<p style="text-align: justify;">
				We may, but have no obligation to, remove Content and Accounts containing Content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or these Terms of Service.
				</p>
				
				<p style="text-align: justify;">
				You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service without the express written permission by Amartdigital.
				</p>				
        </div>
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Comments and Feedback</b>
			</nav>
				<p style="text-align: justify;">
				You agree that your comments will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right.
				</p>
				
				<p style="text-align: justify;">
				You further agree that your comments will not contain libelous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website.
				</p>
				
				<p style="text-align: justify;">
				You may not use a false e-mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments.
				</p>
				
				<p style="text-align: justify;">
				You are solely responsible for any comments you make and their accuracy.
				</p>
				
				<p style="text-align: justify;">
				We take no responsibility and assume no liability for any comments posted by you or any third-party.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Contents from Other Services</b>
			</nav>
				<p style="text-align: justify;">
				We have not reviewed, and cannot review, all of the material, including computer software, made available through the websites and webpages to which Amartdigital.com links, that link to Amartdigital.com, or on which Amartdigital products are embed.
				</p>
				
				<p style="text-align: justify;">
				Amartdigital does not have any control over those non-Amartdigital websites and webpages, and is not responsible for their contents or their use. By linking to a non-Amartdigital website or webpage, Amartdigital does not represent or imply that it endorses such website or webpage.
				</p>
				
				<p style="text-align: justify;">
				You are responsible for taking precautions as necessary to protect yourself and your computer systems from viruses, worms, Trojan horses, and other harmful or destructive content.
				</p>
				
				<p style="text-align: justify;">
				Amartdigital disclaims any responsibility for any harm resulting from your use of non-Amartdigital websites and webpages.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Intellectual Property of Users</b>
			</nav>
				<p style="text-align: justify;">
				We do not claim any intellectual property rights over the material you provide to the Amartdigital. 
				</p>
				
				<p style="text-align: justify;">
				All material you upload remains yours.
				</p>
				
				<p style="text-align: justify;">
				You can remove the content or the entirety of your amartdigital store at any given time. This will also remove all content you have stored on the Service.
				</p>
				
				<p style="text-align: justify;">
				By uploading images and item description content to Amartdigital, you agree to allow other internet users to view them and you agree to allow Amartdigital to display and store them and you agree that Amartdigital can, at any time, review all the content submitted by you to its Service.
				</p>
				
				<p style="text-align: justify;">
				The Terms of Service constitutes the entire agreement between you and Amartdigital and govern your use of the Service, superseding any prior agreements between you  and Amartdigital (including, but not limited to, any prior versions of the Terms of Service).
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3 show-on-small-only hide-on-med-and-up" style="color: black; text-align: center; float: center; font-size: 10px;">
				<b>Intellectual Property of Amartdigital</b>
			</nav>
			<nav class="amber darken-3 hide-on-small-only show-on-med-and-up" style="color: black; text-align: center; float: center;">
				<b>Intellectual Property of Amartdigital</b>
			</nav>
				<p style="text-align: justify;">
				This Agreement does not transfer from Amartdigital to you any Amartdigital or third party intellectual property, and all right, title and interest in and to such property will remain with Amartdigital Inc.
				</p>
				
				<p style="text-align: justify;">
				Your use of the Services grants you no right or license to reproduce or otherwise use any Amartdigital or third-party trademarks.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3 show-on-small-only hide-on-med-and-up" style="color: black; text-align: center; float: center; font-size: 8px;">
				<b>Business Owner Account Holder’s Responsibility</b>
			</nav>
			<nav class="amber darken-3 hide-on-small-only show-on-med-and-up" style="color: black; text-align: center; float: center;">
				<b>Business Owner Account Holder’s Responsibility</b>
			</nav>
				<p style="text-align: justify;">
				If you publish a product, sell a product/service or otherwise make (or allow any third party to make) a product/service available by means of the Services (any such material, “Products/Services”), you are entirely responsible for the Product, and any harm resulting from, that Product/Services. By making Products/Services available, you represent and warrant that such Product:
				</p>
				
				<p style="text-align: justify;">
				*Is not false, inaccurate or misleading;
				</p>
				
				<p style="text-align: justify;">
				*Is not fraudulent or involve the sale of counterfeit, stolen or infringing items;
				</p>
				
				<p style="text-align: justify;">
				*Does not infringe or misappropriate any third party’s copyright, patent, trademark, trade secret or other proprietary rights or rights of publicity or privacy;
				</p>
				
				<p style="text-align: justify;">
				*Does not violate any law, statute, ordinance or regulation (including those governing privacy, publicity, export control, consumer protection, intellectual  property, gambling, unfair competition, anti-discrimination, criminal activities or false advertising);
				</p>
				
				<p style="text-align: justify;">
				*Is not defamatory, libelous, offensive, unlawfully threatening or harassing, or advocating, promoting or providing assistance involving violence, significant risk of death or injury, or other unlawful activities;
				</p>
				
				<p style="text-align: justify;">
				*Is not obscene or immoral and does not contain child pornography;
				</p>
				
				<p style="text-align: justify;">
				*Does not involve the transmission of any unsolicited commercial or bulk email (known as “spamming”) and you shall not use your Account as a return address  for unsolicited commercial mail originating elsewhere or participate in any activities related to so-called pyramid, ponzi schemes or the like;
				</p>
				
				<p style="text-align: justify;">
				*Does not involve the collection, sale or transmission of, or attempt to collect personally identifiable information of any person or entity, except with the express written consent of that person or entity;
				</p>
				
				<p style="text-align: justify;">
				*Does not link directly or indirectly to or include descriptions of goods or services that violate any applicable law, statute, ordinance or regulation, or that violate this Agreement.
				</p>
				
				<p style="text-align: justify;">
				You expressly understand and agree that Amartdigital shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses resulting from the use of or inability to use the Site.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Payment and Renewal</b>
			</nav>
				<p style="text-align: justify;">
				By subscribing to Amartdigital you agree to pay Amartdigital the periodic fees indicated for the Services through Paymaya.
				</p>
				
				<p style="text-align: justify;">
				The Services will be billed depending on your choice of subscription.
				</p>
				
				<p style="text-align: justify;">
				Amartdigital will have no obligation to provide a refund of any amounts previously paid.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3 show-on-small-only hide-on-med-and-up" style="color: black; text-align: center; float: center; font-size: 8px;">
				<b>User/Consumer Account Holder’s Responsibility</b>
			</nav>
			<nav class="amber darken-3 hide-on-small-only show-on-med-and-up" style="color: black; text-align: center; float: center;">
				<b>User/Consumer Account Holder’s Responsibility</b>
			</nav>
			
				<p style="text-align: justify;">
				Amartdigital may, but has no obligation to, remove Product and/or Account containing Product that we determine in our sole discretion is unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or this Agreement.
				</p>
				
				<p style="text-align: justify;">
				Amartdigital does not warrant that the quality of any products, services, information, or other material purchased or obtained by you through the Site will meet your expectations, or that any errors in the Site will be corrected.
				</p>
				
				<p style="text-align: justify;">
				You expressly understand and agree that Amartdigital shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses resulting from the use of or inability to use the Site.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Termination</b>
			</nav>
				<p style="text-align: justify;">
				These Terms of Use are effective unless and until terminated by either you or us. 
				</p>
				
				<p style="text-align: justify;">
				You may terminate these Terms of Service at any time by notifying us that you no longer wish to use our Services, or when you cease using our site.
				</p>
				
				<p style="text-align: justify;">
				Without limiting any other remedies, Amartdigital may suspend or terminate your Account if we suspect that you (by conviction, settlement, insurance or escrow investigation, or otherwise) have engaged in fraudulent activity in connection with the Site.
				</p>
				
				<p style="text-align: justify;">
				Amartdigital may terminate your access to all or any part of the Services at any time, with or without cause, with or without notice, effective immediately.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3 show-on-small-only hide-on-med-and-up" style="color: black; text-align: center; float: center; font-size: 9px;">
				<b>Disclaimer of Warranties: Limitation of Liability</b>
			</nav>
			<nav class="amber darken-3 hide-on-small-only show-on-med-and-up" style="color: black; text-align: center; float: center;">
				<b>Disclaimer of Warranties: Limitation of Liability</b>
			</nav>
			
				<p style="text-align: justify;">
				The service and all products and services delivered to you through the service are provided 'as is' and 'as available' for your use, without any representation, warranties or  conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a  particular purpose, durability, title, and non-infringement.
				</p>
				
				<p style="text-align: justify;">
				We do not guarantee, represent or warrant that your use of our service will be uninterrupted, timely, secure or error-free.
				</p>
				
				<p style="text-align: justify;">
				We do not warrant that the results that may be obtained from the use of the service will be accurate or reliable.
				</p>
				
				<p style="text-align: justify;">
				You agree that from time to time we may remove the service for indefinite periods of time or cancel the service at any time, without notice to you.
				</p>
				
				<p style="text-align: justify;">
				In no event shall Amartdigital or our suppliers be liable for lost profits or any special, incidental or consequential damages arising out of or in connection with the Site, the Services or this agreement (however arising including negligence).
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Indemnification</b>
			</nav>
				<p style="text-align: justify;">
				You agree to indemnify and hold us and (as applicable) our parent, subsidiaries, affiliates, partners, officers, directors, agents, and employees, harmless from any claim or demand, including reasonable attorneys’ fees, made by any third party due to or arising out of your breach of this Agreement or the documents it incorporates by reference, or your violation of any law or the rights of a third party.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Dispute Resolution</b>
			</nav>
				<p style="text-align: justify;">
				In the event a dispute arises between you and Amartdigital, please contact Amartdigital.
				</p>
				
				<p style="text-align: justify;">
				In case you have a dispute with one or more users, or an outside party, you release Amartdigital (and Amartdigital’s officers, directors, agents, subsidiaries, joint ventures and employees) from any and all claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way connected with such disputes.
				</p>
				
				<p style="text-align: justify;">
				Amartdigital, to a certain extent, may try to help users resolve disputes and disagreements. However, Amartdigital has no obligation to resolve disputes between users and between users and outside parties.
				</p>
        </div>
		
		<div class="row">
			<nav class="amber darken-3" style="color: black; text-align: center; float: center;">
				<b>Entire Agreement</b>
			</nav>
				<p style="text-align: justify;">
				The failure of Amartdigital to exercise or enforce any right or provision of the Terms of Service shall not constitute a waiver of such right or provision.
				</p>
				
				<p style="text-align: justify;">
				These Terms of Service and any policies or operating rules posted by us on this site or in respect to The Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms of Service).
				</p>
				
				<p style="text-align: justify;">
				Any ambiguities in the interpretation of these Terms of Service shall not be construed against the drafting party.
				</p>
        </div>
		
      </div>
    </div>	