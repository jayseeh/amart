</main>
<footer>

</footer>
<script>
    $(document).ready(function(){
        $('.collapsible').collapsible();
        $('.modal').modal();
        $('.carousel').carousel({
            duration: 10000
        });
        $('.carousel.carousel-slider').carousel({
            fullWidth: true,
            indicators: true,
        });
        $('.materialboxed').materialbox();
        $('.tabs').tabs();
        $('.timepicker').timepicker();
        $('select').formSelect();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('.sidenav').sidenav();
        M.AutoInit();
        M.updateTextFields();
    });

    document.addEventListener('touchmove', function(event) {
        event = event.originalEvent || event;
        if (event.scale !== 1) {
           event.preventDefault();
        }
    }, false);
</script>

<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="../../../assets/js/datatable-materialize.js"></script>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCV8jXa9ac0MOtqwICz0WZ9QqE8gnHWNA&libraries=places&region=PH&callback=initMap"></script> -->
</body>
</html>