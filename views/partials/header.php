<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include_once "../../../db.sql/connect_db.php";

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 3600)) {
    // last request was more than 30 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
    header("Location: http://" . $_SERVER['SERVER_NAME']);
}

$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp

if (!isset($_SESSION['userID'])) {
    header("Location: http://" . $_SERVER['SERVER_NAME']);
	exit;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="../../../assets/css/materialize.min.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link href="../../../assets/css/styles.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/datatable-materialize.css" rel="stylesheet" type="text/css" />
        
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
        <script type="text/javascript" src="../../../assets/js/jquery.js"></script>
        <script type="text/javascript" src="../../../assets/js/materialize.min.js"></script>
        <!--<script type="text/javascript" src="../../../js/mapdashboard.js"></script>-->
        <!--<script type="text/javascript" src="../../../js/mapUser.js"></script>-->
        <script type="text/javascript" src="../../../js/tourSpotMap.js"></script>
        <!--<script type="text/javascript" src="../../../js/tourView.js"></script>-->
	</head>
    <body>