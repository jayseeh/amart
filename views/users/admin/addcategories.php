<?php include "../../../db.sql/connect_db.php"; ?>
<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php';

	$fetchrequest = mysqli_query($connect, "SELECT categoryID FROM categories WHERE categoryStatus='Pending' ORDER BY categoryFee ASC");
	$requestcount = mysqli_num_rows($fetchrequest);

	if($requestcount > 0){
		$newreqbadge = "<span id='request-badge' class='new badge red' style='position:absolute; top: 15px;'> $requestcount </span>";
	} else {
		$newreqbadge = "";
	}

?>
<link rel="stylesheet" href="../../../css/datatable.css">
<link rel="stylesheet" href="css/material.min.css">
<link rel="stylesheet" href="css/dataTables.material.min.css">

<div class="main">

<div class="row">
	<center>
		<br>
		<div class="col s12 m5 l4">
			<a href="#addcategories" class="modal-trigger" id="addcat"><button class='btn btn-large waves-effect yellow darken-3 col s12'>Add Categories</button></a>
		</div>
		<div class="col s12 m5 l4">
			<a href="categoriesrequest.php" class="modal-trigger" id="reqcat"><button class='btn btn-large waves-effect yellow darken-3 col s12'>Categories Request <?php echo $newreqbadge; ?></button></a>
		</div>
		<div class="col s12 m5 l4">
			<a href="#updatetypes" class="modal-trigger" id="updatetype"><button class='btn btn-large waves-effect yellow darken-3 col s12' id="updatetype">Update Types</button></a>
		</div>
	</center>
<div class="col s12 l12">
    <div class="">
        <table id="business_category_table">
            <thead style="text-align: left;">
                <th style="text-align: left;">Categories</th>
                <th style="text-align: left;">Type</th>
                <th style="text-align: left;">Value</th>
                <th style="text-align: left;">Action</th>
         	</thead>
			<tbody>
				<?php 
					$fetchquery= mysqli_query($connect, "SELECT * FROM categories WHERE categoryStatus = 'Active' ORDER BY categoryFee ASC"); 
					while($fetchdata = mysqli_fetch_array($fetchquery)){
					$catID = $fetchdata['categoryID'];
					$catName = $fetchdata['categoryName'];
					$catType = $fetchdata['categoryType'];
					$catFee = $fetchdata['categoryFee'];
				?>
				<tr>
					<td><?php echo $catName;?></td>
					<td><?php echo $catType;?></td>					
					<td><?php echo '₱ '.$catFee;?></td>
	                <td><a href="#viewcategories-<?php echo $catID; ?>" class="modal-trigger">View</a></td>
				</tr>
				<?php } ?>
			</tbody>
        </table>
	</div>
</div>

<!-- Add Categories -->
	<div class="modal modal-fixed-footer overflow-modal" id="addcategories">
		<form action="../../../db.sql/insertnewcategory.php" class="form-horizontal"  role="form" method="post">
		<div class="modal-content">
			<h4>
				<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
				Add Categories
			</h4>
			
			<div class="row">
				<div class="col s12 m12 l12">
					<label for="types">Types</label>
					<select name="types" id="types" required>
						<option value="" disabled selected>Category Type</option>
						<?php 
							$fetchquery= mysqli_query($connect, "SELECT * FROM categories GROUP BY categoryType ORDER BY categoryFee ASC"); 
							while($fetchdata = mysqli_fetch_array($fetchquery)){
							$catType = $fetchdata['categoryType'];
							$catFee = $fetchdata['categoryFee'];
							if ($catFee == 0 && $catType == 'Free'){
								$finalcatFee = ' ';
							}else if($catFee == 0 && $catType != 'Free'){
								$finalcatFee = ' - Free';
							}else{
								$finalcatFee = ' - ₱ '.$catFee;
							}
							?>
							<!-- <optgroup label=""> -->
								<option value="<?php echo $catType;?>" id="<?php echo $catType;?>"><?php echo $catType.' '.$finalcatFee;?> </option>
							<!-- </optgroup> -->
						<?php } ?>
					</select>
				</div>
			</div>
			<div class='row'>
				<div class='input-field col s12 m12 l12'>
					<i class="material-icons prefix yellow-text text-darken-3">edit</i>
		            <input placeholder="Name" class='validate' type='text' name='categoryname' id='categoryname' required/>
		            <label for='categoryname'>Name</label>
	            </div>
	        </div>
	    </div>
		<div class="modal-footer">
			<div class="row center-align">
				<div class="col s12 m12 l12">
					<input type="submit" name="submit" value="Add" class="btn yellow darken-3">
				</div>
			</div>
		</div>
		</form>
	</div>

	<!-- View Categories -->
	<?php 
		$fetchview= mysqli_query($connect, "SELECT * FROM categories"); 
		while($fetchdata = mysqli_fetch_array($fetchview)){
		$catID = $fetchdata['categoryID'];
		$catName = $fetchdata['categoryName'];
		$catType = $fetchdata['categoryType'];
		$catFee = $fetchdata['categoryFee'];
	?>
	<div class="modal modal-fixed-footer overflow-modal" class="blocked-modal" id="viewcategories-<?php echo $catID; ?>">
		<div class="modal-content">
			<h5>
				<div class="row">
					<div class="col s2 m1 l1">
						<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
					</div>
					<div class="col s10 m11 l11">
						Details
					</div>
				</div>
			</h5>
			<div class="divider"></div>
			<center>
				<p><b>Name: </b> <?php echo $catName; ?></p>
				<p><b>Type: </b> <?php echo $catType; ?></p>
				<p><b>Value: </b> <?php echo $catFee; ?></p>
			</center>
		</div>
		<div class="modal-footer">
			<div class="row center-align">
				<a href="#updatecategories-<?php echo $catID; ?>" class="modal-trigger btn green darken-3">Update</a>
				<a href="#archive-<?php echo $catID; ?>" class="modal-trigger btn red darken-3">Archive</a>
			</div>
		</div>
	</div>
	
<!-- Comfirm Categories -->
<div class="modal modal-fixed-footer overflow-modal" id="confirmcategories">
		<form action="../../../db.sql/insertnewcategory.php" class="form-horizontal"  role="form" method="post">
		<div class="modal-content">
			<h4>
				<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
				Confirm Categories
			</h4>
			
			<div class="row">
				<div class="col s12 m12 l12">
					<label for="types">Types</label>
					<select name="types" id="types" required>
						<option value="" disabled selected>Category Type</option>
						<?php 
							$fetchquery= mysqli_query($connect, "SELECT * FROM categories GROUP BY categoryType ORDER BY categoryFee ASC"); 
							while($fetchdata = mysqli_fetch_array($fetchquery)){
							$catType = $fetchdata['categoryType'];
							$catFee = $fetchdata['categoryFee'];
							if ($catFee == 0 && $catType == 'Free'){
								$finalcatFee = ' ';
							}else if($catFee == 0 && $catType != 'Free'){
								$finalcatFee = ' - Free';
							}else{
								$finalcatFee = ' - ₱ '.$catFee;
							}
							?>
							<!-- <optgroup label=""> -->
								<option value="<?php echo $catType;?>" id="<?php echo $catType;?>"><?php echo $catType.' '.$finalcatFee;?> </option>
							<!-- </optgroup> -->
						<?php } ?>
					</select>
				</div>
			</div>
			<div class='row'>
				<div class='input-field col s12 m12 l12'>
					<i class="material-icons prefix yellow-text text-darken-3">edit</i>
		            <input placeholder="Name" class='validate' type='text' name='categoryname' id='categoryname' required/>
		            <label for='categoryname'>Name</label>
	            </div>
	        </div>
	    </div>
		<div class="modal-footer">
			<div class="row center-align">
				<div class="col s12 m12 l12">
					<input type="submit" name="submit" value="Add" class="btn yellow darken-3">
				</div>
			</div>
		</div>
		</form>
	</div>

	<!-- End of View Categories -->
	
	<!-- Update Categories -->
	<div class="modal modal-fixed-footer overflow-modal" class="blocked-modal" id="updatecategories-<?php echo $catID; ?>">
		<form action="" class="form-horizontal"  role="form" method="post">
		<div class="modal-content">
			<h5>
				<div class="row">
					<div class="col s2 m1 l1">
						<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
					</div>
					<div class="col s10 m11 l11">
						Update
					</div>
				</div>
			</h5>
			<div class="divider"></div>
			<div class="row">
				<div class="col s12 m12 l12">
					<label for="update_types">Types</label>
					<select name="update_types" id="update_types" required>
						<option value="" disabled selected>Choose Type</option>
						<?php 
							$catquery= mysqli_query($connect, "SELECT * FROM categories GROUP BY categoryType ORDER BY categoryFee ASC"); 
							while($fetchcat = mysqli_fetch_array($catquery)){
							$fetchCatType = $fetchcat['categoryType'];
							$fetchCatFee = $fetchcat['categoryFee'];
							if ($fetchCatFee == 0 && $fetchCatType == 'Free'){
								$finalcatFee = ' ';
							}else if($fetchCatFee == 0 && $fetchCatType != 'Free'){
								$finalcatFee = ' - Free';
							}else{
								$finalcatFee = ' - ₱ '.$fetchCatFee;
							}
							?>
							<!-- <optgroup label=""> -->
								<option value="<?php echo $fetchCatType;?>" <?php if($fetchCatType == $catType) echo "selected"; ?> ><?php echo $fetchCatType.' '.$finalcatFee;?> </option>
							<!-- </optgroup> -->
						<?php } ?>
					</select>
				</div>
			</div>
			<div class='row'>
				<div class='input-field col s12 m12 l12'>
					<i class="material-icons prefix yellow-text text-darken-3">edit</i>
		            <input placeholder="Name" class='validate' type='text' name='update_categoryname' id='update_categoryname' value="<?php echo $catName; ?>"required/>
		            <label for='update_categoryname'>Name</label>
	            </div>
	        </div>
	    </div>
		<div class="modal-footer">
			<div class="row center-align">
				<input type="hidden" name="cat_id" id="cat_id" value="<?php echo $catID; ?>">
				<input type="submit" name="submit" value="Confirm" class="btn yellow darken-3">
			</div>
		</div>
		</form>
	</div>
	
	<!-- End of Update Categories -->

	<!-- Archive Category-->
	<div class="modal modal-fixed-footer overflow-modal" class="blocked-modal" id="archive-<?php echo $catID; ?>">
		<div class="modal-content">
			<h5>
				<div class="row">
					<div class="col s2 m1 l1">
						<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
					</div>
					<div class="col s10 m11 l11">
						Archive
					</div>
				</div>
			</h5>
			<div class="divider"></div>
				<div class='row'>
					<center>
						Are you sure to archive this Category?
					<center>
	            </div>
	    </div>
		<div class="modal-footer">
			<div class="row center-align">
				<form action="../../../db.sql/archivecategory.php" method="post">
					<input type="hidden" name="archive_cat_id" id="archive_cat_id" value="<?php echo $catID; ?>">
					<input type="submit" name="submit" value="Confirm" class="btn yellow darken-3">
				</form>
			</div>
		</div>
	</div>
	<?php } ?>
	<!-- End of Archive Category -->

	<!-- Update Types -->
	<div class="modal modal-fixed-footer overflow-modal" class="blocked-modal" id="updatetypes">
		<div class="modal-content">
			<h5>
				<div class="row">
					<div class="col s2 m1 l1">
						<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
					</div>
					<div class="col s10 m11 l11">
						Update Types
					</div>
				</div>
			</h5>
			<div class="divider"></div>
			<div class="col s12 l12">
			    <div class="">
			        <table id="update_types_table">
			            <thead style="text-align: left;">
			                <th style="text-align: left;">Type</th>
			                <th style="text-align: left;">Value</th>
			                <th style="text-align: left;">Action</th>
			         	</thead>
						<tbody>
					
							<?php 
							$catquery= mysqli_query($connect, "SELECT * FROM categories WHERE categoryStatus ='Active' GROUP BY categoryType ORDER BY categoryFee ASC"); 
							while($fetchcat = mysqli_fetch_array($catquery)){
								$fetchCatID = $fetchcat['categoryID'];
								$fetchCatType = $fetchcat['categoryType'];
								$fetchCatFee = $fetchcat['categoryFee'];
								
							if ($fetchCatFee == 0 && $fetchCatType == 'Free'){
								$finalcatFee = ' ';
							}else if($fetchCatFee == 0 && $fetchCatType != 'Free'){
								$finalcatFee = ' - Free';
							}else{
								$finalcatFee = ' - ₱ '.$fetchCatFee;
							}
							?>
								<tr>
									<td><?php echo $fetchCatType; ?></td>
									<td><?php echo $fetchCatFee; ?></td>
									<td><a href="#updatevalue-<?php echo $fetchCatID; ?>" class="modal-trigger">Update</a></td>
								</tr>							
						<?php } ?>
						</tbody>
					</table>
					
				</div>
			</div>
		</div>
			
	</div>
	
	<!-- End of Update Types -->

	<!-- Update Value -->
	<?php 
		$catquery= mysqli_query($connect, "SELECT * FROM categories GROUP BY categoryType ORDER BY categoryFee ASC"); 
		while($fetchcat = mysqli_fetch_array($catquery)){
		$fetchCatID = $fetchcat['categoryID'];
		$fetchCatType = $fetchcat['categoryType'];
		$fetchCatFee = $fetchcat['categoryFee'];
	?>
	<div class="modal modal-fixed-footer overflow-modal" class="blocked-modal" id="updatevalue-<?php echo $fetchCatID; ?>">
		<form action="../../../db.sql/updatecategories.php" method="post">
		<div class="modal-content">
			<h5>
				<div class="row">
					<div class="col s2 m1 l1">
						<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
					</div>
					<div class="col s10 m11 l11">
						Update Value
					</div>
					
				</div>
			</h5>
			<div class="divider"></div>
			<div class="row">
				<div class="col s12 m12 l12">
					<b>Type: </b> <?php echo $fetchCatType; ?>
				</div>
			</div>
			<div class="row">
				<div class='input-field col s12 m12 l12'>
					<i class="material-icons prefix yellow-text text-darken-3">dialpad</i>
					<input placeholder="Value" class='validate' type='number' name='catFee' id='update_type_value' value='<?php echo $fetchCatFee; ?>'required/>
					<input type='hidden' value='<?php echo $fetchCatType;?>' name='catType'>
					<label for='update_type_value'>Type Value</label>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<div class="row center-align">
				<input type="hidden" name="update_cat_id" id="update_cat_id" value="<?php echo $fetchCatID; ?>">
				<input type="submit" name="submit" value="Confirm" class="btn yellow darken-3">
			</div>
		</div>
		</form>
	</div>
		<?php } ?>
	<!-- End of Update Value -->


	
<script src="jquery/jquery-1.8.3.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.material.min.js"></script>

<script>
	$(document).ready(function() {
		$('#business_category_table').DataTable({
			columnDefs: [
				{
					targets: [ 0, 1, 2],
					className: 'mdl-data-table__cell--non-numeric'
				}
			]				
		});
	});
	$(document).ready(function() {
		$('#update_types_table').DataTable({
			columnDefs: [
				{
					targets: [ 0, 1, 2],
					className: 'mdl-data-table__cell--non-numeric'
				}
			]				
		});
	});

</script>

</div>
</div>
<?php require '../../../views/partials/footer.php' ?>
