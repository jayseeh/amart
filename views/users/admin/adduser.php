<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php' ?>

<div class="main dashboard">

<!-- Add User -->
  <center><h3>Add User</h3></center>
  <div class="container">
    <form action="../../../db.sql/insertuserbyadmin.php" class="form-horizontal"  role="form" method="post">

          <div class='row'>
            <div class="col s12 m6 l6">
              <label for="department">Department</label>
                <select name="department" id="department" required>
                  <option value="" disabled selected>Choose Department</option>
                      <option value="Dev Team">Dev Team</option>
                      <option value="Finance">Finance</option>
                      <option value="Legal">Legal</option>
                      <option value="Marketing">Marketing</option>
                      <option value="Operations">Operations</option>
                  </option>
                </select>
            </div>
          </div>
          
          <div class='row'>
                  <div class='input-field col s4 m4 l4'>
            <i class="material-icons prefix yellow-text text-darken-3  hide-on-small-only">person</i>
                    <input class='validate' type='text' name='fname' id='fname' required/>
                    <label for='fname'>First Name</label>
                  </div>
                
                  <div class='input-field col s4 m4 l4'>
            <i class="material-icons prefix yellow-text text-darken-3  hide-on-small-only">person</i>
                    <input class='validate' type='text' name='mname' id='mname' required/>
                    <label for='mname'>Middle Name</label>
                  </div>
            
            <div class='input-field col s4 m4 l4'>
            <i class="material-icons prefix yellow-text text-darken-3  hide-on-small-only">person</i>
                    <input class='validate' type='text' name='lname' id='lname' required/>
                    <label for='lname'>Last Name</label>
                  </div>
                </div>
          
          <div class='row'>
          <div class='input-field col s12 m12 l12'>
            <i class="material-icons prefix yellow-text text-darken-3  hide-on-small-only">phone</i>
                    <input class='validate' type='number' name='pn' id='pn' required/>
                    <label for='pn'>Phone Number</label>
                  </div>
                </div>
          
          <br>
          <center><button type="submit" class='modal-trigger col s12 btn btn-large waves-effect yellow darken-3'>ADD</button>			
    </form>
  </div>
  <!-- End of Add User -->
</div>

<?php require '../../../views/partials/footer.php' ?>
