<?php include "../../../db.sql/connect_db.php"; ?>
<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php' ?>
<link rel="stylesheet" href="../../../css/datatable.css">
<link rel="stylesheet" href="css/material.min.css">
<link rel="stylesheet" href="css/dataTables.material.min.css">

<div class="main">
	<h4 style="margin-left: 10px;">Categories Request</h4>
<div class="row">
	<center>
		<br>
		<div class="col s12 m4 l4">
			<!-- <a href="#addcategories" class="modal-trigger" id="addcat"><button class='btn btn-large waves-effect yellow darken-3'>Add Categories</button></a> -->
		</div>
		<div class="col s12 m4 l4">
			<!-- <a href="categoriesrequest.php" class="modal-trigger" id="reqcat"><button class='btn btn-large waves-effect yellow darken-3'>Categories Request</button></a> -->
		</div>
		<div class="col s12 m4 l4">
			<!-- <a href="#updatetypes" class="modal-trigger" id="updatetype"><button class='btn btn-large waves-effect yellow darken-3' id="updatetype">Update Types</button></a> -->
		</div>
	</center>
	<div class="col s12 l12">
		<div class="">
			<table id="business_category_table">
				<thead style="text-align: left;">
					<th style="text-align: left;">Category</th>
					<th style="text-align: left;">Requester</th>
					<th style="text-align: left;">Date</th>
					<th style="text-align: left;">Action</th>
				</thead>
				<tbody>
					<?php 
						$fetchquery= mysqli_query($connect, "SELECT * FROM categoryrequests 
						INNER JOIN accounts on categoryrequests.catRequesterID  = accounts.aID
						WHERE categoryrequests.catRequestStatus='Pending' ORDER BY categoryrequests.catDateRequested"); 
						while($fetchdata = mysqli_fetch_array($fetchquery)){
						$catID = $fetchdata['catRequestID'];
						$catName = $fetchdata['catRequestText'];
						$catRequester = $fetchdata['catRequesterID'];
						$catDateRequested = $fetchdata['catDateRequested'];
					?>
					<tr>
						<td><?php echo $catName;?></td>
						<td><?php echo $catRequester;?></td>					
						<td><?php echo $catDateRequested;?></td>
						<td><a href="#view-details-<?php echo $catID; ?>" class="modal-trigger">View</a></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>

	<!-- View Details Modal -->
	<?php 
		$fetchquery= mysqli_query($connect, "SELECT * FROM categoryrequests 
		INNER JOIN accounts on categoryrequests.catRequesterID  = accounts.aID
		WHERE categoryrequests.catRequestStatus='Pending' ORDER BY categoryrequests.catDateRequested"); 
		while($fetchdata = mysqli_fetch_array($fetchquery)){
		$catID = $fetchdata['catRequestID'];
		$catName = $fetchdata['catRequestText'];
		$catRequester = $fetchdata['catRequesterID'];
		$catDateRequested = $fetchdata['catDateRequested'];
		$reason = $fetchdata['catRequestReason'];
	?>
		<form action="../../../db.sql/categoryapprove.php" method="POST">
			<div id="view-details-<?php echo $catID; ?>" class="modal modal-fixed-footer">
				<div class="modal-content">
					<h4>
						<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
						Category Details
					</h4>
					<div class="row">
						<div class="col s12">
							<span>Requested by: <b><?php echo $catRequester; ?></b></span>	
						</div>
						<div class="col s12">
							<span>Date Requested: <b><?php echo $catDateRequested; ?></b></span>	
						</div>
						<div class="col s12">
							<span>Reason: <b><?php echo $reason; ?></b></span>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label for="catName">Category Name</label>
							<input type="text" name="catName" class="catName" value="<?php echo $catName ?>">
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label for="catType">Category Type</label>
							<select name="catType" class="catType" required>
								<option value="" disabled selected>Category Type</option>
								<?php 
									$fetchtypequery= mysqli_query($connect, "SELECT * FROM categories GROUP BY categoryType ORDER BY categoryFee ASC"); 
									while($fetchtypes = mysqli_fetch_array($fetchtypequery)){
										$catType = $fetchtypes['categoryType'];
										$catFee = $fetchtypes['categoryFee'];
										if ($catType == 'Free'){
											$finalcatFee = '';
										}else if($catFee == 0 && $catType != 'Free'){
											$finalcatFee = ' - Free';
										}else{
											$finalcatFee = ' - ₱ '.$catFee;
										}
										?>
									<!-- <optgroup label=""> -->
										<option value="<?php echo $catType;?>" data-fee="<?php echo $catFee; ?>"><?php echo $catType.' '.$finalcatFee;?> </option>
									<!-- </optgroup> -->
									<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer modal-footer-ps">
					<a href="#decline-<?php echo $catID;?>" class="modal-trigger btn red darken-3">Decline</a>
					<a href="#approval-<?php echo $catID;?>" class="modal-trigger btn yellow darken-3">Approve</a>
				</div>
			</div>

			<!-- Confirm Approval Modal -->
			<div id="approval-<?php echo $catID;?>" class="modal">
				<div class="modal-content" style="text-align: center;">
					<p><h4>Ready to approve request?</h4></p>
					<p>Please review the request details before approving</p>
				</div>
				<div class="modal-footer">
					<a href="#" class="modal-close btn grey darken-3">Return</a>
					<input type="submit" name="approve" value="Approved" class="btn yellow darken-3">
					<input type="hidden" name="catFee" class="catFee" readonly>
					<input type='hidden' value='<?php echo $catID;?>' name='catID'>		
				</div>
			</div>
		</form>
		
		<!-- Confirm Decline Modal -->
		<form action="../../../db.sql/categorydecline.php" method="POST">
			<div id="decline-<?php echo $catID;?>" class="modal">
				<div class="modal-content overflow-modal">
					<h4>
						<a href="#" class="modal-close"><i class="material-icons yellow-text text-darken-3">arrow_back</i></a>
						Declining Request
					</h4>
					<div class="row">
						<div class="col s12 m8 l8 offset-m2 offset-l2">
						<label for="reason">Reason for Declining</label>
						<select name="reason">
						<?php
						$sqlstatement = "SELECT * FROM subdeclinestatements";
						$querystatement = mysqli_query($connect, $sqlstatement);
						while($fetchstatement = mysqli_fetch_array($querystatement)){
						$statement = $fetchstatement['subdeclineStatement']; ?>
							<option value="<?php echo $statement;?>"><?php echo $statement;?></option>
						<?php } ?>
						</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row center-align">
						<div class="col s8 m8 l8 offset-s2 offset-m2 offset-l2">
							<input type="submit" name="decline" value="Declined" class="btn red darken-3">
							<input type='hidden' value='<?php echo $catID;?>' name='catID'>
						</div>
					</div>
				</div>
			</div>
		</form>
	<?php } ?>
	
<script src="jquery/jquery-1.8.3.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.material.min.js"></script>

<script>
	$(document).ready(function() {
		$('#business_category_table').DataTable({
			columnDefs: [
				{
					targets: [ 0, 1, 2],
					className: 'mdl-data-table__cell--non-numeric'
				}
			]				
		});
	});

	$('.catType').on('change', function(){
            var categoryFee = $(this).find(':selected').attr('data-fee');
            
            $('.catFee').val(categoryFee);
        });

</script>

</div>
</div>
<?php require '../../../views/partials/footer.php' ?>
