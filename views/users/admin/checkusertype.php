<?php

if(isset($_SESSION['sID'])) {
    $aType = $_SESSION['aType'];

    if($aType != "Admin") {
        header("Location:../" . $aType . "/dashboard.php");
    }
}

?>