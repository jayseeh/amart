<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>
<div class="main">
    <div class="container">
        <h3>
            Dashboard
        </h3>
        <div class="row">
            <div class="col s12 m6 l6">
                <div class="card horizontal">
                    <div class="card-image valign-wrapper">
                        <i class="large material-icons">list</i>
                    </div>
                    
                    <div class="card-stacked">
                        <span class="card-title"><h5>Registrations</h5></span>
                        <div class="card-action">
                            <a href="registrations.php">Manage</a> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l6">
                <div class="card horizontal">
                    <div class="card-image valign-wrapper">
                        <i class="large material-icons">subscriptions</i>
                    </div>
                    
                    <div class="card-stacked">
                        <span class="card-title"><h5>Subscriptions</h5></span>
                        <div class="card-action">
                        <a href="subscription.php">View</a>
                        <span class="new badge">1</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l6">
                <div class="card horizontal">
                    <div class="card-image valign-wrapper">
                        <i class="large material-icons">places</i>
                    </div>
                    
                    <div class="card-stacked">
                        <span class="card-title"><h5>Tourist Spot</h5></span>
                        <div class="card-action">
                        <a href="touristspot.php">View</a>
                        <span class="new badge">1</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col s12 m6 l6">
                <div class="card horizontal">
                    <div class="card-image valign-wrapper">
                        <i class="large material-icons">map</i>
                    </div>
                    <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a>
                    <div class="card-stacked">
                        <span class="card-title"><h5>Map</h5></span>
                        <div class="card-action">
                        <a href="map.php">View</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>
<?php require '../../partials/footer.php'; ?>
