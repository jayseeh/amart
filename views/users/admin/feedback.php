<?php include "../../../db.sql/connect_db.php"; ?>
<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>

    <div class="main">
        <div class="container">
            <h4>Feedback</h4>
                <table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Feedback</th>
                        </tr>
                    </thead>
                    
                    <?php
                        $query = mysqli_query($connect, "SELECT * FROM feedbacks inner join accounts on feedbacks.userID=accounts.aID ORDER BY feedbackID DESC");
                        while($fetchdata = mysqli_fetch_array($query)){
                            $feedbackBy = $fetchdata['fname'].' '.$fetchdata['mname'].' '.$fetchdata['lname'];
                    ?>
                    <tr>
                        <td><a href="dashboard_business.php?bname=Business Name" class="black-text"><?php echo $feedbackBy; ?></a></td>
                        <td><a href="#details-<?php echo $fetchdata['feedbackID']; ?>" class="modal-trigger yellow-text text-darken-3"><?php echo $fetchdata['feedbackContent'];?></a></td>
                    </tr>

                    <div class="modal" id="details-<?php echo $fetchdata['feedbackID']; ?>">
                        <div class="modal-content">
                            <h4>
                            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                                Feedback Details
                            </h4>
                            <p>
                                <label for="date">Feedback Date: </label><span class="profile-details" id="date"><?php echo $fetchdata['feedbackDate']; ?></span>
                                <br>
                                <label for="by">Sent By: </label><span class="profile-details" id="businessname"><?php echo $feedbackBy; ?></span>
                                <br>
                                <label for="content">Content</label><span class="profile-details" id="content"><?php echo $fetchdata['feedbackContent']; ?></span>
                                <br>
                            </p>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                </table>
        </div>
    </div>

<?php require '../../partials/footer.php'; ?>