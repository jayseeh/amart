<?php
//session_start();
include "../../../db.sql/connect_db.php";
include "checkusertype.php";

if(!isset($_SESSION['sID'])) {
    header("Location:../../../index.php");
	exit;
}
?>
<header>
  <nav class="grey darken-3" style="text-decoration: none;">
      <a href="#" class="brand-logo center hide-on-med-and-up" style="text-decoration: none;"><span class="yellow-text text-darken-3" style="text-decoration: none;">A</span><small style="text-decoration: none;">MART</small>&nbsp;<span class="yellow-text text-darken-3" style="text-decoration: none;">D</span><small style="text-decoration: none;">IGITAL</small></a> 
	  <a href="#" data-target="slide-out" class="sidenav-trigger" style="text-decoration: none;"><i class="material-icons  yellow-text text-darken-2">menu</i></a> 
  </nav>
	<style>
	a.disabled {
    pointer-events: none;
}
	</style>
</header>
	<ul id="slide-out" class="sidenav sidenav-fixed grey darken-2">
    <nav class="grey darken-3 hide-on-med-and-down">
      <a href="dashboard.php" class="brand-logo center" style="text-decoration: none;"><span class="yellow-text text-darken-3" style="text-decoration: none;">A</span><small style="text-decoration: none;">MART</small>&nbsp;<span class="yellow-text text-darken-3" style="text-decoration: none;">D</span><small style="text-decoration: none;">IGITAL</small></a>
    </nav>

<!-- Profile -->
	<li>
		<?php 
			$ID = $_SESSION['userID'];
			$fetchquery= mysqli_query($connect, "SELECT * FROM accounts WHERE aID = '$ID'"); 
			while($fetchdata = mysqli_fetch_array($fetchquery)){
			?>
		<div class="user-view">
				<div class="background">
				<img src="../../../assets/images/bg.png">
				</div>
				<a href="profile.php"><img class="circle" src="../../../assets/images/<?php echo $fetchdata['uPic']?>"></a>
				<span class="white-text name"><?php 
				$fname = $fetchdata['fname'];
				$mname = $fetchdata['mname'];
				$minitial = substr($mname,0, 1);
				$lname = $fetchdata['lname'];
				echo $fullname = $fname.' '.$minitial.'. '.$lname;
				?><a href="../../../db.sql/logout.php" class="btn btn-flat right transparent" id="options"><i class="material-icons yellow-text text-darken-3">exit_to_app</i>
				<span style="position: absolute; bottom: -10px; right: 10px;" class="logout-label yellow-text text-darken-3"><small>Logout</small></span></a></span>
		</div>
		<?php } ?>
	</li>

	
	<?php 
	include "../../../db.sql/connect_db.php";
	$id =$_SESSION['userID'];
	$query = mysqli_query($connect,
	"SELECT * from accounts where accounts.aID= '$id'");
	$count = mysqli_num_rows($query);
	while ($fetchdata = mysqli_fetch_array($query))  {?>
  

    <li>
      <ul class="collapsible" data-collapsible="accordion">
        <a href="dashboard.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3"><i class="material-icons dashboard yellow-text text-darken-3">dashboard</i>Dashboard</a>
		<a href="subscription.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3
			<?php $checkstatus = $fetchdata['aStatus']; 
			if ( $checkstatus == 'Confirmed'){ echo ' '; }
			else{ echo 'enabled';} ?>">
			<i class="material-icons chevron yellow-text text-darken-3">subscriptions</i>Subscription<span class="badge new red" data-badge-caption="" >10</span>
		</a>
	  </ul>
	  <ul>
		<a href="adduser.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3 
			<?php $checkstatus = $fetchdata['aStatus']; 
			if ( $checkstatus == 'Confirmed'){ echo ' '; }
			else{ echo 'enabled';} ?>">
			<i class="material-icons add yellow-text text-darken-3">people</i>Add User
		</a>
		<a href="addcategories.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3 
			<?php $checkstatus = $fetchdata['aStatus']; 
			if ( $checkstatus == 'Confirmed'){ echo ' '; }
			else{ echo 'enabled';} ?>">
			<i class="material-icons add yellow-text text-darken-3">list</i>Add Categories
		</a>
		<a href="feedback.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3
			<?php $checkstatus = $fetchdata['aStatus']; 
			if ( $checkstatus == 'Confirmed'){ echo ' '; }
			else{ echo 'enabled';} ?>">
			<i class="material-icons yellow-text text-darken-3">feedback</i>Feedback
		</a>
		
		<a href="report.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3
			<?php $checkstatus = $fetchdata['aStatus']; 
			if ( $checkstatus == 'Confirmed'){ echo ' '; }
			else{ echo 'enabled';} ?>">
			<i class="material-icons yellow-text text-darken-3">feedback</i>Report
		</a>
		</ul>
		<ul class="collapsible" data-collapsible="accordion">
		<a href="touristspot.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3
			<?php $checkstatus = $fetchdata['aStatus']; 
			if ( $checkstatus == 'Confirmed'){ echo ' '; }
			else{ echo 'enabled';} ?>">
			<i class="material-icons yellow-text text-darken-3">place</i>Tourist Spot<span class="badge new red" data-badge-caption="" >10</span>
		</a>
			  
		</ul>
	</li>
  </ul>
<?php } ?>

<script>
	var is_mobile = 0;
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	// some code..
		is_mobile = 1;
	} else {
		is_mobile = 0;
	}

	$(document).ready(function(){
		if(!is_mobile){
			$('.logout-label').css('display', 'none');
		}
		
		$("#options").mouseenter(function () {
			$(".logout-label").show();
		});

		$("#options").mouseleave(function () {
			$(".logout-label").hide();
		});
	});
</script>