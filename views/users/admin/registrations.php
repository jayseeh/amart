<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>
<div class="main">
    <div class="container">
        <h3>
            Registrations
        </h3>        
        <table id="table_id" class="display responsive-table striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Owner</th>
                    <th>Address</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Lorma Colleges</td>
                    <td>Rufino Macagba Jr.</td>
                    <td>Carlatan, City of San Fernando, La Union</td>
                    <td>School</td>
                    <td class="red-text">Inactive</td>
                    <td>
                        <div class="row">
                            <span class="col m4 l4"><a href="#"><i class="material-icons">search</i></a></span>
                            <span class="col m4 l4"><a href="#"><i class="material-icons">edit</i></a></span>
                            <span class="col m4 l4"><a href="#"><i class="material-icons">delete</i></a></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Sunset Bay Beach Resort</td>
                    <td>John Doe</td>
                    <td>San Francisco, City of San Fernando, La Union</td>
                    <td>Resort, Restaurant</td>
                    <td class="green-text">Active</td>
                    <td>
                        <div class="row">
                            <span class="col m4 l4"><a href="#"><i class="material-icons">search</i></a></span>
                            <span class="col m4 l4"><a href="#"><i class="material-icons">edit</i></a></span>
                            <span class="col m4 l4"><a href="#"><i class="material-icons">delete</i></a></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Jorisan Resort</td>
                    <td>Jane Doe</td>
                    <td>San Vicente, City of San Fernando, La Union</td>
                    <td>Resort, Restaurant</td>
                    <td class="yellow-text text-darken-3">Pending</td>
                    <td>
                        <div class="row">
                            <span class="col m4 l4"><a href="#"><i class="material-icons">search</i></a></span>
                            <span class="col m4 l4"><a href="#"><i class="material-icons">edit</i></a></span>
                            <span class="col m4 l4"><a href="#"><i class="material-icons">delete</i></a></span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php require '../../partials/footer.php'; ?>
