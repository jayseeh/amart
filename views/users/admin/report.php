<?php include "../../../db.sql/connect_db.php"; ?>
<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>

    <div class="main">
        <div class="container">
            <h4>Report</h4>
                <table>
                    <thead>
                        <tr>
                            <th>Business Name</th>
                            <th>Reporter</th>
                            <th>Reason</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    
                    <?php
                        $query1 = mysqli_query($connect, "SELECT * FROM reports inner join accounts on reports.userID=accounts.aID inner join business on reports.bisnesID=business.businessID ORDER BY rID DESC");
                        while($fetchdata = mysqli_fetch_array($query1)){
                    ?>
                    <tr>
                        <td><?php echo $fetchdata['businessName']; ?></a></td>
                        <td><?php echo $fetchdata['fname'];?></a></td>
                        <td class="content-description"><?php echo $fetchdata['reportContent'];?></a></td>
                        <td><a href="#details-<?php echo $fetchdata['rID']; ?>" class="modal-trigger yellow-text text-darken-3">Action</a></td>
                    </tr>
                    
                    <div class="modal" id="details-<?php echo $fetchdata['rID']; ?>">
                        <div class="modal-content">
                            <h4>
                            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                                Report Details
                            </h4>
                            <p>
                                <label for="date">Reported Date: </label><span class="profile-details" id="date"><?php echo $fetchdata['reportDate']; ?></span>
                                <br>
                                <label for="businessname">Reported: </label><span class="profile-details" id="businessname"><?php echo $fetchdata['businessName']; ?></span>                                
                                <br>
                                <label for="by">Reported By: </label><span class="profile-details" id="businessname"><?php echo $fetchdata['fname']; ?></span>
                                <br>
                                <label for="content">Content</label><span class="profile-details content-description" id="content"><?php echo $fetchdata['reportContent']; ?></span>
                                <br>
                            </p>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col s12 l6">
                                        <a href="" class="btn btn-flat red darken-3 white-text col s12">Block</a>
                                    </div>
                                    <div class="col s12 l6">
                                        <a href="" class="btn btn-flat green darken-3 white-text col s12">Decline</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                </table>
        </div>
        <div class="modal" id="edit">
            <div class="modal-content">
                <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    Feedback
                </h4>
                <form action="#" method="post">
                    <input type="text" placeholder="Note" value="I'll buy here someday">
                    <div class="section"></div>
                    <center>
                        <input type="submit" value="Submit" class="btn btn-flat yellow darken-3">
                    </center>
                </form>
            </div>
        </div>
    </div>

<?php require '../../partials/footer.php'; ?>