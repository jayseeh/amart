<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>
<div class="main">
    <div class="container">
        <h3>
            Requests
        </h3>
        <table id="table_id" class="display responsive-table striped">
            <thead>
                <tr>
                    <th>Company</th>
                    <th>Owner</th>
                    <th>Reference Number</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Jorisan Resort</td>
                    <td>Jane Doe</td>
                    <td class="green-text">0081923812</td>
                    <td>
                        <div class="row">
                            <span class="col m6 l6"><a href="#"><i class="material-icons">search</i></a></span>
                            <span class="col m6 l6"><a href="#"><i class="material-icons">delete</i></a></span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php require '../../partials/footer.php'; ?>
