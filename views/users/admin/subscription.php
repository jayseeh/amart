<?php include "../../../db.sql/connect_db.php"; ?>
<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php' ?>

<link rel="stylesheet" href="../../../css/datatable.css">
<link rel="stylesheet" href="css/material.min.css">
<link rel="stylesheet" href="css/dataTables.material.min.css">
<div class="main">
    <!-- <div class="container"> -->
		  <div class="row">
			<div class="col s12 l12">
                <h4 class="center-align">SUBCRIPTIONS</h4>
                <div class="">
               <table id="example">
                   <thead style="text-align: left;">
                   		<th style="text-align: left;">ID</th>
                        <th style="text-align: left;">Business Name</th>
                        <th style="text-align: left;">Owner</th>
                        <th style="text-align: left;">Date</th>
                        <th style="text-align: left;">Status</th>
                        <th style="text-align: left;">Action</th>
                    </thead>
					<tbody>
					<?php
						$sql = "SELECT * FROM subscription 
								INNER JOIN business 
								ON subscription.bisSubIDkey = business.businessID 
								INNER JOIN categories 
								ON business.businessCategory = categories.categoryID
								INNER JOIN accounts
								ON business.businessOwner = accounts.aID
								ORDER BY subscriptionID DESC, subStatus DESC ";
						
						$query = mysqli_query($connect, $sql);
                        while($fetchdata = mysqli_fetch_array($query)){
							$subID = $fetchdata['subscriptionID'];
							$businessName = $fetchdata['businessName'];
							$businessOwner = $fetchdata['fname'] . ' ' . $fetchdata['mname'] . ' ' . $fetchdata['lname'];
							$datePaid = $fetchdata['subDatePaid'];
							$subStatus = $fetchdata['subStatus'];

							if($subStatus == 'Pending'){
								$status = "<span class='yellow-text text-darken-3'>$subStatus</span>";
							} elseif($subStatus == 'Approved'){
								$status = "<span class='green-text text-darken-3'>$subStatus</span>";
							} else {
								$status = "<span class='red-text text-darken-3'>$subStatus</span>";
							}
					?>
						<tr>
							<td><?php echo $subID; ?></td>
							<td><?php echo $businessName; ?></td>
							<td><?php echo $businessOwner; ?></td>
							<td><?php echo $datePaid; ?></td>
                            <td><?php echo $status; ?></td>
                            <td><a href="#view-details-<?php echo $subID; ?>" class="modal-trigger">View</a></td>
						</tr>
					<?php
					}
					?>
						
					</tbody>
                </table>
				</div>
            </div>
			<!-- Modals  -->
				<?php
					$sql = "SELECT DISTINCT 
						subscriptionID, subNumOfMonth, businessID,
						businessName, subRefPhoto,
						fname, mname, lname, businessID,
						subDatePaid, subStatus, subAmount,
						subDateEnd, subNewCategory
						FROM subscription 
							INNER JOIN business 
							ON subscription.bisSubIDkey = business.businessID 
							INNER JOIN categories 
							ON business.businessCategory = categories.categoryID
							INNER JOIN accounts
							ON business.businessOwner = accounts.aID
							ORDER BY subscriptionID desc ";

					$query = mysqli_query($connect, $sql);
					while($fetchdata = mysqli_fetch_array($query)){
					$subID = $fetchdata['subscriptionID'];
					$businessName = $fetchdata['businessName'];
					$businessOwner = $fetchdata['fname'] . ' ' . $fetchdata['mname'] . ' ' . $fetchdata['lname'];
					$datePaid = $fetchdata['subDatePaid'];
					$subStatus = $fetchdata['subStatus'];
					$subAmount = $fetchdata['subAmount'];
					$duration = $fetchdata['subNumOfMonth'];
					$receipt = $fetchdata['subRefPhoto'];
					$businessID = $fetchdata['businessID'];
					$subDateEnd = $fetchdata['subDateEnd'];
					$newCategory = $fetchdata['subNewCategory'];

					if($subStatus == 'Pending'){
						$status = "<span class='yellow-text text-darken-3'>$subStatus</span>";
					} elseif($subStatus == 'Active'){
						$status = "<span class='green-text text-darken-3'>$subStatus</span>";
					} else {
						$status = "<span class='red-text text-darken-3'>$subStatus</span>";
					}
			?>
			<!-- View Details Modal -->
			<form action="../../../db.sql/subscriptionapprove.php" method="POST">
				<div id="view-details-<?php echo $subID; ?>" class="modal modal-fixed-footer">
					<div class="modal-content">
						<h4>
							<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
							Subscription Details
						</h4>
						<div class="row">
							<div class="col s12 m6 l6">
								<img src="../../../assets/refPics/<?php echo $receipt; ?>"  class="col s12 m12 l12 materialboxed" alt="receipt">
							</div>
							<div class="col s12 m6 l6">
								<small>Business Name:</small><br> <strong><?php echo $businessName; ?></strong><br>
								<small>Owner:</small><br> <strong><?php echo $businessOwner; ?></strong><br>
								<small>Date Paid:</small><br> <strong><?php echo $datePaid; ?></strong><br>
								<small>Duration(Months):</small><br> <strong><input type="number" name="duration" id="duration" value="<?php echo $duration; ?>"></strong><br>
								<small>Amount Paid:</small><br> <strong><?php echo $subAmount; ?></strong><br>
							</div>
						</div>
					</div>
					<div class="modal-footer modal-footer-ps">
						<a href="#decline-<?php echo $subID;?>" class="modal-trigger btn red darken-3">Decline</a>
						<a href="#approval-<?php echo $subID;?>" class="modal-trigger btn yellow darken-3">Approve</a>
					</div>
				</div>

				<!-- Confirm Approval Modal -->
				<div id="approval-<?php echo $subID;?>" class="modal">
					<div class="modal-content" style="text-align: center;">
						<p><h4>Ready to approve request?</h4></p>
						<p>Please review the request details before approving</p>
					</div>
					<div class="modal-footer">
						<a href="#" class="modal-close btn grey darken-3">Return</a>
						<input type="submit" name="approve" value="Approved" class="btn yellow darken-3">
						<input type='hidden' value='<?php echo $subID;?>' name='subID'>		
						<input type='hidden' value='<?php echo $newCategory;?>' name='newcategory'>		
						<input type='hidden' value='<?php echo $businessID;?>' name='businessID'>	
						<input type='hidden' value='<?php echo $subDateEnd;?>' name='subDateEnd'>	
					</div>
				</div>
			</form>
			<!-- Confirm Decline Modal -->
			<form action="../../../db.sql/subscriptiondecline.php" method="POST">
			<div id="decline-<?php echo $subID;?>" class="modal">
				<div class="modal-content overflow-modal">
					<h4>
						<a href="#" class="modal-close"><i class="material-icons yellow-text text-darken-3">arrow_back</i></a>
						Declining Request
					</h4>
					<div class="row">
						<div class="col s12 m8 l8 offset-m2 offset-l2">
						<label for="reason">Reason for Declining</label>
						<select name="reason">
						<?php
						$sqlstatement = "SELECT * FROM subdeclinestatements";
						$querystatement = mysqli_query($connect, $sqlstatement);
						while($fetchstatement = mysqli_fetch_array($querystatement)){
						$statement = $fetchstatement['subdeclineStatement']; ?>
							<option value="<?php echo $statement;?>"><?php echo $statement;?></option>
						<?php } ?>
						</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row center-align">
						<div class="col s8 m8 l8 offset-s2 offset-m2 offset-l2">
							<input type="submit" name="decline" value="Declined" class="btn red darken-3">
							<input type='hidden' value='<?php echo $subID;?>' name='subID' id='subID'>
						</div>
					</div>
				</div>
			</div>
			</form>
			<?php
				}
			?>
			<script>
            $(document).ready(function() {
                $('#example').DataTable({
					"columnDefs": [ 
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }],
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        order : [ 4, 'desc' ],
        bAutoWidth: false,
        resposive: true
            });
});
        </script>
			
        </div>
    </div>

<script src="jquery/jquery-1.8.3.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.material.min.js"></script>

<?php require '../../../views/partials/footer.php' ?>