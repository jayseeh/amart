<?php include "../../../db.sql/connect_db.php"; ?>
<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php' ?>
<link rel="stylesheet" href="../../../css/datatable.css">
<link rel="stylesheet" href="css/material.min.css">
<link rel="stylesheet" href="css/dataTables.material.min.css">
<div class="main">
    <!-- <div class="container"> -->
		  <div class="row">
			<div class="col s12 l12">
                <h4 class="center-align">Tourist Spot</h4>
                <div class="">
               <table id="touristspot">
                   <thead style="text-align: left;">
                   		<th style="text-align: left;">ID</th>
                        <th style="text-align: left;">Tourist Spot</th>
                        <th style="text-align: left;">Location</th>
                        <th style="text-align: left;">Status</th>
                        <th style="text-align: left;">Action</th>
                    </thead>
					<tbody>
					
					<?php
				     	
					   $query=mysqli_query($connect, "SELECT * FROM tourspot INNER JOIN accounts on tourspot.tourspotUploader=accounts.aID WHERE  tourspotStatus ='Pending' ORDER BY tourspotID");
					// $query = mysqli_query($connect, "SELECT * FROM subscription inner join business on subscription.bisSubIDkey=business.businessID inner join categories on business.businessCategory=categories.categoryID ORDER BY subscriptionID DESC");
                        while($fetchdata = mysqli_fetch_array($query)){
							$address_arr = explode(" | ", $fetchdata['fullAddress']);
							$filtered_arr = array();

							foreach($address_arr as $val){
								if($val != ""){
									array_push($filtered_arr, $val);
								}
							}
							
							$addresscount = count($filtered_arr);
							$fulladdress = implode(", ", $filtered_arr);
							// echo $addresscount;
					?>
						<tr>
							<td><?php echo $fetchdata['tourspotID']; ?></td>
							<td><?php echo $fetchdata['tourSpotTitle']; ?></td>
							<td><?php echo $fulladdress; ?></td>
                            <td><span class="green-text"><?php echo $fetchdata['tourspotStatus']; ?></span></td>
                            <td><a href="#tourspotview-<?php echo $fetchdata['tourspotID']; ?>" class="modal-trigger">View</a></td>
						</tr>
						<!-- View Tour Spot Modal -->
						<div class="modal modal-fixed-footer overflow-modal" id="tourspotview-<?php echo $fetchdata['tourspotID']; ?>">
							<div class="modal-content">
								<h4>
									<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
									Tourist Spot Details
								</h4>
								<div class="row">
									<div class="col s12 m6 l6">
										<img src="../../../assets/tourPics/<?php echo $fetchdata['tourspotPic']; ?>" class="col s12 m12 l12" alt="tourspot">
									</div>
									<div class="col s12 m6 l6">
										<small>Name:</small><br> <strong>
											<?php echo $fetchdata['tourSpotTitle']; ?></strong><br>
										<small>Description:</small><br> <strong>
											<?php echo $fetchdata['tourspotComment']; ?></strong><br>
										<small>Location:</small><br> <strong>
											<?php echo $fulladdress; ?></strong><br>
										<small>Uploader:</small><br> <strong>
											<?php echo $fetchdata['fname']; ?></strong><br>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<div class="row center-align">
									<div class="col s6 m6 l6">
										<a href="#decline-<?php echo $fetchdata['tourspotID']; ?>" class="modal-trigger btn red darken-3 col s12 m12 l12">Decline</a>
									</div>
									<div class="col s6 m6 l6">
										<a href="#accept-<?php echo $fetchdata['tourspotID']; ?>" class="modal-trigger btn yellow darken-3  col s12 m12 l12">Accept</a>
									</div>
								</div>
							</div>
						</div>
						<!-- Decline Modal -->
						<div class="modal modal-fixed-footer overflow-modal" class="blocked-modal" id="decline-<?php echo $fetchdata['tourspotID']; ?>">
							<div class="modal-content">
								<h5>
									<div class="row">
										<div class="col s2 m1 l1">
											<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
										</div>
										<div class="col s10 m11 l11">
											Declining Request
										</div>
									</div>
								</h5>
								<div class="divider"></div>
								<p>Are you sure you want to decline this request?</p>
							</div>
							<div class="modal-footer">
								<div class="row center-align">
									<form action="../../../db.sql/updatetourspotrequest.php" method="post">
										<input type="hidden" name="tourspotid" id="tourspotid" value="<?php echo $fetchdata['tourspotID']; ?>">
										<input type="hidden" name="tourspotaction" id="tourspotaction" value="Declined">
										<input type="submit" name="submit" value="Confirm" class="btn yellow darken-3">
									</form>
								</div>
							</div>
						</div>
						<!-- End of Decline modal -->
						
						<!-- Accept Modal -->
						<div class="modal modal-fixed-footer overflow-modal" class="blocked-modal" id="accept-<?php echo $fetchdata['tourspotID']; ?>">
							<div class="modal-content">
								<h5>
									<div class="row">
										<div class="col s2 m1 l1">
											<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
										</div>
										<div class="col s10 m11 l11">
											Accepting Request
										</div>
									</div>
								</h5>
								<div class="divider"></div>
								<p>Are you sure you want to Accept this request?</p>
							</div>
							<div class="modal-footer">
								<div class="row center-align">
									<form action="../../../db.sql/updatetourspotrequest.php" method="post">
										<input type="hidden" name="tourspotid" id="tourspotid" value="<?php echo $fetchdata['tourspotID']; ?>">
										<input type="hidden" name="tourspotaction" id="tourspotaction" value="Confirmed">
										<input type="submit" name="submit" value="Confirm" class="btn yellow darken-3">
									</form>
								</div>
							</div>
						</div>
						<!-- End of Accept modal -->
					<?php
					}
					?>
						
					</tbody>
                </table>
				</div>
            </div>
        </div>
    <!-- </div> -->

	<!-- End of Tour Spot Modal -->
<script src="jquery/jquery-1.8.3.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.material.min.js"></script>

<script>
	$(document).ready(function() {
		$('#touristspot').DataTable({
			"columnDefs": [ 
		            {
		                "targets": [ 0 ],
		                "visible": false,
		                "searchable": false
		            }],
		        "oLanguage": {
		            "sStripClasses": "",
		            "sSearch": "",
		            "sSearchPlaceholder": "Enter Keywords Here",
		            "sInfo": "_START_ -_END_ of _TOTAL_",
		            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
		            '<option value="10">10</option>' +
		            '<option value="20">20</option>' +
		            '<option value="30">30</option>' +
		            '<option value="40">40</option>' +
		            '<option value="50">50</option>' +
		            '<option value="-1">All</option>' +
		            '</select></div>'
		        },
		        order : [ 0, 'desc' ],
		        bAutoWidth: false,
		        resposive: true
		            });
				});			
</script>

<?php require '../../../views/partials/footer.php' ?>