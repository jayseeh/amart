<!-- Confirm Deletion Business Modal -->
	<div id="delete" class="modal">
		<div class="modal-content" style="float: center; text-align: center;">
			<h4>Confirm to delete?</h4>
		<div class='row'>
			<div class='col s6 m6'>
				<a href="#!" class="modal-close col s12 btn btn-large waves-effect grey darken-2">Cancel</a>
			</div>
			<div class='col s6 m6'>
				<a href="#deletebusiness" type="submit" name='deleteconfirm' class='modal-trigger col s12 btn btn-large waves-effect grey darken-2'>Delete</a></p>
			</div>
		</div>
		</div>
	</div>

<!-- Delete Business Modal -->
	<div id="deletebusiness" class="modal">
		<div class="modal-content" style="float: center; text-align: center;">
			<p><i class="material-icons col s12 offline_pin black-text text-darken-3" style="font-size: 250px;">offline_pin</i></p>
			<p><h5>Business Deleted!</h5></p>
			<p><a href="registrations.php"><button type="submit" class='modal-trigger col s12 btn btn-large waves-effect grey darken-2'>Proceed</button></a></p>
		</div>
	</div>
				