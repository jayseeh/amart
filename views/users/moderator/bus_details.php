		<!-- Business Details Modal -->
			<div id="details" class="modal">
				<div class="modal-content" style="float: left; text-align: left;">
					<h2>Business Name</h2><hr>
					<h4>Business Details</h4>
				<div class='row'>
					<div class='col s6 m6'>
						Business Name:
					</div>
					<div class='col s6 m6'>
						XYZ
					</div>
					<div class='col s6 m6'>
						Business Owner:
					</div>
					<div class='col s6 m6'>
						XYZ
					</div>
					<div class='col s6 m6'>
						Business Contacts:
					</div>
					<div class='col s6 m6'>
						Mobile: (090) *****/ Tel:(070) ******
					</div>
					<div class='col s6 m6'>
						Business Address:
					</div>
					<div class='col s6 m6'>
						XYZ
					</div>
					<div class='col s6 m6'>
						Business Type:
					</div>
					<div class='col s6 m6'>
						XYZ
					</div>
					<div class='col s6 m6'>
						Business Status:
					</div>
					<div class='col s6 m6'>
						XYZ
					</div>
					<div class='col s6 m6'>
						Business Other Infos:
					</div>
					<div class='col s6 m6'>
						XYZ
					</div>
				</div>
				</div>
			</div>