<!-- Edit Business Modal -->
	<div id="edit" class="modal">
		<div class="modal-content" style="float: center; text-align: center;">
			<h4>Update Business Details</h4>
		 <form class="col s12" action="dashboard.php" method="post">
            <div class='row'>
              <div class='input-field col s12'>
				<i class="material-icons prefix yellow-text text-darken-3">business</i>
                <input class='validate' type='text' name='bname' id='bname'>
                <label for='bname'>Business Name</label>
              </div>
            </div>

            <div class='row'>
                <div class='input-field col s12'>
					<i class="material-icons prefix yellow-text text-darken-3">person</i>
                    <input class='validate' type='text' name='bowner' id='bowner'>
                    <label for='bowner'>Business Owner</label>
                </div>
            </div>
			
			<div class='row'>
                <div class='input-field col s12'>
					<i class="material-icons prefix yellow-text text-darken-3">contact_phone</i>
                    <input class='validate' type='text' name='bcontacts' id='bcontacts'>
                    <label for='bcontacts'>Mobile Number</label>
                </div>
				<div class='input-field col s12'>
					<i class="material-icons prefix yellow-text text-darken-3">phone</i>
                    <input class='validate' type='text' name='bcontacts' id='bcontacts'>
                    <label for='bcontacts'>Telephone Number</label>
                </div>
            </div>
			
			<div class='row'>
                <div class='input-field col s12'>
					<i class="material-icons prefix yellow-text text-darken-3">address</i>
                    <input class='validate' type='text' name='baddress' id='baddress'>
                    <label for='baddress'>Business Address</label>
                </div>
            </div>
			
			<div class='row'>
                <div class='input-field col s12'>
					<i class="material-icons prefix yellow-text text-darken-3">location_city</i>
                    <input class='validate' type='text' name='btype' id='btype'>
                    <label for='btype'>Business Type</label>
                </div>
            </div>
			
			<div class='row'>
                <div class='input-field col s12'>
					<i class="material-icons prefix yellow-text text-darken-3">flag</i>
                    <input class='validate' type='text' name='bstatus' id='bstatus'>
                    <label for='bstatus'>Business Status</label>
                </div>
            </div>
			
			<div class='row'>
				<div class='col s6 m6'>
					Business Other Infos:
				</div>
				<div class='col s6 m6'>
					XYZ
				</div>
			</div>

            <br/>
            <center>
				<div class='row'>
					<div class='col s6 m6'>
						<a href="#!" class="modal-close col s12 btn btn-large waves-effect grey darken-2">Cancel</a>
					</div>
					<div class='col s6 m6'>
						<a href="#addbusiness" name='addconfirm' class='modal-trigger col s12 btn btn-large waves-effect grey darken-2'>Update</a></p>
					</div>
				</div>
			</center>
			
          </form>
		</center>	
</div>
</div>
