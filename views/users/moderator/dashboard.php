<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col s12 m6 l6">
                <div class="card horizontal">
                    <div class="card-image valign-wrapper">
                        <i class="large material-icons">store</i>
                    </div>
                    <!-- <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a> -->
                    <div class="card-stacked">
                        <span class="card-title"><h5>Business Accounts</h5></span>
                        <div class="card-action">
                            <a href="registrations.php">Manage</a> 
							<span class="badge" style="background-color: orange; border-radius: 2px; color: black;">3 users</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l6">
                <div class="card horizontal">
                    <div class="card-image valign-wrapper">
                        <i class="large material-icons">warning</i>
                    </div>
                    <!-- <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a> -->
                    <div class="card-stacked">
                        <span class="card-title"><h5>Pending Requests</h5></span>
                        <div class="card-action">
                        <a href="requests.php">View</a>
                        <span class="new badge">1</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
			  <div class="col s12 m6 l6">
                <div class="card horizontal">
                    <div class="card-image valign-wrapper">
                        <i class="large material-icons">verified_user</i>
                    </div>
                    <!-- <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a> -->
                    <div class="card-stacked">
                        <span class="card-title"><h5>Registered Users</h5></span>
                        <div class="card-action">
                        <a href="reg_users.php">View</a>
                        <span class="badge" style="background-color: orange; border-radius: 2px; color: black;">3 users</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l6">
                <div class="card horizontal">
                    <div class="card-image valign-wrapper">
                        <i class="large material-icons">archive</i>
                    </div>
                    <!-- <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a> -->
                    <div class="card-stacked">
                        <span class="card-title"><h5>Archive</h5></span>
                        <div class="card-action">
                        <a href="archive.php">View</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
			  <div class="col s12 m6 l6">
                <div class="card horizontal">
                    <div class="card-image valign-wrapper">
                        <i class="large material-icons">low_priority</i>
                    </div>
                    <!-- <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a> -->
                    <div class="card-stacked">
                        <span class="card-title"><h5>Declined Requests</h5></span>
                        <div class="card-action">
                        <a href="dec_req.php">View</a>
                        <span class="badge" style="background-color: orange; border-radius: 2px; color: black;">3 users</span>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </div>
</div>
<?php require '../../partials/footer.php'; ?>
