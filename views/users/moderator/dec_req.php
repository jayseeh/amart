<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>
<?php include 'req_details.php'; ?>
<?php include 'req_remarks.php'; ?>
<div class="main">
    <div class="container">
        <h3>
            Declined Requests
        </h3>
        <table id="table_id" class="display responsive-table striped">
            <thead>
                <tr>
                    <th data-field="company" style="float: center; text-align: center;">Company</th>
                    <th data-field="owner" style="float: center; text-align: center;">Owner</th>
                    <th data-field="rn" style="float: center; text-align: center;">Contact Number</th>
					<th data-field="info" style="float: center; text-align: center;">Action</th>
                    <th data-field="remarks" style="float: center; text-align: center;">Remarks</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Jorisan Resort</td>
                    <td>Jane Doe</td>
                    <td class="green-text">0081923812</td>
                    <td style="float: center; text-align: center;"><a href="#details" name="details" class="modal-trigger"><button class='col s12 btn btn-small waves-effect orange darken-2'>Details</button></a></td>
					<td style="float: center; text-align: center;">
						<a href="#remarks" name="remarks" class="modal-trigger"><button class='col s12 btn btn-small waves-effect red darken-2'>Remarks</button><a>
					</td>
				</tr>
            </tbody>
        </table>
    </div>
</div>
<?php require '../../partials/footer.php'; ?>
