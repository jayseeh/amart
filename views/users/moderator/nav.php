<header>
  <nav class="grey darken-3">
      <a href="#" class="brand-logo center hide-on-med-and-up"><span class="yellow-text text-darken-3">A</span><small>MART</small></a> 
	  <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons  yellow-text text-darken-2">menu</i></a> 
  </nav>
</header>

  <ul id="slide-out" class="sidenav sidenav-fixed grey darken-2">
    <nav class="grey darken-3 hide-on-med-and-down">
      <a href="#" class="brand-logo center"><span class="yellow-text text-darken-3">A</span><small>MART</small></a>
    </nav>

    <li>
      <ul class="collapsible" data-collapsible="accordion">
        <a href="dashboard.php" class="collapsible-header yellow-text text-darken-3"><i class="material-icons dashboard yellow-text text-darken-3">dashboard</i>Dashboard</a>
	  </ul>
	  <ul>
	  <a href="map.php" class="collapsible-header yellow-text text-darken-3"><i class="material-icons map yellow-text text-darken-3">map</i>Map</a>
      <a href="../../../login.php"class="collapsible-header yellow-text text-darken-3"><i class="material-icons user yellow-text text-darken-3">person</i>Logout</a>
	  </ul>
	</li>
  </ul>