<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>
<?php include 'reg_details.php'; ?>
<div class="main">
    <div class="container"> 
		<h3>
			Registered Users
		</h3>
        <table id="table_id" class="display responsive-table highlight">
            <thead class="display centered">
                <tr>
                    <th data-field="bname">Name</th>
                    <th data-field="bcontacts">Contacts</th>
					<th data-field="baddress">Address</th>
					<th data-field="binfo" style="float: center; text-align: center;">Infos</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Rufino Macagba Jr.</td>
                    <td>09938728965</td>
                    <td>Lorma Colleges</td>
					<td style="float: center; text-align: center;"><a href="#details" name="details" class="modal-trigger"><button class='col s12 btn btn-small waves-effect orange darken-2'>Details</button></a></td>
                </tr>
                
            </tbody>
        </table>
	</div>
</div>		
<?php require '../../partials/footer.php'; ?>
