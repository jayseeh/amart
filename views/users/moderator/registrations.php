<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>
<?php include 'bus_details.php'; ?>
<?php include 'bus_edit.php'; ?>
<?php include 'bus_delete.php'; ?>
<div class="main">
    <div class="container">    
		<h3>
            Registered Business Accounts
        </h3>
        <table id="table_id" class="display responsive-table highlight">
            <thead class="display centered">
                <tr>
                    <th data-field="bname" style="float: center; text-align: center;">Name</th>
                    <th data-field="bowner" style="float: center; text-align: center;">Owner</th>
                    <th data-field="bcategory" style="float: center; text-align: center;">Category</th>
					<th data-field="baction" style="float: center; text-align: center;">Action</th>
					<th data-field="baction1" style="float: center; text-align: center;">Activate/Deactivate Account</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Lorma Colleges</td>
                    <td>Rufino Macagba Jr.</td>
                    <td>School</td>
					<td>
                        <div class="row">
                            <span class="col m4 l4"><a href="#details" name="details" class="modal-trigger"><i class="material-icons">view_list</i></a></span>
                            <span class="col m4 l4"><a href="#edit" name="edit" class="modal-trigger"><i class="material-icons">edit</i></a></span>
                            <span class="col m4 l4"><a href="#delete" name="delete" class="modal-trigger"><i class="material-icons">delete</i></a></span>
                        </div>
                    </td>
					<td style="float: center; text-align: center;">
						<div class="switch" style="margin: 0px 0; float: center;">
							<label>
									<input type="checkbox" id="acceptTC">
									<span class="lever"></span>
							</label>
						</div>
					</td>
                </tr>
                <tr>
                    <td>Sunset Bay Beach Resort</td>
                    <td>John Doe</td>
                    <td>Resort, Restaurant</td>
					<td>
                        <div class="row">
                            <span class="col m4 l4"><a href="#details" name="details" class="modal-trigger"><i class="material-icons">view_list</i></a></span>
                            <span class="col m4 l4"><a href="#edit" name="edit" class="modal-trigger"><i class="material-icons">edit</i></a></span>
                            <span class="col m4 l4"><a href="#delete" name="delete" class="modal-trigger"><i class="material-icons">delete</i></a></span>
                        </div>
                    </td>
					<td style="float: center; text-align: center;">
						<div class="switch" style="margin: 0px 0; float: center;">
							<label>
									<input type="checkbox" id="acceptTC">
									<span class="lever"></span>
							</label>
						</div>
					</td>
                </tr>
                <tr>
                    <td>Jorisan Resort</td>
                    <td>Jane Doe</td>
                    <td>Resort, Restaurant</td>
					<td>
                        <div class="row">
                            <span class="col m4 l4"><a href="#details" name="details" class="modal-trigger"><i class="material-icons">view_list</i></a></span>
                            <span class="col m4 l4"><a href="#edit" name="edit" class="modal-trigger"><i class="material-icons">edit</i></a></span>
                            <span class="col m4 l4"><a href="#delete" name="delete" class="modal-trigger"><i class="material-icons">delete</i></a></span>
                        </div>
                    </td>
					<td style="float: center; text-align: center;">
						<div class="switch" style="margin: 0px 0; float: center;">
							<label>
									<input type="checkbox" id="acceptTC">
									<span class="lever"></span>
							</label>
						</div>
					</td>
                </tr>
            </tbody>
        </table>
	</div>
</div>		
<?php require '../../partials/footer.php'; ?>
