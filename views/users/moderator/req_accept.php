<!-- Confirm Accept Business Modal -->
	<div id="accept" class="modal">
		<div class="modal-content" style="float: center; text-align: center;">
			<h2>Are you sure to accept <business name>?</h2>
			<h5>Please review contents before accepting</h5>
		<div class='row'>
			<div class='col s6 m6'>
				<a href="#!" class="modal-close col s12 btn btn-large waves-effect grey darken-2">Cancel</a>
			</div>
			<div class='col s6 m6'>
				<a href="#acceptbusiness" type="submit" name='deleteconfirm' class='modal-trigger col s12 btn btn-large waves-effect grey darken-2'>Accept</a></p>
			</div>
		</div>
		</div>
	</div>

<!-- Accepted Business Modal -->
	<div id="acceptbusiness" class="modal">
		<div class="modal-content" style="float: center; text-align: center;">
			<p><i class="material-icons col s12 offline_pin black-text text-darken-3" style="font-size: 250px;">offline_pin</i></p>
			<p><h5>Business Accepted!</h5></p>
			<p><a href="requests.php"><button type="submit" class='modal-trigger col s12 btn btn-large waves-effect grey darken-2'>Proceed</button></a></p>
		</div>
	</div>