<!-- Reason to Decline Business Modal -->
	<div id="decline" class="modal">
		<div class="modal-content" style="float: center; text-align: center;">
			<h2>Are you sure to decline <business name>?</h2>
			<h5>Please review contents before accepting</h5>
	<form class="col s12" action="requests.php" method="post">
		<div class='row'>
            <div class='input-field col s12'>
				<i class="material-icons prefix yellow-text text-darken-3">message</i>
                <textarea class='message materialize-textarea' type='text' name='decmessage' id='decmessage' required/></textarea>
                <label for='decmessage'>Reason To Decline Message</label>
			</div>
		</div>
		<div class='row'>
            <div class='col s6 m6'>
				<a href="#!" class="modal-close col s12 btn btn-large waves-effect grey darken-2">Cancel</a>
			</div>
			<div class='col s6 m6'>
				<a href="#declinebusiness" type="submit" name='declineconfirm' class='modal-trigger col s12 btn btn-large waves-effect grey darken-2'>Accept</a></p>
			</div>
		</div>
	</form>
		<div class='row'>
			
		</div>
		</div>
	</div>

<!-- Declined Business Modal -->
	<div id="declinebusiness" class="modal">
		<div class="modal-content" style="float: center; text-align: center;">
			<p><i class="material-icons col s12 offline_pin black-text text-darken-3" style="font-size: 250px;">offline_pin</i></p>
			<p><h4>Business Declined!</h4></p>
			<p><h5 style="color: red;">*Notification will be sent to owner via sim number!</h5></p>
			<p><a href="requests.php"><button type="submit" class='modal-trigger col s12 btn btn-large waves-effect grey darken-2'>Proceed</button></a></p>
		</div>
	</div>