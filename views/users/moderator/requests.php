<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>
<?php include 'req_details.php'; ?>
<?php include 'req_accept.php'; ?>
<?php include 'req_decline.php'; ?>
<div class="main">
    <div class="container">
        <h3>
            Pending Requests
        </h3>
        <table id="table_id" class="display responsive-table striped">
            <thead>
                <tr>
                    <th data-field="company" style="float: center; text-align: center;">Company</th>
                    <th data-field="owner" style="float: center; text-align: center;">Owner</th>
                    <th data-field="rn" style="float: center; text-align: center;">Reference Number</th>
					<th data-field="Info" style="float: center; text-align: center;">Infos</th>
                    <th data-field="action" style="float: center; text-align: center;">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Jorisan Resort</td>
                    <td>Jane Doe</td>
                    <td class="green-text">0081923812</td>
                    <td style="float: center; text-align: center;"><a href="#details" name="details" class="modal-trigger"><button class='col s12 btn btn-small waves-effect orange darken-2'>Details</button></a></td>
					<td style="float: center; text-align: center;">
						<a href="#accept" name="accept" class="modal-trigger"><button class='col s12 btn btn-small waves-effect green darken-2'>Accept</button><a>
						<a href="#decline" name="decline" class="modal-trigger"><button class='col s12 btn btn-small waves-effect red darken-2'>Decline</button><a>
					</td>
				</tr>
            </tbody>
        </table>
    </div>
</div>
<?php require '../../partials/footer.php'; ?>
