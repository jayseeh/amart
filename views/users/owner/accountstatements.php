<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php' ?>
<link rel="stylesheet" href="../../../css/datatable.css">
<link rel="stylesheet" href="css/material.min.css">
<link rel="stylesheet" href="css/dataTables.material.min.css">
<div class="main">
    <!-- <div class="container"> -->
        <div class="row">
			<div class="col s12 l12">
                <div class="card material-table">
                    <div class="table-header">
                        <span class="table-title">Account Statements</span>
                        <div class="actions">
                        <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                        </div>
                    </div>
                    <table id="accountstatements">
                        <thead style="text-align: left;">
                            <th style="text-align: left;">Business Name</th>                            
                            <th style="text-align: left;">Ref No.</th>
                            <th style="text-align: left;">Date</th>
                            <th style="text-align: left;">Status</th>
                            <th style="text-align: left;">Action</th>
                        </thead>
                        <tbody>
                        <?php 
                            $ownerID = $_SESSION['userID'];
                            $sql = "SELECT * from business INNER JOIN subscription on
                            business.businessID = subscription.BisSubIDkey 
                            WHERE businessOwner = '$ownerID'
                            ORDER BY subDatePaid DESC";
                            $query = mysqli_query($connect, $sql);
                            $count = mysqli_num_rows($query);
                            while ($fetchdata = mysqli_fetch_array($query)){
                                $refid = $fetchdata['subscriptionID'];
                                $busname = $fetchdata['businessName'];
                                $receipt = $fetchdata['subRefPhoto'];
                                $amount = $fetchdata['subAmount'];
                                $refnum = $fetchdata['subRefNum'];
                                $stat =    $fetchdata['subStatus'];
                                $date = $fetchdata['subDatePaid'];
                                $date= date_create($date);
                                $thisdate = date_format($date,"F d, Y");
                        ?>
                            <tr>
                                <td><?php echo $busname; ?></td>
                                <td><?php echo $refnum; ?></td>
                                <td><?php echo $thisdate; ?></td>
                                <td><span class="green-text"><?php echo $stat; ?></span></td>
                                <td><a href="#viewdetails-<?php echo $refid; ?>" class="modal-trigger"><i class="material-icons">search</i></a></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
				</div>
            </div>
        </div>
    </div>
</div>
  <!-- Modal Structure -->
  <?php 
        $sql = "SELECT * from business INNER JOIN subscription on
        business.businessID = subscription.BisSubIDkey 
        where businessOwner = '$ownerID' order by subDatePaid";
        $query = mysqli_query($connect, $sql);
        $count = mysqli_num_rows($query);
      
        while ($fetchdata = mysqli_fetch_array($query)){
            $refid = $fetchdata['subscriptionID'];
            $busname = $fetchdata['businessName'];
            $receipt = $fetchdata['subRefPhoto'];
            $amount = $fetchdata['subAmount'];
            $refnum = $fetchdata['subRefNum'];
            $stat =    $fetchdata['subStatus'];
            $date = $fetchdata['subDatePaid'];
            $date= date_create($date);
            $thisdate = date_format($date,"F d, Y");
    ?>
  <div id="viewdetails-<?php echo $refid; ?>" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>
        <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
        <span>Details</span>
        </h4>
        <div class="divider"></div>
        <div class="row">
            <div class="col s12 l4">
                Business Name: <strong><?php echo $busname; ?></strong><br>
                Date: <strong><?php echo $thisdate; ?></strong><br>
                Reference No: <strong><?php echo $refnum; ?></strong><br>
                Amount: <strong>Php <?php echo $amount; ?></strong><br>
            </div>
            <div class="col s12 l8">
                <center>
                <img src="../../../assets/refPics/<?php echo $receipt; ?>"  class="col s12 m12 l12 materialboxed" alt="receipt"><br>
                Proof of Payment
                </center>
            </div>
        </div>
    </div>
  </div>
<?php } ?>
</div>

<script>
    $(document).ready(function() {
        $('#accountstatements').dataTable({
            "oLanguage": {
                "sStripClasses": "",
                "sSearch": "",
                "sSearchPlaceholder": "Enter Keywords Here",
                "sInfo": "_START_ -_END_ of _TOTAL_",
                "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
                '<option value="10">10</option>' +
                '<option value="20">20</option>' +
                '<option value="30">30</option>' +
                '<option value="40">40</option>' +
                '<option value="50">50</option>' +
                '<option value="-1">All</option>' +
                '</select></div>'
            },
            bAutoWidth: false,
            resposive: true
        });
    });

</script>
<script src="jquery/jquery-1.8.3.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.material.min.js"></script>

<?php require '../../../views/partials/footer.php' ?>