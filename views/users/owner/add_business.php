<?php require '../../../views/partials/header.php'; ?>
<?php 
	include "../../../db.sql/connect_db.php"; 
	require 'nav.php'; 

	if (isset($_GET['message'])){
		$message = $_GET['message'];
	}

?>

<div class="main">
<div class="container">
<?php 
	$sql = "SELECT DISTINCT categoryType, categoryFee from categories order by categoryType asc";
	$query = mysqli_query($connect, $sql);
	$count = mysqli_num_rows($query);
	$categoryFee = [];
	while ($fetchdata = mysqli_fetch_array($query))  {
		$categoryFee[] = array($fetchdata['categoryType']=>$fetchdata['categoryFee']);		
	}

	
?>
	<div class="section">
        <h4>
            Add Business
        </h4>
        <div class="divider"></div>

		<br>

		<form class="col s12" action="../../../db.sql/insertbusiness.php" method="post">
			<?php 
				$sql = "SELECT * from business order by businessID desc limit 1";
				$query = mysqli_query($connect, $sql);
				$count = mysqli_num_rows($query);
				while ($fetchdata = mysqli_fetch_array($query))  {?>
				<input type='hidden' name='lastID' value='<?php echo $fetchdata['businessID'];?>'>
			<?php 
				} 
			?>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='text' name='bname' id='bname' placeholder='Business Name' required/>
                <label for='bname'>Business Name</label>
              </div>
			</div>
            <div class='row'>
              	<div class='col s12'>
					<div class="row">
						<div class="col s12">
							<label for="category">Category</label>
							<select name="category" id="category" required>
								<option value="" disabled selected>Choose Category</option>
								<optgroup label="Free">
								<?php 
								$sql = "SELECT * from categories where categoryType = 'Free' and categoryStatus = 'Active' order by categoryName asc";
								$query = mysqli_query($connect, $sql);
								$count = mysqli_num_rows($query);
								while ($fetchdata = mysqli_fetch_array($query))  {?>
									<option value="<?php echo $fetchdata['categoryID']; ?>"><?php echo $fetchdata['categoryName']; ?></option>
								<?php } ?>
								</optgroup>
								<optgroup label="Micro ( Php <?php echo $categoryFee[2]["Micro"]; ?> )">
								<?php 
								$sql = "SELECT * from categories where categoryType = 'Micro' and categoryStatus = 'Active' order by categoryName asc";
								$query = mysqli_query($connect,$sql);
								$count = mysqli_num_rows($query);
								while ($fetchdata = mysqli_fetch_array($query))  {?>
									<option value="<?php echo $fetchdata['categoryID']; ?>"><?php echo $fetchdata['categoryName']; ?></option>
								<?php } ?>
								</optgroup>
								<optgroup label="Small ( Php <?php echo $categoryFee[3]["Small"]; ?> )">
								<?php 
								$sql = "SELECT * from categories where categoryType = 'Small' and categoryStatus = 'Active'  order by categoryName asc";
								$query = mysqli_query($connect,$sql);
								$count = mysqli_num_rows($query); 
								while ($fetchdata = mysqli_fetch_array($query))  {?>
									<option value="<?php echo $fetchdata['categoryID']; ?>"><?php echo $fetchdata['categoryName']; ?></option>
								<?php } ?>
								</optgroup>
								<optgroup label="Medium ( Php <?php echo $categoryFee[1]["Medium"]; ?> )">
								<?php 
									$sql = "SELECT * from categories where categoryType = 'Medium' and categoryStatus = 'Active'  order by categoryName asc";
									$query = mysqli_query($connect,$sql);
									$count = mysqli_num_rows($query); 
									while ($fetchdata = mysqli_fetch_array($query))  {
								?>
									<option value="<?php echo $fetchdata['categoryID']; ?>"><?php echo $fetchdata['categoryName']; ?></option>
								<?php } ?>
								</optgroup>
							</select>
						</div>
					</div>
				</div>
			</div>
			<center>
			<input type='hidden' name='ownerID'value='<?php echo $_SESSION['userID'];?>'>
			<div class="row">
				<div class="col s12">
					<button type="submit" class='btn btn-large yellow darken-3' style="text-transform: capitalize;">Make your Business Visible</button>
				</div>
			</div>
			</center>
		  </form>
		  <p class="center-align">
			  <em><a href="#request" class="modal-trigger">Click here if the category of your business isn't listed above</a></em>
			</p>
		</div>		
	</div>
	<div class="modal" id="request">
		<form action="../../../db.sql/insertCategoryRequest.php" method="POST">
			<div class="modal-content">
				<div class="row">
					<div class="col s12">
						<p class="center-align">
							<em> Your request will be checked by the administrator and once the category is approved you'll receive a notification. </em>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col s12 input-field">
						<label for="categoryname">Category Name</label>
						<input type="text" name="categoryname" required>
					</div>
				</div>
				<div class="row">
					<div class="col s12 input-field">
						<label for="categorydetails">Details</label>
						<input type="text" name="categorydetails" placeholder="Tell us more about this category" required>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="requester" value="<?php echo $_SESSION['userID'];?>">
				<input type="submit" value="Send Request" class="btn yellow darken-3">
			</div>
		</form>
	</div>

	 <!-- Start of Success Modal -->
	 <div class="modal" id="messagemodal">
        <div class="modal-content">
            <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                <span id="messagetype"></span>
            </h4>
            <div>
                <p class="center-align">
                    <i><span id="message"></span></i>
                </p>
            </div>    
        </div>
    </div>
    <!-- End of Success Modal -->
</div>

<?php require '../../../views/partials/footer.php' ?>

<script>
	$(document).ready(function(){
		var message = "<?php echo $_GET['message']; ?>";

        if( message != "" ) {
			if( message == "success" ){
				$('#messagetype').text('Success');
				$('#message').text('Requesting is successful, please wait for the confirmation of the administrator.');
            	$('#messagemodal').modal('open');
			} else if ( message == "failed" ){
				$('#messagetype').text('Error');
				$('#message').text('Ooops, something went wrong. Please try again later');
            	$('#messagemodal').modal('open');
			}

        }
	});
</script>