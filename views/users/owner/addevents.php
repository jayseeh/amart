<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php' ?>

<div class="main">
<div class="container">
	<br>
	<div class="row valign-wrapper show-on-med-and-up hide-on-small-only">
		<div class="col s1 m1 l1">
			<br>
			<a href="events.php" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
		</div>
		<div class="col s11 m11 l11">
			<h4>
            	<span>Add Events</span>
       		</h4>
    	</div>
	</div>
	<div class="row valign-wrapper hide-on-med-and-up show-on-small-only">
		<div class="col s1 m1 l1">
			<a href="events.php" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
		</div>
		<div class="col s11 m11 l11">
			<h4>
            	<span>Add Events</span>
       		</h4>
    	</div>
	</div>
	
        
        <div class="divider"></div>
        
		<br>
		
		<?php //require 'date.php' ?>
		<form action="../../../db.sql/insertevent.php" class="form-horizontal"  role="form" class="col s12"  method="post" enctype="multipart/form-data">
		
		<div class="row">
			<div class="input-field col s12 m6 l6">
				<label for="startdate">Start Date</label>
				<input type="text" class="datepicker" name="startDate">
			</div>
		
			<div class="input-field col s12 m6 l6">
				<label for="enddate">End Date</label>
				<input type="text" class="datepicker" name="endDate">
			</div>
		</div>
			
			<div class="row">
				<div class="col s12 m12 l12">
					<label for="bname">Business Name</label>
					<select name="bname" id="bname" required>
						<option value="" disabled selected>Choose Business</option>
						<?php
							$q = mysqli_query($connect, "SELECT * FROM business WHERE businessOwner='$id'");
							while($row = mysqli_fetch_array($q)){
								echo "<option value='".$row['businessID']."'>".$row['businessName']."</option>";
							}
						?>		
					</select>
				</div>
			</div>

			<div class='row'>
              <div class='input-field col s12'>
				<i class="material-icons prefix yellow-text text-darken-3  hide-on-small-only">location_city</i>
                <input class='validate' type='text' name='etitle' id='etitle' required/>
                <label for='etitle'>Event Title</label>
              </div>
            </div>
			
			<div class='row'>
                <div class='input-field col s12 m12 l12'>
					<i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">edit</i>
                    <textarea class='message materialize-textarea' type='text' name='edescription' id='edescription' required/></textarea>
                    <label for='edescription' style="font-size:15px;">Event Description</label>
                </div>
            </div>
			
			<div class="row">
				<div class="col s12 m12 l12">
					<label for="proof" style="text-align: left; font-size: 20px; float: left; color: black;">Event Poster</label>
					<br><br>
					<div class="file-field input-field">
                        <div class="btn btn-flat white yellow-text text-darken-3">
                            <i class="material-icons">search</i>
                            <input type="file" name="eposter" class="validate" accept="image/*">
                        </div>
                        <div class="file-path-wrapper">
                            <input type="text" class="file-path validate" placeholder="Upload Photo" id="eposter">
                        </div>
                    </div>
				</div>
					<input type='hidden'name='datenow' value='
					<?php date_default_timezone_set("Asia/Bangkok"); echo date("Y-m-d H:i:s") ;?>' >
				<div class="col s4 m4"></div>
				<div class="col s4 m4"></div>
			</div>

			<center>
              <div class='' style="float: center; text-align: center;">
				<input type="submit" id="addevent" name='addevent' class='col m12 btn btn-large waves-effect grey' value="Post New Event">
				 <!--<a href="#confirmevent" id="addevent" name='addevent' class='col m12 btn btn-large waves-effect grey'>Post New Event</a>-->
			  </div>
            </center>
			</form>
			<!-- Confirm Event Modal -->
			<div id="confirmevent" class="modal">
				<div class="modal-content" style="float: center; text-align: center;">
					<p>Please review your new business event details before confirming</p>
				<div class='row'>
					<div class='col s6 m6'>
						<a href="#!" class="modal-close col s12 btn btn-large waves-effect grey darken-2">Back</a>
					</div>
					<div class='col s6 m6'>
						<a href="#success" name='addconfirm' class='modal-trigger col s12 btn btn-large waves-effect grey darken-2'>Confirm</a></p>
					</div>
				</div>
				</div>
			</div>
			
		<!-- Add Event Modal -->
			<div id="success" class="modal">
				<div class="modal-content" style="float: center; text-align: center;">
					<p><i class="material-icons col s12 offline_pin black-text text-darken-3" style="font-size: 250px;">offline_pin</i></p>
					<p><h5>You succesfully created new event!</h5></p>
					<p><button type="submit" class='modal-trigger col s12 btn btn-large waves-effect grey darken-2'>Proceed</button></p>
				</div>
			</div>
</div>
</div>
<script>
    function disablePostButton(){
		var check = document.getElementById("addevent");
        var start = document.getElementById("startevent");
		var final = document.getElementById("finalevent");
		var title = document.getElementById("etitle");
		var desc = document.getElementById("edescription");
        var poster = document.getElementById("eposter");

        if(start.value != "" && final.value != "" && title.value != "" && desc.value != "" && poster.value != ""){
            check.classList.remove("grey");
            check.className += " modal-trigger yellow darken-3";
            console.log("button is enabled");
            console.log(check.classList);
        }else{

        }
    }

    document.getElementById("startevent").addEventListener("focusout", function(){
         disablePostButton();
	});
 
    document.getElementById("finalevent").addEventListener("focusout", function(){
         disablePostButton();
	});
	
    document.getElementById("etitle").addEventListener("focusout", function(){
         disablePostButton();
	});
 
    document.getElementById("edescription").addEventListener("focusout", function(){
         disablePostButton();
	});

	document.getElementById("eposter").addEventListener("change", function(){
         disablePostButton();
	});
	
</script>

<?php require '../../../views/partials/footer.php' ?>