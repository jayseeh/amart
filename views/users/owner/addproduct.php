<form action="#" method="post">

    <div class="row">
        <div class="input-field col s12 l12">
            <label for="pname">Product Name</label>
            <input type="pname" required>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12 l12">
            <label for="price">Price</label>
            <input type="number" required>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12 l12">
            <label for="qty">Qty</label>
            <input type="qty" required>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12 l12">
            <label for="description">Description</label>
            <textarea name="desc" id="" cols="30" rows="10" required></textarea>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12 l12">
            <input type="file">
        </div>
    </div>

    <input type="submit">Add Products
</form>