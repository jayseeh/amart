<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>
<?php
    $bisID= $_GET['bID'];
    $fetchDay= mysqli_query($connect, "SELECT * FROM businesscalendar WHERE bisCalendarIDkey='$bisID'");
    $dayGet = mysqli_fetch_array($fetchDay);
    $day = explode(" ", $dayGet['bisSchedDay']);
?>
<div class="main">
	<form class="col s12" action="../../../db.sql/insertsched.php" method="post">
    <div class="container">
        <h4>Add Schedule</h4>
        <!-- Custom Day -->
        <div class="row">
            <div class="col s12 l12">
                <label for="1">
                    <?php
                        for ($x=0;$x<count($day); $x++) {
                            if($day[$x]=="Monday"){
                                $dis="disabled";
                                break;
                            }
                            else{
                                $dis="";
                            }
                        }
                    ?>
                    <input type="checkbox" name='day[]' value="Monday" id="1" <?php echo $dis; ?> ><span>Monday</span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l12">
                <label for="2">
                    <?php
                        for ($x=0;$x<count($day); $x++) {
                            if($day[$x]=="Tuesday"){
                                $dis="disabled";
                                break;
                            }
                            else{
                                $dis="";
                            }
                        }
                    ?>
                    <input type="checkbox" name='day[]' value="Tuesday" id="2"  <?php echo $dis; ?> ><span>Tuesday</span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l12">
                <label for="3">
                    <?php
                        for ($x=0;$x<count($day); $x++) {
                            if($day[$x]=="Wednesday"){
                                $dis="disabled";
                                break;
                            }
                            else{
                                $dis="";
                            }
                        }
                    ?> 
                    <input type="checkbox" name='day[]' value="Wednesday" id="3"  <?php echo $dis; ?> ><span>Wednesday</span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l12">
                <label for="4">
                    <?php
                        for ($x=0;$x<count($day); $x++) {
                            if($day[$x]=="Thursday"){
                                $dis="disabled";
                                break;
                            }
                            else{
                                $dis="";
                            }
                        }
                    ?>
                    <input type="checkbox" name='day[]' value="Thursday" id="4" <?php echo $dis; ?> ><span>Thursday</span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l12">
                <label for="5">
                    <?php
                        for ($x=0;$x<count($day); $x++) {
                            if($day[$x]=="Friday"){
                                $dis="disabled";
                                break;
                            }
                            else{
                                $dis="";
                            }
                        }
                    ?>
                    <input type="checkbox" name='day[]' value="Friday" id="5" <?php echo $dis; ?> ><span>Friday</span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l12">
                <label for="6">
                    <?php
                        for ($x=0;$x<count($day); $x++) {
                            if($day[$x]=="Saturday"){
                                $dis="disabled";
                                break;
                            }
                            else{
                                $dis="";
                            }
                        }
                    ?>
                    <input type="checkbox" name='day[]' value="Saturday" id="6" <?php echo $dis; ?> ><span>Saturday</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col s12 l12">
                <label for="7">
                    <?php
                        for ($x=0;$x<count($day); $x++) {
                            if($day[$x]=="Sunday"){
                                $dis="disabled";
                                break;
                            }
                            else{
                                $dis="";
                            }
                        }
                    ?>
                    <input type="checkbox" name='day[]' value="Sunday"  id="7"  <?php echo $dis; ?> ><span>Sunday</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col s12 l12 input-field">
                <label for="fromtime">From</label>
                <input type="text" class="timepicker" name='fromtime'id="fromtime">
            </div>
        </div>

        <div class="row">
            <div class="col s12 l12">
                <label for="totime">To</label>
                <input type="text" class="timepicker" name='totime'id="totime">
            </div>
        </div>


        <div class="row">
            <a href="single.php" class="btn yellow darken-3">Return</a>
            <input type='hidden' name='bID'value='<?php echo $_GET['bID'];?>'>
            <input type='hidden' name='userID'value='<?php echo $_SESSION['userID'];?>'>
            <input type='submit' class="btn yellow darken-3" value='Save'>
        </div>
    </div>
</div>
<?php 
$ownerID =$_SESSION['userID'];
$bisnesID = $_GET['bID'];
$sql = "SELECT * from business INNER JOIN categories on business.businessCategory = categories.categoryID where businessOwner = '$ownerID' and businessID = '$bisnesID'";
$query = mysqli_query($connect, $sql);
$count = mysqli_num_rows($query);
while ($fetchdata = mysqli_fetch_array($query))  {?>

<?php 
$status = $fetchdata['categoryType'];
}
 
if ($status == 'Free') {
	echo "<input type='hidden' name='targetpage' value='singlefree.php'>";
} else if($status == 'Worship'){
    echo "<input type='hidden' name='targetpage' value='singleworship.php'>";
} else {
	echo "<input type='hidden' name='targetpage' value='single.php'>";
}
?>
</form>
<?php require '../../partials/footer.php'; ?>
