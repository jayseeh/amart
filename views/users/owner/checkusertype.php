<?php

if(isset($_SESSION['sID'])) {
    $aType = $_SESSION['aType'];

    if($aType != "Owner") {
        header("Location:../" . $aType . "/dashboard.php");
    }
}

?>