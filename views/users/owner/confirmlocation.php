<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php' ?>
<script>
    var map;
var pos;
var lats,lngs;
function initMap() {

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      lats= position.coords.latitude;
      lngs= position.coords.longitude;
      document.getElementById('businessLat').value=lats;
      document.getElementById('businessLng').value=lngs;

    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  }

  setTimeout(function(){
    console.log("mapss");
    console.log("Lat: "+lats+ " Lng: "+lngs);
    var iconBase = '../../../assets/marker/pin.png';
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: lats, lng: lngs},
      zoom: 18  ,
      mapTypeId: 'satellite',
      streetViewControl: false,
      mapTypeControl: true,
      fullscreenControl:false,
      zoomControl:true,
      draggable:true
    });
    
    var marker = new google.maps.Marker({
      position: {lat: lats, lng: lngs},
      map: map,
      icon: iconBase,
      draggable:true
    });
    google.maps.event.addListener(marker, 'dragend', function(evt){
      console.log( 'Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(6) + ' Current Lng: ' + evt.latLng.lng().toFixed(6) );
      //alert( 'Marker dropped: Current Lat: ' + evt.latLng.lat() + ' Current Lng: ' + evt.latLng.lng() );
      document.getElementById('businessLat').value=evt.latLng.lat().toFixed(6);
      document.getElementById('businessLng').value=evt.latLng.lng().toFixed(6);
    });
  },1000);

}
</script>
<div class="main">
    <div class="container">
        <h3>Location</h3>
        <p class="center-align"><em>You can drag and reposition your location if it is not accurate</em></p>
        <div class="dvmap">
            <!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15291.761607788309!2d120.32638765!3d16.629759999999997!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1sen!2sph!4v1536324297149" style="border: 0; height: 500px; width: 100%;" allowfullscreen></iframe>-->
            <div id="map" style="width:100%; height:50vh;" ></div>
        </div>
        <br>
        <form action="../../../db.sql/insertLocation.php" method="post">
            <input type="hidden" name="bKey" value="<?php echo $_GET['bID']; ?>">
            <input type="hidden" name="lat" id="businessLat">
            <input type="hidden" name="long" id="businessLng">
            <center><input type="submit" name="submit" value="Confirm" class="btn btn-large yellow darken-3"></center>
        </form>
    </div>
</div>
<?php require '../../../views/partials/footer.php' ?>