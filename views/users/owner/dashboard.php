<?php 
  include '../../partials/header.php'; 
//   $navtitle = "E-Town Special";
  $thisID = $_SESSION['userID'];  
  $datenow = date('Y-m-d');

  $sql = "SELECT uAddress, uEmail, uGender FROM accounts where aID='$thisID'";
  $fetchquery = mysqli_query($connect, $sql);
  while($fetchdata = mysqli_fetch_array($fetchquery)){
    $data_address = $fetchdata['uAddress'];
    $data_email = $fetchdata['uEmail'];
    $data_gender = $fetchdata['uGender'];
  }

  if( $data_address == '' || $data_email == '' || $data_gender =='' ){
    header('location: profile.php?new');
  } else {
  	include 'nav.php'; 
?>

<div class="main ">
<?php 
$ownerID =$_SESSION['userID'];
$sql = "SELECT * from business 
		INNER JOIN categories 
		on business.businessCategory = categories.categoryID 
		where businessOwner = '$ownerID' 
		and categories.categoryType != 'Worship' 
		order by businessSubscriptionEnd DESC";
$query = mysqli_query($connect, $sql);
$count = mysqli_num_rows($query);
while ($fetchdata = mysqli_fetch_array($query))  {?>
<!-- Business Card -->
	<div class="row">
		<div class="col s12 m12 l6">
			<div class="card hoverable">
				<div class="card-content">
					<span class="card-title">
					<h5><?php echo $fetchdata['businessName'];?></h5></span>
					<p>
					<?php 
					$status = $fetchdata['businessStatus'];
					$bisnesType = $fetchdata['categoryType'];
					$categoryName = $fetchdata['categoryName'];
					$end_of_subscription = $fetchdata['businessSubscriptionEnd'];
					$date_end = date_create($end_of_subscription);
					$date_now = date_create($datenow);
					$interval = date_diff($date_end, $date_now);

					$days_remaining = $interval->format('%a Days Remaining') . "\n"; 

					if($bisnesType != "Free") {
						if($end_of_subscription < $datenow || $end_of_subscription == "0000-00-00"){
							
							$status = "Inactive";
						} else {
							$status = "Active";
						}
					} else {
						$days_remaining = "";
					}

					if ($status == 'Active') {
						echo "<h6><small>Status:</small> <span class='green-text'>$status</span> <small>$days_remaining</small></h6>";
					} elseif ($status == 'Inactive') {
						echo "<h6><small>Status:</small> <span class='red-text'>".$status." </span></h6>";
					} else {
						echo "<h6><small>Status:</small> <span class='orange-text'>".$status."</span></h6>";
					}

					?>
					<h6><small>Category:</small> <?php echo $categoryName; ?></h6>
					<h6><small>Type:</small> <?php echo $bisnesType; ?></h6>
					</p>
				</div>
				<div class="card-action">
					<?php 
					$status = $fetchdata['categoryType'];
					$businessID = $fetchdata['businessID'];
					//if ($status == 'Free') {
						echo "<a href='singlefree.php?bID=".$businessID."'>View</a>";
					/*} else {
						echo "<a href='single.php?bID=".$businessID."'>View</a>";
					}*/
					?>
					<a href="managealbum.php?bID=<?php echo $businessID; ?>" class="right">Manage Album</a>
				</div>
			</div>
		</div>
<?php } ?>
	</div>

<div class="section"></div>
<!-- place of worship -->
<div class="row">
<?php
$sql = "SELECT * from business INNER JOIN categories on business.businessCategory = categories.categoryID where businessOwner = '$ownerID' and categories.categoryType = 'Worship' order by businessName asc";
$query = mysqli_query($connect, $sql);
$count = mysqli_num_rows($query);
while ($fetchdata = mysqli_fetch_array($query))  {?>
<!-- Place of worship Card -->
	<div class="col s12 m12 l6">
		<div class="card hoverable">
			<div class="card-content">
				<span class="card-title">
				<h5><?php echo $fetchdata['businessName'];?></h5></span>
				<p>
				<?php 
				$status = $fetchdata['businessStatus'];
				$bisnesType = $fetchdata['categoryType'];
				$categoryName = $fetchdata['categoryName'];
				$status = 'Active';

				if ($status == 'Active') {
					echo "<h6><small>Status:</small> <span class='green-text'>".$status."</span></h6>";
				} elseif ($status == 'Inactive') {
					echo "<h6><small>Status:</small> <span class='red-text'>".$status."</span></h6>";
				} else {
					echo "<h6><small>Status:</small> <span class='orange-text'>".$status."</span></h6>";
				}?>
				<h6><small>Category:</small> <?php echo $categoryName; ?></h6>
				<h6><small>Type:</small> <?php echo $bisnesType; ?></h6>
				</p>
			</div>
			<div class="card-action">
				<?php 
				$status = $fetchdata['categoryType'];
				$businessID = $fetchdata['businessID'];
				
				echo "<a href='singleworship.php?bID=".$businessID."'>View</a>";
				?>
				<a href="managealbum.php?bID=<?php echo $businessID; ?>" class="right">Manage Album</a>
			</div>
		</div>
	</div>
<?php } ?>
</div>

<?php 
	require '../../../views/partials/footer.php';
}
?>