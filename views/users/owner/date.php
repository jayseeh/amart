<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

		<form action="dashboard.php" class="form-horizontal"  role="form" class="col s12"  method="post">
            <div class="form-group">
                <label for="dtp_input1" class="col-md-3 control-label" style="font-size: 15px;"> Start Event:</label>
					<div class="input-group date form_datetime col-md-8" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
						<input class="form-control" size="16" type="text" value="" readonly>
						<span class="input-group-addon" style="background-color: transparent; border: 0;"><span class="glyphicon glyphicon-remove"></span></span>
						<span class="input-group-addon" style="background-color: transparent; border: 0;"><span class="glyphicon glyphicon-th"></span></span>
					</div>
				<input type="hidden" id="dtp_input1" value="" /><br/>
            </div>
			<div class="form-group">
                <label for="dtp_input2" class="col-md-3 control-label" style="font-size: 15px;"> End Event:</label>
					<div class="input-group date form_datetime col-md-8" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input2">
						<input class="form-control" size="16" type="text" value="" readonly>
						<span class="input-group-addon" style="background-color: transparent; border: 0;"><span class="glyphicon glyphicon-remove"></span></span>
						<span class="input-group-addon" style="background-color: transparent; border: 0;"><span class="glyphicon glyphicon-th"></span></span>
					</div>
				<input type="hidden" id="dtp_input2" value="" /><br/>
            </div>
			
				<label for="startevent" style="font-size: 20px;">Start Date of Event</label>
              </div>
			  

<script type="text/javascript" src="js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>

<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1
    });
	$('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
	$('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0
    });
</script>

<script type="text/javascript" src="jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>