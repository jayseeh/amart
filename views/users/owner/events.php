<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>

<div class="main">
    <div class="container">
        <h4>Events <a href="addevents.php" class="btn yellow darken-3">Add Event</a></h4>
        <div class="row">
            <ul class="tabs">
                <li class="tab"><a href="#ongoing">Ongoing</a></li>
                <li class="tab"><a href="#finished">Finished</a></li>
            </ul>
            <!-- Start of Ongoing Tab -->
            <div id="ongoing">
            <!-- ongoing datatable -->
                <div class="row">
                    <div class="col s12">
                        <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">Ongoing Events</span>
                            <div class="actions">
                            <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table id="ongoing_events" class="striped">
                            <thead>
                                <th class="center-align">ID</th>
                                <th class="center-align">Business</th>
                                <th>Event Name</th>
                                <th class="hide-on-small-only">Date</th>
                                <th class="center-align">Action</th>
                            </thead>
                            <tbody>
                                <?php
                                    $ownerID =$_SESSION['userID'];
                                    $datenow = date('Y-m-d');
                                    $query = mysqli_query($connect, "SELECT * FROM events inner JOIN business on events.eventBisIDkey=business.businessID WHERE business.businessOwner='$ownerID' and events.endDate >= '$datenow' ORDER BY eventID DESC");
                                    while($fetchData = mysqli_fetch_array($query)){
                                ?>
                                    <tr>
                                        <td class="center-align"><?php echo $fetchData['eventID']; ?></td>
                                        <td class="center-align"><?php echo $fetchData['businessName']; ?></td>
                                        <td><?php echo $fetchData['eventTitle']; ?></td>
                                        <td class="hide-on-small-only"><?php $startDate =  $fetchData['startDate']; 
                                        $startDate = date("M d,Y", strtotime($startDate));
                                        echo $startDate;?> 
                                        -
                                        <?php $endDate =  $fetchData['endDate']; 
                                        $endDate = date("M d,Y", strtotime($endDate));
                                        echo $endDate;?></td>
                                        <td class="center-align"><a href="#view<?php echo $fetchData['eventID']; ?>" class='modal-trigger'><i class='material-icons'>search</i></a></td>
                                    </tr>
                                <?php 
                                    }
                                ?>
                            </tbody> 
                        </table>
                        </div>
                    </div>
                </div>
                <!-- end of ongoing datatable -->
            </div>
            <!-- End of Ongoing Tab -->

            <!-- Start of Finished Tab -->
            <div id="finished">
                <!-- finished datatable -->
                <div class="row">
                    <div class="col s12">
                        <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">Finished Events</span>
                            <div class="actions">
                            <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table id="finished_events" class="striped">
                            <thead>
                                <th class="center-align">ID</th>
                                <th class="center-align">Business</th>
                                <th>Event Name</th>
                                <th class="hide-on-small-only">Date</th>
                                <th class="center-align">Action</th>
                            </thead>
                            <tbody>
                                <?php
                                    $ownerID =$_SESSION['userID'];
                                    $datenow = date('Y-m-d');
                                    $query = mysqli_query($connect, "SELECT * FROM events inner JOIN business on events.eventBisIDkey=business.businessID WHERE business.businessOwner='$ownerID' and events.endDate <= '$datenow' ORDER BY eventID DESC");
                                    while($fetchData = mysqli_fetch_array($query)){
                                ?>
                                    <tr>
                                        <td class="center-align"><?php echo $fetchData['eventID']; ?></td>
                                        <td class="center-align"><?php echo $fetchData['businessName']; ?></td>
                                        <td><?php echo $fetchData['eventTitle']; ?></td>
                                        <td class="hide-on-small-only"><?php $startDate =  $fetchData['startDate']; 
                                        $startDate = date("M d,Y", strtotime($startDate));
                                        echo $startDate;?> 
                                        -
                                        <?php $endDate =  $fetchData['endDate']; 
                                        $endDate = date("M d,Y", strtotime($endDate));
                                        echo $endDate;?></td>
                                        <td class="center-align"><a href="#view<?php echo $fetchData['eventID']; ?>" class='modal-trigger'><i class='material-icons'>search</i></a></td>
                                    </tr>
                                <?php 
                                    }
                                ?>
                            </tbody> 
                        </table>
                        </div>
                    </div>
                </div>
                <!-- end of ongoing datatable -->
            </div>
            <!-- End of Finished Tab -->
        </div>
    </div>
</div>

<!-- Modals -->

<!-- Ongoing Modals -->
<?php
    $ownerID =$_SESSION['userID'];
    $query = mysqli_query($connect, "SELECT * FROM events inner JOIN business on events.eventBisIDkey=business.businessID WHERE business.businessOwner='$ownerID' AND events.eventStatus ='Active' ORDER BY eventID DESC");
    while($fetchData = mysqli_fetch_array($query)){
        $checkDesc =  $fetchData['eventDesc'];
		if ($checkDesc == '') {
			$desc = "Description is empty";
		} else {
			$desc = $checkDesc;
			$desc = preg_replace("/[\n]/", "<br>\n", $desc);
        }
        
        $startDate = $fetchData['startDate'];
        $startDate = date_create($startDate);

        $endDate = $fetchData['endDate'];
        $endDate = date_create($endDate);
?>
<!-- delete modal -->
<div class="modal" id="delete<?php echo $fetchData['eventID']; ?>">
    <div class="modal-content">
        <center>
            <form action='../../../db.sql/deleteevent.php' method='post'>
                <input type='hidden' name='eventid' value='<?php echo $fetchData['eventID']; ?>'>
                <span><h5><?php echo $fetchData['eventTitle']; ?></h5></span>
                <p>Are you sure you want to delete this event?</p>
                <input type='submit'class="btn yellow darken-3" value='Confirm' >
            </form>
        </center>
    </div>
</div>

<!-- view modal -->
<div class="modal modal-fixed-footer" id="view<?php echo $fetchData['eventID']; ?>">
    <div class="modal-content">
        <div class="row valign-wrapper">
            <div class="col s1 m1 l1">
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            </div>
            <div class="col s11 m11 l11">
                <h4>Details</h4>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m6 l6">
                <img src="../../../assets/eventPics/<?php echo $fetchData['eventPoster']; ?>" class="col s12 m12 l12 materialboxed" alt="poster">
                <a href="#poster-update-<?php echo $fetchData['eventID']; ?>" class="modal-trigger btn transparent grey-text text-darken-3 col s12 m10 l10 offset-l1 offset-m1"><i class="material-icons left">search</i> Update Photo</a>
            </div>
            <div class="col s12 m6 l6">
                <small>Event Title:</small><br> <strong><?php echo $fetchData['eventTitle']; ?></strong><br>
                <small>Business:</small><br> <strong><?php echo $fetchData['businessName']; ?></strong><br>
				<small>Date:</small><br> <strong><?php echo date_format($startDate, "M d, Y"); ?> - <?php echo  date_format($endDate, "M d, Y"); ?></strong><br>
				<small>Description:</small><br> <strong><?php echo $desc; ?></strong><br>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col s6 m6 l6">
                <a href="#delete<?php echo $fetchData['eventID']; ?>" class='modal-trigger btn col s12 m12 l12 red darken-3 truncate'><i class='material-icons left'>delete</i>Delete</a>
            </div>
            <div class="col s6 m6 l6">
                <a href="#edit<?php echo $fetchData['eventID']; ?>" class='modal-trigger btn col s12 m12 l12 yellow darken-3'><i class='material-icons left'>edit</i>Edit</a>
            </div>
        </div>
    </div>
</div>

<!-- Update Poster Photo -->
<div class="modal modal-fixed-footer overflow-modal" id="poster-update-<?php echo $fetchData['eventID']; ?>">
    <form action="../../../db.sql/updateeventphoto.php" method="POST" enctype="multipart/form-data">
    <div class="modal-content">
        <h5>
            <div class="row">
                <div class="col s2 m2 l2">
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                </div>
                <div class="col s10 m10 l10">
                    Change Poster
                    <input type='hidden' name='eventID' value='<?php echo $fetchData['eventID']; ?>'>
                    <input type='hidden' name='oldpath' value='<?php echo $fetchData['eventPoster']; ?>'>
                </div>
            </div>
        </h5>
        <div class="file-field input-field">
            <div class="btn btn-flat yellow darken-3 white-text">
                <i class="material-icons">search</i>
                <input type="file" name="image" accept="image/*">
            </div>
            <div class="file-path-wrapper">
                <input type="text" class="file-path validate" placeholder="Upload Photo" id="photo">
            </div>
        </div>
    </div>    
    <div class="modal-footer">
        <div class="row center-align">
            <div class="col s12 m12 l12">
                <input type="submit" name="submit" value="Save Changes" class="btn yellow darken-3">
            </div>
        </div>                                    
    </div>
    </form>
</div>

<!-- edit modal -->
<div class="modal" id="edit<?php echo $fetchData['eventID']; ?>">
    <div class="modal-content">                     
    <h4>Edit <?php echo $fetchData['businessName']; ?>'s event</h4>
    <form action="../../../db.sql/updateeventdetails.php"  method="post" >
    <div class="row">
        <div class="input-field col s12 m6 l6">
            <label for="startdate">Start Date</label>
            <input type="text" class="datepicker" name="startDate" value="<?php echo $fetchData['startDate']; ?>">
        </div>
    
        <div class="input-field col s12 m6 l6">
            <label for="enddate">End Date</label>
            <input type="text" class="datepicker" name="endDate" value="<?php echo $fetchData['endDate']; ?>">
        </div>
    </div>
        <div class='row'>
        <div class='input-field col s12'>
            <i class="material-icons prefix yellow-text text-darken-3  hide-on-small-only">location_city</i>
            <input class='validate' type='text' name='etitle' id='etitle' value="<?php echo $fetchData['eventTitle']; ?>" required/>
            <label for='etitle'>Event Title</label>
        </div>
        </div>
        
        <div class='row'>
            <div class='input-field col s12 m12 l12'>
                <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">edit</i>
                <textarea class='message materialize-textarea' type='text' name='edescription' id='edescription' required><?php echo $fetchData['eventDesc']; ?></textarea>
                <label for='edescription' style="font-size:15px;">Event Description</label>
            </div>
        </div>
        <center>
        <div class='row'>
            <input type='hidden' name='eventID' value='<?php echo $fetchData['eventID']; ?>'>
            <input type="submit" name='addevent' class='btn btn-large orange' value="Save Changes">
        </div>
        </center>
    </div>
</div>
</form>
<?php
}
?>
<!-- end of ongoing modals -->

<!-- finished modals -->
<?php
    $ownerID =$_SESSION['userID'];
    $query = mysqli_query($connect, "SELECT * FROM events inner JOIN business on events.eventBisIDkey=business.businessID WHERE business.businessOwner='$ownerID' AND events.eventStatus !='Active' ORDER BY eventID DESC");
    while($fetchData = mysqli_fetch_array($query)){
        $checkDesc =  $fetchData['eventDesc'];
		if ($checkDesc == '') {
			$desc = "Description is empty";
		} else {
			$desc = $checkDesc;
			$desc = preg_replace("/[\n]/", "<br>\n", $desc);
		}
        
?>
<!-- view modal -->
<div class="modal" id="view<?php echo $fetchData['eventID']; ?>">
    <div class="modal-content">
    <h4>Details</h4>
        <span><h5>Event Title: <?php echo $fetchData['eventTitle']; ?></h5></span>
        <div class="poster-image">
            <img src="../../assets/eventPics/<?php echo $fetchData['eventPoster']; ?>" alt="poster">
        </div>
        <p>Business: <?php echo $fetchData['businessName']; ?> <br>
            Date: <?php echo $fetchData['startDate']; ?> - <?php echo $fetchData['endDate']; ?>
        </p>
        <h5>Description</h5>
        <p><?php echo $desc; ?></p>
    </div>
</div>
<?php 
    } 
?>
<!-- end of finished modals -->

<!-- end of Modals -->
<script>
	$(document).ready(function() {
		$('#ongoing_events').dataTable({
            "columnDefs": [ 
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }],
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        order : [ 0, 'desc' ],
        bAutoWidth: false,
        resposive: true
        });

        $('#finished_events').dataTable({
            "columnDefs": [ 
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }],
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        order : [ 0, 'desc' ],
        bAutoWidth: false,
        resposive: true
        });
	});
</script>
<?php require '../../partials/footer.php'; ?>



