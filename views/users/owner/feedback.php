<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php' ?>
<link rel="stylesheet" href="../../../css/datatable.css">
<link rel="stylesheet" href="css/material.min.css">
<link rel="stylesheet" href="css/dataTables.material.min.css">
<div class="main">
    <!-- <div class="container"> -->
        <div class="row">
			<div class="col s12 l12"><br>
            <a href="#addfeedback" class="modal-trigger btn btn-small waves-effect yellow darken-2">Add Feedback</a>
                <div class="">
               <table id="example">
                    <thead style="text-align: left;">
                        <th width='30%' style="text-align: left;">Date</th>
                        <th style="text-align: left;">Content</th>
                        
                    </thead>
					<tbody>
                    <?php 
                        $myID =$_SESSION['userID'];
                        $sql = "SELECT * from feedbacks INNER JOIN accounts on
                        feedbacks.userID = accounts.aID 
                        where feedbacks.userID = '$myID' order by feedbackDate asc";
                        $query = mysqli_query($connect, $sql);
                        $count = mysqli_num_rows($query);
                        while ($fetchdata = mysqli_fetch_array($query))  {?>
						<tr>
                            <td><?php $date = $fetchdata['feedbackDate'];
                            $date=date_create($date);
                            echo date_format($date,"F d, Y");
                            
                            ?></td>
							<td><?php echo $fetchdata['feedbackContent'];?></td>
						</tr>
                    <?php } ?>
					</tbody>
                </table>
				</div>
            </div>
<!-- Feedback  Modal -->
<div id="addfeedback" class="modal modal-fixed-footer">
<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
	<div class="modal-content">
		<h4>
        <form action="../../../db.sql/feedback.php" method="POST">
		<textarea name='feedback' style="width: 100%;
	height: 300px;
    resize:none;
    font-size: 15px;
	border: 3px solid #cccccc;
	padding: 5px;
	font-family: Tahoma, sans-serif;
	background-image: url(bg.gif);
	background-position: bottom right;
	background-repeat: no-repeat;"></textarea>
	</div>
	<div class="modal-footer">
		<input type='hidden' name='userID' value='<?php echo $_SESSION['userID'];?>'>
			<input type="submit" name="submit" value="Submit" class="btn yellow darken-3">
		</form>
	</div>
</div>
			
			<script>
            $(document).ready(function() {
                $('#example').DataTable();
            });

        </script>
			
        </div>
    </div>
</div>
  <!-- Modal Structure -->
  <div id="viewdetails" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>
        <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
        <span>Details</span>
        </h4>
        <div class="divider"></div>
        <div class="row">
            <div class="col s12 l4">
                Date: 05/04/2018 <br>
                Reference No: 00023421424 <br>
                Amount: Php 150 <br>
            </div>
            <div class="col s12 l8">
                <center>
                <img src="../../../assets/images/no-image.jpg" alt="proof" style="width: 300px; height: 300px;"><br>
                Proof of Payment
                </center>
            </div>
        </div>
    </div>
  </div>
</div>
<script src="jquery/jquery-1.8.3.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.material.min.js"></script>

<?php require '../../../views/partials/footer.php' ?>