$(document).ready(function() {
    $("#user_editID").click(function(){
        console.log("CLick");
        var province, dataProvince="<option>-SELECT PROVINCE-</option>";
        var mun, dataMun="<option>-SELECT MUNICIPALITY-</option>";
        var brgy, dataBrgy = "<option>-SELECT BARANGAY-</option>";
        var pCode="", mCode="";
        setTimeout(function() {                
        //GET PROVINCE
        $.ajax({    //create an ajax request to display.php
            type: "GET",
            url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refprovince.json",
            success: function(response){                    
                //$("#responsecontainer").html(response); 
                province=JSON.parse(response);
                console.log(province);
                for (x in province.RECORDS){
                    dataProvince = dataProvince + "<option value="+province.RECORDS[x].provCode+">"+province.RECORDS[x].provDesc+"</option>";
                }
                $("#user_province").html(dataProvince);
                $("#user_municipality").html(dataMun);
                $("#user_brgy").html(dataBrgy);
            }
        });
        //GET MUNICIPALITY
        $.ajax({    //create an ajax request to display.php
            type: "GET",
            url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refcitymun.json",
            success: function(response){                    
                //$("#responsecontainer").html(response); 
                mun=JSON.parse(response);
                console.log(mun);
            }
        });
        //GET BARANGAY
        $.ajax({    //create an ajax request to display.php
            type: "GET",
            url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refbrgy.json",
            success: function(response){                    
                //$("#responsecontainer").html(response); 
                brgy=JSON.parse(response);
                console.log(brgy);
            }
        });
        },100);
        $("#user_province").change(function(){
            console.log("value: "+$(this).val());
            dataMun="<option>-SELECT MUNICIPALITY-</option>";
            pCode = $(this).val();
            filterMun = mun.RECORDS.filter( element => element.provCode == String(pCode));
            for (x in filterMun){
                dataMun= dataMun + "<option value="+filterMun[x].citymunCode+">"+(filterMun[x].citymunDesc).replace(/ *\([^)]*\) */g, "")+"</option>";
            }
            $("#user_municipality").html(dataMun);
            console.log(filterMun);
        });
        $("#user_municipality").change(function(){
            mCode = $(this).val();
            dataBrgy = "<option>-SELECT BARANGAY-</option>" ;
            filterBrgy = brgy.RECORDS.filter( element => element.citymunCode == String(mCode));
            for (x in filterBrgy){
                dataBrgy= dataBrgy + "<option value='"+filterBrgy[x].brgyDesc+"'>"+filterBrgy[x].brgyDesc.toUpperCase() + "</option>";
            }
            $("#user_brgy").html(dataBrgy);

        });
        $("#user_brgy").change(function(){
            p = province.RECORDS.filter( element => element.provCode == String(pCode));
            m = mun.RECORDS.filter( element => element.citymunCode == String(mCode));
            address = $(this).val() + ", " + m[0].citymunDesc+ ", " + p[0].provDesc;
            $("#disp").html(address);
            $("#u_brgy").val($(this).val());
            // $("#u_city").val((m[0].citymunDesc).replace(/ *\([^)]*\) */g, ""));
            // $("#u_province").val(p[0].provDesc);
            uccity = (m[0].citymunDesc).replace(/ *\([^)]*\) */g, "");
            uccity = uccity.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            $("#u_city").val(uccity);
            ucprovince = p[0].provDesc;
            ucprovince = ucprovince.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            $("#u_province").val(ucprovince);

        });
    
    });
});