<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php' ?>

<?php 
	$thisbisnes = $_GET['bID'];

	$fetchquery= mysqli_query($connect, "SELECT * FROM business WHERE business.businessID = '$thisbisnes'"); 
	while($fetchdata = mysqli_fetch_array($fetchquery)){
		$busName = $fetchdata['businessName'];
	}
?>
<div class="main">
	<div class="album-container">
		<div class="row valign-wrapper">
			<div class="col s1 m1 l1">
				<h4><a href="dashboard.php" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a></h4>
			</div>
			<div class="col s11 m11 l11">
				<h4 class=""><?php echo $busName; ?> Album</h4>
			</div>
		</div>

		<!-- Business Albums -->
		<div id="business_album" class="col s12">
			<!-- Start of Row -->
			<div class="row">
				<?php 
					$fetchquery= mysqli_query($connect, "SELECT * FROM business 
					inner join experience on business.businessID = experience.businessID
					inner join accounts on experience.uploaderID = accounts.aID
					WHERE business.businessID = $thisbisnes
					ORDER BY expID DESC"); 
					while($fetchdata = mysqli_fetch_array($fetchquery)){
				?>
			
				<div class="col s12 m4 l4 row s12">
					<!-- card album-entry -->
					<div class="card card-album-entry">
						<div class="card-image">
							<img src="../../../assets/expPics/<?php echo $fetchdata['expPhoto'];?>" class="materialboxed"  >
						</div>
						<div class="card-content">
							<table border='1' width='100%' border='1'>
								<tr>
									<td>
									
									<b>Uploader: </b> 
								<?php 
									$fname = $fetchdata['fname'];
									$mname = $fetchdata['mname'];
									$minitial = substr($mname,0, 1);
									$lname = $fetchdata['lname'];
									echo $fullname = $fname.' '.$minitial.'. '.$lname;
									?> <br>
									<?php 
									$dateposted = $fetchdata['dateTime'];
									$date = substr($dateposted,0,10);  
									$thisdate = date_create($date);
									$time = substr($dateposted,11,19);  
									$thistime = date_create($time);
									?>
								<b>Date: </b><?php
									echo date_format($thisdate,"M d, Y").' | '.date_format($thistime,"h:i A");?>
									<br>
									<span class="card-title activator grey-text text-darken-4" style="font-size: 17px;"><strong style="font-size: 17px;">Caption: </strong>
									<?php $caption =  $fetchdata['expDesc'];
									$stringnum = strlen($caption);
									if($stringnum >= 19){
										echo substr("$caption",0,19).'...';
									}else{ echo $caption;
									}
									?></span>
									</td>
									</p>
									<td>
									<div style="margin-left: -20px; z-index: 9;">
										<a href="#delete-<?php echo $fetchdata['expID']; ?>" class="modal-trigger btn btn-small waves-effect red">Delete</a>
									</div>
									</td>
									<td>
									<span class="card-title activator grey-text text-darken-4"><i class="material-icons right">more_vert</span>
									</td>
								</tr>
							</table>
						</div>
						<div class="card-reveal">
						<span class="card-title grey-text text-darken-4">Caption<i class="material-icons right">close</i></span>
						<p><?php  echo $fetchdata['expDesc']; ?></p>
						</div>
						
					</div>
				</div>
			<?php } ?>
		</div>
		<!-- End of Row -->
		
	</div>
</div>
<!-- END -->
	
<!-- Delete Modal -->
<?php 
	$fetchquery= mysqli_query($connect, "SELECT * FROM business 
	inner join experience on business.businessID = experience.businessID
	inner join accounts on experience.uploaderID = accounts.aID
	WHERE business.businessID = $thisbisnes"); 
	while($fetchdata = mysqli_fetch_array($fetchquery)){
?>
<div id="delete-<?php echo $fetchdata['expID']; ?>" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>
		<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
		Confirm Deletion
		</h4>
		<p>Are you sure you want to delete this photo in the album?</p>
	</div>
	<div class="modal-footer">
		<form action="../../../db.sql/deleteexp.php" method="POST">
		<input type='hidden' name='expID' value='<?php echo $fetchdata['expID'];?>'>
		<input type='hidden' name='bID' value='<?php echo $_GET['bID'];?>'>
			<input type="submit" name="submit" value="Delete" class="btn yellow darken-3">
		</form>
	</div>
</div>
<?php
	}
?>
	
<?php require '../../../views/partials/footer.php' ?>