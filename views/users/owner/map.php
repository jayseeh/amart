<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>
<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul>
                <li><a href="#">All</a></li>
                <li><a href="#">Eat</a></li>
                <li><a href="#">Stay</a></li>
                <li><a href="#">Play</a></li>
            </ul>
        </div>
    </nav>
    <div class="map-container">
        <div class="dvmap">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15291.761607788309!2d120.32638765!3d16.629759999999997!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1sen!2sph!4v1536324297149" height="100%" width="100%" style="border:0" allowfullscreen></iframe>
        </div>
        <div class="dash-btn">
            <a class="btn-floating btn-large pulse waves-effect waves-light light-blue fab"><i class="material-icons">location_searching</i></a>
            <a class="btn-floating btn-large waves-effect waves-light red fab"><i class="material-icons">phone</i></a>
        </div>
    </div>
</div>
<?php require '../../partials/footer.php'; ?>
