<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>

<div class="main">
	<div class="container">
		
	</div>
	
	<div class="row">
		<div class="col s12">
			<div class="card material-table">
			<div class="table-header">
				<span class="table-title">Marketplace</span>
				<div class="actions">
				<a href="#owned" class="modal-trigger waves-effect btn-flat nopadding"><i class="material-icons">perm_identity</i></a>
				<a href="#additem" class="modal-trigger waves-effect btn-flat nopadding"><i class="material-icons">add</i></a>
				<a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
				</div>
			</div>
			<table id="marketplace" class="striped">
			<thead>
				<th class="center-align">ID</th>
				<th class="center-align">Type</th>
				<th>Description</th>
				<th>Price</th>
				<th style="text-align: left;" class="hide-on-small-only">Location</th>
				<th class="center-align">Action</th>
			</thead>
			<tbody>
				<?php 
				$fetchquery= mysqli_query($connect, "SELECT * FROM marketplace inner join accounts on marketplace.uploaderID = accounts.aID WHERE marketpostStatus = 'Active' ORDER BY marketpostID DESC"); 
				while($fetchdata = mysqli_fetch_array($fetchquery)){
					$address = explode(" | ", $fetchdata['uAddress']);
				?>
				<tr>
					<td class="center-align"><?php echo $fetchdata['marketpostID']; ?></td>
					<td class="center-align"><?php echo $fetchdata['marketpostType']; ?></td>
					<td><?php echo $fetchdata['marketpostDescription']; ?></td>
					<td>Php <?php echo $fetchdata['marketpostPrice']; ?></td>
					<td class="hide-on-small-only"><?php echo $address[3]; ?></td>
					<td class="center-align"><a href="#viewdetails?<?php echo $fetchdata['marketpostID']; ?>" class="modal-trigger"><i class="material-icons">search</i></a></td>
				</tr>
				<?php 
					} 
				?>
			</tbody>
			</table>
			</div>
		</div>
	</div>
</div>

<!-- Modals -->

<!-- view item details modal -->
<?php 
	//$thisID = $_GET['userid'];
	$fetchquery= mysqli_query($connect, "SELECT * FROM marketplace inner join accounts on marketplace.uploaderID = accounts.aID WHERE marketpostStatus = 'Active'"); 
	while($fetchdata = mysqli_fetch_array($fetchquery)){
		$checkDesc =  $fetchdata['marketpostDescription'];
		if ($checkDesc == '') {
			$desc = "Description is empty";
		} else {
			$desc = $checkDesc;
			$desc = preg_replace("/[\n]/", "<br>\n", $desc);
		}
?>
<div class="modal" id="viewdetails?<?php echo $fetchdata['marketpostID']; ?>">     
	<div class="modal-content">
		<h4>
			<a href="#" class="modal-close"><i class="material-icons yellow-text text-darken-3">arrow_back</i></a>
			Details
		</h4>
		
		<div class="row">
			<div class="col s12 m6 l6">
				<img src="../../../assets/marketPics/<?php echo $fetchdata['marketpostPic'];?>" class="materialboxed col s12 m12 l12" alt="marketplace-photo">
			</div>
			<div class="col s12 m6 l6">
				<small>Owner:</small><br>
				<strong>
				<?php 
				$fname = $fetchdata['fname'];
				$mname = $fetchdata['mname'];
				$minitial = substr($mname,0, 1);
				$lname = $fetchdata['lname'];
				echo $fullname = $fname.' '.$minitial.'. '.$lname;?>
				</strong><br>
				<!-- <small>Location:</small><br> <strong>La Union</strong><br> -->
				<small>Type:</small><br> <strong><?php echo $fetchdata['marketpostType']; ?></strong><br>
				<small>Description:</small><br> <strong><?php echo $desc; ?></strong><br>
			    <small>Contact Number:</small><br> <strong><?php echo $fetchdata['phoneNumber']; ?></strong><br>
				<small>Price:</small><br> <strong>Php <?php echo $fetchdata['marketpostPrice']; ?></strong>
			</div>
		</div>
		
	</div>
</div>
<?php } ?>
<!-- add item modal -->
<div id="additem" class="modal">
	<form action="../../../db.sql/inserttomarketplace.php" method="post" enctype="multipart/form-data">
	<div class="modal-content">
		<h4>
			Post Something
		</h4>
		<div class="row">
			<div class="input-field col s12 l12">
				<select name="category">
					<option value="" disabled selected>Choose Type</option>
					<option value="Buying">Buying</option>
					<option value="Selling">Selling</option>
				</select>
				
			</div>
		</div>
		<input type='hidden' name='uploader' value="<?php echo $_SESSION['userID'];?>">
		<div class="row">
			<div class="input-field col s12 l12">
				<label for="description">Description</label>
				<input type="text" name="description">
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12 l12">
				<label for="price">Price</label>
				<input type="text" name="price">
			</div>
		</div>
		<div class="row">
			<div class="file-field input-field">
				<div class="btn btn-flat yellow darken-3 white-text">
					<i class="material-icons">search</i>
					<input type="file" name='photo' id='photo' accept="image/*">
				</div>
				<div class="file-path-wrapper">
					<input type="text" class="file-path validate" placeholder="Upload Photo">
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<input type="submit" name='submit' value="Add Item" class="btn modal-close yellow darken-3">
	</form>
	</div>
</div>

<div class="modal" id="owned">
	<div class="modal-content">
		<h4>Owned Posts</h4>

		<table id="example">
		<thead class="center-align">
			<th class="center-align">Type</th>
			<th>Description</th>
			<th>Price</th>
			<th class="hide-on-small-only">Location</th>
			<th class="center-align">Action</th>
		</thead>
		<tbody>
			<?php 
				$userID = $_SESSION['userID'];
				$fetchquery= mysqli_query($connect, "SELECT * FROM marketplace inner join accounts on marketplace.uploaderID = accounts.aID
				WHERE marketpostStatus = 'Active' and uploaderID = '$userID'"); 
				while($fetchdata = mysqli_fetch_array($fetchquery)){
					$checkDesc =  $fetchdata['marketpostDescription'];
					if ($checkDesc == '') {
						$desc = "Description is empty";
					} else {
						$desc = $checkDesc;
						$desc = preg_replace("/[\n]/", "<br>\n", $desc);
					}
			?>
			<tr>
				<td class="center-align"><?php echo $fetchdata['marketpostType']; ?></td>
				<td><?php echo $desc; ?></td>
				<td>Php <?php echo $fetchdata['marketpostPrice']; ?></td>
				<td class="hide-on-small-only">La Union</td>
				<td class="center-align">
				<a href="#viewowneddetails<?php echo $fetchdata['marketpostID']; ?>" class="modal-trigger"><i class="material-icons">search</i></a>
			</tr>
			<?php 
				} 
			?>
		</tbody>
	</table>
	</div>
</div>

<!-- Owned Posts CRUD Modal -->
<?php 
	$userID = $_SESSION['userID'];
	$fetchquery= mysqli_query($connect, "SELECT * FROM marketplace inner join accounts on marketplace.uploaderID = accounts.aID
	WHERE marketpostStatus = 'Active' and uploaderID = '$userID'"); 
	while($fetchdata = mysqli_fetch_array($fetchquery)){
?>
<!-- Mark as Done -->
<div id="done<?php echo $fetchdata['marketpostID']; ?>" class="modal">
	<div class="modal-content">
		<h4>
			Mark as Done
		</h4>
		<p>You are about to remove this item in the marketplace</p>
	</div>
	<div class="modal-footer">
		<form action="../../../db.sql/deletemarketpost.php" method="post">
		<a href="#" class="modal-close btn grey darken-3">Cancel</a>
			<input type='hidden' name='postID' value='<?php echo $fetchdata['marketpostID'];?>'>
			<input type="submit" class="btn yellow darken-3" value="Remove">
		</form>
	</div>
</div>

<!-- Edit an Item -->
<div id="edit<?php echo $fetchdata['marketpostID']; ?>" class="modal">
	<form action="../../../db.sql/updatemerketpost.php" method="POST">
	<div class="modal-content">
		<h4>
			Edit Post
		</h4>
		<p>
			<div class="row">
				<div class="input-field col s12 l12">
					<select name="category" id="category">
						<?php 
							$value = 0;
							if($fetchdata['marketpostType'] == "Buying"){
								$value = 1;
						?>
							<option value="<?php echo $value; ?>" selected><?php echo $fetchdata['marketpostType']; ?></option>
							<option value="Selling">Selling</option>
						<?php
							}else{
								$value = 2;
						?>
							<option value="Buying">Buying</option>
							<option value="<?php echo $value; ?>" selected><?php echo $fetchdata['marketpostType']; ?></option>
						<?php
							}
						?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m12 l12 input-field">
					<label for="description">Description</label>
					<input type='hidden' name='postID' value='<?php echo $fetchdata['marketpostID']; ?>'>
					<input type="text" name="description" value="<?php echo $fetchdata['marketpostDescription']; ?>" required>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m12 l12 input-field">
					<label for="price">Price</label>
					<input type="text" name="price" value="<?php echo $fetchdata['marketpostPrice']; ?>" required>
				</div>
			</div>
		</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="modal-close btn grey darken-3">Cancel</a>
		<input type="submit" name="submit" value="Save Settings" class="btn yellow darken-3">
	</div>
	</form>
</div>

<!-- view owned items details modal -->
<div id="viewowneddetails<?php echo $fetchdata['marketpostID']; ?>" class="modal">
	<div class="modal-content">
		<h4>
			<a href="#" class="modal-close"><i class="material-icons yellow-text text-darken-3">arrow_back</i></a>
			Details
		</h4>
		<div class="row">
			<div class="col s12 m6 l6 center-align">
				<img src="../../../assets/marketPics/<?php echo $fetchdata['marketpostPic']; ?>" class="materialboxed col s12 m12 l12" alt="marketplace-photo">
				<a href="#updatefeaturedphoto<?php echo $fetchdata['marketpostID']; ?>" class="modal-trigger">Change Featured Photo</a>
			</div>
			<div class="col s12 m6 l6">
				<small>Owner:</small><br>
				<strong>
				<?php 
				$fname = $fetchdata['fname'];
				$mname = $fetchdata['mname'];
				$minitial = substr($mname,0, 1);
				$lname = $fetchdata['lname'];
				echo $fullname = $fname.' '.$minitial.'. '.$lname;?>
				</strong><br>
				<!-- <small>Location:</small><br> <strong>La Union</strong><br> -->
				<small>Type:</small><br> <strong><?php echo $fetchdata['marketpostType']; ?></strong><br>
				<small>Description:</small><br> <strong><?php echo $desc; ?></strong><br>
			    <small>Contact Number:</small><br> <strong><?php echo $fetchdata['phoneNumber']; ?></strong><br>
				<small>Price:</small><br> <strong>Php <?php echo $fetchdata['marketpostPrice']; ?></strong>
			</div>
		</div>
	</div>
	<div class="modal-footer center-align">
		<a href="#edit<?php echo $fetchdata['marketpostID']; ?>" class="btn modal-trigger green darken-3">Update</a>
		<a href="#done<?php echo $fetchdata['marketpostID']; ?>" class="btn modal-trigger yellow darken-3">Mark as Done</a>
	</div>
</div>


<!-- Change Featured Photo -->
<div class="modal" id="updatefeaturedphoto<?php echo $fetchdata['marketpostID']; ?>">
	<form action="../../../db.sql/updatemarketpic.php" method="post" enctype="multipart/form-data">
	<div class="modal-content">
		<h4>
			Upload A Photo
		</h4>
		<div class="row">
			<div class="file-field input-field">
				<div class="btn btn-flat yellow darken-3 white-text">
					<i class="material-icons">search</i>
					<input type="file" name='image' id='photo' accept="image/*">
				</div>
				<div class="file-path-wrapper">
					<input type="text" class="file-path validate" placeholder="Upload Photo">
				</div>
			</div>
		</div>	
	</div>
	<div class="modal-footer">
		<a href="#" class="modal-close btn grey darken-3">Cancel</a>
		<input type='hidden' value='<?php echo $fetchdata['marketpostID']; ?>' name='postID'>
		<input type="submit" name='submit' value="Change Featured Photo" class="btn modal-close yellow darken-3">
	</div>
	</form>
</div>
<?php 
	}
?>
<!-- End of Owned Posts CRUD -->

<script>
	$(document).ready(function() {
		$('#marketplace').dataTable({
		"columnDefs": [ 
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }],
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        order : [ 0, 'desc' ],
        bAutoWidth: false,
        resposive: true
        });
	});
</script>

<?php require '../../partials/footer.php' ?>