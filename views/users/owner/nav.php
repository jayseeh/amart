<?php include "checkusertype.php"; ?>
<style>
	a.disabled {
    pointer-events: none;
	}
</style>

<header>
  <nav class="grey darken-3">
      <a href="#" class="brand-logo center hide-on-med-and-up"><span class="yellow-text text-darken-3">E</span><small>-Town</small>&nbsp;<span class="yellow-text text-darken-3">B</span><small>usiness</small></a>
			<ul class="left">
				<!-- <li class="page-title truncate"><h4><?php echo $navtitle; ?></h4></li> -->
			</ul>
	  	<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons  yellow-text text-darken-2">menu</i></a> 
  </nav>

</header>
	<ul id="slide-out" class="sidenav sidenav-fixed grey darken-2">
    <nav class="grey darken-3">
      <a href="#" class="brand-logo center"><span class="yellow-text text-darken-3">A</span><small>MART</small>&nbsp;<span class="yellow-text text-darken-3">D</span><small>IGITAL</small></a> 
	  </nav>

	<!-- Profile -->
	<?php 
		$ID = $_SESSION['userID'];
		$fetchquery= mysqli_query($connect, "SELECT * FROM accounts WHERE aID = '$ID'"); 
		while($fetchdata = mysqli_fetch_array($fetchquery)){
	?>

	<div class="user-view">
    <div class="background">
    <img src="../../../assets/images/bg.png">
    </div>
    <a href="profile.php"><img class="circle" src="../../../assets/images/<?php echo $fetchdata['uPic']?>"></a>
			<span class="white-text name">
			<?php 
				$fname = $fetchdata['fname'];
				$mname = $fetchdata['mname'];
				$minitial = substr($mname,0, 1);
				$lname = $fetchdata['lname'];
				echo $fullname = $fname.' '.$minitial.'. '.$lname;
			?>
			<a href="../../../db.sql/logout.php" class="btn btn-flat right transparent" id="options"><i class="material-icons yellow-text text-darken-3">exit_to_app</i><span style="position: absolute; bottom: -10px; right: 10px;" class="logout-label yellow-text text-darken-3"><small>Logout</small></span></a>
			</span>
	</div>
	<?php 
		} 
	?>
	</li>
	
	<?php 
	$ID = $_SESSION['userID'];
	$fetchquery= mysqli_query($connect, "SELECT count(cartRefCodekey) as x, cartprocessStatus, businessIDkey, businessOwner 
	FROM cartprocess inner join business on cartprocess.businessIDkey = business.businessID 
	WHERE business.businessOwner = '$ID' and cartprocessStatus = 'Pending'"); 
	while($fetchdata = mysqli_fetch_array($fetchquery)){
	$reqnum = $fetchdata['x'];

	}?>
	<?php 
	include "../../../db.sql/connect_db.php";
	$id =$_SESSION['userID'];
	$query = mysqli_query($connect,
	"SELECT * from accounts where accounts.aID= '$id'");
	$count = mysqli_num_rows($query);
	while ($fetchdata = mysqli_fetch_array($query))  {?>
  

    <li>
      <ul class="collapsible" data-collapsible="accordion">
        <a href="dashboard.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3"><i class="material-icons dashboard yellow-text text-darken-3">dashboard</i>Dashboard</a>
			<a href="productrequest.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3
			<?php $checkstatus = $fetchdata['aStatus']; 
			if ( $checkstatus == 'Confirmed'){ echo ' '; }
			else{ echo 'disabled';} ?>">
			<i class="material-icons yellow-text text-darken-3">notifications</i>Product/Service Request<?php if($reqnum > 1){
					echo 's: '.$reqnum;
			}else{echo ': '.$reqnum;}?>
			<?php 
			  /*$thisID =$_SESSION['userID'];   notifxxx
				$fetchquery= mysqli_query($connect, "SELECT count(cartRefCodekey) as noticenum FROM cartprocess
				inner join business on cartprocess.businessIDkey = business.businessID 
				where cartprocess.cartprocessStatus = 'Pending' and business.businessOwner= $thisID"); 
				while($fetchdata = mysqli_fetch_array($fetchquery)){
				?>
				<?php
				$thisnum = $fetchdata['noticenum'];
				if ($thisnum > 0) {
						echo "<span class='badge new red' data-badge-caption=''>$thisnum</span>";
				}  else {
						echo "";
				}
				?>
			<?php } */ ?>
		</a>
		<a href="subscription_active.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3">
			<i class="material-icons chevron yellow-text text-darken-3">subscriptions</i>Subscription
		</a>
		<a href="accountstatements.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3">
			<i class="material-icons chevron yellow-text text-darken-3">contacts</i>Account Statements
		</a>
	  </ul>
	  <ul>
		<a href="add_business.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3 
			<?php $checkstatus = $fetchdata['aStatus']; 
			if ( $checkstatus == 'Confirmed'){ echo ' '; }
			else{ echo 'disabled';} ?>">
			<i class="material-icons add  yellow-text text-darken-3">add</i>Add Business
		</a>
		<a href="placeofworship.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3 
			<?php $checkstatus = $fetchdata['aStatus']; 
				if ( $checkstatus == 'Confirmed'){ echo ' '; }
				else{ echo 'disabled';} ?>">
			<i class="material-icons add  yellow-text text-darken-3">add</i>Add Place of Worship
		</a>
		<a href="events.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3
			<?php $checkstatus = $fetchdata['aStatus']; 
			if ( $checkstatus == 'Confirmed'){ echo ' '; }
			else{ echo 'disabled';} ?>">
			<i class="material-icons yellow-text text-darken-3">event</i>Events
		</a>
		<a href="marketplace.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3
			<?php $checkstatus = $fetchdata['aStatus']; 
			if ( $checkstatus == 'Confirmed'){ echo ' '; }
			else{ echo 'disabled';} ?>">
			<i class="material-icons yellow-text text-darken-3">shopping_basket</i>Marketplace
		</a>
		<a href="feedback.php" style="text-decoration: none;" class="collapsible-header yellow-text text-darken-3">
			<i class="material-icons yellow-text text-darken-3">chat</i>Feedback
		</a>

		<!-- <a href="guest/dashboard.php" class="collapsible-header yellow-text text-darken-3"><i class="material-icons">map</i> View Map as Guest</a> -->
	
	  </ul>
	</li>
  </ul>
<?php } ?>

<script>
	var is_mobile = 0;
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	// some code..
		is_mobile = 1;
	} else {
		is_mobile = 0;
	}

	$(document).ready(function(){
		if(!is_mobile){
			$('.logout-label').css('display', 'none');
		}
		
		$("#options").mouseenter(function () {
			$(".logout-label").show();
		});

		$("#options").mouseleave(function () {
			$(".logout-label").hide();
		});
	});
</script>