<?php require '../../../views/partials/header.php'; ?>
<?php include "../../../db.sql/connect_db.php"; ?>
<?php require 'nav.php' ?>
<div class="main">
<div class="container">
	<div class="section">
		<h4>
            Add Place of Worship
        </h4>
        <div class="divider"></div>
		<form class="col s12" action="../../../db.sql/insertbusiness.php" method="post">
		<input type='hidden' name='ownerID'value='<?php echo $_SESSION['userID'];?>'>
		<?php 
			$sql = "SELECT * from business order by businessID desc limit 1";
			$query = mysqli_query($connect, $sql);
			$count = mysqli_num_rows($query);
			while ($fetchdata = mysqli_fetch_array($query))  {?>
			<input type='hidden' name='lastID' value='<?php echo $fetchdata['businessID'];?>'>
			<?php } ?>
            <div class='row'>
              <div class='col s12 m6'>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
				<i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">location_city</i>
                <input class='validate' type='text' name='bname' id='bname' required/>
                <label for='bname'>Name</label>
              </div>
			</div>
            <div class='row'>
              	<div class='col s12'>
					<div class="row">
						<div class="col m1 l1">
							<i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">tab</i>
						</div>
						<div class="col s12 m11 l11">
							<label for="category">Category</label>
							<select name="category" id="category" required>
								<option value="" disabled selected>Choose Category</option>
								<?php 
								$sql = "SELECT * from categories where categoryType = 'Worship' order by categoryName asc";
								$query = mysqli_query($connect, $sql);
								$count = mysqli_num_rows($query);
								while ($fetchdata = mysqli_fetch_array($query))  {?>
									<option value="<?php echo $fetchdata['categoryID']; ?>"><?php echo $fetchdata['categoryName']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			
            <center>
			
              <div>
			  	<input type="submit" class='btn btn-large yellow darken-3' value='Proceed'>
			  </div>
	
            </center>
			
          </form>
		</div>		
</div>
</div>
<script>
    //Edit Form Validation
    function disableAddButton(){
        var check = document.getElementById("businesscheck");
        var name = document.getElementById("bname");
		// var desc = document.getElementById("bdescription");
		var category = document.getElementById("category");
        }
    }

    document.getElementById("bname").addEventListener("change", function(){
         disableAddButton();
	});

	document.getElementById("category").addEventListener("change", function(){
         disableAddButton();
	});
	</script>
<?php require '../../../views/partials/footer.php' ?>

