<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php'; 
$userID = $_SESSION['userID'];
?>
<div class="main">
	<div class="row">
		<div class="col s12">
			<div class="card material-table">
			<div class="table-header">
				<span class="table-title">Requests</span>
				<div class="actions">
				<a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
				</div>
			</div>
			
			<table id="requests" class="striped">
				<thead>
					<th class="center-align">ID</th>
					<th class="center-align">Ref Code</th>
					<th class="center-align">Date</th>
					<th class="center-align">Status</th>
					<th class="center-align">Action</th>
				</thead>
				<tbody>
				<?php 
					$sql = "SELECT * from cartprocess 
							INNER JOIN cart 
							ON cartprocess.cartRefCodekey = cart.cartRefCode
							INNER JOIN business 
							ON cart.cartBisIDkey = business.businessID
							WHERE business.businessOwner = '$userID' 
							-- AND cartprocess.cartprocessStatus = 'Pending' 
							-- GROUP BY cartRefCodekey
							-- ORDER BY cart.cartBisIDkey DESC
							ORDER BY cartReqDate DESC";
					$query = mysqli_query($connect, $sql);
					$count = mysqli_num_rows($query);
					while ($fetchdata = mysqli_fetch_array($query))  {
						$date = date_create($fetchdata['cartReqDate']);
						$request_date = date_format($date, 'F d, Y');	
						$request_status = $fetchdata['cartprocessStatus']; 
						$request_id = $fetchdata['cartprocessID'];
						$request_reference = $fetchdata['cartRefCodekey'];

						if($request_status == "Approved"){
							$status_class = "green-text text-darken-3";
						} else if($request_status == "Declined"){
							$status_class = "red-text text-darken-3";
						} else {
							$status_class = "yellow-text text-darken-3";
						}
				?>
					<tr>
						<td class="center-align"><?php echo $request_id;?></td>
						<td class="center-align"><?php echo $request_reference;?></td>
						<td class="center-align"><?php echo $request_date; ?></td>
						<td class="center-align <?php echo $status_class; ?>"><?php echo $request_status; ?></td>
                        <td class="center-align">
							<a href="#details-<?php echo $request_id?>" name="details" class="modal-trigger"><i class="material-icons">search</i></a>
						</td>
					</tr>									
				<?php } ?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<?php 
$sql = "SELECT * from cartprocess
		INNER JOIN cart
		ON cartprocess.cartRefCodekey = cart.cartRefCode
		INNER JOIN accounts
		ON cart.userID = accounts.aID
		INNER JOIN businessps 
		ON cart.productID = businessps.businessPSID";
$query = mysqli_query($connect, $sql);
$count = mysqli_num_rows($query);
while ($fetchdata = mysqli_fetch_array($query))  {
	
	$cart_request_status = $fetchdata['cartprocessStatus'];

	if($cart_request_status == "Approved"){
		$status_class = "green-text text-darken-3";
	} else if($cart_request_status == "Declined"){
		$status_class = "red-text text-darken-3";
	} else {
		$status_class = "yellow-text text-darken-3";
	}
	
?>
<!-- Request Details Modal -->
<div id="details-<?php echo $fetchdata['cartprocessID'];?>" class="modal">
	<div class="modal-content">
		<h4>Details</h4>
		
		<div>
			<p>
				<span><strong>Status: <span class="<?php echo $status_class; ?>"><?php echo $cart_request_status;?></span></strong></span><br>
				<span><strong>Customer: <?php 
				$fname = $fetchdata['fname'];
				$mname = $fetchdata['mname'];
				$minitial = substr($mname,0, 1);
				$lname = $fetchdata['lname'];
				echo $fullname = $fname.' '.$minitial.'. '.$lname;?></strong></span><br>
				<span><strong>Contact Details: <?php echo $fetchdata['phoneNumber'];?></strong></span><br>
				<span class="content-description"><strong>Message: <?php echo $fetchdata['msg'];?></strong></span><br>
				<span><strong>Request Date: <?php $date = date_create($fetchdata['cartReqDate']);
				echo date_format($date, "F m, Y");?></strong></span><br>
				<span><strong>Pickup Date: <?php echo $fetchdata['cartpickupDateTime'];?></strong></span><br>

				<?php if($cart_request_status == 'Declined') { ?>
					<span><strong>Declined Reason: <span class="red-text text-darken-3"><?php echo $fetchdata['reply']; ?></span></strong></span><br>
				<?php } ?>

			</p>
			<table class="table table-responsive center">
				<thead>
					<tr>
						<th>Item</th>
						<th>Quantity</th>
						<th>Price</th>
					</tr>
				</thead>
				<?php
					$refCode = $fetchdata['cartRefCodekey'];
					$cartsql = "SELECT * FROM cart 
								INNER JOIN businessps 
								ON cart.productID = businessps.businessPSID
								WHERE cartRefCode = '$refCode'";

					$cartquery = mysqli_query($connect, $cartsql);
					// $cartcount = mysqli_num_rows($cartquery);
					while ($fetchcart = mysqli_fetch_array($cartquery))  {
						$cartTotal = $fetchcart['cartTotal'];
				?>
				<tr>
					<td><?php echo $fetchcart['businessPSName'];?></td>
					<td><?php echo $fetchcart['itemQty'];?></td>
					<td><?php echo 'P ' . $fetchcart['itemPrice'];?></td>
				</tr>
				<?php 
					}
				?>
				<tr>
					<td></td>
					<td class="right"><b>Total</b>	</td>
					<td><?php echo 'P ' . $cartTotal;?></td>
				</tr>
			</table>
		</div>
			
	</div>
	<div class="modal-footer">
		<a href="#" class="modal-close btn grey darken-3">Return</a>
		<?php if($cart_request_status == 'Pending') { ?>
		<a href="#decline-<?php echo $fetchdata['cartprocessID'];?>" class="modal-trigger btn red darken-3">Decline</a>
		<a href="#approval-<?php echo $fetchdata['cartprocessID'];?>" class="modal-trigger btn yellow darken-3">Approve</a>
		<?php } ?>
	</div>
</div>

<!-- Confirm Approval Modal -->
<div id="approval-<?php echo $fetchdata['cartprocessID'];?>" class="modal">
	<div class="modal-content" style="text-align: center;">
		<p><h4>Ready to approve request?</h4></p>
		<p>Please review the request details before approving</p>
	</div>
	<div class="modal-footer">
		<form action="../../../db.sql/updatecartapprove.php" method="POST">
			<a href="#" class="modal-close btn grey darken-3">Return</a>
			<input type="submit" name="approve" value="Approve" class="btn yellow darken-3">
			<input type='hidden' value='<?php echo $fetchdata['cartRefCodekey'];?>' name='refcode'>
		</form>
	</div>
</div>

<!-- Confirm Decline Modal -->
<div id="decline-<?php echo $fetchdata['cartprocessID'];?>" class="modal modal-fixed-footer">
	<form action="../../../db.sql/updatecartdecline.php" method="POST">
	<div class="modal-content overflow-modal">
		<h4>
			<a href="#" class="modal-close"><i class="material-icons yellow-text text-darken-3">arrow_back</i></a>
			Declining Request
		</h4>
		<div class="row">
			<div class="col s12 m8 l8 offset-m2 offset-l2">
			<label for="reason">Reason for Declining</label>
			<select name="reason" required>
			<option selected disabled value="">Select reason</option>

			<?php $fetchquery= mysqli_query($connect, "SELECT * FROM declinestatements order by statementID asc"); 
			while($fetchreasons = mysqli_fetch_array($fetchquery)){
			$content = $fetchreasons['statementContent'];
			?>
				<option value="<?php echo $content;?>"><?php echo $content;?></option>
			<?php } ?>
			
			</select>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<div class="row center-align">
			<div class="col s8 m8 l8 offset-s2 offset-m2 offset-l2">
				<input type="submit" name="decline" value="Decline" class="col s10 m10 l10 btn red darken-3">
				<input type='hidden' value='<?php echo $refCode;?>' name='refcode'>
			</div>
		</div>
	</div>
	</form>
</div>

<?php
	}
?>

<script>
	$(document).ready(function() {
		$('#requests').dataTable({
			"columnDefs": [ 
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }],
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        order : [ 3, 'desc' ],
        bAutoWidth: false,
        resposive: true
        });
	});
</script>
<?php require '../../../views/partials/footer.php' ?>