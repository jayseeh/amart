<?php 
	include '../../../db.sql/connect_db.php'; 
	require '../../partials/header.php'; 
	require 'nav.php'; 

?>

<div class="main">
	<div class="container">
	<br>
	<?php
	$userID = $_SESSION['userID'];
	$sql = "SELECT * from accounts where aID = '$userID'";
	$query = mysqli_query($connect, $sql);
	$count = mysqli_num_rows($query);
	while ($fetchdata = mysqli_fetch_array($query))  {
		$address = explode(" | ", $fetchdata['uAddress']);
		$address = implode(", ", $address);	
		$uPic = $fetchdata['uPic'];
		$status = $fetchdata['aStatus'];
	?>
	<div class="row">
		<div class="col s12 m6 l6 center-align">
			<a href="#updatephoto" class="modal-trigger"><img src="../../../assets/images/<?php echo $uPic; ?>" class="user-photo"></a>
			<h4><?php 
			$aType = $fetchdata['aType'];
			$uname = $fetchdata['username'];
			$uID = $fetchdata['aID'];
			$fname = $fetchdata['fname'];
			$mname = $fetchdata['mname'];
			$minitial = substr($mname,0, 1);
			$lname = $fetchdata['lname'];
			echo $fullname = $fname.' '.$minitial.'. '.$lname;
			?></h4>
		</div>
		<div class="col s12 m6 l6">
			<div class="section hide-on-small-only"></div>
			<p><label for="address">Address</label><span class="profile-details" id="address"><?php echo $address;?></span>
			<br>
			<label for="gender">Gender</label><span class="profile-details" id="gender"><?php echo $fetchdata['uGender'];?></span>
			<br>
			<label for="phone">Phone</label><span class="profile-details" id="phone"><?php echo $fetchdata['phoneNumber'];?></span>
			<br>
			<label for="email">Email</label><span class="profile-details" id="email"><?php echo $fetchdata['uEmail'];?></span>
			<br>
			<br>
			<a href="#changepassword" class="modal-trigger btn btn-small red darken-3 right">Change Password</a>
			</p>
		</div>
	</div>
	<?php } ?>
	<div class="row center-align">
		<a href="#update" name="update" class="modal-trigger col s12 btn btn-large waves-effect yellow darken-3" id="user_editID">Update<a>
	</div>

	<div class="modal" id="updatephoto">
		<form action="../../../db.sql/updateprofpic.php" method="POST" enctype="multipart/form-data" >
		<div class="modal-content">
			<h4>
				<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
				Change Photo
			</h4>
			<div class="row">
				<div class="col s12">
					<div class="file-field input-field">
						<div class="btn btn-flat white yellow-text text-darken-3">
							<i class="material-icons">camera</i>
							<input id="pic" type="file" name="profpic" class="validate" accept="image/*">
							
						</div>
						<div class="file-path-wrapper">
							<input type="text" class="file-path validate" placeholder="Upload Photo">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="ID" value='<?php echo $_SESSION['userID']?>'>
				<input type="hidden" name="type" value='<?php echo $_SESSION['aType']?>'>
				<input type="hidden" name="oldpath" value='<?php echo $uPic; ?>'>
				<input type="submit" name='submit' value="Upload" class="btn yellow darken-3">
			</div>
		</div>
		</form>
	</div>

	<div class="modal" id="changepassword">
		<form action="../../../db.sql/updatepassword.php" method="POST" id="form">
		<div class="modal-content">
			<h4>
				<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
				<input type='hidden' name='uID' value='<?php echo $uID?>'> Password
			</h4>
			<div class="row">
				<div class="col s12">
					<div class="input-field">
						<input type="password" id="currentpass" autofocus  minlength=6 name="currentpass" class="validate" required>
						<label for="currentpass">Current Password</label>
					</div>
				</div>
				<div class="col s12">
					<div class="input-field">
						<input type="password" id="newpass" minlength=6 required>
						<label for="newpass">New Password</label>
					</div>
				</div>
				<div class="col s12">
					<div class="input-field">
						<input type="password" id="confirmpass" name="confirmpass" minlength=6 required>
						<label for="confirmpass">Confirm Password</label>
					</div>
				</div>
				<div class="col s12">
					<span class="error" style="color:red"></span><br />
				</div>
			</div>
			<div class="modal-footer">
				<input type='hidden' name='aType' id= "aType" value='<?php echo $aType;?>'>
				<input type='hidden' name='uname' id= "uName" value='<?php echo $uname;?>'>
				<input type='hidden' name='uID' id= "uID" value='<?php echo $uID;?>'>
				<input type="submit" value="Update" class="btn yellow darken-3">
			</div>
		</div>
		</form>
	</div>

	</div>
</div>

<!-- Getting Started -->
<div id="getting-started" class="modal">
		<form action="../../../db.sql/updateSecretQuestion.php" method='post'>
        <div class="modal-content">
            <h4>
                Update Your Secret Question
            </h4>
            <p class="center-align"><em>If you lose your password, you can use this to retrieve your account.</em></p>
            <!-- <div class="row">
				<div class="col s12 m12 l12 input-field">
					<label for="email">Email Address</label>
					<input type="email" name="email" class="validate" required autofocus/>
				</div>
			</div> -->
			<div class="row">
				<div class="col s12 m12 l12">  
					<label for="secretquestion">Secret Question</label>
					<select name="secretquestion">
						<option value="" disabled selected>--Choose Secret Question--</option>
						<?php
							$query = "SELECT content FROM secretquestions";
							$fetchquery = mysqli_query($connect, $query);
							while($fetchdata = mysqli_fetch_array($fetchquery)) {

						?>
						<option value="<?php echo $fetchdata['content']; ?>"><?php echo $fetchdata['content']; ?></option>
							<?php } ?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m12 l12 input-field">
					<label for="secretanswer">Answer</label>
					<input type="text" name="secretanswer" class="validate" required />
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<input type='hidden' name='ID' value='<?php echo $_SESSION['userID'];?>'>
			<input type='hidden' name='type'value='<?php echo $_SESSION['aType'];?>'>
			<input type="submit" value="Proceed" class="btn yellow darken-3">
            </form>
		</div>
    </div>

</div>
<!-- End of Getting Started -->
<?php 
	require '../../partials/footer.php'; 
	include 'user_update.php'; 
?>

<script>
	var submit = false;
	$(document).ready(function(){
		$('#currentpass').keyup(function(e){
			$.ajax({
				//ajax functions here
				url: "../../../db.sql/checkpassword.php",
				method: "POST",
				dataType: "json",
				data: {
					'username' : $('#uName').val(),
					'password' : $('#currentpass').val()
				},
				success: 
					function(data){
						console.log(data);
						if(data.result == "true"){
							$('.error').text('Current Password Correct').removeClass("red-text").addClass("green-text");
							submit = true;
						} else {
							$('.error').text('Current Password Incorrect').removeClass("green-text").addClass("red-text");
							submit = false;
						}
					},
				error: 
					function (xhr, ajaxOptions, thrownError) {
						console.log('Error', JSON.stringify(xhr) + " " + thrownError, 'error');
					}
			});
		});

		$('#newpass').keyup(function(e){
			var newpass = $(this).val();
			var confirmpass = $('#confirmpass').val();
			if( (newpass.length >= 6 && confirmpass.length >=6 ) || confirmpass != ""){
				if(newpass == confirmpass){
					$('.error').text('MATCHED').removeClass("red-text").addClass("green-text");
					submit = true;
				}else{
					$('.error').text('NOT MATCHED').removeClass("green-text").addClass("red-text");
					submit = false;
				}
			}
		});

		$('#confirmpass').keyup(function(e){
			var newpass = $('#newpass').val();
			var confirmpass = $(this).val();
			if( (newpass.length >= 6 && confirmpass.length >=6) || newpass != ""){
				if(newpass == confirmpass){
					$('.error').text('MATCHED').removeClass("red-text").addClass("green-text");
					submit = true;
				}else{
					$('.error').text('NOT MATCHED').removeClass("green-text").addClass("red-text");
					submit = false;
				}
			}
		});
		

		$('#form').submit(function(){
			var checkIfEmpty = $('#newpass').val() != "" && $('#confirmpass').val() != "" && $('#currentpass').val() != "" ;
			if(submit && checkIfEmpty){
				return true;
			}else{
				return false;
			}
		});

		<?php 
			if(isset($_GET['new'])){
		?>
				$('#update').modal('open');
				$('#user_editID').click();
		<?php
			}
			if($status == "Active"){
		?>
				$('#getting-started').modal('open'); 
		<?php
			}
		?>
	});
</script>
