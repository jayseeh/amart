<?php 

require '../../partials/header.php'; 
require 'nav.php'; 


$ownerID = $_SESSION['userID'];
$bisnesID = $_GET['bID'];
$status = 'Free';

$busName = '';
$busAddNo = '';
$busAddStreet = '';
$busAddBrgy = '';
$busAddCity = '';
$busAddProv = '';
$busDescription = '';
$busCompAddress = '';

$sql = "SELECT * from business INNER JOIN businesslocation ON businesslocation.bisLocIDkey = business.businessID WHERE businessOwner = '$ownerID' AND businessID = '$bisnesID'";
$query = mysqli_query($connect, $sql);
$count = mysqli_num_rows($query);
while ($fetchdata = mysqli_fetch_array($query))  {
    $busName = $fetchdata['businessName'];
    $busAddNo = $fetchdata['bisNumber'];
    $busAddStreet = $fetchdata['bisStreet'];
    $busAddBrgy = $fetchdata['bisBrgy']; 
    $busAddCity = $fetchdata['bisCityTown'];
    $busAddProv = $fetchdata['bisProvince'];
    $busDescription = $fetchdata['businessDescription'];
    $busPhone = $fetchdata['businessPhone'];
    $busFB = $fetchdata['businessFB'];

    $busCompAddress = $busAddNo . ' ' . $busAddStreet . ' ' . $busAddBrgy . ' ' . $busAddCity . ' ' . $busAddProv;
    $busCompAddress = strtolower($busCompAddress);
    $busCompAddress = ucwords($busCompAddress);

    $provinceCode = $fetchdata['provCode'];
    $cityCode = $fetchdata['munCode'];
    $brgyCode = $fetchdata['brgyCode'];

    $subsql = "SELECT * FROM subscription WHERE bisSubIDkey = $bisnesID AND subStatus = 'Pending'";
    $subquery = mysqli_query($connect, $subsql);
    $subcount = mysqli_num_rows($subquery);
}
?>
<div class="main">
    <div class="section top-section"></div>
    <div class="row">
        <div class="col s12 m12 l12">
            <?php 
                $querycheckcategory = mysqli_query($connect, "SELECT * FROM business inner join categories on business.businessCategory= categories.categoryID WHERE businessID = $bisnesID");
                $cats = mysqli_fetch_array($querycheckcategory);
                if($cats['categoryType']=='Free' && $subcount == 0){
                    echo '<p class="center-align"><em>This business is currently at a free subscription <br><a href="upgrade.php?bID='. $bisnesID .'"class="yellow-text text-darken-3">Upgrade now to have more Features</a></em></p>';
                } elseif($cats['categoryType']=='Free' && $subcount > 0) {
                    echo '<p class="center-align"><em>This business is currently at a free subscription <br><span class="yellow-text text-darken-3"> Confirmation of Upgrade is in Progress </span> </em></p>';    
                }
                ?>
            <div class="card">
                <div class="card-image card-business-photo">
                    <div class="carousel carousel-slider">
                        <?php 
                            $counter = 0;
                            $sqll = "SELECT * FROM businessphotos WHERE bisPhotoIDKey = '$bisnesID'";
                            //$query = mysqli_query($connect, $sql);
                            $queryy = mysqli_query($connect, $sqll);
                            $counter = mysqli_num_rows($queryy);
                            /*while($fetchPhotoData = mysqli_fetch_array($query)){
                                $counter = $fetchPhotoData['countBisPhotos'];
                            }*/
                            if($counter == 0){
                            ?>
                        <a class="carousel-item"><img src="../../../assets/images/default-image.png"></a>
                        <?php 
                            }else{ 
                                $sqla = "SELECT * FROM businessphotos WHERE bisPhotoIDKey = '$bisnesID'";
                                $querya = mysqli_query($connect, $sqla);
                                while($fetchPhotoData = mysqli_fetch_array($querya)){ ?>
                        <a class="carousel-item modal-trigger" height="100%" href="#viewbisphoto-<?php echo $fetchPhotoData['photoID']; ?>"><img src="../../../assets/images/<?php echo $fetchPhotoData['bisPhoto']; ?>" height="100%"></a>
                        <?php 
                                }
                            } ?>
                    </div>
                    <a href="#add-photo-modal" class="btn-floating modal-trigger halfway-fab waves-effect waves-light yellow darken-3" id="addphoto"><i class="material-icons">add_a_photo</i></a>
                </div>
                <div class="card-content">
                    <span class="card-title">
                        <h5>
                            <?php echo $busName; ?> <a href="#editnameadd" id="owner_editID" class="modal-trigger"><i class="material-icons">edit</i></a></h5>
                    </span>
                    <p>Address:
                        <?php echo $busCompAddress; ?>
                    </p>
                    <p>Phone:
                        <?php $check = $busPhone;
                        if($check == ''){
                            echo 'n/a';
                        }else{
                            echo $check;
                        }
                        ?>
                    </p>
                    <p>FB Page:
                        <?php $check =  $busFB;
                        if($check == ''){
                            echo 'n/a';
                        }else{
                            echo "<a href='$check' target='_blank'>".$check."</a>";
                        }
                        ?>
                    </p>
                    <table>
                        <thead>
                            <tr>
                                <th>Schedule: <a href="addsched.php?bID=<?php echo $bisnesID; ?>">Add Schedule</a><br></th>
                                <th>Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $sql = "SELECT * from businesscalendar where bisCalendarIDkey = '$bisnesID'";
                                $query = mysqli_query($connect, $sql);
                                $count = mysqli_num_rows($query);
                                while ($fetchdata = mysqli_fetch_array($query))  {?>
                            <tr data-toggle="modal" data-target="#removeschedule">
                                <td>
                                    <?php echo $fetchdata['bisSchedDay']?>
                                </td>
                                <td>
                                    <?php echo $fetchdata['bisSchedTime']?>
                                </td>
                                <td><a href="#removeschedule<?php echo $fetchdata['calendarID']; ?>" class="modal-trigger"><i class="material-icons">delete</i></a></td>
                            </tr>
                            <!-- Remove Schedule Modal -->
                            <div class="modal" id="removeschedule<?php echo $fetchdata['calendarID']; ?>">
                                <div class="modal-content">
                                    <h4>
                                        <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                                        <span>Remove Schedule</span>
                                        <div class="divider"></div>
                                    </h4>
                                    <h5 class="center-align">Are you sure you want to remove this schehule?</h5>
                                    <p class="center-align">
                                        <?php echo $fetchdata['bisSchedDay']?> <br>
                                        <?php echo $fetchdata['bisSchedTime']?>
                                    </p>
                                    <form name="removesched" id="removesched" action="../../../db.sql/deletesched.php" method="post">
                                        <input type="hidden" name="calendarID" value="<?php echo $fetchdata['calendarID']; ?>">
                                        <input type="hidden" name="bisnessID" value="<?php echo $bisnesID; ?>">
                                        <?php 
                                            echo "<input type='hidden' name='page' value='singlefree.php'>";
                                        ?>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="submit" form="removesched" class="btn-flat btn-floating yellow darken-3 modal-trigger"><i class="large material-icons">check</i></a>
                                    </center>
                                </div>
                            </div>
                            <?php } ?>
                        </tbody>
                    </table>
                    <h5>Businesss Description <a href="#editoverview" class="modal-trigger"><i class="material-icons">edit</i></a></h5>
                    <?php 
                            $sql = "SELECT * from business where businessID = '$bisnesID'";
                            $query = mysqli_query($connect, $sql);
                            $count = mysqli_num_rows($query);
                            while ($fetchdata = mysqli_fetch_array($query))  {?>
                    <p>
                        <?php 
                            $checkDesc =  $fetchdata['businessDescription'];
                            if ($checkDesc == '') {
                                echo "Description is empty";
                            } else {
                                $desc = $checkDesc;
                                $desc = preg_replace("/[\n]/", "<br>\n", $desc);
                                echo $desc;
                            }
                            ?>
                    </p>
                    <?php 
                            } 
                        ?>
                    <h5>Products<span><a href="#addproduct" style="margin-left: 10px; font-size: 12px;" class="modal-trigger btn btn-small yellow darken-3">Add</a></span></h5>
                    <div class="row">
                        <?php 
                             $fetchquery= mysqli_query($connect, "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Product' and businessPSstatus = 'Active'"); 
                            while($fetchproduct = mysqli_fetch_array($fetchquery)){
                                ?>
                        <a href="#productview<?php echo $fetchproduct['businessPSID']; ?>" class="modal-trigger">
                            <div class="col s6 m4 l4">
                                <div class="card card-product center-align">
                                    <div class="card-image card-product-photo">
                                        <img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage'] ;?>" alt="<?php echo $fetchproduct['businessPSName']; ?>">
                                        <span class="card-title card-product-title truncate">
                                            <?php echo $fetchproduct['businessPSName']; ?></span>
                                    </div>
                                    <div class="card-content card-product-content">
                                        <span class="product-price"><small>Php</small>
                                            <?php echo $fetchproduct['businessPSPrice']; ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                                }
                            ?>
                    </div>
                    <h5>Services<span><a href="#addservice" style="margin-left: 10px; font-size: 12px;" class="modal-trigger btn btn-small yellow darken-3">Add</a></span></h5>
                    <div class="row">
                        <?php
                            $fetchquerySer= mysqli_query($connect, "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Service' and businessPSstatus = 'Active'"); 
                            while($fetchservice = mysqli_fetch_array($fetchquerySer)){
                                ?>
                        <div class="col s6 m4 l4">
                            <a href="#serviceview<?php echo $fetchservice['businessPSID']; ?>" class="modal-trigger">
                                <div class="card card-product center-align">
                                    <div class="card-image card-product-photo">
                                        <img src="../../../assets/ps/<?php echo $fetchservice['businessPSImage'] ;?>" alt="<?php echo $fetchservice['businessPSName']; ?>">
                                        <span class="card-title card-product-title truncate">
                                            <?php echo $fetchservice['businessPSName']; ?></span>
                                    </div>
                                    <div class="card-content card-product-content">
                                        <span class=":w"><small>Php</small>
                                            <?php echo $fetchservice['businessPSPrice']; ?></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php
                                }
                            ?>
                    </div>
                    <h5>Archived Products</h5>
                    <div class="row">
                        <?php 
                                $fetchquery= mysqli_query($connect, "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Product' and businessPSstatus = 'Archived'"); 
                                while($fetchproduct = mysqli_fetch_array($fetchquery)){
                            ?>
                        <a href="#archivedproductview<?php echo $fetchproduct['businessPSID']; ?>" class="modal-trigger">
                            <div class="col s6 m4 l4">
                                <div class="card card-product center-align">
                                    <div class="card-image card-product-photo">
                                        <img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage'] ;?>" alt="<?php echo $fetchproduct['businessPSName']; ?>">
                                        <span class="card-title card-product-title truncate">
                                            <?php echo $fetchproduct['businessPSName']; ?></span>
                                    </div>
                                    <div class="card-content card-product-content">
                                        <span class="product-price"><small>Php</small>
                                            <?php echo $fetchproduct['businessPSPrice']; ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php } ?>
                    </div>
                    <h5>Archived Services</h5>
                    <div class="row">
                        <?php 
                                $fetchquery= mysqli_query($connect, "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Service' and businessPSstatus = 'Archived'"); 
                                while($fetchproduct = mysqli_fetch_array($fetchquery)){
                            ?>
                        <a href="#archivedserviceview<?php echo $fetchproduct['businessPSID']; ?>" class="modal-trigger">
                            <div class="col s6 m4 l4">
                                <div class="card card-product center-align">
                                    <div class="card-image card-product-photo">
                                        <img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage'] ;?>" alt="<?php echo $fetchproduct['businessPSName']; ?>">
                                        <span class="card-title card-product-title truncate">
                                            <?php echo $fetchproduct['businessPSName']; ?></span>
                                    </div>
                                    <div class="card-content card-product-content">
                                        <span class="product-price"><small>Php</small>
                                            <?php echo $fetchproduct['businessPSPrice']; ?></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                                }
                            ?>
                    </div>
                </div>
                <div class="card-action center-align">
                    <a href="confirmlocation.php" class="modal-trigger" id="editbusiness">Locate</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Edit Name and Address Modal -->
<div class="modal" id="editnameadd">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <span>Edit Information</span>
            <div class="divider"></div>
        </h4>
        <form name="editname" id="editname" action="../../../db.sql/updatebusinessdetails.php" method="POST">
            <?php 
                        $sql = "SELECT * from business INNER JOIN categories on business.businessCategory = categories.categoryID where businessID = '$bisnesID'";
                        $query = mysqli_query($connect, $sql);
                        $count = mysqli_num_rows($query);
                        while ($fetchdata = mysqli_fetch_array($query))  {
                        $phone = $fetchdata['businessPhone'];
                        $fb = $fetchdata['businessFB'];    
                            
                        ?>
            <div class="row">
                <div class="col s12 m12 l12 input-field">
                    <label for="newBusinessName">Business Name</label>
                    <input type="text" name="newBusinessName" value="<?php echo $fetchdata['businessName'];?>" autofocus required>
                    <input type="hidden" name="brgy1" id="brgy1">
                    <input type="hidden" name="mun1" id="mun1">
                    <input type="hidden" name="province1" id="province1">
                    <input type='hidden' name='businessID' value='<?php echo $_GET['bID'];?>'>
                    <input type='hidden' name='page' value='singlefree.php'>
                </div>
            </div>
            <?php } ?>
            <?php 
                $sql = "SELECT * from businesslocation where bisLocIDkey = '$bisnesID'"; 
                $query = mysqli_query($connect, $sql);
                $count = mysqli_num_rows($query);
                while ($fetchdata = mysqli_fetch_array($query)){ 
                    $bisNumber =  $fetchdata['bisNumber'];
                    $bisStreet = $fetchdata['bisStreet'];
                }
            ?>
            <div class="row">
                <div class="col s12 m4 l4">
                    <label for="newProvince">Province</label>
                    <select name="owner_province" id="owner_province" class="browser-default"></select>
                </div>
                <div class="col s12 m4 l4">
                    <label for="newCity">City/Town</label>
                    <select name="owner_city" id="owner_city" class="browser-default"></select>
                </div>
                <div class="col s12 m4 l4">
                    <label for="newBrgy">Brgy</label>
                    <select name="owner_brgy" id="owner_brgy" class="browser-default"></select>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 l6 input-field">
                    <label for="newNumber">Number</label>
                    <input type="text" name="newNumber" value="<?php echo $bisNumber; ?>">
                </div>
                <div class="col s12 m6 l6 input-field">
                    <label for="newStreet">Street</label>
                    <input type="text" name="newStreet" value="<?php echo $bisStreet; ?>">
                </div>
            </div>
            <div class='row'>
                <div class='input-field col s12'>
                    <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">phone</i>
                    <input class='validate' type='text' name='bnum' id='bnum' value="<?php $check =  $phone;
                            if($check == ''){
                                echo '';
                            }else{
                                echo $check;
                            }
                            ?>" required />
                    <label for='bnum'>Business Telephone No</label>
                </div>
            </div>
            <?php
            if($cats['categoryType']!='Free'){
                // echo "<div class='row'><div class='input-field col s12'><i class='material-icons prefix yellow-text text-darken-3 hide-on-small-only'>email</i><input class='validate' type='text' name='email' id='email' /><label for='email'>Email</label></div></div>";
            }
            ?>
            <div class='row'>
                <div class='input-field col s12'>
                    <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">website</i>
                    <input class='validate' type='text' name='fb' id='fb' value="<?php $check =  $fb;
                            if($check == ''){
                                echo '';
                            }else{
                                echo $check;
                            }
                            ?>" required />
                    <label for='fb'>Facebook Page</label>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <center>
            <div class="row center-align">
                <div class="col s12 m12 l12 input-field">
                    <input type="submit" class="btn yellow darken-3" value="Update">
                </div>
            </div>
        </center>
    </div>
</div>
</form>
<!-- Edit Business Description -->
<div class="modal" id="editoverview">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <span>Edit Description</span>
            <div class="divider"></div>
        </h4>
        <form name="overview" id="overview" action="../../../db.sql/updatebusinessdescription.php" method="POST">
            <div class="row">
                <div class="col s12 input-field">
                    <?php 
                            $sql = "SELECT * from business INNER JOIN categories on business.businessCategory = categories.categoryID where businessID = '$bisnesID'";
                            $query = mysqli_query($connect, $sql);
                            $count = mysqli_num_rows($query);
                            while ($fetchdata = mysqli_fetch_array($query))  {?>
                    <p>
                        <label for="newDescription">Business Description</label>
                            <?php
                                $desc = $fetchdata['businessDescription'];
                            ?>
                        <textarea class="materialize-textarea" name="newDescription" autofocus><?php echo $desc; ?></textarea>
                        <input type='hidden' name='businessID' value='<?php echo $bisnesID;?>'>
                        <?php 
                            echo "<input type='hidden' name='page' value='singlefree.php'>";
                        ?>
                        <?php } ?>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <center>
            <div class="row center-align">
                <div class="col s12 m12 l12 input-field">
                    <input type="submit" class="btn yellow darken-3" value="Update">
                </div>
            </div>
        </center>
        </form>
    </div>
</div>
<!-- Add Photo Modal -->
<div class="modal" id="add-photo-modal">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <span>Add Photo</span>
            <div class="divider"></div>
        </h4>
        <div class="row">
            <form name="add_photo" id="add_photo" action="../../../db.sql/insertphoto.php" method="post" enctype="multipart/form-data">
                <div class="file-field input-field">
                    <div class="btn btn-flat yellow darken-3 white-text">
                        <i class="material-icons">search</i>
                        <input type="file" name="file" accept="image/*">
                    </div>
                    <div class="file-path-wrapper">
                        <input type="text" class="file-path validate" placeholder="Upload Photo" id="photo">
                    </div>
                </div>
                <input type='hidden' name='businessID' value='<?php echo $bisnesID;?>'>
                <?php 
                    echo "<input type='hidden' name='page' value='singlefree.php'>";
                ?>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <center>
            <a href="#confirm_photo" id="photocheck" class="modal-trigger btn-flat btn-floating yellow darken-3"><i class="large material-icons">check</i></a>
        </center>
    </div>
</div>
<!-- Add Photo Confirmation Modal -->
<div class="modal" id="confirm_photo">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <span>Confirmation</span>
            <div class="divider"></div>
        </h4>
        <p class="center-align">Are you sure you want to upload the photo?</p>
    </div>
    <div class="modal-footer">
        <center>
            <button form="add_photo" name="submit" value="submit" class="btn-flat btn-floating yellow darken-3"><i class="large material-icons">check</i></a>
                <div class="section"></div>
        </center>
    </div>
</div>
<!-- Add Product Modal -->
<div id="addproduct" class="modal">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <span>Add Product</span>
            <hr>
        </h4>
        <form action="../../../db.sql/insertPS.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="businessID" value="<?php echo $bisnesID ; ?>">
            <div class="row">
                <div class="input-field col s12 l12">
                    <label for="pname">Product Name</label>
                    <input type="text" id="pname" name="pname" autofocus required>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 l12">
                    <label for="price">Price</label>
                    <input type="number" id="price" name="price" required>
                </div>
            </div>
            <input type="hidden" id="qty" name="qty" value='1'>
            <input type="hidden" name="homeservice" value='Nope'>
            <div class="row">
                <div class="input-field col s12 l12">
                    <label for="description">Description</label>
                    <textarea class='materialize-textarea' type='text' name='description' id='description' required></textarea>
                </div>
            </div>
            <div class="row">
                <div class="file-field input-field">
                    <div class="btn btn-flat yellow darken-3 white-text">
                        <i class="material-icons">search</i>
                        <input type="file" name="photo" accept="image/*">
                    </div>
                    <div class="file-path-wrapper">
                        <input type="text" class="file-path validate" placeholder="Upload Photo" id="photo">
                    </div>
                </div>
            </div>
            <center>
                <input type="submit" value="Add Product" class="modal-close btn-flat yellow darken-3" name="addPS">
            </center>
            <?php 
                echo "<input type='hidden' name='page' value='singlefree.php'>";
            ?>
        </form>
    </div>
</div>
<!-- Add Service Modal -->
<div id="addservice" class="modal">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <span>Add Service</span>
            <hr>
        </h4>
        <form action="../../../db.sql/insertPS.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="businessID" value="<?php echo $bisnesID ; ?>">
            <div class="row">
                <div class="input-field col s12 l12">
                    <label for="pname">Service Name</label>
                    <input type="text" id="pname" name="pname" autofocus required>
                </div>
            </div>
            <?php 
                echo "<input type='hidden' name='page' value='singlefree.php'>";
            ?>
            <div class="row">
                <div class="input-field col s12 l12">
                    <label for="price">Price</label>
                    <input type="number" id="price" required name="price">
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 l12">
                    <label for="description">Description</label>
                    <textarea class='materialize-textarea' type='text' name='description' id='description' required></textarea>
                </div>
            </div>
            <div class="row">
                <div class="file-field input-field">
                    <div class="btn btn-flat yellow darken-3 white-text">
                        <i class="material-icons">search</i>
                        <input type="file" name="photo" accept="image/*">
                    </div>
                    <div class="file-path-wrapper">
                        <input type="text" class="file-path validate" placeholder="Upload Photo" id="photo">
                    </div>
                </div>
            </div>
            <center>
                <input type="submit" value="Add Service" class="modal-close btn-flat yellow darken-3" name="addService">
            </center>
        </form>
    </div>
</div>
<!-- Edit Confirmation Modal -->
<div class="modal" id="confirm_edit">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <span>Confirmation</span>
            <hr>
        </h4>
        <p class="center-align">Are you sure you want to save the changes?</p>
    </div>
    <div class="modal-footer">
        <center>
            <button form="edit" value="submit" class="btn-flat btn-floating yellow darken-3"><i class="large material-icons">check</i></a>
                <div class="section"></div>
        </center>
    </div>
</div>
</div>
<!-- View Business Photo Modal -->
<?php
    $sql = "SELECT * FROM businessphotos WHERE bisPhotoIDKey = '$bisnesID'";
    $query = mysqli_query($connect, $sql);
    while($fetchPhotoData = mysqli_fetch_array($query)){ ?>
<div class="modal modal-fixed-footer modal-business-photo" id="viewbisphoto-<?php echo $fetchPhotoData['photoID']; ?>">
    <div class="modal-content">
        <img class="materialboxed" src="../../../assets/images/<?php echo $fetchPhotoData['bisPhoto']; ?>">
    </div>
    <div class="modal-footer">
        <a href="#confirm-delete-<?php echo $fetchPhotoData['photoID']; ?>" class="modal-trigger btn btn-flat yellow darken-3 white-text">Delete</a>
    </div>
</div>
<!-- Delete Business Photo Confirmation Modal -->
<div class="modal" id="confirm-delete-<?php echo $fetchPhotoData['photoID']; ?>">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <span>Confirmation</span>
            <div class="divider"></div>
        </h4>
        <p class="center-align">Are you sure you want to delete this photo?</p>
    </div>
    <div class="modal-footer">
        <center>
            <form action="../../../db.sql/deletecoverphoto.php" method="POST">
                <input type="hidden" name="businessPhotoID" value="<?php echo $fetchPhotoData['photoID']; ?>">
                <input type="hidden" name="businessID" value="<?php echo $bisnesID; ?>">
                <?php 
                    echo "<input type='hidden' name='page' value='singlefree.php'>";
                ?>
                <input type="submit" name="submit" value="Delete" class="btn btn-flat yellow darken-3 white-text">
            </form>
        </center>
    </div>
</div>
<?php
    }
    ?>
<!-- PRODUCTS VIEW MODAL -->
<?php 
        $fetchquery= mysqli_query($connect, "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Product'"); 
        while($fetchproduct = mysqli_fetch_array($fetchquery)){
            $checkDesc = $fetchproduct['businessPSDesc'];
            if ($checkDesc == '') {
                echo "<em>Not Available</em>";
            } else {
                $desc = $checkDesc;
                $desc = preg_replace("/[\n]/", "<br>\n", $desc);
            }
    ?>
<!-- View Product Modal -->
<div class="modal modal-fixed-footer  overflow-modal" id="productview<?php echo $fetchproduct['businessPSID']; ?>">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Product Details
        </h4>
        <div class="row">
            <div class="col s12 m6 l6">
                <a href="#photo-update-<?php echo $fetchproduct['businessPSID']; ?>" class="modal-trigger"><img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage']; ?>" class="col s12 m12 l12" alt="product"></a>
            </div>
            <div class="col s12 m6 l6">
                <small>Name:</small><br> <strong>
                    <?php echo $fetchproduct['businessPSName']; ?></strong><br>
                <small>Description:</small><br> <strong>
                    <?php echo $desc; ?></strong><br>
                <small>Price:</small><br> <strong>Php
                    <?php echo $fetchproduct['businessPSPrice']; ?></strong><br>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row center-align">
            <div class="col s6 m6 l6">
                <a href="#archive-<?php echo $fetchproduct['businessPSID']; ?>" class="modal-trigger btn red darken-3 col s12 m12 l12">Archive</a>
            </div>
            <div class="col s6 m6 l6">
                <a href="#editproduct-<?php echo $fetchproduct['businessPSID']; ?>" class="modal-trigger btn yellow darken-3  col s12 m12 l12">Edit</a>
            </div>
        </div>
    </div>
</div>
<!-- Archived Product Modal -->
<div class="modal modal-fixed-footer  overflow-modal" id="archivedproductview<?php echo $fetchproduct['businessPSID']; ?>">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Archived Product Details
        </h4>
        <div class="row">
            <div class="col s12 m6 l6">
                <a href="#photo-update-<?php echo $fetchproduct['businessPSID']; ?>" class="modal-trigger"><img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage']; ?>" class="col s12 m12 l12" alt="product"></a>
            </div>
            <div class="col s12 m6 l6">
                <small>Name:</small><br> <strong>
                    <?php echo $fetchproduct['businessPSName']; ?></strong><br>
                <small>Description:</small><br> <strong>
                    <?php echo $desc; ?></strong><br>
                <small>Price:</small><br> <strong>Php
                    <?php echo $fetchproduct['businessPSPrice']; ?></strong><br>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row center-align">
            <div class="col s6 m6 l6">
                <a href="#unarchive-<?php echo $fetchproduct['businessPSID']; ?>" class="modal-trigger btn green darken-3 col s12 m12 l12">Unarchive</a>
            </div>
        </div>
    </div>
</div>
<!-- Edit Featured Photo Modal -->
<div class="modal" id="photo-update-<?php echo $fetchproduct['businessPSID']; ?>">
    <div class="modal-content">
        <h5>
            <div class="row">
                <div class="col s2 m1 l1">
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                </div>
                <div class="col s10 m11 l11">
                    Change Featured Photo1
                </div>
            </div>
        </h5>
        <div class="divider"></div>
        <div class="section"></div>
        <div class="row center-align">
            <a href="#upload-featured-<?php echo $fetchproduct['businessPSID']; ?>" class="btn yellow darken-3 modal-trigger">Upload</a>
        </div>
        <div class="row">
            <?php
                $qwe = $fetchproduct['businessPSID'];
                $sql = "SELECT * from imagelogs where pixLogIDKey = '$qwe'";
                $query = mysqli_query($connect, $sql);
                $count = mysqli_num_rows($query);
                while ($fetchdata = mysqli_fetch_array($query))  { 
            ?>
            <div class="col s6 m4 l4 primary-photo">
                <div class="card card-featured-options">
                    <div class="card-content">
                        <a href="#confirm-photo-<?php echo $fetchdata['logID']; ?>" class="modal-trigger"><img src="../../../assets/ps/<?php echo $fetchdata['picName'] ;?>" alt="featured product"></a>
                    </div>
                </div>
                <!-- Featured Photo Options Modal -->
                <div class="modal modal-fixed-footer overflow-modal" class="blocked-modal" id="confirm-photo-<?php echo $fetchdata['logID']; ?>">
                    <div class="modal-content">
                        <h5>
                            <div class="row">
                                <div class="col s2 m1 l1">
                                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                                </div>
                                <div class="col s10 m11 l11">
                                    Saving Changes
                                </div>
                            </div>
                        </h5>
                        <div class="divider"></div>
                        <p>Are you sure you want to update the current featured photo with this photo?</p>
                        <img src="../../../assets/ps/<?php echo $fetchdata['picName'] ;?>">
                    </div>
                    <div class="modal-footer">
                        <div class="row center-align">
                            <form action="../../../db.sql/updatePSexistingphoto.php" method="post">
                                <input type="hidden" name="businessID" value="<?php echo $bisnesID; ?>">
                                <input type="hidden" name="productID" value="<?php echo $fetchdata['pixLogIDKey']; ?>">
                                <input type="hidden" name="picName" value="<?php echo $fetchdata['picName'] ;?>">
                                <input type="hidden" name="photoID" value="<?php echo $fetchdata['logID']; ?>">
                                <?php 
                                    echo "<input type='hidden' name='page' value='singlefree.php'>";
                                ?>
                                <input type="submit" name="submit" value="Confirm" class="btn yellow darken-3">
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End of modal -->
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- Upload Featured product Photo Modal -->
<div class="modal" id="upload-featured-<?php echo $fetchproduct['businessPSID']; ?>">
    <div class="modal-content">
        <h5>
            <div class="row">
                <div class="col s2 m2 l2">
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                </div>
                <div class="col s10 m10 l10">
                    Upload Featured Photo
                </div>
            </div>
        </h5>
        <div class="divider"></div>
        <div class="section"></div>
        <form action="../../../db.sql/updatePSphoto.php" method="post" enctype="multipart/form-data">
            <div class="file-field input-field">
                <div class="btn btn-flat yellow darken-3 white-text">
                    <i class="material-icons">search</i>
                    <input type="file" name="image" accept="image/*">
                    
                </div>
                <div class="file-path-wrapper">
                    <input type="text" class="file-path validate" placeholder="Upload Photo">
                </div>
            </div>
            
            <?php 
                echo "<input type='hidden' name='page' value='singlefree.php'>";
            ?>
            <div class="modal-footer center-align">
                <input type="hidden" name='uploadtype' value="Product">
                <input type='hidden' name='businessID' value='<?php echo $bisnesID;?>'>
                <input type="hidden" name="productID" value="<?php echo $fetchproduct['businessPSID']; ?>">
                <input type="submit" name="submit" value="Save Changes" class="btn yellow darken-3">
            </div>
        </form>
    </div>
</div>
<!-- Edit Product Modal -->
<div class="modal modal-fixed-footer" id="editproduct-<?php echo $fetchproduct['businessPSID']; ?>">
    <form action="../../../db.sql/updateproduct.php" method="post">
        <div class="modal-content">
            <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                <span>Edit:
                    <?php echo $fetchproduct['businessPSName']; ?></span>
                <div class="divider"></div>
            </h4>
            <div class="container">
                <div class="row">
                    <div class="col s12">
                        <div class="input-field">
                            <label for="prodname">Product Name</label>
                            <input type="text" name="prodname" value="<?php echo $fetchproduct['businessPSName']; ?>" required autofocus />
                            <input type="hidden" name="servID" value="<?php echo $fetchproduct['businessPSID']; ?>" />
                            <input type="hidden" name="businessID" value="<?php echo $bisnesID ?>" />
                            <?php 
                                echo "<input type='hidden' name='page' value='singlefree.php'>";
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <div class="input-field">
                            <label for="prodnameprice">Price</label>
                            <input type="text" name="prodprice" value="<?php echo $fetchproduct['businessPSPrice']; ?>" required />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <div class="input-field">
                            <label for="proddesc">Product Description</label>
                            <textarea name="proddesc" class="materialize-textarea" required><?php echo $fetchproduct['businessPSDesc']; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn yellow darken-3" name="submit" value="Save Changes">
        </div>
    </form>
</div>
<!-- Archive Product Modal -->
<div class="modal modal-fixed-footer" id="archive-<?php echo $fetchproduct['businessPSID']; ?>">
    <form action="../../../db.sql/updatearchiveproduct.php" method="post">
        <div class="modal-content">
            <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                <span>Archive:
                    <?php echo $fetchproduct['businessPSName']; ?></span>
                <div class="divider"></div>
            </h4>
            <div class="container">
                <div class="row">
                    <div class="col s12 center-align">
                        Are you sure you want to archive this product?
                        <input type="hidden" name="itemID" value="<?php echo $fetchproduct['businessPSID']; ?>" />
                        <input type="hidden" name="businessID" value="<?php echo $bisnesID ?>" />
                        <?php 
                            if ($status == 'Free') {
                                echo "<input type='hidden' name='page' value='singlefree.php'>";
                            } else {
                                echo "<input type='hidden' name='page' value='single.php'>";
                        }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn yellow darken-3" name="submit" value="Confirm">
        </div>
    </form>
</div>
<!-- Unarchive Product Modal -->
<div class="modal modal-fixed-footer" id="unarchive-<?php echo $fetchproduct['businessPSID']; ?>">
    <form action="../../../db.sql/updateunarchiveproduct.php" method="post">
        <div class="modal-content">
            <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                <span>Archive:
                    <?php echo $fetchproduct['businessPSName']; ?></span>
                <div class="divider"></div>
            </h4>
            <div class="container">
                <div class="row">
                    <div class="col s12 center-align">
                        Are you sure you want to unarchive this product?
                        <input type="hidden" name="itemID" value="<?php echo $fetchproduct['businessPSID']; ?>" />
                        <input type="hidden" name="businessID" value="<?php echo $bisnesID ?>" />
                        <?php 
                            if ($status == 'Free') {
                                echo "<input type='hidden' name='page' value='singlefree.php'>";
                            } else {
                                echo "<input type='hidden' name='page' value='single.php'>";
                        }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn yellow darken-3" name="submit" value="Confirm">
        </div>
    </form>
</div>
<?php } ?>
<!-- SERVICES MODALS -->
<?php
    $fetchquerySer= mysqli_query($connect, "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Service' "); 
    while($fetchproductS = mysqli_fetch_array($fetchquerySer)){
        $checkDesc = $fetchproductS['businessPSDesc'];
            if ($checkDesc == '') {
                echo "<em>Not Available</em>";
            } else {
                $desc = $checkDesc;
                $desc = preg_replace("/[\n]/", "<br>\n", $desc);
            }
?>
<!-- View Service Modal -->
<div class="modal modal-fixed-footer overflow-modal" id="serviceview<?php echo $fetchproductS['businessPSID']; ?>">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Service Details
        </h4>
        <div class="row">
            <div class="col s12 m6 l6">
                <a href="#service-photo-update-<?php echo $fetchproductS['businessPSID']; ?>" class="modal-trigger"><img src="../../../assets/ps/<?php echo $fetchproductS['businessPSImage']; ?>" class="col s12 m12 l12" alt="service"></a>
            </div>
            <div class="col s12 m6 l6">
                <small>Name:</small><br> <strong>
                    <?php echo $fetchproductS['businessPSName']; ?></strong><br>
                <small>Description:</small><br> <strong>
                    <?php echo $desc; ?></strong><br>
                <small>Price:</small><br> <strong>Php
                    <?php echo $fetchproductS['businessPSPrice']; ?></strong><br>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row center-align">
            <div class="col s6 m6 l6">
                <a href="#archive-<?php echo $fetchproductS['businessPSID']; ?>" class="modal-trigger btn red darken-3 col s12 m12 l12">Archive</a>
            </div>
            <div class="col s6 m6 l6">
                <a href="#editservice-<?php echo $fetchproductS['businessPSID']; ?>" class="modal-trigger btn yellow darken-3  col s12 m12 l12">Edit</a>
            </div>
        </div>
    </div>
</div>
<!-- Edit Featured Photo Modal -->
<div class="modal" id="service-photo-update-<?php echo $fetchproductS['businessPSID']; ?>">
    <div class="modal-content">
        <h5>
            <div class="row">
                <div class="col s2 m1 l1">
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                </div>
                <div class="col s10 m11 l11">
                    Change Featured Photo2
                </div>
            </div>
        </h5>
        <div class="divider"></div>
        <div class="section"></div>
        <div class="row center-align">
            <a href="#service-upload-featured-<?php echo $fetchproductS['businessPSID']; ?>" class="btn yellow darken-3 modal-trigger">Upload</a>
        </div>
        <div class="row">
        <?php
                $qwe = $fetchproductS['businessPSID'];
                $sql = "SELECT * from imagelogs where pixLogIDKey = '$qwe'";
                $query = mysqli_query($connect, $sql);
                $count = mysqli_num_rows($query);
                while ($fetchdata = mysqli_fetch_array($query))  { 
            ?>
            <div class="col s6 m4 l4 primary-photo">
                <div class="card card-featured-options">
                    <div class="card-content">
                        <a href="#confirm-photo-<?php echo $fetchdata['logID']; ?>" class="modal-trigger"><img src="../../../assets/ps/<?php echo $fetchdata['picName'] ;?>" alt="featured product"></a>
                    </div>
                </div>
                <!-- Featured Photo Options Modal -->
                <div class="modal modal-fixed-footer overflow-modal" class="blocked-modal" id="confirm-photo-<?php echo $fetchdata['logID']; ?>">
                    <div class="modal-content">
                        <h5>
                            <div class="row">
                                <div class="col s2 m1 l1">
                                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                                </div>
                                <div class="col s10 m11 l11">
                                    Saving Changes
                                </div>
                            </div>
                        </h5>
                        <div class="divider"></div>
                        <p>Are you sure you want to update the current featured photo with this photo?</p>
                        <img src="../../../assets/ps/<?php echo $fetchdata['picName'] ;?>">
                    </div>
                    <div class="modal-footer">
                        <div class="row center-align">
                            <form action="../../../db.sql/updatePSexistingphoto.php" method="post">
                                <input type="hidden" name="businessID" value="<?php echo $bisnesID; ?>">
                                <input type="hidden" name="productID" value="<?php echo $fetchdata['pixLogIDKey']; ?>">
                                <input type="hidden" name="picName" value="<?php echo $fetchdata['picName'] ;?>">
                                <input type="hidden" name="photoID" value="<?php echo $fetchdata['logID']; ?>">
                                <?php 
                                    echo "<input type='hidden' name='page' value='singlefree.php'>";
                                ?>
                                <input type="submit" name="submit" value="Confirm" class="btn yellow darken-3">
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End of modal -->
            </div>
            <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- Upload Featured Services Photo Modal -->
<div class="modal" id="service-upload-featured-<?php echo $fetchproductS['businessPSID']; ?>">
    <div class="modal-content">
        <h5>
            <div class="row">
                <div class="col s2 m2 l2">
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                </div>
                <div class="col s10 m10 l10">
                    Upload Featured Photo
                </div>
            </div>
        </h5>
        <div class="divider"></div>
        <div class="section"></div>
        <form action="../../../db.sql/updatePSphoto.php" method="post" enctype="multipart/form-data">
            <div class="file-field input-field">
                <div class="btn btn-flat yellow darken-3 white-text">
                    <i class="material-icons">search</i>
                    <input type="file" name='image' id='photo' accept="image/*">
                </div>
                <div class="file-path-wrapper">
                    <input type="text" class="file-path validate" placeholder="Upload Photo">
                </div>
            </div>
            <input type='hidden' name='businessID' value='<?php echo $bisnesID; ?>'>
            <input type='hidden' name='productID' value='<?php echo $fetchproductS['businessPSID']; ?>'>
            <?php 
                echo "<input type='hidden' name='page' value='singlefree.php'>";
            ?>
            <div class="modal-footer center-align">
                <input type="submit" name="submit" value="Save Changes" class="btn yellow darken-3">
            </div>
        </form>
    </div>
</div>
<!-- Edit Service Modal -->
<div class="modal" id="editservice-<?php echo $fetchproductS['businessPSID']; ?>">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <span>Edit:
                <?php echo $fetchproductS['businessPSName']; ?></span>
            <div class="divider"></div>
        </h4>
        <form action="../../../db.sql/updateservice.php" method='post'>
            <div class="container">
                <form action="" method="post">
                    <div class="row">
                        <div class="col s12">
                            <div class="input-field">
                                <label for="servicename"> Name</label>
                                <input type="text" name="servicename" value="<?php echo $fetchproductS['businessPSName']; ?>" required autofocus />
                                <input type="hidden" name="servID" value="<?php echo $fetchproductS['businessPSID']; ?>" />
                                <input type="hidden" name="businessID" value="<?php echo $bisnesID; ?>" />
                                <?php 
                                    echo "<input type='hidden' name='page' value='singlefree.php'>";
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <div class="input-field">
                                <label for="serviceprice">Price</label>
                                <input type="text" name="serviceprice" value="<?php echo $fetchproductS['businessPSPrice']; ?>" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <div class="input-field">
                                <label for="servicedesc">Service Description</label>
                                <textarea name="servicedesc" class="materialize-textarea" required><?php echo $fetchproductS['businessPSDesc']; ?></textarea>
                            </div>
                        </div>
                    </div>
                </form>
                <center>
                    <div class="row center-align">
                        <div class="col s12 m12 l12 input-field">
                            <input type="submit" class="btn yellow darken-3" name="submit" value="Save Changes">
                        </div>
                    </div>
                </center>
            </div>
        </form>
    </div>
</div>
<!-- Archived Service Details Modal -->
<div class="modal modal-fixed-footer  overflow-modal" id="archivedserviceview<?php echo $fetchproductS['businessPSID']; ?>">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Archived Service Details
        </h4>
        <div class="row">
            <div class="col s12 m6 l6">
                <a href="#photo-update-<?php echo $fetchproductS['businessPSID']; ?>" class="modal-trigger"><img src="../../../assets/ps/<?php echo $fetchproductS['businessPSImage']; ?>" class="col s12 m12 l12" alt="product"></a>
            </div>
            <div class="col s12 m6 l6">
                <small>Name:</small><br> <strong>
                    <?php echo $fetchproductS['businessPSName']; ?></strong><br>
                <small>Description:</small><br> <strong>
                    <?php echo $desc; ?></strong><br>
                <small>Price:</small><br> <strong>Php
                    <?php echo $fetchproductS['businessPSPrice']; ?></strong><br>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row center-align">
            <div class="col s6 m6 l6">
                <a href="#unarchive-<?php echo $fetchproductS['businessPSID']; ?>" class="modal-trigger btn green darken-3 col s12 m12 l12">Unarchive</a>
            </div>
        </div>
    </div>
</div>
<!-- Archive Service Modal -->
<div class="modal modal-fixed-footer" id="archive-<?php echo $fetchproductS['businessPSID']; ?>">
    <form action="../../../db.sql/updatearchiveproduct.php" method="post">
        <div class="modal-content">
            <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                <span>Archive:
                    <?php echo $fetchproductS['businessPSName']; ?></span>
                <div class="divider"></div>
            </h4>
            <div class="container">
                <div class="row">
                    <div class="col s12 center-align">
                        Are you sure you want to archive this product?
                        <input type="hidden" name="itemID" value="<?php echo $fetchproductS['businessPSID']; ?>" />
                        <input type="hidden" name="businessID" value="<?php echo $bisnesID ?>" />
                        <?php 
                            echo "<input type='hidden' name='page' value='singlefree.php'>";
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn yellow darken-3" name="submit" value="Confirm">
        </div>
    </form>
</div>
<!-- Unarchive Service Modal -->
<div class="modal modal-fixed-footer" id="unarchive-<?php echo $fetchproductS['businessPSID']; ?>">
    <form action="../../../db.sql/updateunarchiveproduct.php" method="post">
        <div class="modal-content">
            <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                <span>Archive:
                    <?php echo $fetchproductS['businessPSName']; ?></span>
                <div class="divider"></div>
            </h4>
            <div class="container">
                <div class="row">
                    <div class="col s12 center-align">
                        Are you sure you want to unarchive this product?
                        <input type="hidden" name="itemID" value="<?php echo $fetchproductS['businessPSID']; ?>" />
                        <input type="hidden" name="businessID" value="<?php echo $bisnesID ?>" />
                        <?php 
                            echo "<input type='hidden' name='page' value='singlefree.php'>";
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn yellow darken-3" name="submit" value="Confirm">
        </div>
    </form>
</div>
<?php } ?>

<script>
$(document).ready(function() {
    $("#owner_editID").click(function() {
        var province, dataProvince="<option value='' selected disabled>-SELECT PROVINCE-</option>";
        var mun, dataMun="<option value='' selected disabled>-SELECT MUNICIPALITY-</option>";
        var brgy, dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>";
        var pCode="", mCode="", bCode="";
        setTimeout(function() {                
            //GET PROVINCE
            $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refprovince.json",
                success: function(response){                    
                    //$("#responsecontainer").html(response); 
                    province=JSON.parse(response);
                    console.log(province);
                    for (x in province.RECORDS){
                        dataProvince = dataProvince + "<option value="+province.RECORDS[x].provCode+">"+province.RECORDS[x].provDesc+"</option>";
                    }
                    var provCode = '<?php echo $provinceCode; ?>';

                    $("#owner_province").html(dataProvince);
                    $("#owner_city").html(dataMun);
                    $("#owner_brgy").html(dataBrgy);

                    if(provCode != ""){
                        $("#owner_province").val(provCode).attr("selected", "selected");
                        pCode = $("#owner_province").val();
                        p = province.RECORDS.filter( element => element.provCode == String(pCode));

                        ucprovince = p[0].provDesc;
                        ucprovince = ucprovince.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                            return letter.toUpperCase();
                        });
                        $("#province1").val(ucprovince);
                    }
                }
            });
            //GET MUNICIPALITY
            $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refcitymun.json",
                success: function(response){                    
                    //$("#responsecontainer").html(response); 
                    mun=JSON.parse(response);
                    console.log(mun);

                    var cityCode = '<?php echo $cityCode; ?>';

                    if(cityCode != ""){
                        pCode = $("#owner_province").val();
                        filterMun = mun.RECORDS.filter( element => element.provCode == String(pCode));
                        for (x in filterMun){
                            dataMun= dataMun + "<option value="+filterMun[x].citymunCode+">"+(filterMun[x].citymunDesc).replace(/ *\([^)]*\) */g, "")+"</option>";
                        }
                        $("#owner_city").html(dataMun);
                        $("#owner_city").val(cityCode).attr("selected", "selected");

                        mCode =  $("#owner_city").val();
                        m = mun.RECORDS.filter( element => element.citymunCode == String(mCode));

                        uccity = (m[0].citymunDesc).replace(/ *\([^)]*\) */g, "");
                        uccity = uccity.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                            return letter.toUpperCase();
                        });

                        $("#mun1").val(uccity);                           
                    }
                }
            });
            //GET BARANGAY
            $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refbrgy.json",
                success: function(response){                    
                    //$("#responsecontainer").html(response); 
                    brgy=JSON.parse(response);
                    console.log(brgy);

                    var brgyCode = '<?php echo $brgyCode; ?>';

                    if(brgyCode != ""){
                        mCode = $("#owner_city").val();
                        filterBrgy = brgy.RECORDS.filter( element => element.citymunCode == String(mCode));
                        for (x in filterBrgy){
                            dataBrgy= dataBrgy + "<option value='"+filterBrgy[x].brgyCode+"'>"+filterBrgy[x].brgyDesc.toUpperCase() + "</option>";
                        }
                        $("#owner_brgy").html(dataBrgy);
                        $("#owner_brgy").val(brgyCode).attr("selected", "selected");

                        bCode =  $("#owner_brgy").val();
                        b = brgy.RECORDS.filter( element => element.brgyCode == String(bCode));

                        ucbrgy = (b[0].brgyDesc).replace(/ *\([^)]*\) */g, "");
                        ucbrgy = ucbrgy.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                            return letter.toUpperCase();
                        });

                        $("#brgy1").val(ucbrgy);                           
                    }
                }
            });
        },100);

        $("#owner_province").change(function(){
            console.log("value: "+$(this).val());
            dataMun="<option value='' selected disabled>-SELECT MUNICIPALITY-</option>";
            dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>";
            pCode = $(this).val();
            filterMun = mun.RECORDS.filter( element => element.provCode == String(pCode));
            for (x in filterMun){
                dataMun= dataMun + "<option value="+filterMun[x].citymunCode+">"+(filterMun[x].citymunDesc).replace(/ *\([^)]*\) */g, "")+"</option>";
            }
            $("#owner_city").html(dataMun);
            $("#owner_brgy").html(dataBrgy);
            
            p = province.RECORDS.filter( element => element.provCode == String(pCode));

            ucprovince = p[0].provDesc;
            ucprovince = ucprovince.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });

            $("#province1").val(ucprovince);
            $("#mun1").val("");
            $("#brgy1").val("");

        });

        $("#owner_city").change(function(){
            mCode = $(this).val();
            dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>" ;
            filterBrgy = brgy.RECORDS.filter( element => element.citymunCode == String(mCode));
            for (x in filterBrgy){
                dataBrgy= dataBrgy + "<option value='"+filterBrgy[x].brgyCode+"'>"+filterBrgy[x].brgyDesc.toUpperCase() + "</option>";
            }
            $("#owner_brgy").html(dataBrgy);

            m = mun.RECORDS.filter( element => element.citymunCode == String(mCode));

            uccity = (m[0].citymunDesc).replace(/ *\([^)]*\) */g, "");
            uccity = uccity.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });

            $("#mun1").val(uccity);
            $("#brgy1").val("");

        });

        $("#owner_brgy").change(function(){
            bCode = $(this).val();
            b = brgy.RECORDS.filter( element => element.brgyCode == String(bCode));
            console.log(b);
            ucbrgy = (b[0].brgyDesc).replace(/ *\([^)]*\) */g, "");
            ucbrgy = ucbrgy.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            
            $("#brgy1").val(ucbrgy);
            
        });
    });
});  
</script>
<?php require '../../partials/footer.php'; ?>