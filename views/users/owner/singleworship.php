<?php 

require '../../partials/header.php'; 
require 'nav.php'; 


$ownerID = $_SESSION['userID'];
$bisnesID = $_GET['bID'];
$status = 'Worship';

$busName = '';
$busAddNo = '';
$busAddStreet = '';
$busAddBrgy = '';
$busAddCity = '';
$busAddProv = '';
$busDescription = '';
$busCompAddress = '';

$sql = "SELECT * from business INNER JOIN businesslocation ON businesslocation.bisLocIDkey = business.businessID WHERE businessOwner = '$ownerID' AND businessID = '$bisnesID'";
$query = mysqli_query($connect, $sql);
$count = mysqli_num_rows($query);
while ($fetchdata = mysqli_fetch_array($query))  {
    $busName = $fetchdata['businessName'];
    $busAddNo = $fetchdata['bisNumber'];
    $busAddStreet = $fetchdata['bisStreet'];
    $busAddBrgy = $fetchdata['bisBrgy']; 
    $busAddCity = $fetchdata['bisCityTown'];
    $busAddProv = $fetchdata['bisProvince'];
    $busDescription = $fetchdata['businessDescription'];

    $provinceCode = $fetchdata['provCode'];
    $cityCode = $fetchdata['munCode'];
    $brgyCode = $fetchdata['brgyCode'];

    $busCompAddress = $busAddNo . ' ' . $busAddStreet . ' ' . $busAddBrgy . ' ' . $busAddCity . ' ' . $busAddProv;
}
?>

<div class="main">
    <div class="section top-section"></div>
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-image card-business-photo">
                        <div class="carousel carousel-slider">
                            <?php 
                            $counter = 0;
                            $sql = "SELECT COUNT(*) AS countBisPhotos FROM businessphotos WHERE bisPhotoIDKey = '$bisnesID'";
                            $query = mysqli_query($connect, $sql);
                            while($fetchPhotoData = mysqli_fetch_array($query)){
                                $counter = $fetchPhotoData['countBisPhotos'];
                            }
                            if($counter == 0){
                            ?>
                            <a class="carousel-item"><img src="../../../assets/images/default-image.png"></a>
                            <?php 
                            }else{ 
                                $sql = "SELECT * FROM businessphotos WHERE bisPhotoIDKey = '$bisnesID'";
                                $query = mysqli_query($connect, $sql);
                                while($fetchPhotoData = mysqli_fetch_array($query)){ ?>
                            <a class="carousel-item modal-trigger" href="#viewbisphoto-<?php echo $fetchPhotoData['photoID']; ?>"><img src="../../../assets/images/<?php echo $fetchPhotoData['bisPhoto']; ?>"></a>
                            <?php 
                                }
                            } ?>
                        </div>
                        <a href="#add-photo-modal" class="btn-floating modal-trigger halfway-fab waves-effect waves-light yellow darken-3" id="addphoto"><i class="material-icons">add_a_photo</i></a>
                    </div>

                    <div class="card-content">
                        <span class="card-title"><h5><?php echo $busName; ?> <a href="#editnameadd" id="owner_editID" class="modal-trigger"><i class="material-icons">edit</i></a></h5></span>
                        <p>Address: <?php echo $busCompAddress; ?></p>
	
                        <table>
                            <thead>
                                <tr>
                                    <th>Schedule: <a href="addsched.php?bID=<?php echo $bisnesID; ?>">Add Schedule</a><br></th>
                                    <th>Time</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
								<?php 
								$sql = "SELECT * from businesscalendar where bisCalendarIDkey = '$bisnesID'";
								$query = mysqli_query($connect, $sql);
								$count = mysqli_num_rows($query);
								while ($fetchdata = mysqli_fetch_array($query))  {?>
                                <tr data-toggle="modal" data-target="#removeschedule">
                                    <td><?php echo $fetchdata['bisSchedDay']?></td>
                                    <td><?php echo $fetchdata['bisSchedTime']?></td>
                                    <td><a href="#removeschedule<?php echo $fetchdata['calendarID']; ?>" class="modal-trigger"><i class="material-icons">delete</i></a></td>
                                </tr>
                                <!-- Remove Schedule Modal -->
                                <div class="modal" id="removeschedule<?php echo $fetchdata['calendarID']; ?>">
                                    <div class="modal-content">
                                        <h4>
                                            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                                            <span>Remove Schedule</span>
                                            <div class="divider"></div>
                                        </h4>
                                        <h5 class="center-align">Are you sure you want to remove this schehule?</h5>
                                        <p class="center-align"><?php echo $fetchdata['bisSchedDay']?> <br> <?php echo $fetchdata['bisSchedTime']?></p>
                                        <form name="removesched" id="removesched" action="../../../db.sql/deletesched.php" method="post">
                                            <input type="hidden" name="calendarID" value="<?php echo $fetchdata['calendarID']; ?>">
                                            <input type="hidden" name="bisnessID" value="<?php echo $bisnesID; ?>">
                                            <input type='hidden' name='page' value='singleworship.php'>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                    <center>
                                        <button type="submit" form="removesched" class="btn-flat btn-floating yellow darken-3 modal-trigger"><i class="large material-icons">check</i></a>
                                    </center>
                                    </div>
                                </div>
								<?php } ?>	
                            </tbody>
                            
                        </table>
                        
                        <h5>Overview<a href="#editoverview" class="modal-trigger"><i class="material-icons">edit</i></a></h5>
                        <?php 
                            $sql = "SELECT * from business where businessID = '$bisnesID'";
                            $query = mysqli_query($connect, $sql);
                            $count = mysqli_num_rows($query);
                            while ($fetchdata = mysqli_fetch_array($query))  {?>
                        <p>
                            <?php 
                            $checkDesc =  $fetchdata['businessDescription'];
                            if ($checkDesc == '') {
                                echo "Description is empty";
                            } else {
                                $desc = $checkDesc;
                                $desc = preg_replace("/[\n]/", "<br>\n", $desc);
                                echo $desc;
                            }
                            ?>
                        </p>
                        <?php 
                            } 
                        ?>	
                    </div>
                    
                    <div class="card-action center-align">
                        <a href="confirmlocation.php" class="modal-trigger" id="editbusiness">Locate</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

		<!-- Edit Name and Address Modal -->
		<div class="modal" id="editnameadd">     
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    <span>Edit Information</span>
                    <div class="divider"></div>
                </h4>
                <form name="editname" id="editname" action="../../../db.sql/updatebusinessdetails.php" method="POST">
                    <?php 
                        $sql = "SELECT * from business INNER JOIN categories on business.businessCategory = categories.categoryID where businessID = '$bisnesID'";
                        $query = mysqli_query($connect, $sql);
                        $count = mysqli_num_rows($query);
                        while ($fetchdata = mysqli_fetch_array($query))  {        
                    ?>
            <div class="row">
                <div class="col s12 m12 l12 input-field">
                    <label for="newBusinessName">Business Name</label>
                    <input type="text" name="newBusinessName" value="<?php echo $fetchdata['businessName'];?>" autofocus required>
                    <input type="hidden" name="brgy1" id="brgy1">
                    <input type="hidden" name="mun1" id="mun1">
                    <input type="hidden" name="province1" id="province1">
                    <input type='hidden' name='businessID' value='<?php echo $_GET['bID'];?>'>
                    <input type='hidden' name='page' value='singleworship.php'>
                </div>
            </div>
            <?php } ?>
            <?php 
                $sql = "SELECT * from businesslocation where bisLocIDkey = '$bisnesID'"; 
                $query = mysqli_query($connect, $sql);
                $count = mysqli_num_rows($query);
                while ($fetchdata = mysqli_fetch_array($query)){ 
                    $bisNumber =  $fetchdata['bisNumber'];
                    $bisStreet = $fetchdata['bisStreet'];
                }
            ?>
            <div class="row">
                <div class="col s12 m4 l4">
                    <label for="newProvince">Province</label>
                    <select name="owner_province" id="owner_province" class="browser-default"></select>
                </div>
                <div class="col s12 m4 l4">
                    <label for="newCity">City/Town</label>
                    <select name="owner_city" id="owner_city" class="browser-default"></select>
                </div>
                <div class="col s12 m4 l4">
                    <label for="newBrgy">Brgy</label>
                    <select name="owner_brgy" id="owner_brgy" class="browser-default"></select>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 l6 input-field">
                    <label for="newNumber">Number</label>
                    <input type="text" name="newNumber" value="<?php echo $bisNumber; ?>">
                </div>
                <div class="col s12 m6 l6 input-field">
                    <label for="newStreet">Street</label>
                    <input type="text" name="newStreet" value="<?php echo $bisStreet; ?>">
                </div>
            </div>
            </div>  
            <div class="modal-footer">
                <center>
                <div class="row center-align">
                    <div class="col s12 m12 l12 input-field">
                        <input type="submit" class="btn yellow darken-3"value="Update">
                    </div>
                </div>
                </center>
            </div>
        </div>
    </form>

        <!-- Edit Business Description -->
		<div class="modal" id="editoverview">     
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    <span>Edit Description</span>
                    <div class="divider"></div>
                </h4>
                <form name="overview" id="overview" action="../../../db.sql/updatebusinessdescription.php" method="POST">
                    <div class="row">
                        <div class="col s12 input-field">
                            <?php 
                            $sql = "SELECT * from business INNER JOIN categories on business.businessCategory = categories.categoryID where businessID = '$bisnesID'";
                            $query = mysqli_query($connect, $sql);
                            $count = mysqli_num_rows($query);
                            while ($fetchdata = mysqli_fetch_array($query))  {?>
                            <p>
                            <label for="newDescription">Business Description</label>
                            <?php
                                $desc = $fetchdata['businessDescription'];
                            ?>
                            <textarea class="materialize-textarea" name="newDescription" autofocus><?php echo $desc; ?></textarea>
                            <input type='hidden' name='businessID' value='<?php echo $bisnesID;?>'>
                            <input type='hidden' name='page' value='singleworship.php'>
                            <?php } ?>
                        </div>
                    </div>
            </div>  
            <div class="modal-footer">
                <center>
                <div class="row center-align">
                    <div class="col s12 m12 l12 input-field">
                        <input type="submit" class="btn yellow darken-3"value="Update">
                    </div>
                </div>
                </center>
                </form>
            </div>
        </div>

        <!-- Add Photo Modal -->
        <div class="modal" id="add-photo-modal">     
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    <span>Add Photo</span>
                    <div class="divider"></div>
                </h4>
                <div class="row">
                    <form name="add_photo" id="add_photo" action="../../../db.sql/insertphoto.php" method="post"  enctype="multipart/form-data">   
                    <div class="file-field input-field">
                        <div class="btn btn-flat yellow darken-3 white-text">
                            <i class="material-icons">search</i>
                            <input type="file" name="file" accept="image/*">
                        </div>
                        <div class="file-path-wrapper">
                            <input type="text" class="file-path validate" placeholder="Upload Photo" id="photo">
                        </div>
                    </div>
                    <input type='hidden' name='businessID' value='<?php echo $bisnesID;?>'> 
                    <input type='hidden' name='page' value='singleworship.php'>
                    </form>
                </div>  
            </div>
            <div class="modal-footer">
                <center>
                    <a href="#confirm_photo" id="photocheck" class="modal-trigger btn-flat btn-floating yellow darken-3"><i class="large material-icons">check</i></a>
                </center>
            </div>
        </div>

        <!-- Add Photo Confirmation Modal -->
        <div class="modal" id="confirm_photo">
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    <span>Confirmation</span>
                    <div class="divider"></div>
                </h4>
                <p class="center-align">Are you sure you want to upload the photo?</p>
            </div>
            <div class="modal-footer">
                <center>
                    <button form="add_photo" name="submit" value="submit" class="btn-flat btn-floating yellow darken-3"><i class="large material-icons">check</i></a>
                    <div class="section"></div>
                </center>
            </div>
        </div>

        <!-- Edit Confirmation Modal -->
        <div class="modal" id="confirm_edit">
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    <span>Confirmation</span>
                    <hr>
                </h4>
                <p class="center-align">Are you sure you want to save the changes?</p>
            </div>
            <div class="modal-footer">
                <center>
                    <button form="edit" value="submit" class="btn-flat btn-floating yellow darken-3"><i class="large material-icons">check</i></a>
                    <div class="section"></div>
                </center>
            </div>
        </div>
    </div>

    <!-- View Business Photo Modal -->
    <?php
    $sql = "SELECT * FROM businessphotos WHERE bisPhotoIDKey = '$bisnesID'";
    $query = mysqli_query($connect, $sql);
    while($fetchPhotoData = mysqli_fetch_array($query)){ ?>
    <div class="modal modal-fixed-footer modal-business-photo" id="viewbisphoto-<?php echo $fetchPhotoData['photoID']; ?>">
        <div class="modal-content">
            <img class="materialboxed" src="../../../assets/images/<?php echo $fetchPhotoData['bisPhoto']; ?>">
        </div>
        <div class="modal-footer">
            <a href="#confirm-delete-<?php echo $fetchPhotoData['photoID']; ?>" class="modal-trigger btn btn-flat yellow darken-3 white-text">Delete</a>
        </div>
    </div>

     <!-- Delete Business Photo Confirmation Modal -->
     <div class="modal" id="confirm-delete-<?php echo $fetchPhotoData['photoID']; ?>">
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    <span>Confirmation</span>
                    <div class="divider"></div>
                </h4>
                <p class="center-align">Are you sure you want to delete this photo?</p>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="../../../db.sql/deletecoverphoto.php" method="POST">
                        <input type="hidden" name="businessPhotoID" value="<?php echo $fetchPhotoData['photoID']; ?>">
                        <input type="hidden" name="businessID" value="<?php echo $bisnesID; ?>">
                        <input type='hidden' name='page' value='singleworship.php'>
                        <input type="submit" name="submit" value="Delete" class="btn btn-flat yellow darken-3 white-text">
                    </form>
                </center>
            </div>
        </div>
    <?php
    }
    ?>
<script>
$(document).ready(function() {
    $("#owner_editID").click(function() {
        var province, dataProvince="<option value='' selected disabled>-SELECT PROVINCE-</option>";
        var mun, dataMun="<option value='' selected disabled>-SELECT MUNICIPALITY-</option>";
        var brgy, dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>";
        var pCode="", mCode="", bCode="";
        setTimeout(function() {                
            //GET PROVINCE
            $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refprovince.json",
                success: function(response){                    
                    //$("#responsecontainer").html(response); 
                    province=JSON.parse(response);
                    console.log(province);
                    for (x in province.RECORDS){
                        dataProvince = dataProvince + "<option value="+province.RECORDS[x].provCode+">"+province.RECORDS[x].provDesc+"</option>";
                    }
                    var provCode = '<?php echo $provinceCode; ?>';

                    $("#owner_province").html(dataProvince);
                    $("#owner_city").html(dataMun);
                    $("#owner_brgy").html(dataBrgy);

                    if(provCode != ""){
                        $("#owner_province").val(provCode).attr("selected", "selected");
                        pCode = $("#owner_province").val();
                        p = province.RECORDS.filter( element => element.provCode == String(pCode));

                        ucprovince = p[0].provDesc;
                        ucprovince = ucprovince.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                            return letter.toUpperCase();
                        });
                        $("#province1").val(ucprovince);
                    }
                }
            });
            //GET MUNICIPALITY
            $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refcitymun.json",
                success: function(response){                    
                    //$("#responsecontainer").html(response); 
                    mun=JSON.parse(response);
                    console.log(mun);

                    var cityCode = '<?php echo $cityCode; ?>';

                    if(cityCode != ""){
                        pCode = $("#owner_province").val();
                        filterMun = mun.RECORDS.filter( element => element.provCode == String(pCode));
                        for (x in filterMun){
                            dataMun= dataMun + "<option value="+filterMun[x].citymunCode+">"+(filterMun[x].citymunDesc).replace(/ *\([^)]*\) */g, "")+"</option>";
                        }
                        $("#owner_city").html(dataMun);
                        $("#owner_city").val(cityCode).attr("selected", "selected");

                        mCode =  $("#owner_city").val();
                        m = mun.RECORDS.filter( element => element.citymunCode == String(mCode));

                        uccity = (m[0].citymunDesc).replace(/ *\([^)]*\) */g, "");
                        uccity = uccity.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                            return letter.toUpperCase();
                        });

                        $("#mun1").val(uccity);                           
                    }
                }
            });
            //GET BARANGAY
            $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refbrgy.json",
                success: function(response){                    
                    //$("#responsecontainer").html(response); 
                    brgy=JSON.parse(response);
                    console.log(brgy);

                    var brgyCode = '<?php echo $brgyCode; ?>';

                    if(brgyCode != ""){
                        mCode = $("#owner_city").val();
                        filterBrgy = brgy.RECORDS.filter( element => element.citymunCode == String(mCode));
                        for (x in filterBrgy){
                            dataBrgy= dataBrgy + "<option value='"+filterBrgy[x].brgyCode+"'>"+filterBrgy[x].brgyDesc.toUpperCase() + "</option>";
                        }
                        $("#owner_brgy").html(dataBrgy);
                        $("#owner_brgy").val(brgyCode).attr("selected", "selected");

                        bCode =  $("#owner_brgy").val();
                        b = brgy.RECORDS.filter( element => element.brgyCode == String(bCode));

                        ucbrgy = (b[0].brgyDesc).replace(/ *\([^)]*\) */g, "");
                        ucbrgy = ucbrgy.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                            return letter.toUpperCase();
                        });

                        $("#brgy1").val(ucbrgy);                           
                    }
                }
            });
        },100);

        $("#owner_province").change(function(){
            console.log("value: "+$(this).val());
            dataMun="<option value='' selected disabled>-SELECT MUNICIPALITY-</option>";
            dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>";
            pCode = $(this).val();
            filterMun = mun.RECORDS.filter( element => element.provCode == String(pCode));
            for (x in filterMun){
                dataMun= dataMun + "<option value="+filterMun[x].citymunCode+">"+(filterMun[x].citymunDesc).replace(/ *\([^)]*\) */g, "")+"</option>";
            }
            $("#owner_city").html(dataMun);
            $("#owner_brgy").html(dataBrgy);
            
            p = province.RECORDS.filter( element => element.provCode == String(pCode));

            ucprovince = p[0].provDesc;
            ucprovince = ucprovince.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });

            $("#province1").val(ucprovince);
            $("#mun1").val("");
            $("#brgy1").val("");

        });

        $("#owner_city").change(function(){
            mCode = $(this).val();
            dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>" ;
            filterBrgy = brgy.RECORDS.filter( element => element.citymunCode == String(mCode));
            for (x in filterBrgy){
                dataBrgy= dataBrgy + "<option value='"+filterBrgy[x].brgyCode+"'>"+filterBrgy[x].brgyDesc.toUpperCase() + "</option>";
            }
            $("#owner_brgy").html(dataBrgy);

            m = mun.RECORDS.filter( element => element.citymunCode == String(mCode));

            uccity = (m[0].citymunDesc).replace(/ *\([^)]*\) */g, "");
            uccity = uccity.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });

            $("#mun1").val(uccity);
            $("#brgy1").val("");

        });

        $("#owner_brgy").change(function(){
            bCode = $(this).val();
            b = brgy.RECORDS.filter( element => element.brgyCode == String(bCode));
            console.log(b);
            ucbrgy = (b[0].brgyDesc).replace(/ *\([^)]*\) */g, "");
            ucbrgy = ucbrgy.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            
            $("#brgy1").val(ucbrgy);
            
        });
    });
});  
</script>
<!-- <script src="js/prefilled-dropdown.js"></script> -->
<?php require '../../partials/footer.php'; ?>
