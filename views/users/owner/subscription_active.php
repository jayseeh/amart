<?php 
    require '../../../views/partials/header.php'; 
    require 'nav.php';
?>
<div class="main">
    <!-- <div class="container"> -->
        <div class="row">
            <div class="col s12 l12">
                <h4>
                    Subscription
                </h4>
                <div class="divider"></div>
                <div class="grey lighten-4">
                    <center>
                    <div class="section"></div>
                    <p>Payment is through paymaya: 0912309217</p>
                    <div class="section"></div>
                    
                </div>
                </center>
            </div>

            <div class="row">
			<div class="col s12 l12">
                <div class="card material-table">
                    <div class="table-header">
                        <span class="table-title">Subscriptions</span>
                        <div class="actions">
                        <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                        </div>
                    </div>
                    <table id="subscription" class="striped">
                    <thead>
                        <th>Business Name</th>
                        <th class="hide-on-small-only">Category</th>
                        <th class="hide-on-small-only">Price</th>
                        <th>Status</th>
                        <th class="center-align">Action</th>
                    </thead>
					<tbody>
                        <?php 
                        $ownerID =$_SESSION['userID'];
                        $sql = "SELECT * FROM business 
                                INNER JOIN categories 
                                ON business.businessCategory = categories.categoryID 
                                WHERE businessOwner = '$ownerID' 
                                AND categoryType != 'Worship' 
                                ORDER BY businessID ASC ";
                        $query = mysqli_query($connect, $sql);
                        $count = mysqli_num_rows($query);
                        while ($fetchdata = mysqli_fetch_array($query))  {
                            $businessID = $fetchdata['businessID'];
                            $businessName = $fetchdata['businessName'];
                            $categoryName = $fetchdata['categoryName'];
                            $categoryFee = $fetchdata['categoryFee'];
                            if($categoryFee == "0.00"){
                                $categoryFee = "Free";
                            }
                            $categoryType = $fetchdata['categoryType'];
                            $businessStatus = $fetchdata['businessStatus'];

                            $sqlstatus = "SELECT subStatus from subscription  
                                WHERE bisOwner = '$ownerID' AND bisSubIDkey = '$businessID' AND subStatus = 'Pending'";
                                $querystatus = mysqli_query($connect, $sqlstatus);
                                $countstatus = mysqli_num_rows($query);
                                $thisstatus = '';
                                while ($fetchstatus = mysqli_fetch_array($querystatus)){ 
                                    $thisstatus = $fetchstatus['subStatus'];
                                }

                                if($thisstatus == 'Pending'){
                                    $disabled = "disabled grey-text text-lighten-1";
                                    $businessStatus = 'Pending';
                                } else {
                                    $disabled = "green-text text-darken-3 ";
                                }
                        ?>

						<tr>
							<td><?php echo $businessName; ?></td>
							<td class="hide-on-small-only"><?php echo $categoryName; ?></td>
							<td class="hide-on-small-only"><?php echo $categoryFee; ?></td>
                            <td><?php echo $businessStatus; ?></td>
                            <?php if($categoryType == 'Free'){ ?>
                                <td class="center-align"><a href="#" class="upgradeBtn <?php echo $disabled; ?>" data-id="<?php echo $businessID; ?>">Upgrade</a></td>
                            <?php } else { ?>
                                <td class="center-align"><a href="#" class="activateBtn <?php echo $disabled; ?>" data-id="<?php echo $businessID; ?>">Renew</a></td>
                            <?php } ?>
						</tr>
                        <?php } ?>
					</tbody>
                </table>
				</div>
            </div>
        </div>
    </div>
</div>
  <!-- Subscriptions Modal -->
  
  <div id="activation" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>
        <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
        <span>Activation</span>
        </h4>
        <div class="divider"></div>
        <div class="col s12">
            
        </div>
        <form name="activate" id="activate" action="../../../db.sql/insertsubscription.php" method="post" enctype="multipart/form-data">

        <div class="section"></div>
        <div class="row">
            <h5 class="subscription-title"></h4>
        </div>
        <div class="row">
            <div class="input-field col s12 l12">
            <input placeholder="Reference Number" name='ref_no' id="ref_no" type="text" class="validate" required autofocus>
            <label for="ref_no">Reference Number:</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 l6">
            <select class="validate" name="subscription_type" id="subscription_type">
                <option value="1" selected>Monthly</option>
                <option value="2">Yearly</option>
            </select>
            <label for="subscription_type">Subscription Type</label>
            </div>
            <div class="input-field col s12 l6">
            <input placeholder="Duration" name='duration' id="duration" type="number" class="validate" value="1" min="1" required>
            <label for="duration">Duration</label>
            </div>
        </div>
            <div class="row">
            <div class="input-field col s12 l12">
            <input placeholder="Amount" id="amount" name='amount' type="text" class="validate" readonly required>
            <label for="amount">Amount:</label>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <label for="proof">Proof of payment</label>
                <div class="file-field input-field">
                    <div class="btn btn-flat white yellow-text text-darken-3">
                        <i class="material-icons">search</i>
                        <input type="file" name="file" accept="image/*">
                    </div>
                    <div class="file-path-wrapper">
                        <input type="text" class="file-path validate" placeholder="Upload Photo" required>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="hidden" id='business_id' name='business_id'>
        <input type="hidden" id='category' name='category'>
        <input type="hidden" id='category_fee' name='category_fee'>
        <input type="hidden" value='<?php echo $_SESSION['userID'];?>' name='bisnesOwner'> 
        <input type="hidden" value='<?php echo $_SESSION['userID'];?>' name='subscription_type'> 
        <input type="hidden" value='<?php date_default_timezone_set("Asia/Manila"); echo date("Y-m-d"); ?>' name='dateNow'>
        <input type="submit" form="activate" name="submit" value="Submit" class="btn yellow darken-3 white-text">
        </form>
        </div>
    </div>
    <!-- End of Subscription Modal -->

    <!-- Upgrade Modal -->
    <?php
        // category dropdown prices
        $sql = "SELECT DISTINCT categoryType, categoryFee from categories order by categoryType asc";
        $query = mysqli_query($connect, $sql);
        $count = mysqli_num_rows($query);
        $categoryFee = [];
        while ($fetchdata = mysqli_fetch_array($query))  {
            $categoryFee[] = array($fetchdata['categoryType']=>$fetchdata['categoryFee']);		
        } 
    ?>
    <div id="upgrade" class="modal modal-fixed-footer">
        <form name="upgrade" id="upgrade" action="../../../db.sql/upgradesubscription.php" method="post" enctype="multipart/form-data">
        <div class="modal-content">
            <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <span>Upgrade</span>
            </h4>
            <div class="divider"></div>
            <!-- <div class="col s12">
            
            </div> -->

            <div class="section"></div>
            <div class="row">
                <h5 class="subscription-title"></h4>
            </div>
            <div class="row">
                <div class="input-field col s12 l12">
                <input placeholder="Reference Number" name='upgrade_ref_no' id="upgrade_ref_no" type="text" class="validate" required autofocus>
                <label for="upgrade_ref_no">Reference Number:</label>
                </div>
            </div>
            <div class="row">
                <div class="col s12 l12">
                    <label for="upgrade_category">Category</label>
                    <select name="upgrade_category" id="upgrade_category" required>
                        <option value="" disabled selected>Choose Category</option>
                        <optgroup label="Micro ( Php <?php echo $categoryFee[2]["Micro"]; ?> )">
                        <?php 
                        $sql = "SELECT * from categories where categoryType = 'Micro' order by categoryID desc";
                        $query = mysqli_query($connect,$sql);
                        $count = mysqli_num_rows($query);
                        while ($fetchdata = mysqli_fetch_array($query))  {?>
                            <option value="<?php echo $fetchdata['categoryID']; ?>" data-id="<?php echo $categoryFee[2]["Micro"]; ?>"><?php echo $fetchdata['categoryName']; ?></option>
                        <?php } ?>
                        </optgroup>
                        <optgroup label="Small ( Php <?php echo $categoryFee[3]["Small"]; ?> )">
                        <?php 
                        $sql = "SELECT * from categories where categoryType = 'Small' order by categoryName asc";
                        $query = mysqli_query($connect,$sql);
                        $count = mysqli_num_rows($query); 
                        while ($fetchdata = mysqli_fetch_array($query))  {?>
                            <option value="<?php echo $fetchdata['categoryID']; ?>" data-id="<?php echo $categoryFee[3]["Small"]; ?>"><?php echo $fetchdata['categoryName']; ?></option>
                        <?php } ?>
                        </optgroup>
                        <optgroup label="Medium ( Php <?php echo $categoryFee[1]["Medium"]; ?> )">
                        <?php 
                            $sql = "SELECT * from categories where categoryType = 'Medium' order by categoryName asc";
                            $query = mysqli_query($connect,$sql);
                            $count = mysqli_num_rows($query); 
                            while ($fetchdata = mysqli_fetch_array($query))  {
                        ?>
                            <option value="<?php echo $fetchdata['categoryID']; ?>" data-id="<?php echo $categoryFee[1]["Medium"]; ?>"><?php echo $fetchdata['categoryName']; ?></option>
                        <?php } ?>
                        </optgroup>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12 l6">
                <select class="validate" name="upgrade_subscription_type" id="upgrade_subscription_type">
                    <option value="1" selected>Monthly</option>
                    <option value="2">Yearly</option>
                </select>
                <label for="upgrade_subscription_type">Subscription Type</label>
                </div>
                <div class="input-field col s12 l6">
                <input placeholder="Duration" name='upgrade_duration' id="upgrade_duration" type="number" class="validate" value="1" min="1" required>
                <label for="upgrade_duration">Duration</label>
                </div>
            </div>
                <div class="row">
                <div class="input-field col s12 l12">
                <input placeholder="Amount" id="upgrade_amount" name='upgrade_amount' type="text" class="validate" readonly required>
                <label for="upgrade_amount">Amount:</label>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <label for="upgrade_file">Proof of payment</label>
                    <div class="file-field input-field">
                        <div class="btn btn-flat white yellow-text text-darken-3">
                            <i class="material-icons">search</i>
                            <input type="file" name="file" id="upgrade_file" accept="image/*">
                        </div>
                        <div class="file-path-wrapper">
                            <input type="text" class="file-path validate" placeholder="Upload Photo" required>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Modal Content -->
        <div class="modal-footer">
            <input type="hidden" id='upgrade_business_id' name='upgrade_business_id'>
            <input type="hidden" id='upgrade_category_fee' name='upgrade_category_fee'>
            <input type="hidden" value='<?php echo $_SESSION['userID'];?>' name='bisnesOwner'>
            <input type="hidden" value='<?php date_default_timezone_set("Asia/Manila"); echo date("Y-m-d"); ?>' name='dateNow'>
            <input type="submit" name="submit" value="Submit" class="btn yellow darken-3 white-text"> 
        </div>
        </form>
    </div>
    <!-- End of Updagrade Modal -->
    <!-- Start of Success Modal -->
    <div class="modal" id="success">
        <div class="modal-content">
            <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                Success
            </h4>
            <div>
                <p class="center-align">
                    <i>Subscription is successful, please wait for the confirmation of the administrator.</i>
                </p>
            </div>    
        </div>
    </div>
    <!-- End of Success Modal -->
</div>

<script>
	$(document).ready(function() {
		$('#subscription').dataTable({
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        bAutoWidth: false,
        resposive: true
        });

        $('.activateBtn').on('click', function(){
            dataBusinessID = $(this).attr('data-id');
            $.ajax({
                //ajax functions here
                url: "../../../db.sql/getBusinessDetails.php",
                method: "POST",
                data: {
                    'businessID' : dataBusinessID
                },
                success: 
                    function(data){
                        //Populate Input Fields Here
                        // data = JSON.stringify(data);
                        console.log(data);
                        $('.subscription-title').text(data.businessName);
                        $('#business_id').val(dataBusinessID);
                        $('#category_fee').val(data.categoryFee);
                        $('#category').val(data.businessCategory);

                        var categoryFee = parseInt($('#category_fee').val());
                        var subscriptionType = $('#subscription_type').children("option:selected").val();
                        var duration = parseInt($('#duration').val());
                        var yearly = 0;
                        console.log(subscriptionType);

                        if(subscriptionType == '1'){
                            amount = categoryFee * duration;
                        } else {
                            yearly = duration * 12;
                            amount = categoryFee * yearly;
                        }
                        $('#amount').val(amount);

                        $('#activation').modal('open');
                    },
                error: 
                    function (xhr, ajaxOptions, thrownError) {
                        console.log('Error', xhr + " " + thrownError, 'error');
                    }
            });
        });

        $('#duration').on('keyup', function(){
            var categoryFee = parseInt($('#category_fee').val());
            var subscriptionType = $('#subscription_type').children("option:selected").val();
            var duration = parseInt($('#duration').val());
            var yearly = 0;
            console.log(subscriptionType);

            if(subscriptionType == '1'){
                amount = categoryFee * duration;
            } else {
                yearly = duration * 12;
                amount = categoryFee * yearly;
            }
            if(isNaN(amount)){
                amount = 0;
            }
            $('#amount').val(amount);
        });

        $('#subscription_type').on('change', function(){
            var categoryFee = parseInt($('#category_fee').val());
            var subscriptionType = $('#subscription_type').children("option:selected").val();
            var duration = parseInt($('#duration').val());
            var yearly = 0;
            console.log(subscriptionType);

            if(subscriptionType == '1'){
                amount = categoryFee * duration;
            } else {
                yearly = duration * 12;
                amount = categoryFee * yearly;
            }
            if(isNaN(amount)){
                amount = 0;
            }
            $('#amount').val(amount);
        });

        $('.upgradeBtn').on('click', function(){
            dataBusinessID = $(this).attr('data-id');
            $('.subscription-title').text("");
            $('#upgrade_business_id').val("");
            $('#upgrade_category').find("").attr('selected', 'selected');
            $('#upgrade_category_fee').val("");
            $('#upgrade_duration').val("1");
            $('#upgrade_amount').val("");

            $.ajax({
                //ajax functions here
                url: "../../../db.sql/getBusinessDetails.php",
                method: "POST",
                data: {
                    'businessID' : dataBusinessID
                },
                success: 
                    function(data){
                        //Populate Input Fields Here
                        // data = JSON.stringify(data);
                        console.log(data);
                        $('.subscription-title').text(data.businessName);
                        $('#upgrade_business_id').val(dataBusinessID);
                        $('#upgrade_category_fee').val(data.categoryFee);

                        var categoryFee = parseInt($('#upgrade_category_fee').val());
                        var subscriptionType = $('#upgrade_subscription_type').children("option:selected").val();
                        var duration = parseInt($('#upgrade_duration').val());
                        var yearly = 0;
                        console.log(subscriptionType);

                        if(subscriptionType == '1'){
                            amount = categoryFee * duration;
                        } else {
                            yearly = duration * 12;
                            amount = categoryFee * yearly;
                        }
                        $('#upgrade_amount').val(amount);

                        $('#upgrade').modal('open');
                    },
                error: 
                    function (xhr, ajaxOptions, thrownError) {
                        console.log('Error', xhr + " " + thrownError, 'error');
                    }
            });
        });

        $('#upgrade_category').on('change', function(){
            var fee = $(this).find(':selected').attr('data-id');
            $('#upgrade_category_fee').val(fee);
            console.log(fee);
            var categoryFee = parseInt($('#upgrade_category_fee').val());
            var subscriptionType = $('#upgrade_subscription_type').children("option:selected").val();
            var duration = parseInt($('#upgrade_duration').val());
            var yearly = 0;
            console.log(subscriptionType);

            if(subscriptionType == '1'){
                amount = categoryFee * duration;
            } else {
                yearly = duration * 12;
                amount = categoryFee * yearly;
            }

            if(isNaN(amount)){
                amount = 0;
            }

            $('#upgrade_amount').val(amount);
        });

        $('#upgrade_duration').on('keyup', function(){
            var categoryFee = parseInt($('#upgrade_category_fee').val());
            var subscriptionType = $('#upgrade_subscription_type').children("option:selected").val();
            var duration = parseInt($('#upgrade_duration').val());
            var yearly = 0;
            console.log(subscriptionType);

            if(subscriptionType == '1'){
                amount = categoryFee * duration;
            } else {
                yearly = duration * 12;
                amount = categoryFee * yearly;
            }
            if(isNaN(amount)){
                amount = 0;
            }
            $('#upgrade_amount').val(amount);
        });

        $('#upgrade_subscription_type').on('change', function(){
            var categoryFee = parseInt($('#upgrade_category_fee').val());
            var subscriptionType = $('#upgrade_subscription_type').children("option:selected").val();
            var duration = parseInt($('#upgrade_duration').val());
            var yearly = 0;
            console.log(subscriptionType);

            if(subscriptionType == '1'){
                amount = categoryFee * duration;
            } else {
                yearly = duration * 12;
                amount = categoryFee * yearly;
            }
            if(isNaN(amount)){
                amount = 0;
            }
            $('#upgrade_amount').val(amount);
        });

    });
</script>

<?php require '../../../views/partials/footer.php' ?>

<script>
    $(document).ready(function(){
        <?php if(isset($_GET['result'])) { ?>
            var message = "<?php echo $_GET['result']; ?>";

            console.log(message);
            if( message == "success" ) {
                $('#success').modal('open');
            }
        <?php } ?>
    });
</script>