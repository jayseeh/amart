<?php require '../../../views/partials/header.php'; ?>
<?php require 'nav.php' ?>
<div class="main">
<div class="container">
    <div class="section">
        <h4>
            Subscription and Payments
        </h4>
        <div class="divider"></div>
        <div class="grey lighten-4">
        
        <center>
        
        <p class="flow-text">
            <strong>Account Inactive</strong><br>
            Activation will take 1 - 2 working days.
        </p>
        <a href="#activation" class="modal-trigger btn btn-large waves-effect waves-light yellow darken-2 ">Activate Now</a>
        
        <p>Select any applicable payment method below</p>
      <table class="responsive-table center">
          <thead>
              <th>Bank</th>
              <th>Account Name</th>
              <th>Accont Number</th>
            </thead>
            <tr>
                <td>BDO</td>
                <td>Amart Co.</td>
                <td>981928391000</td>
            </tr>
            <tr>
                <td>Metrobank</td>
                <td>Amart Co.</td>
                <td>09001202121</td>
            </tr>
            <tr>
                <td>BPI</td>
                <td>Amart Co.</td>
                <td>999989192891</td>
            </tr>
      </table>

        </center>        
    </div>
</div>
  <!-- Modal Structure -->
  <div id="activation" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>
        <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
        <span>Activation</span>
        </h4>
        <div class="divider"></div>
        <div class="col s12">
        
        </div>
        <form name="activate" id="activate" action="activate.php" method="post">
        <div class="row">
        <div class="input-field col s12 l9">
            <input placeholder="Reference Number" id="ref_no" type="text" class="validate" required>
            <label for="ref_no">Reference Number:</label>
            </div>
            <div class="input-field col s12 l3">
            <input placeholder="Amount(php)" id="amount" type="number" class="validate" required>
            <label for="amount">Amount:</label>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <label for="proof">Proof of payment</label>
                <div class="file-field input-field">
                    <div class="btn btn-flat white yellow-text text-darken-3">
                        <i class="material-icons">search</i>
                        <input id="proof" type="file" name="eposter" class="validate" accept="image/*">
                    </div>
                    <div class="file-path-wrapper">
                        <input type="text" class="file-path validate" placeholder="Upload Photo" required>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="modal-footer">
    <button type="submit" form="activate" class="waves-effect waves-green btn-flat yellow darken-3 white-text">Submit</button>
    
    </div>
  </div>
</div>

<?php require '../../../views/partials/footer.php' ?>