<?php
    require '../../partials/header.php'; 
    require 'nav.php'; 
 
    if(isset($_GET['bID'])){
        $bID = $_GET['bID'];
    } else {
        $bID = "";
    }

    $sql = "SELECT * from business INNER JOIN businesslocation ON businesslocation.bisLocIDkey = business.businessID WHERE businessID = '$bID'";
    $query = mysqli_query($connect, $sql);
    $count = mysqli_num_rows($query);
    while ($fetchdata = mysqli_fetch_array($query))  {
        $busName = $fetchdata['businessName'];
    }

	// category dropdown prices
	$sql = "SELECT DISTINCT categoryType, categoryFee from categories order by categoryType asc";
	$query = mysqli_query($connect, $sql);
	$count = mysqli_num_rows($query);
	$categoryFee = [];
	while ($fetchdata = mysqli_fetch_array($query))  {
		$categoryFee[] = array($fetchdata['categoryType']=>$fetchdata['categoryFee']);		
    }

?>
<div class="main">
    <div class="container">
        <h4>
            Upgrade Subscription
        </h4>
        <p class="center-align">
            <em>List here the benefits of having an upgraded subscription from free account.</em>
        </p>
        <div class="row">
            <div class="col s12 m12 l12">
            <form name="upgrade" id="upgrade" action="../../../db.sql/upgradesubscription.php" method="post" enctype="multipart/form-data">

        <div class="section"></div>
        <div class="row">
            <h5 class="subscription-title"><?php echo $busName; ?></h4>
        </div>

        <div class="row">
            <div class="input-field col s12 l12">
            <input placeholder="Reference Number" name='upgrade_ref_no' id="upgrade_ref_no" type="text" class="validate" required>
            <label for="upgrade_ref_no">Reference Number:</label>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l12">
                <label for="upgrade_category">Category</label>
                <select name="upgrade_category" id="upgrade_category" required>
                    <option value="" disabled selected>Choose Category</option>
                    <optgroup label="Micro ( Php <?php echo $categoryFee[2]["Micro"]; ?> )">
                    <?php 
                    $sql = "SELECT * from categories where categoryType = 'Micro' order by categoryName asc";
                    $query = mysqli_query($connect,$sql);
                    $count = mysqli_num_rows($query);
                    while ($fetchdata = mysqli_fetch_array($query))  {?>
                        <option value="<?php echo $fetchdata['categoryID']; ?>" data-id="<?php echo $categoryFee[2]["Micro"]; ?>"><?php echo $fetchdata['categoryName']; ?></option>
                    <?php } ?>
                    </optgroup>
                    <optgroup label="Small ( Php <?php echo $categoryFee[3]["Small"]; ?> )">
                    <?php 
                    $sql = "SELECT * from categories where categoryType = 'Small' order by categoryName asc";
                    $query = mysqli_query($connect,$sql);
                    $count = mysqli_num_rows($query); 
                    while ($fetchdata = mysqli_fetch_array($query))  {?>
                        <option value="<?php echo $fetchdata['categoryID']; ?>" data-id="<?php echo $categoryFee[3]["Small"]; ?>"><?php echo $fetchdata['categoryName']; ?></option>
                    <?php } ?>
                    </optgroup>
                    <optgroup label="Medium ( Php <?php echo $categoryFee[1]["Medium"]; ?> )">
                    <?php 
                        $sql = "SELECT * from categories where categoryType = 'Medium' order by categoryName asc";
                        $query = mysqli_query($connect,$sql);
                        $count = mysqli_num_rows($query); 
                        while ($fetchdata = mysqli_fetch_array($query))  {
                    ?>
                        <option value="<?php echo $fetchdata['categoryID']; ?>" data-id="<?php echo $categoryFee[1]["Medium"]; ?>"><?php echo $fetchdata['categoryName']; ?></option>
                    <?php } ?>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12 l6">
            <select class="validate" name="upgrade_subscription_type" id="upgrade_subscription_type">
                <option value="1" selected>Monthly</option>
                <option value="2">Yearly</option>
            </select>
            <label for="upgrade_subscription_type">Subscription Type</label>
            </div>
            <div class="input-field col s12 l6">
            <input placeholder="Duration" name='upgrade_duration' id="upgrade_duration" type="number" onclick="select()"  class="validate" value="1" required>
            <label for="upgrade_duration">Duration</label>
            </div>
        </div>
            <div class="row">
            <div class="input-field col s12 l12">
            <input placeholder="Amount" id="upgrade_amount" name='upgrade_amount' type="text" class="validate" readonly required>
            <label for="upgrade_amount">Amount:</label>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <label for="upgrade_file">Proof of payment</label>
                <div class="file-field input-field">
                    <div class="btn btn-flat white yellow-text text-darken-3">
                        <i class="material-icons">search</i>
                        <input type="file" name="file" id="upgrade_file" accept="image/*" required>
                    </div>
                    <div class="file-path-wrapper">
                        <input type="text" class="file-path validate" placeholder="Upload Photo" required>
                    </div>
                </div>
            </div>
        </div>
        </div>
        
        <input type="hidden" id='upgrade_business_id' name='upgrade_business_id' value='<?php echo $bID; ?>'>
        <input type="hidden" id='upgrade_category_fee' name='upgrade_category_fee'>
        <input type="hidden" value='<?php echo $_SESSION['userID'];?>' name='bisnesOwner'>
        <input type="hidden" value='<?php date_default_timezone_set("Asia/Manila"); echo date("Y-m-d"); ?>' name='dateNow'>
        <input type="submit" form="upgrade" name="submit" value="Submit" class="btn yellow darken-3 white-text">
        </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#upgrade_category').on('change', function(){
            var fee = $(this).find(':selected').attr('data-id');
            $('#upgrade_category_fee').val(fee);
            console.log(fee);
            var categoryFee = parseInt($('#upgrade_category_fee').val());
            var subscriptionType = $('#upgrade_subscription_type').children("option:selected").val();
            var duration = parseInt($('#upgrade_duration').val());
            var yearly = 0;
            console.log(subscriptionType);

            if(subscriptionType == '1'){
                amount = categoryFee * duration;
            } else {
                yearly = duration * 12;
                amount = categoryFee * yearly;
            }

            if(isNaN(amount)){
                amount = 0;
            }

            $('#upgrade_amount').val(amount);
        });

        // $('.upgradeBtn').on('click', function(){
        //     dataBusinessID = $(this).attr('data-id');
        //     $.ajax({
        //         //ajax functions here
        //         url: "../../../db.sql/getBusinessDetails.php",
        //         method: "POST",
        //         data: {
        //             'businessID' : dataBusinessID
        //         },
        //         success: 
        //             function(data){
        //                 //Populate Input Fields Here
        //                 // data = JSON.stringify(data);
        //                 console.log(data);
        //                 $('.subscription-title').text(data.businessName);
        //                 $('#upgrade_business_id').val(dataBusinessID);
        //                 $('#upgrade_category_fee').val(data.categoryFee);

        //                 var categoryFee = parseInt($('#upgrade_category_fee').val());
        //                 var subscriptionType = $('#upgrade_subscription_type').children("option:selected").val();
        //                 var duration = parseInt($('#upgrade_duration').val());
        //                 var yearly = 0;
        //                 console.log(subscriptionType);

        //                 if(subscriptionType == '1'){
        //                     amount = categoryFee * duration;
        //                 } else {
        //                     yearly = duration * 12;
        //                     amount = categoryFee * yearly;
        //                 }
        //                 $('#upgrade_amount').val(amount);

        //                 $('#upgrade').modal('open');
        //             },
        //         error: 
        //             function (xhr, ajaxOptions, thrownError) {
        //                 console.log('Error', xhr + " " + thrownError, 'error');
        //             }
        //     });
        // });

        $('#upgrade_duration').on('keyup', function(){
            var categoryFee = parseInt($('#upgrade_category_fee').val());
            var subscriptionType = $('#upgrade_subscription_type').children("option:selected").val();
            var duration = parseInt($('#upgrade_duration').val());
            var yearly = 0;
            console.log(subscriptionType);

            if(subscriptionType == '1'){
                amount = categoryFee * duration;
            } else {
                yearly = duration * 12;
                amount = categoryFee * yearly;
            }
            
            if(isNaN(amount)){
                amount = 0;
            }

            $('#upgrade_amount').val(amount);
        });

        $('#upgrade_subscription_type').on('change', function(){
            var categoryFee = parseInt($('#upgrade_category_fee').val());
            var subscriptionType = $('#upgrade_subscription_type').children("option:selected").val();
            var duration = parseInt($('#upgrade_duration').val());
            var yearly = 0;
            console.log(subscriptionType);

            if(subscriptionType == '1'){
                amount = categoryFee * duration;
            } else {
                yearly = duration * 12;
                amount = categoryFee * yearly;
            }

            if(isNaN(amount)){
                amount = 0;
            }

            $('#upgrade_amount').val(amount);
        });
    });
</script>
<?php require '../../partials/footer.php'; ?>