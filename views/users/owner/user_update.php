<!-- Edit User Details Modal -->
<?php include '../../../db.sql/connect_db.php'; ?>
<?php
$userID = $_SESSION['userID'];
$sql = "SELECT * from accounts where aID = '$userID'";
$query = mysqli_query($connect, $sql);
$count = mysqli_num_rows($query);
while ($fetchdata = mysqli_fetch_array($query))  {
    
    $gender = $fetchdata['uGender'];
    $address = explode(" | ", $fetchdata['uAddress']);
    $provinceCode = $fetchdata['uCityProvince'];
    $cityCode = $fetchdata['uCity'];
    $brgyCode = $fetchdata['uBrgy'];
    
?>

<div id="update" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Update Personal Info
        </h4>
        <form action="../../../db.sql/updateaccdetails.php" method="post" id="userupdateform">
        <div class='row'>
            <div class='input-field col s12 m4 l4'>
                <!-- <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">person</i> -->
                <input class='validate' type='text' name='fname' id='fname' value='<?php echo $fetchdata['fname'];?>' autofocus required>
                <label for='fname'>First Name</label>
            </div>
            <div class='input-field col s12 m4 l4'>
                <input class='validate' type='text' name='mname' id='mname' value='<?php echo $fetchdata['mname'];?>'  required>
                <label for='mname'>Middle Name</label>
            </div>
            <div class='input-field col s12 m4 l4'>
                <input class='validate' type='text' name='lname' id='lname' value='<?php echo $fetchdata['lname'];?>' required>
                <label for='lname'>Last Name</label>
            </div>
        </div>
        
        <div class='row'>
            <div class='input-field col s12'>
                <!-- <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">contact_phone</i> -->
                <input class='validate' type='text' name='phoneNumber' id='phoneNumber' value='<?php echo $fetchdata['phoneNumber'];?>' required>
                <label for='phoneNumber'>Mobile Number</label>
            </div>
        </div>
        
        <div class='row'>
            <!-- Address Line 1, Address Line 2, Province, City/Town, Brgy -->
            <div class='input-field col s12 m12 l12'>
                <input class='validate' type='text' name='addressline1' value='<?php echo $address[0]; ?>' required>
                <label for='addressline1'>Address Line 1</label>
            </div>

            <div class="row">
                <div class="col s12 m4 l4">
                    <label for="newProvince">Province</label>
                    <select name="newProvince" id="user_province" class="browser-default"></select>
                    <input type = "hidden" id = "u_province" name="u_province" >
                </div>
                <div class="col s12 m4 l4">
                    <label for="newCity">City/Town</label>
                    <select name="newCity" id="user_municipality" class="browser-default"></select>
                    <input type = "hidden" id="u_city" name="u_city">
                </div>
                <div class="col s12 m4 l4">
                    <label for="newBrgy">Brgy</label>
                    <select name="newBrgy" id="user_brgy" class="browser-default"></select>
                    <input type = "hidden" name="u_brgy" id="u_brgy">
                </div>
            </div>
        </div>
        
        <div class='row'>
            <div class='col s12'>
                <label for="gender">Gender</label>
                <select name="gender" id="gender" required>
                    <?php
                        if($gender != ''){
                            echo '<option value="'.$gender.'">'.$gender.'</option>';
                        } 
                        if($gender == "Male"){
                            echo '<option value="Female">Female</option>';
                        }elseif($gender == "Female"){
                            echo '<option value="Male">Male</option>';
                        }else{
                            echo '<option value="" disabled selected>--Choose Gender--</option>';
                            echo '<option value="Male">Male</option>';
                            echo '<option value="Female">Female</option>';
                        }
                    ?>
                </select>
                <label for='gender'>Gender</label>
            </div>
        </div>
        <div class='row'>
            <div class='input-field col s12'>
                <!-- <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">flag</i> -->
                <input class='validate' type='email' name='email' id='update_email' value='<?php echo $fetchdata['uEmail'];?>'  >
                <label for='email'>Email</label>
            </div>
            <div class="col s12">
                <span class="error"></span><br />
            </div>
        </div>
    </div>
    
    <div class="modal-footer">
        <input type='hidden' name="uType" value="<?php echo $_SESSION['aType']?>">
        <input type='hidden' name='userID' value='<?php echo $_SESSION['userID'];?>' id="update_userID">
        <input type='submit' class='col s12 btn yellow darken-3' value='Update'>
    </div>
    </form>
</div>
<?php } ?>
<script>
    $(document).ready(function(){
        submit = true;
        $("#userupdateform").on("submit", function(){
            var provfield = $("#u_province").val();
            var cityfield = $("#u_city").val();
            var brgyfield = $("#u_brgy").val();
            
            if(provfield == "" || cityfield == "" || brgyfield == "" || submit == false){
                return false;
            }
        });

        $('#update_email').on('input', function(){
        // $('#update_email').focusout(function(){
            var email = $(this).val();
            var userid = $('#update_userID').val();
            console.log(userid);
            $.ajax({
                //ajax functions here
                url: "../../../db.sql/checkduplicateemail.php",
                method: "POST",
                data: {
                    'userid'  : userid,
                    'email' : email
                },
                success: 
                    function(data){
                        //Populate Input Fields Here
                        // data = JSON.stringify(data);
                        // console.log(data);
                        if(data.result == "available"){
							$('.error').text('Email is Available').removeClass("red-text").addClass("green-text");
							submit = true;
						} else {
							$('.error').text('Existing Email').removeClass("green-text").addClass("red-text");
							submit = false;
						}
                        
                    },
                error: 
                    function (xhr, ajaxOptions, thrownError) {
                        console.log('Error', xhr + " " + thrownError, 'error');
                    }
            });
        });
    }); 
</script>
<script>
    $(document).ready(function() {
        $("#user_editID").click(function(){
        // $('#update').modal({
        // ready: function() {
            var province, dataProvince="<option value='' selected disabled>-SELECT PROVINCE-</option>";
            var mun, dataMun="<option value='' selected disabled>-SELECT MUNICIPALITY-</option>";
            var brgy, dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>";
            var pCode="", mCode="", bCode="";
            setTimeout(function() {                
                //GET PROVINCE
                $.ajax({    //create an ajax request to display.php
                    type: "GET",
                    url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refprovince.json",
                    success: function(response){                    
                        //$("#responsecontainer").html(response); 
                        province=JSON.parse(response);
                        console.log(province);
                        for (x in province.RECORDS){
                            dataProvince = dataProvince + "<option value="+province.RECORDS[x].provCode+">"+province.RECORDS[x].provDesc+"</option>";
                        }
                        var provCode = '<?php echo $provinceCode; ?>';

                        $("#user_province").html(dataProvince);
                        $("#user_municipality").html(dataMun);
                        $("#user_brgy").html(dataBrgy);

                        if(provCode != ""){
                            $("#user_province").val(provCode).attr("selected", "selected");
                            pCode = $("#user_province").val();
                            p = province.RECORDS.filter( element => element.provCode == String(pCode));

                            ucprovince = p[0].provDesc;
                            ucprovince = ucprovince.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                return letter.toUpperCase();
                            });
                            $("#u_province").val(ucprovince);
                        }
                    }
                });
                //GET MUNICIPALITY
                $.ajax({    //create an ajax request to display.php
                    type: "GET",
                    url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refcitymun.json",
                    success: function(response){                    
                        //$("#responsecontainer").html(response); 
                        mun=JSON.parse(response);
                        console.log(mun);

                        var cityCode = '<?php echo $cityCode; ?>';

                        if(cityCode != ""){
                            pCode = $("#user_province").val();
                            filterMun = mun.RECORDS.filter( element => element.provCode == String(pCode));
                            for (x in filterMun){
                                dataMun= dataMun + "<option value="+filterMun[x].citymunCode+">"+(filterMun[x].citymunDesc).replace(/ *\([^)]*\) */g, "")+"</option>";
                            }
                            $("#user_municipality").html(dataMun);
                            $("#user_municipality").val(cityCode).attr("selected", "selected");

                            mCode =  $("#user_municipality").val();
                            m = mun.RECORDS.filter( element => element.citymunCode == String(mCode));

                            uccity = (m[0].citymunDesc).replace(/ *\([^)]*\) */g, "");
                            uccity = uccity.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                return letter.toUpperCase();
                            });

                            $("#u_city").val(uccity);                           
                        }
                    }
                });
                //GET BARANGAY
                $.ajax({    //create an ajax request to display.php
                    type: "GET",
                    url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refbrgy.json",
                    success: function(response){                    
                        //$("#responsecontainer").html(response); 
                        brgy=JSON.parse(response);
                        console.log(brgy);

                        var brgyCode = '<?php echo $brgyCode; ?>';

                        if(brgyCode != ""){
                            mCode = $("#user_municipality").val();
                            filterBrgy = brgy.RECORDS.filter( element => element.citymunCode == String(mCode));
                            for (x in filterBrgy){
                                dataBrgy= dataBrgy + "<option value='"+filterBrgy[x].brgyCode+"'>"+filterBrgy[x].brgyDesc.toUpperCase() + "</option>";
                            }
                            $("#user_brgy").html(dataBrgy);
                            $("#user_brgy").val(brgyCode).attr("selected", "selected");

                            bCode =  $("#user_brgy").val();
                            b = brgy.RECORDS.filter( element => element.brgyCode == String(bCode));

                            ucbrgy = (b[0].brgyDesc).replace(/ *\([^)]*\) */g, "");
                            ucbrgy = ucbrgy.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                return letter.toUpperCase();
                            });

                            $("#u_brgy").val(ucbrgy);                           
                        }
                    }
                });
            },100);

            $("#user_province").change(function(){
                console.log("value: "+$(this).val());
                dataMun="<option value='' selected disabled>-SELECT MUNICIPALITY-</option>";
                dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>";
                pCode = $(this).val();
                filterMun = mun.RECORDS.filter( element => element.provCode == String(pCode));
                for (x in filterMun){
                    dataMun= dataMun + "<option value="+filterMun[x].citymunCode+">"+(filterMun[x].citymunDesc).replace(/ *\([^)]*\) */g, "")+"</option>";
                }
                $("#user_municipality").html(dataMun);
                $("#user_brgy").html(dataBrgy);
                
                p = province.RECORDS.filter( element => element.provCode == String(pCode));

                ucprovince = p[0].provDesc;
                ucprovince = ucprovince.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                });

                $("#u_province").val(ucprovince);
                $("#u_city").val("");
                $("#u_brgy").val("");

            });

            $("#user_municipality").change(function(){
                mCode = $(this).val();
                dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>" ;
                filterBrgy = brgy.RECORDS.filter( element => element.citymunCode == String(mCode));
                for (x in filterBrgy){
                    dataBrgy= dataBrgy + "<option value='"+filterBrgy[x].brgyCode+"'>"+filterBrgy[x].brgyDesc.toUpperCase() + "</option>";
                }
                $("#user_brgy").html(dataBrgy);

                m = mun.RECORDS.filter( element => element.citymunCode == String(mCode));

                uccity = (m[0].citymunDesc).replace(/ *\([^)]*\) */g, "");
                uccity = uccity.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                });

                $("#u_city").val(uccity);
                $("#u_brgy").val("");

            });

            $("#user_brgy").change(function(){
                bCode = $(this).val();
                b = brgy.RECORDS.filter( element => element.brgyCode == String(bCode));
                console.log(b);
                ucbrgy = (b[0].brgyDesc).replace(/ *\([^)]*\) */g, "");
                ucbrgy = ucbrgy.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                });
                
                $("#u_brgy").val(ucbrgy);
                
            });
        });
    });
</script>
