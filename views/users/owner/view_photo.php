<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>

<div class="main">
    <div class="container">
        <h4>
            <a href="single.php" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Photo
            <div class="divider"></div>
        </h4>
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-image">
                        <img class="img-responsive materialboxed" src="../../../assets/images/default-image.png">
                    </div>
                    <div class="card-action">
                        <a href="#confirm_delete" class="modal-trigger">Delete</a>
                    </div>
                </div>
            </div>    
        </div>

        <form name="delete" id="delete" action="delete_photo.php" method="post">
            <input type="hidden" name="photoID" value="">
        </form>
        <!-- Delete Photo Confirmation Modal -->
        <div class="modal" id="confirm_delete">
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    <span>Confirmation</span>
                    <div class="divider"></div>
                </h4>
                <p class="center-align">Are you sure you want to delete this photo?</p>
            </div>
            <div class="modal-footer">
                <center>
                    <button form="delete" value="submit" class="btn-flat btn-floating yellow darken-3"><i class="large material-icons">check</i></a>
                    <div class="section"></div>
                </center>
            </div>
        </div>
    </div>
</div>

<?php require '../../partials/footer.php'; ?>