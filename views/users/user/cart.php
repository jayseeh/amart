<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>

<div class="main">
    <div class="container">
        <h4>Cart</h4>
        <div class="row">
            <ul class="tabs">
                <li class="tab"><a href="#all">All Carts</a></li>
                <li class="tab"><a href="#finished">Approved</a></li>
                <li class="tab"><a href="#declined">Declined</a></li>
                <!-- <li class="tab"><a href="#unclaimed">Unclaimed</a></li> -->
            </ul>
            <!-- start of all carts -->
            <div id="all">
                <div class="row">
                    <div class="col s12">
                        <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">Pending</span>
                            <div class="actions">
                            <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table id="allcart" class="striped">
                            <thead>
                                <th class="center-align">Business Name</th>
                                <th class="center-align">Ref Code</th>
                                <th class="center-align">Date</th>
                                <th class="center-align">Action</th>
                            </thead>
                            <tbody>
                                <?php 
                                $ID = $_SESSION['userID'];
                                $fetchquery= mysqli_query($connect, "SELECT DISTINCT cartRefCode, businessName, cartReqDate, userID, cartRefCode, cartStatus
                                FROM cart inner join business on cart.cartBisIDkey = business.businessID 
                                inner join cartprocess on cart.cartRefCode = cartprocess.cartRefCodekey
                                WHERE userID = '$ID' and cartStatus = 'Pending'"); 
                                while($fetchdata = mysqli_fetch_array($fetchquery)){
                                $date = $fetchdata['cartReqDate'];
                                $date=date_create($date);
                                $date = date_format($date,"F d, Y");
                                ?>
                                <tr>
                                    <td class="center-align"><?php echo $fetchdata['businessName'];?></td>
                                    <td class="center-align"><?php echo $fetchdata['cartRefCode'];?></td>
                                    <td class="center-align"><?php echo $date;?></td>
                                    <td class="center-align"><a href="#viewdetails-<?php echo $fetchdata['cartRefCode']; ?>" class="modal-trigger"><i class="material-icons">search</i></a></td>
                                </tr>
                                <?php 
                                    } 
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of all carts -->

            <!-- start of finished -->
            <div id="finished">
                <div class="row">
                    <div class="col s12">
                        <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">Confirmed</span>
                            <div class="actions">
                            <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table id="finishedcart" class="striped">
                            <thead>
                                <th class="center-align">Business Name</th>
                                <th>Ref Code</th>
                                <th>Date</th>
                                <th class="center-align">Action</th>
                            </thead>
                            <tbody>
                                <?php 
                                $ID = $_SESSION['userID'];
                                $fetchquery= mysqli_query($connect, "SELECT DISTINCT bisProvince, businessCategory, businessID, cartRefCode, businessName, cartReqDate, userID, cartRefCode, cartStatus
                                FROM cart inner join business on cart.cartBisIDkey = business.businessID 
                                INNER JOIN businesslocation on business.businessID = businesslocation.bisLocIDkey
                                inner join cartprocess on cart.cartRefCode = cartprocess.cartRefCodekey
                                WHERE userID = '$ID' and cartStatus = 'Approved'"); 
                                while($fetchdata = mysqli_fetch_array($fetchquery)){
                                    $province_data = $fetchdata['bisProvince'];
                                    $bid_data = $fetchdata['businessID'];
                                    $cid_data = $fetchdata['businessCategory'];
                                ?>
                                <tr>
                                    <td class="center-align"><?php echo $fetchdata['businessName'];?></td>
                                    <td class="center-align"><?php echo $fetchdata['cartRefCode'];?></td>
                                    <td class="center-align"><?php 
                                    $date = date_create($fetchdata['cartReqDate']);
                                    echo date_format($date,'F m,Y');?></td>
                                    <td class="center-align"><a href="#viewdetails-<?php echo $fetchdata['cartRefCode']; ?>" class="modal-trigger"><i class="material-icons">search</i></a></td>
                                </tr>
                                <?php 
                                    } 
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of finished cart -->

            <!-- start of declined cart -->
            <div id="declined">
                <div class="row">
                    <div class="col s12">
                        <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">Declined</span>
                            <div class="actions">
                            <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table id="declinedcart" class="striped">
                            <thead>
                                <th class="center-align">Business Name</th>
                                <th>Ref Code</th>
                                <th>Date</th>
                                <th class="center-align">Action</th>
                            </thead>
                        <tbody>
                                <?php 
                                $ID = $_SESSION['userID'];
                                $fetchquery= mysqli_query($connect, "SELECT DISTINCT cartRefCode, businessName, cartReqDate, userID, cartStatus
                                FROM cart inner join business on cart.cartBisIDkey = business.businessID 
                                inner join cartprocess on cart.cartRefCode = cartprocess.cartRefCodekey
                                WHERE userID = '$ID' and cartStatus = 'Declined'"); 
                                while($fetchdata = mysqli_fetch_array($fetchquery)){
                                ?>
                                <tr>
                                    <td class="center-align"><?php echo $fetchdata['businessName'];?></td>
                                    <td class="center-align"><?php echo $fetchdata['cartRefCode'];?></td>
                                    <td class="center-align"><?php 
                                    $date = date_create($fetchdata['cartReqDate']);
                                    echo date_format($date, 'F m,Y');?></td>
                                    <td class="center-align"><a href="#declineddetails-<?php echo $fetchdata['cartRefCode']; ?>" class="modal-trigger"><i class="material-icons">search</i></a></td>
                                </tr>
                                <?php 
                                    } 
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of declined cart -->

            <!-- start of unclaimed cart
            <div id="unclaimed">
                <div class="row">
                    <div class="col s12">
                        <div class="card material-table">
                        <div class="table-header">
                            <span class="table-title">Unclaimed</span>
                            <div class="actions">
                            <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table id="unclaimedcart" class="striped">
                            <thead>
                                <th class="center-align">Business Name</th>
                                <th>Ref Code</th>
                                <th>Date</th>
                                <th class="center-align">Action</th>
                            </thead>
                        <tbody>
                                <?php 
                                $ID = $_SESSION['userID'];
                                $fetchquery= mysqli_query($connect, "SELECT DISTINCT cartRefCode, businessName, cartReqDate, userID, cartStatus
                                FROM cart inner join business on cart.cartBisIDkey = business.businessID 
                                inner join cartprocess on cart.cartRefCode = cartprocess.cartRefCodekey
                                WHERE userID = '$ID' and cartStatus = 'Unclaimed'"); 
                                while($fetchdata = mysqli_fetch_array($fetchquery)){
                                ?>
                                <tr>
                                    <td class="center-align"><?php echo $fetchdata['businessName'];?></td>
                                    <td class="center-align"><?php echo $fetchdata['cartRefCode'];?></td>
                                    <td class="center-align"><?php 
                                    $date = date_create($fetchdata['cartReqDate']);
                                    echo date_format($date,'F m,Y');?></td>
                                    <td class="center-align"><a href="#viewdetails-<?php echo $fetchdata['cartRefCode']; ?>" class="modal-trigger"><i class="material-icons">search</i></a></td>
                                </tr>
                                <?php 
                                    } 
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- end of unclaimed cart -->

        </div>
    </div>
</div>

<!-- start of modals -->
<?php 
    $fetchquery= mysqli_query($connect, "SELECT DISTINCT bisProvince, bisCityTown, businessID, businessCategory, cartRefCode, businessName, cartReqDate, userID, cartStatus, cartprocess.cartTotal, cartprocess.cartpickupDateTime, reply
    FROM cart 
    INNER JOIN business ON cart.cartBisIDkey = business.businessID 
    INNER JOIN businesslocation on business.businessID = businesslocation.bisLocIDkey
    INNER JOIN cartprocess ON cart.cartRefCode = cartprocess.cartRefCodekey
    WHERE userID = '$ID'"); 
    while($fetchdata = mysqli_fetch_array($fetchquery)){
        $province_data = $fetchdata['bisProvince'];
        $city_data = $fetchdata['bisCityTown'];
        $bid_data = $fetchdata['businessID'];
        $cid_data = $fetchdata['businessCategory'];
?>
<div id="viewdetails-<?php echo $fetchdata['cartRefCode']; ?>" class="modal">
	<div class="modal-content">
        <h4>Details</h4>
        <div>
            Business Name: <strong><a href="<?php echo "dashboard_business.php?province=$province_data&town=$city_data&bID=$bid_data&cID=$cid_data"; ?>">
            <?php echo $fetchdata['businessName']; ?></a></strong><br>
            Request Date: <strong>
            <?php echo $fetchdata['cartReqDate']; ?></strong><br>
            Pickup Date and Time:<strong>
            <?php echo $fetchdata['cartpickupDateTime']; ?></strong><br>
            Total:<strong>
			<?php echo $fetchdata['cartTotal']; ?></strong><br>
        </div>
		<div>
			<table class="table table-responsive">
				<thead>
					<tr>
						<td>Name</td>
						<td>Qty</td>
						<td>Price</td>
						<td>Total</td>
					</tr>
				</thead>
                <?php
                    $refCode = $fetchdata['cartRefCode'];
                    $cartquery= mysqli_query($connect, "SELECT itemName, itemQty, itemPrice, cart.cartTotal
                    FROM cart
                    INNER JOIN cartprocess ON cart.cartRefCode = cartprocess.cartRefCodekey
                    WHERE cart.cartRefCode = '$refCode'"); 
                     while($fetchcart = mysqli_fetch_array($cartquery)){
                ?>
                    <tr>
                        <td><?php echo $fetchcart['itemName']; ?></td>
                        <td><?php echo $fetchcart['itemQty']; ?></td>
                        <td><?php echo $fetchcart['itemPrice']; ?></td>
                        <td><?php echo $fetchcart['cartTotal']; ?></td>
                    </tr>
                <?php 
                    } 
                ?>
			</table>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#" class="modal-close btn grey darken-3">Return</a>
	</div>
</div>

<div id="declineddetails-<?php echo $fetchdata['cartRefCode']; ?>" class="modal">
	<div class="modal-content">
		<h4>Details</h4>
        <div>
            Business Name: <strong>
            <?php echo $fetchdata['businessName']; ?></strong><br>
            Request Date: <strong>
            <?php echo $fetchdata['cartReqDate']; ?></strong><br>
            Pickup Date and Time:<strong>
            <?php echo $fetchdata['cartpickupDateTime']; ?></strong><br>
            Total:<strong>
			<?php echo $fetchdata['cartTotal']; ?></strong><br>
        </div>
		<div>
			<table class="table table-responsive">
				<thead>
					<tr>
						<td>Name</td>
						<td>Qty</td>
						<td>Price</td>
						<td>Total</td>
					</tr>
				</thead>
				<?php
                    $refCode = $fetchdata['cartRefCode'];
                    $cartquery= mysqli_query($connect, "SELECT itemName, itemQty, itemPrice, cart.cartTotal
                    FROM cart
                    INNER JOIN cartprocess ON cart.cartRefCode = cartprocess.cartRefCodekey
                    WHERE cart.cartRefCode = '$refCode'"); 
                     while($fetchcart = mysqli_fetch_array($cartquery)){
                ?>
                    <tr>
                        <td><?php echo $fetchcart['itemName']; ?></td>
                        <td><?php echo $fetchcart['itemQty']; ?></td>
                        <td><?php echo $fetchcart['itemPrice']; ?></td>
                        <td><?php echo $fetchcart['cartTotal']; ?></td>
                    </tr>
                <?php 
                    } 
                ?>
			</table>
		</div>
        <p>
            Reason for being declined: <br>
            <span id="reason"><em><?php echo $fetchdata['reply']; ?></em></span>
        </p>
	</div>
	<div class="modal-footer">
		<a href="#" class="modal-close btn grey darken-3">Return</a>
	</div>
</div>

<?php 
    } //end of modals loop
?>
<!-- end of modals -->
<script>
    $(document).ready(function() {
		$('#allcart').dataTable({
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        order : [ 2, 'desc' ],
        bAutoWidth: false,
        resposive: true
        });

        $('#finishedcart').dataTable({
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        order : [ 2, 'desc' ],
        bAutoWidth: false,
        resposive: true
        });

        $('#declinedcart').dataTable({
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        order : [ 2, 'desc' ],
        bAutoWidth: false,
        resposive: true
        });

        $('#unclaimedcart').dataTable({
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        order : [ 2, 'desc' ],
        bAutoWidth: false,
        resposive: true
        });
	});
</script>

<?php require '../../partials/footer.php'; ?>