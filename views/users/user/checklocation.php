<?php 
    require '../../partials/header.php'; 
    require 'nav_town.php'; 
    $businessID = $_GET['bID'];
    $cID = $_GET['cID'];
    $province = $_GET['province'];
    $town = $_GET['town'];
    $sql = "SELECT businessID, businessName, businessCategory FROM business WHERE businessID = $businessID";
    $query = mysqli_query($connect, $sql);
    while ($fetchdata = mysqli_fetch_array($query))  {
        $bID = $fetchdata['businessID'];
        $bName = $fetchdata['businessName'];
        $bCat = $fetchdata['businessCategory'];
    }
?>

<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul class="left">
                <li>
                    <a href="dashboard_business.php?province=<?php echo $province; ?>&town=<?php echo $town; ?>&bID=<?php echo $bID; ?>&cID=<?php echo $cID; ?>" class="large white-text"><i class="material-icons">arrow_back</i></a>
                </li>
                <li class="page-title truncate"><h4><?php echo $bName; ?></h4></li>
            </ul>
            <ul class="right">    
                <!-- <li><a href="#notif" class="modal-trigger"><i class="large material-icons">notifications</i></a></li> -->
            </ul>
        </div>
    </nav>
    <div class="map-container" style="overflow: hidden;">
        <div class="dvmap">
            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15291.761607788309!2d120.32638765!3d16.629759999999997!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1sen!2sph!4v1536324297149" height="400vh" width="100%" style="border:2"></iframe> -->
            <div id="mapUser" style="width:100%; height:200vh;" ></div>
        </div>
    </div>
</div>
<?php require '../../partials/footer.php'; ?>