<?php

if(isset($_SESSION['sID'])) {
    $aType = $_SESSION['aType'];

    if($aType != "User") {
        header("Location:../" . $aType . "/dashboard.php");
    }
}

?>