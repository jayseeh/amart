<?php 
  session_start();
  include_once "../../../db.sql/connect_db.php";

  $navtitle = "E-Town Special";
  $thisID = $_SESSION['userID'];
  $datenow = date('Y-m-d');

  $sql = "SELECT uAddress, uEmail, uGender FROM accounts where aID='$thisID'";
  $fetchquery = mysqli_query($connect, $sql);
  while($fetchdata = mysqli_fetch_array($fetchquery)){
    $data_address = $fetchdata['uAddress'];
    $data_email = $fetchdata['uEmail'];
    $data_gender = $fetchdata['uGender'];
  }

  if( $data_address == '' || $data_email == '' || $data_gender =='' ){
    header('location: profile.php?new');
  } else {
    include '../../partials/header.php'; 
    include 'nav.php'; 
    
?>

<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
          <ul class="left">
            <li class="page-title truncate"><h4><?php echo $navtitle; ?></h4></li>
          </ul>
          <ul class="right">
            <!-- <li><a href="#notif" class="modal-trigger"><i class="large material-icons">notifications</i></a></li> -->
		      </ul>
        </div>
    </nav>

  <div class="container">
  <div class="row">
    <?php 
        $query = "SELECT *
        FROM favorites 
        INNER JOIN events ON favorites.noteBusinessIDkey = events.eventBisIDkey 
        INNER JOIN business ON events.eventBisIDkey = business.businessID
        INNER JOIN businesslocation ON events.eventBisIDkey = businesslocation.bisLocIDkey
        WHERE favorites.noteUserID = '$thisID' 
        AND business.businessSubscriptionEnd >= '$datenow'
        AND events.endDate >= '$datenow'
        OR businessSubscriptionEnd = '0000-00-00'
        -- AND business.businessStatus = 'Active' 
        AND events.endDate >= '$datenow'
        -- AND events.eventStatus='Active' 
        ORDER BY eventID desc";
        
        $fetchquery= mysqli_query($connect, $query);
        $count = mysqli_num_rows($fetchquery);
        
        if($count > 0 ){
          while($fetchdata = mysqli_fetch_array($fetchquery)){
            $dateposted = $fetchdata['datePosted'];
            $date = substr($dateposted,0,10);  
            $thisdate = date_create($date);
            $time = substr($dateposted,11,19);  
            $thistime = date_create($time);
            $dateposted = date_format($thisdate,"M d, Y");
            $timeposted = date_format($thistime,"h:i A");

            $startDate = $fetchdata['startDate'];
            $startDate = date_create($startDate);

            $endDate = $fetchdata['endDate'];
            $endDate = date_create($endDate);
        ?>
          <div class='col s12 m4 l4'>
            <div class='card card-dashboard'>
            <div class="card-image">
                <img src='../../../assets/eventPics/<?php echo $fetchdata['eventPoster'];?>' class='materialboxed col s12 m12 l12 show-on-med-and-down'>
            </div>
            <div class='card-content'>
                <span class='card-title'>
                <a href="dashboard_business.php?province=<?php echo $fetchdata['bisProvince'];?>&town=<?php echo $fetchdata['bisCityTown'];?>&bID=<?php echo $fetchdata['businessID'];?>&cID=<?php echo $fetchdata['businessCategory'];?>&event">
                <p class="truncate"><p class="truncate"><?php echo $fetchdata['eventTitle']; ?></p></a>
                </span>
                <small><?php echo date_format($startDate,"M d, Y") . ' - ' . date_format($endDate,"M d, Y");?>
                <br>By: <?php echo $fetchdata['businessName']; ?>
                </small>
                <div class='divider'></div>
                <p class="truncate"><?php echo $fetchdata['eventDesc']; ?></p>
            </div>
            </div>
        </div>
        <?php 
          } 
        }else{
          echo "<br><br><br><br><center><img src='../../../assets/images/partners.PNG' width='60%'>";
        }
      ?>
                            
    </div>
  </div>
</div>
<!-- end of main -->

<!-- Getting Started -->
<div id="getting-started" class="modal">
  <form action="../../../db.sql/updateSecretQuestion.php" method='post'>
      <div class="modal-content">
          <h4>
              Getting Started
          </h4>
          <p class="center-align"><em>If you lose your password, you can use this to retrieve your account.</em></p>
          <div class="row">
            <div class="col s12 m12 l12 input-field">
                <label for="email">Email Address</label>
                <input type="email" name="email" class="validate" required autofocus/>
            </div>
          </div>     
    <label for="secretquestion"></label>
    <select name="secretquestion">
      <option value="" disabled selected>--Choose Secret Question--</option>
      <?php
        $query = "SELECT content FROM secretquestions";
        $fetchquery = mysqli_query($connect, $query);
        while($fetchdata = mysqli_fetch_array($fetchquery)) {

      ?>
      <option value="<?php echo $fetchdata['content']; ?>"><?php echo $fetchdata['content']; ?></option>
        <?php } ?>
    </select>
    <div class="row">
      <div class="col s12 m12 l12 input-field">
        <label for="secretanswer">Answer</label>
        <input type="text" name="secretanswer" class="validate" required />
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <input type='hidden' name='ID' value='<?php echo $_SESSION['userID'];?>'>
    <input type='hidden' name='type'value='<?php echo $_SESSION['aType'];?>'>
    <input type="submit" value="Proceed" class="btn yellow darken-3">
  </div>
  </form>
</div>

<script>
  var map;
  var pos;
  var lats,lngs;
  function initMap() {

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        lats= position.coords.latitude;
        lngs= position.coords.longitude;
        document.getElementById('businessLat').value=lats;
        document.getElementById('businessLng').value=lngs;

      }, function() {
        handleLocationError(true, infoWindow, map.getCenter());
      });
    }

    setTimeout(function(){
      console.log("mapss");
      console.log("Lat: "+lats+ " Lng: "+lngs);
      var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png';
      map = new google.maps.Map(document.getElementById('mapUser'), {
        center: {lat:12.054440, lng:121.521023},
        zoom: 5.5,
        mapTypeId: 'satellite',
        streetViewControl: false,
        mapTypeControl: false,
        fullscreenControl:false,
        zoomControl:true,
        draggable:true
      });
      
      var marker = new google.maps.Marker({
        position: {lat: lats, lng: lngs},
        map: map,
        icon: iconBase,
        draggable:true
      });
      google.maps.event.addListener(marker, 'dragend', function(evt){
        console.log( 'Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(6) + ' Current Lng: ' + evt.latLng.lng().toFixed(6) );
        //alert( 'Marker dropped: Current Lat: ' + evt.latLng.lat() + ' Current Lng: ' + evt.latLng.lng() );
        document.getElementById('businessLat').value=evt.latLng.lat().toFixed(6);
        document.getElementById('businessLng').value=evt.latLng.lng().toFixed(6);
      });
    },1000);
  }
</script>

<?php require '../../../views/partials/footer.php' ?>

<script>
  <?php
    $currentUser =$_SESSION['userID'];

    $sql = "SELECT aStatus FROM accounts where aID = '$currentUser'";
    $query = mysqli_query($connect, $sql);
    while($fetchdata = mysqli_fetch_array($query)){
      $status = $fetchdata['aStatus'];
    }
    if($status == "Active"){
  ?>
    $(document).ready(function(){
      $('#getting-started').modal('open'); 
    });
  <?php
    } 
  ?>
 </script>
<?php 
  } 
?>