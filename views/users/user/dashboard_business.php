<?php 
    session_start();
    include_once "../../../db.sql/connect_db.php";

    $datenow = date('Y-m-d');

    $bisnesID = $_GET['bID'];
     
    $query = mysqli_query($connect, "SELECT businessSubscriptionEnd, categories.categoryType
            FROM business 
            INNER JOIN categories 
            ON business.businessCategory = categories.categoryID
            WHERE businessID = '$bisnesID'");
    
    while($fetchsubsdate = mysqli_fetch_array($query)){
        $categorytype = $fetchsubsdate['categoryType'];
        $subsenddate = $fetchsubsdate['businessSubscriptionEnd'];
    }

    $excempt = ['Free', 'Worship'];

    if( !in_array($categorytype, $excempt) ){
        if($subsenddate < $datenow){
            header("location: dashboard.php?message=expired&$categorytype");
        } 
    } 

    require '../../partials/header.php'; 
    require 'nav_town.php';
?>

<?php 
    $event = '';
    if(isset($_GET['event'])){
        $event = 'active';
    }

    $album = '';
    if(isset($_GET['album'])){
        $album = 'active';
    }
    
    $status = '';
    $busName = '';
    $busAddNo = '';
    $busAddStreet = '';
    $busAddBrgy = '';
    $busAddCity = '';
    $busAddProv = '';
    $busDescription = '';
    $busCompAddress = '';

    $sql = "SELECT * from business 
            INNER JOIN businesslocation 
            ON businesslocation.bisLocIDkey = business.businessID 
            INNER JOIN categories 
            ON  categories.categoryID = business.businessCategory 
            WHERE businessID = '$bisnesID'
            ";

    $query = mysqli_query($connect, $sql);
    $count = mysqli_num_rows($query);
    while ($fetchdata = mysqli_fetch_array($query))  {
        $busName = $fetchdata['businessName'];
        $busAddNo = $fetchdata['bisNumber'];
        $busAddStreet = $fetchdata['bisStreet'];
        $busAddBrgy = $fetchdata['bisBrgy']; 
        $busAddCity = $fetchdata['bisCityTown'];
        $busAddProv = $fetchdata['bisProvince'];
        $busDescription = $fetchdata['businessDescription'];
        $busCategpryType = $fetchdata['categoryType'];

        $busCompAddress = $busAddNo . ' ' . $busAddStreet . ' ' . $busAddBrgy . ' ' . $busAddCity . ' ' . $busAddProv;
        $busCompAddress = strtolower($busCompAddress);
        $busCompAddress = ucwords($busCompAddress);
    }
    $user = $_SESSION['userID'];
    $fetchquery= mysqli_query($connect, "SELECT count(*) as x FROM cart WHERE userID = '$user' and cartBisIDkey = '$bisnesID' and cartStatus = 'Active' and cartItemType = 'Product'"); 
    $cartcount = mysqli_num_rows($fetchquery);
    while($fetchdata = mysqli_fetch_array($fetchquery)){
        $cartcount = $fetchdata['x'];
    }
?>
<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul class="right"> 
                <?php 
                $favorite = 1;
                $bid= $_GET['bID'];
                $uid=$_SESSION['userID'];
                $query = mysqli_query($connect, "SELECT * FROM favorites WHERE noteBusinessIDkey='$bid' and noteUserID='$uid'");
                $favorite = mysqli_num_rows($query);
                if($favorite == 1){
                ?>
                <li><a href="#unfavorite" class="modal-trigger"><i class="material-icons red-text">favorite</i></a></li>
                <?php
                    }else{
                ?>
                <li><a href="#favorite" class="modal-trigger"><i class="material-icons red-text">favorite_border</i></a></li>
                <?php
                    }
                ?>
                
                <?php 
                    if( $busCategpryType != 'Free' && $busCategpryType != 'Worship' ) {
                ?> 
                    <li><a href="#cart" class="modal-trigger"><i class="material-icons">shopping_cart</i><span style="position: absolute; bottom: -10px; right: 10px;"><?php echo $cartcount; ?></span></a></li>
                <?php 
                    }
                ?>

                <!-- <li><a href="#notif" class="modal-trigger"><i class="large material-icons">notifications</i></a></li> -->
            </ul>
        </div>
    </nav>
    <div class="container">
        <h4><span class="grey-text text-darken-4">
        <?php echo $busName; ?>
        </span><a href="checklocation.php?province=<?php echo $busAddProv; ?>&town=<?php echo $busAddCity; ?>&bID=<?php echo $_GET["bID"]; ?>&cID=<?php echo $_GET['cID'];?>" class="btn yellow darken-3 right"><span class="hide-on-small-only left">Check Location</span><i class="material-icons">my_location</i></a></h4>
        <div class="row">
            <div class="row hide-on-small-only">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s4 blocks"><a href="#business" class="yellow-text text-darken-3">Business</a></li>
                        <li class="tab col s4 blocks"><a href="#events" class="yellow-text text-darken-3 <?php echo $event; ?>">Events</a></li>
                        <li class="tab col s4 blocks"><a href="#business-album" class="yellow-text text-darken-3 <?php echo $album; ?>">Album</a></li>
                    </ul>
                </div>
            </div>
            <div class="row row-icon-dashboard-business show-on-small hide-on-med-and-up">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s4"><a href="#business" class="yellow-text text-darken-3"><span class="yellow-text text-darken-3"><i class="small material-icons">store</i></span></a></li>
                        <li class="tab col s4"><a href="#events" class="yellow-text text-darken-3 <?php echo $event; ?>"><span class="yellow-text text-darken-3"><i class="small material-icons">today</i></span> <!-- <span class="badge white-text grey darken-2 new">1</span>--></a></li>
                        <li class="tab col s4"><a href="#business-album" class="yellow-text text-darken-3  <?php echo $album; ?>"><span class="yellow-text text-darken-3"><i class="small material-icons">camera</i></span></a></li>
                    </ul>
                </div>
            </div>

            <?php 
                $sql = "SELECT * FROM businessphotos WHERE bisPhotoIDKey = '$bisnesID'";
                $query = mysqli_query($connect, $sql);
                $counter = mysqli_num_rows($query);
            ?>
            <!-- Business Tab -->
            <div id="business" class="col s12">
                <div class="business-photo" style="width: 100%; padding: 0;">
                    <div class="carousel carousel-slider">
                        <?php 
                            if($counter == 0){
                        ?>
                            <a class="carousel-item carousel-business-photo"><img src="../../../assets/images/default-image.png"></a>
                        <?php 
                        }else{ 
                            while($fetchPhotoData = mysqli_fetch_array($query)){ ?>
                            <a class="carousel-item modal-trigger" height="100%" href="#viewbisphoto-<?php echo $fetchPhotoData['photoID']; ?>"><img src="../../../assets/images/<?php echo $fetchPhotoData['bisPhoto'];?>" height="100%"></a>
                        <?php 
                            }
                        } ?>
                    </div>
                </div>
                    <p>
                        <div class="row center-align">
                            <div class="col s12 m12 l12">
                                Address: <?php echo $busCompAddress; ?>
                            </div>
                        </div>
                        <?php 
                            $sql = "SELECT * from businesscalendar where bisCalendarIDkey = '$bisnesID'";
                            $query = mysqli_query($connect, $sql);
                            $count = mysqli_num_rows($query);
                        ?>
                        <div class="row">
                            <div class="col s12 m2 l2 valign-wrapper">
                                <strong>Schedule</strong> 
                            </div>
                            <div class="col s12 m10 l10">
                                <table>
                                <?php
                                if($count!=0){
                                    while($fetchdata = mysqli_fetch_array($query)){
                                ?>
                                        <tr>
                                            <td><?php echo $fetchdata['bisSchedDay']?></td>
                                            <td><?php echo $fetchdata['bisSchedTime']?></td>
                                        </tr>
                                <?php 
                                    }
                                }else{
                                ?>
                                    <tr>
                                        <td><em>Schedule is not available</em></td>
                                    </tr>
                                <?php
                                }
                                ?>
                                </table>
                            </div>
                        </div>
                    </p>
                    <?php   
                        $businessID = $_GET['bID'];
                        $fetchquery= mysqli_query($connect, "SELECT * FROM business WHERE businessID = '$businessID'"); 
                        while($fetchdata = mysqli_fetch_array($fetchquery)){
                        ?>
                    <?php 
                        $query = mysqli_query($connect, "SELECT * FROM business inner join categories on business.businessCategory=categories.categoryID WHERE businessID ='$businessID'");
                        $datasss = mysqli_fetch_array($query);
                        if($datasss['categoryType']!='Worship'){
                    ?>
                    <p>Contact: <em><?php 
                    $num = $fetchdata['businessPhone'];
                    if($num == '' ){
                        echo 'n/a';
                    }else{
                        echo $num;    
                    }?>
                    </em></p>
                    <p>Links: <em>
                    <a href="<?php echo $fetchdata['businessFB'];?>" target="_blank">
                    <?php 
                    $fb = $fetchdata['businessFB'];
                    if($fb == '' ){
                        echo 'n/a';
                    }else{
                        echo $fb;    
                    }
                    ?></em></p>
                    <?php
                                }
                                ?>
                                </a> 
                            <?php } ?>
                <h5>Overview</h5>
                <p>
                    <?php 
                        $checkDesc =  $busDescription;
                        if ($checkDesc == '') {
                            echo "<em>Not Available</em>";
                        } else {
                            $desc = $checkDesc;
                            $desc = preg_replace("/[\n]/", "<br>\n", $desc);
                            echo $desc;
                        }
                    ?>
                </p>
                <br />
                <?php 
                    $idid = $_GET['bID'];
                    $query = mysqli_query($connect, "SELECT * FROM business inner join categories on business.businessCategory=categories.categoryID WHERE businessID ='$idid'");
                    $datasss = mysqli_fetch_array($query);
                    if($datasss['categoryType']!='Worship'){
                ?>
                <h5>Products and Services</h5>

                <?php 
                    $sql = "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Product' and businessPSStatus != 'Archived'";
                    $fetchquery= mysqli_query($connect, $sql); 
                    $count = mysqli_num_rows($fetchquery);
                ?>

                <div class="products">
                    <h6>Products</h6>
                    <div class="row">
                        <?php
                            if($count!=0){
                                while($fetchproduct = mysqli_fetch_array($fetchquery)){
                        ?>
                                <a href="#details-<?php echo $fetchproduct['businessPSID']; ?>" class="modal-trigger">
                                    <div class="col s6 m4 l4">
                                        <div class="card card-product center-align">
                                            <div class="card-image card-product-photo">
                                                <img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage'] ;?>" alt="<?php echo $fetchproduct['businessPSName']; ?>">
                                                <span class="card-title card-product-title truncate"><?php echo $fetchproduct['businessPSName']; ?></span>
                                            </div>
                                            <div class="card-content card-product-content">
                                                <span class="product-price"><small>Php</small> <?php echo $fetchproduct['businessPSPrice']; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                        <?php 
                                }
                            }else{
                                echo "<p class='center-align'><em>No Product Available</em></p>";
                            }
                        ?>
                    </div>
                </div>

                <?php 
                    $sql = "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Service' and businessPSStatus != 'Archived'";
                    $fetchquery= mysqli_query($connect, $sql); 
                    $count = mysqli_num_rows($fetchquery);
                ?>
                
                <div class="services">
                    <h6>Services</h6>
                    <div class="row">
                        <?php 
                            if($count!=0){
                                while($fetchservice = mysqli_fetch_array($fetchquery)){
                        ?>
                                <a href="#details-<?php echo $fetchservice['businessPSID']; ?>" class="modal-trigger">
                                    <div class="col s6 m4 l4">
                                        <div class="card card-product center-align">
                                            <div class="card-image card-product-photo">
                                                <img src="../../../assets/ps/<?php echo $fetchservice['businessPSImage'] ;?>" alt="<?php echo $fetchservice['businessPSName']; ?>">
                                                <span class="card-title card-product-title truncate"><?php echo $fetchservice['businessPSName']; ?></span>
                                            </div>
                                            <div class="card-content card-product-content">
                                                <span class="product-price"><small>Php</small> <?php echo $fetchservice['businessPSPrice']; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                        <?php 
                            }
                        }else{
                            echo "<p class='center-align'><em>No Service Available</em></p>";
                        }
                        ?>
                    </div>                
                </div>
            <?php } ?>
                <a href="#reportbusiness" class="modal-trigger white-text"><span class="right red" style="padding: 5px; border-radius: 25px;">Report<i class="material-icons right">report</i></span></a>  
            </div>
        
                    
        
            <div id="events" class="col s12 m12 l12">
                <?php
                    $sql = "SELECT * FROM events WHERE eventBisIDKey = '$bisnesID' AND eventStatus = 'Active' AND events.endDate >= '$datenow' ORDER BY eventID DESC, startDate ASC";
                    $query = mysqli_query($connect, $sql);
                    $counton = mysqli_num_rows($query);
                    if($counton!=0){
                ?>
                    <div class="row"> 
                        <div class="col s12 m12">
                            <h4>Ongoing Events</h4>
                        </div>
                    </div>
                    <div class="row">
                    <?php
                        while($fetchEvent = mysqli_fetch_array($query)){
                            $checkDesc =  $fetchEvent['eventDesc'];
                            if ($checkDesc == '') {
                                $desc = "Description is empty";
                            } else {
                                $desc = $checkDesc;
                                $desc = preg_replace("/[\n]/", "<br>\n", $desc);
                            }

                            $startDate = $fetchEvent['startDate'];
                            $startDate = date_create($startDate);

                            $endDate = $fetchEvent['endDate'];
                            $endDate = date_create($endDate);
                    ?>
                        <div class="col s12 m6 l6">
                            <div class="card card-events">
                                <div class="card-image">
                                    <img src="../../../assets/eventPics/<?php echo  $fetchEvent['eventPoster']; ?>" class="materialboxed">
                                    <span class="card-title card-title-background">
                                        <?php echo  $busName; ?>
                                    </span>
                                </div>
                                <div class="card-content">
                                    <span class="card-title activator">
                                    <p class="truncate"><?php echo  $fetchEvent['eventTitle']; ?></p>
                                        <small><?php echo  date_format($startDate, "M d, Y"); ?> - <?php echo  date_format($endDate, "M d, Y"); ?></small><i class="material-icons right">more_vert</i><br>
                                        
                                    </span>
                                    <div class="divider"></div>
                                    <p class="truncate"><?php echo $checkDesc; ?></p>
                                </div>
                                <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4"><?php echo  $fetchEvent['eventTitle']; ?><i class="material-icons right">close</i></span>
                                <small><?php echo date_format($startDate, "M d, Y"); ?> - <?php echo  date_format($endDate, "M d, Y"); ?></small>
                                <div class="divider"></div>
                                <p><?php echo $desc; ?></p>
                                </div>
                            </div>
                        </div>
                    <?php        
                        }
                    ?>
                    </div>
                    <?php
                    }
                    ?>
                    
                <?php
                    $sql = "SELECT * FROM events WHERE eventBisIDKey = '$bisnesID' AND eventStatus = 'Inactive';";
                    $query = mysqli_query($connect, $sql);
                    $countup = mysqli_num_rows($query);
                    if($countup!=0){
                ?>
                    <div class="row"> 
                        <div class="col s12 m12">
                            <h4>Upcoming Events</h4>
                        </div>
                    </div>
                    <div class="row">
                    <?php
                        while($fetchEvent = mysqli_fetch_array($query)){
                            $checkDesc =  $fetchEvent['eventDesc'];
                            if ($checkDesc == '') {
                                $desc = "Description is empty";
                            } else {
                                $desc = $checkDesc;
                                $desc = preg_replace("/[\n]/", "<br>\n", $desc);
                            }
                    ?>
                        <div class="col s12 m6 l6">
                            <div class="card card-events">
                                <div class="card-image">
                                    <img src="../../../assets/eventPics/<?php echo  $fetchEvent['eventPoster']; ?>">
                                    <span class="card-title">
                                        <?php echo  $busName; ?>
                                    </span>
                                </div>
                                <div class="card-content">
                                    <span class="card-title">
                                        <?php echo  $fetchEvent['eventTitle']; ?><br>
                                        <small><?php echo  $fetchEvent['startDate']; ?> - <?php echo  $fetchEvent['endDate']; ?></small>
                                    </span>
                                    <p class="truncate"><?php echo  $desc; ?></p>
                                </div>
                            </div>
                        </div>
                    <?php        
                        }
                    ?>
                    </div>
                <?php
                    }
                    if($countup == 0 && $counton == 0){
                        echo '<p class="center-align no-data"><em>There is currently no ongoing or upcoming event</em></p>';
                    }
                ?> 
            </div>
            <!-- END of Events Tab -->

            <!-- Business Experience Tab -->
            <div id="business-album" class="col s12">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <a href="#business-experience" class="btn col s12 m12 l12 yellow darken-3 modal-trigger">Post an Experience</a>
                    </div>
                    <?php 
                            $town = $_GET['town'];
                            $bID = $_GET['bID'];
                            $fetchquery= mysqli_query($connect, "SELECT * FROM experience 
                            inner join business on experience.businessID = business.businessID
                            inner join businesslocation on business.businessID = businesslocation.bisLocIDkey 
                            inner join accounts on experience.uploaderID = accounts.aID 
                            where businesslocation.bisCityTown = '$town' and experience.businessID = $bID order by expID desc"); 
                            $albumcount = mysqli_num_rows($fetchquery);
                            
                            if($albumcount > 0){
                                while($fetchdata = mysqli_fetch_array($fetchquery)){
                                    $fname = $fetchdata['fname'];
                                    $mname = $fetchdata['mname'];
                                    $minitial = substr($mname,0, 1);
                                    $lname = $fetchdata['lname'];
                                    $fullname = $fname.' '.$minitial.'. '.$lname;
                                    $date = $fetchdata['dateTime'];
                                    $thisDate = date_create($date);
                                    $thisDate = date_format($thisDate, "M d, Y | h:iA");

                            
                                    $checkDesc =  $fetchdata['expDesc'];
                                    if ($checkDesc == '') {
                                        $desc = "Caption is empty";
                                    } else {
                                        $desc = $checkDesc;
                                        $desc = preg_replace("/[\n]/", "<br>\n", $desc);
                                    }
                            
                            ?>
                    <div class="col s12 m6 l6">
                        <!-- card album-entry -->
                    
                        <div class="card card-album-entry">
                            <div class="card-image">
                                <img src="../../../assets/expPics/<?php echo $fetchdata['expPhoto'];?>" class="materialboxed col s12 m12 l12">
                            </div>
                            <div class="card-content truncate">
                                <span class="card-title activator">
                                    <i class="material-icons right">more_vert</i>
                                </span>
                                <p>
                                    <strong>Uploader: </strong><?php echo $fullname; ?><br>
                                    <strong>Date: </strong><?php echo $thisDate; ?><br>
                                    <span class="truncate"><strong>Caption: </strong><?php echo $checkDesc; ?></span>
                                </p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
                                <p>
                                    <strong>Uploader: </strong><?php echo $fullname; ?><br>
                                    <strong>Date: </strong><?php echo $thisDate; ?><br>
                                    <strong>Caption: </strong><?php echo $checkDesc; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php 
                            } 
                        }else{
                            echo "<p class='center-align no-data'><em>There is Currently no Album Present in this Establishment.</em></p>";
                        }        
                    ?>
                </div>
            </div>
            <!-- END of Experience Tab -->
</div>
<!-- End of Tabs -->
</div>
<!-- End of Container -->

    <!-- Favorite Modal -->
    <div id="favorite" class="modal">
        <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Note to favorite
        </h4>
            <form action="../../../db.sql/insertfavorite.php" method="post">
                <textarea class='message materialize-textarea' name='note' id='note' placeholder="Enter Note" required></textarea>
                <div class="section"></div>
                <center>
                    <input type='hidden' name='businessID' value='<?php echo $_GET['bID']?>'>
                    <input type='hidden' name='userID' value='<?php echo $_SESSION['userID']?>'>
                    <input type='hidden' name='town' value='<?php echo $_GET['town']?>'>
                    <input type="hidden" name="province" value='<?php echo $_GET['province']; ?>'>
                    <input type="hidden" name="cID" value='<?php echo $_GET['cID']; ?>'>
                    <input type="submit" value="Submit" class="btn yellow darken-3">
                </center>
            </form>
        </div>        
    </div>
    
    <!-- unFavorite Modal -->
    <div id="unfavorite" class="modal">
        <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Remove to favorite
        </h4>
            <form action="../../../db.sql/deletefavorite2.php" method="post">
                <p class="center-align">You're about to remove this business in your favorites</p>
                <center>
                    <input type='hidden' name='town' value='<?php echo $_GET['town']?>'>
                    <input type="hidden" name="province" value='<?php echo $_GET['province']; ?>'>
                    <input type="hidden" name="cID" value='<?php echo $_GET['cID']; ?>'>
                    <input type='hidden' name='bID' value='<?php echo $_GET['bID']?>'>
                    <input type='hidden' name='userID' value='<?php echo $_SESSION['userID']?>'>
                    <input type="submit" value="Confirm" class="btn yellow darken-3">
                </center>
            </form>
        </div>        
    </div>

    <!-- Cart Modal -->
    <div id="cart" class="modal">
        <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Cart
        </h4>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <td>Product</td>
                        <td>Quantity</td>
                        <td>Price</td>
                        <td class="center-align">Total</td>
                        <td class="center-align">Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $user = $_SESSION['userID'];
                        $bisnes = $_GET['bID'];
                        $fetchquery= mysqli_query($connect, "SELECT * FROM cart WHERE userID = '$user' and cartBisIDkey = '$bisnes' and cartStatus = 'Active' and cartItemType = 'Product'"); 
                        $cartcount = mysqli_num_rows($fetchquery);
                        while($fetchdata = mysqli_fetch_array($fetchquery)){
                    ?>
                    <tr>
                        <td><?php echo $fetchdata['itemName']?></td>
                        <td><?php echo $fetchdata['itemQty']?> pcs</td>
                        <td>Php <?php echo $fetchdata['itemPrice']?></td>
                        <td class="center-align">Php <?php echo $fetchdata['cartTotal']?></td>
                        <td class="center-align">
                            <form action='../../../db.sql/deletecartitem.php' method='post'>
                                <input type='hidden' name='cartID' value='<?php echo $fetchdata['cartID'];?>'>
                                <input type='hidden' name='town' value='<?php echo $_GET['town']?>'>
                                <input type="hidden" name="province" value='<?php echo $_GET['province']; ?>'>
                                <input type="hidden" name="cID" value='<?php echo $_GET['cID']; ?>'>
                                <input type='hidden' name='bID' value='<?php echo $_GET['bID']?>'>
                                <button type="submit" class="btn-large btn-flat yellow-text text-darken-3" ><i class="material-icons">delete</i></button>
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php 
                    $user = $_SESSION['userID'];
                    $bisnes = $_GET['bID'];
                    $fetchquery= mysqli_query($connect, "SELECT SUM(cartTotal) as total FROM cart WHERE userID = '$user' and cartBisIDkey = '$bisnes' and cartStatus = 'Active' and cartItemType = 'Product'"); 

                    while($fetchdata = mysqli_fetch_array($fetchquery)){
                        $fetchcarttotal = $fetchdata['total'];
                    } 
                    ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td> 
                        <td>
                            <div class="center-align"><strong>Cart Total: Php <?php echo $fetchcarttotal; ?></strong></div>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <div class="section"></div>

            <?php if($cartcount > 0){ ?>
            <div class="center-align">
                <a href="requestnext.php?province=<?php echo $_GET['province']?>&town=<?php echo $_GET['town']?>&bID=<?php echo $_GET['bID']?>&cID=<?php echo $_GET['cID']?>" class="btn yellow darken-3">Proceed</a>
            </div>
            <?php } ?>
        </div>        
    </div>
    <!-- Experience Modal -->
    <div id="business-experience" class="col s12 modal">
        <form action="../../../db.sql/insertexp.php" class="form-horizontal"  role="form" class="col s12"  method="post" enctype="multipart/form-data">
        <div class="modal-content">
            <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                Post an Experience
            </h4>
            <div class='row'>
                <div class='input-field col s12 m12 l12'>
                    <i class="material-icons prefix yellow-text text-darken-3  hide-on-small-only">edit</i>
                    <textarea class='message materialize-textarea' name='myexp' id='myexp' required></textarea>
                    <label for='myexp'>My Experience</label>
                </div>
                </div>
                <div class="row">
                        <input type='hidden' name='uplaoder' value='<?php echo $_SESSION['userID'];?>'>
                        <input type='hidden' name='business' value='<?php echo $_GET['bID'];?>'>
                        <input type='hidden' name='town' value='<?php echo $_GET['town'];?>'>
                        <input type='hidden' name='province' value='<?php echo $_GET['province'];?>'>
                        <input type='hidden' name='cID' value='<?php echo $_GET['cID'];?>'>
                        <input type='hidden' name='datetime' value='<?php echo date("Y-m-d H:i:s");?> '>
                        <div class="file-field input-field">
                            <div class="btn btn-flat yellow darken-3 white-text">
                                <i class="material-icons">search</i>
                                <input type="file" name="photo" id='photo' accept="image/*">
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" class="file-path validate" placeholder="Upload Photo" id="photo">
                            </div>
                        </div>
                    </form>
                </div> 

            <div class="modal-footer">
                <input type="submit" value="Post" name="submit" class="btn yellow darken-3">
            </div>
            </form>
        </div>
    </div>
    <!-- END -->


    <!-- Product Details -->
    <?php 
        $fetchquery= mysqli_query($connect, "SELECT * FROM business inner join businessPS on business.businessID = businessps.businessPSIDkey
        WHERE businessPSIdkey='$bisnesID' and businessPSType='Product'"); 
        while($fetchproduct = mysqli_fetch_array($fetchquery)){ 
            
            $checkDesc =  $fetchproduct['businessPSDesc'];
            if ($checkDesc == '') {
                $desc = "Description is empty";
            } else {
                $desc = $checkDesc;
                $desc = preg_replace("/[\n]/", "<br>\n", $desc);
            }
            
        ?>
        <div id="details-<?php echo $fetchproduct['businessPSID']; ?>" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    Product Details
                </h4>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage'] ;?>"  class="col s12 m12 l12 materialboxed" alt="product">
                    </div>
                    <div class="col s12 m6 l6">
                        <small>Name:</small><br> <strong><?php echo $fetchproduct['businessPSName']; ?></strong><br>
                        <small>Description:</small><br> <strong><?php echo $desc; ?></strong><br>
                        <small>Price:</small><br> <strong>Php <?php echo $fetchproduct['businessPSPrice']; ?></strong><br>
                    </div>
                </div>
            </div>
            <?php if( $busCategpryType != 'Free' && $busCategpryType != 'Worship' ) { ?>
            <div class="modal-footer modal-footer-ps">
                <form action="../../../db.sql/insertToCart.php" method="POST">
                    <input type="hidden" name="prodID" value=<?php echo $fetchproduct['businessPSID'];?>>
                    <input type="hidden" name="userID" value=<?php echo $_SESSION['userID'];?>>
                    <input type="hidden" name="bisnesID" value=<?php echo $fetchproduct['businessID'];?>>
                    <input type="hidden" name="serviceType" value='---'>
                    <input type="hidden" name="itemtype" value='Product'>
                    <input type="hidden" name="itemname" value='<?php echo $fetchproduct['businessPSName']; ?>'>
                    <input type="hidden" name="itemprice" value='<?php echo $fetchproduct['businessPSPrice']; ?>'>
                    <input type="hidden" name="town" value='<?php echo $_GET['town']; ?>'>
                    <input type="hidden" name="province" value='<?php echo $_GET['province']; ?>'>
                    <input type="hidden" name="cID" value='<?php echo $_GET['cID']; ?>'>
                    <div class="row right">
                        <div class="col s3 m3 l3 offset-s2 offset-m4 offset-l4 input-field">
                            <label for="qty">Quantity</label>
                            <input type="number" name="prodqty" value="1" class="validate right" required>
                        </div>
                        <div class="col s4 m4 l4 input-field">
                            <input type="submit" value="Add to Cart" name="submit" class="btn yellow darken-3">
                        </div>
                    </div>
                </form>
            </div>
            <?php } ?>
        </div>
    <?php
        }?>

    <!-- Services Details -->
    <?php 
        $fetchquery= mysqli_query($connect, "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Service'"); 
        while($fetchproduct = mysqli_fetch_array($fetchquery)){

            $checkDesc =  $fetchproduct['businessPSDesc'];
            if ($checkDesc == '') {
                $desc = "Description is empty";
            } else {
                $desc = $checkDesc;
                $desc = preg_replace("/[\n]/", "<br>\n", $desc);
            }

        ?>
        <div id="details-<?php echo $fetchproduct['businessPSID']; ?>" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    Service Details
                </h4>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage'] ;?>"  class="col s12 m12 l12 materialboxed" alt="product">
                    </div>
                    <div class="col s12 m6 l6">
                        <small>Name:</small><br> <strong><?php echo $fetchproduct['businessPSName']; ?></strong><br>
                        <small>Description:</small><br> <strong><?php echo $desc; ?></strong><br>
                        <small>Price:</small><br> <strong>Php <?php echo $fetchproduct['businessPSPrice']; ?></strong><br>
                    </div>
                </div>
            </div>
            <?php if( $busCategpryType != 'Free' && $busCategpryType != 'Worship' ) { ?>
            <!-- <div class="modal-footer modal-footer-ps">
                <a href="reservation.php?psID=<?php echo $fetchproduct['businessPSID']; ?>&province=<?php echo $_GET['province']; ?>&town=<?php echo $_GET['town']; ?>&bID=<?php echo $_GET['bID']; ?>&cID=<?php echo $_GET['cID']; ?>" class="btn yellow darken-3 right">Make an Appointment</a>
            </div> -->
            <?php } ?>
        </div>
    <?php
        }
    ?>

    <!-- business photo modal -->
    <?php
        $sql = "SELECT * FROM businessphotos WHERE bisPhotoIDKey = '$bisnesID'";
        $query = mysqli_query($connect, $sql);
        while($fetchPhotoData = mysqli_fetch_array($query)){ 
            // $photoID = $fetchPhotoData['photoID'];
    ?>
    
    <div class="modal modal-fixed-footer modal-business-photo" id="viewbisphoto-<?php echo $fetchPhotoData['photoID']; ?>">
        <div class="modal-content">
            <div>
                <img class="materialboxed" src="../../../assets/images/<?php echo $fetchPhotoData['bisPhoto']; ?>">
            </div>            
        </div>
        <div class="modal-footer">
            <!-- <div class="col s12 center">
                <button class="prev z-depth-0 btn white btn-flat center"><i class="material-icons grey-text text-darken-1">chevron_left</i></button>
                <button class="next z-depth-0 btn white btn-flat center"><i class="material-icons grey-text text-darken-2">chevron_right</i></button>
            </div> -->
        </div>
    </div>
    <?php 
        }
    ?>

    <!-- Report Modal -->
    <div id="reportbusiness" class="modal">
        <div class="modal-content">
            <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                Report
            </h4>
            <p><em>Tell us why</em></p>
            <form action="../../../db.sql/report.php" method="POST">
                <label for="message">Reason for the report</label>
                <input type="hidden" name="province" value='<?php echo $_GET['province']; ?>'>
                <input type='hidden' value='<?php echo $_GET['cID'];?>' name='cid'>
                <input type='hidden' value='<?php echo $_GET['bID'];?>' name='bisnes'>
                <input type='hidden' value='<?php echo $_GET['town'];?>' name='town'>
                <input type='hidden' value='<?php echo $_SESSION['userID'];?>' name='user'>
                <textarea class='message materialize-textarea' name='content' required></textarea>
        </div>
        <div class="modal-footer">
            <input type="submit" value="Submit Report" class="btn red darken-3">
        </div>
        </form>
    </div>

    <!-- Confirm Service Modal -->
    <div id="confirmservice" class="modal">
        <div class="modal-content" style="float: center; text-align: center;">
            <p>Please review the details before confirming</p>
            <div class='row'>
                <div class='col s6 m6'>
                    <a href="#!" class="modal-close col s12 btn btn-large waves-effect grey darken-2">Back</a>
                </div>
                <div class='col s6 m6'>
                    <button form="hservice" type="submit" name='submitrequest' class='col s12 btn btn-large waves-effect grey darken-2'>Confirm</button></p>
                </div>
            </div>
        </div>
    </div>

    <!-- Start of Result Modal -->
    <?php
        if(isset($_GET['report'])){
            $report = $_GET['report'];
        }
    ?>
    <div class="modal" id="report">
        <div class="modal-content">
            <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                <?php echo ucwords($report); ?>
            </h4>
            <div>
                <p class="center-align">
                <?php if($report == 'success'){ ?>
                    <i>Report is successful, please wait for the confirmation of the administrator.</i>
                <?php } else { ?>
                    <i>Oops, Something went wrong. Please try again later</i>
                <?php } ?>
                </p>
            </div>    
        </div>
    </div>
    <!-- End of Result Modal -->

    <?php include('modalnotif.php'); ?>
</div>

<script src="js/prefilled-dropdown.js"></script>
<?php require '../../partials/footer.php'; ?>

<script>
    $('.slider').slider({
        indicators: false,
    });

    $('.slider').slider('pause');

    $('.next').click(function() {
    $('.slider').slider('next');
    });
    $('.prev').click(function() {
    $('.slider').slider('prev');
    });
</script>

<script>
    $(document).ready(function(){
        <?php if(isset($_GET['report'])) { ?>
            var message = "<?php echo $_GET['report']; ?>";

            $('#report').modal('open');
        <?php } ?>
    });
</script>