<?php require '../../partials/header.php'; ?>
<?php require 'nav_businessfree.php'; ?>

<div class="main dashboard">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul class="left">
                <li><a class="waves-effect waves-light"><img src="../../../assets/images/emergency.png" class="hide-on-small-only valign-wrapper" style="height: 25px; width: 25px;"></a></li>
            </ul>
            <ul class="right">
                <!-- <li><a href="#"><i class="material-icons">favorite_border</i></a></li> -->
                <li><a href="#favorite" class="modal-trigger"><span class="red-text"><i class="material-icons">favorite</i></span></a></li>
				<!-- <li><a href="#notif" class="modal-trigger"><i class="large material-icons">notifications</i></a></li> -->
            </ul>
        </div>
    </nav>
    <div class="map-container">
        <div class="card map">
            <div class="card-image">
                <div class="dvmap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15291.761607788309!2d120.32638765!3d16.629759999999997!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1sen!2sph!4v1536324297149" height="100%" width="100%" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="card-content">
                <span class="card-title activator grey-text text-darken-4"><?php echo $_GET["bname"];?><i class="material-icons right yellow-text text-darken-3">info_outline</i></span>
                
            </div>
            <div class="card-reveal">
                <span class="card-title"><h5><?php echo $_GET["bname"];?><i class="material-icons right yellow-text text-darken-3">close</i></span></h5></span>
                <div class="container business-image" style="max-width: 720px; max-height: 300px; overflow: hidden;">
                    <div class="carousel carousel-slider">
                        <a class="carousel-item" href="#one"><img src="../../../assets/images/image-1.jpg"></a>
                        <a class="carousel-item" href="#two"><img src="../../../assets/images/image-2.jpg"></a>
                        <a class="carousel-item" href="#three"><img src="../../../assets/images/image-3.jpg"></a>
                        <a class="carousel-item" href="#four"><img src="../../../assets/images/image-4.jpg"></a>
                    </div>
                </div>
                <div class="center-align">
                    <p>
                        <strong><em>Schedule: Monday - Friday <br/>
                        Time: 8:00AM - 5:00PM <br />
                        </em></strong> 

                    </p>
                </div>

                <h5>Overview</h5>
                <p>This is the description of the business</p>
                <br />
                <h5>
                    Products and Services 
                </h5>
                <div class="products">
                    <h6>Products</h6>
                    <div class="row">
                        <div class="col s12 m4 l4">
                            <div class="card product-card center-align">
                                <div class="card-image">
                                    <img src="../../../assets/images/prod 1.png" alt="product">
                                </div>
                                <div class="card-content">
                                    <span class="card-title">Product Name</span>
                                    <span class="product-price">Php 150</span>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4 l4">
                            <div class="card product-card center-align">
                                <div class="card-image">
                                    <img src="../../../assets/images/prod 1.png" alt="product">
                                </div>
                                <div class="card-content">
                                    <span class="card-title">Product Name</span>
                                    <span class="product-price">Php 150</span>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4 l4">
                            <div class="card product-card center-align">
                                <div class="card-image">
                                    <img src="../../../assets/images/prod 1.png" alt="product">
                                </div>
                                <div class="card-content">
                                    <span class="card-title">Product Name</span>
                                    <span class="product-price">Php 150</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="services">
                    <h6>Services</h6>
                    <div class="row">
                        <div class="col s12 m4 l4">
                            <div class="card product-card center-align">
                                <div class="card-image">
                                    <img src="../../../assets/images/no-image.jpg" alt="product">
                                </div>
                                <div class="card-content">
                                    <span class="card-title">Service Name</span>
                                    <span class="product-price">Php 150</span>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4 l4">
                            <div class="card product-card center-align">
                                <div class="card-image">
                                    <img src="../../../assets/images/no-image.jpg" alt="product">
                                </div>
                                <div class="card-content">
                                    <span class="card-title">Service Name</span>
                                    <span class="product-price">Php 150</span>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4 l4">
                            <div class="card product-card center-align">
                                <div class="card-image">
                                    <img src="../../../assets/images/no-image.jpg" alt="product">
                                </div>
                                <div class="card-content">
                                    <span class="card-title">Service Name</span>
                                    <span class="product-price">Php 150</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <a href="#reportbusiness" class="modal-trigger white-text"><span class="right red waves-effect waves-light" style="padding: 5px; border-radius: 25px;">Report<i class="material-icons right">report</i></span></a>  
            </div>
        </div>
    </div>

    <!-- Notification Modal -->
    <div class="modal" id="notif">
    <ul class="collection with-header">
		<li class="collection-header" style="float: center; text-align: center;"><h4> Notifications </h4></li>
		<li class="collection-item avatar"><img src="../../../assets/images/a.jpg" class="circle">
			<span class="title"><!-- Get business username where the user requested items/services are already approved --><b>Amart Partners</b> approved your requested 
				<!-- item/product/service name --><b>Car Service</b>
			</span>
			<p style="color: grey;">October 21, 2018</p>
		</li>
		<li class="collection-item avatar"><img src="../../../assets/images/a.jpg" class="circle">
			<span class="title"><!-- Get business username where the user requested items/services are already approved --><b>Amart Partners</b> approved your requested 
				<!-- item/product/service name --><b>GTX SABER MAX</b>
			</span>
			<p style="color: grey;">October 21, 2018</p>
		</li>	
		<li class="collection-item avatar"><img src="../../../assets/images/a.jpg" class="circle">
			<span class="title"><!-- Get business username where the user requested items/services are already approved --><b>Amart Partners</b> approved your requested 
				<!-- item/product/service name --><b>Accessories</b>
			</span>
			<p style="color: grey;">October 21, 2018</p>
		</li>
	</ul>
    </div>
    
    <!-- Favorite Modal -->
    <div id="favorite" class="modal">
        <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Note to favorite
        </h4>
            <form action="#" method="post">
                <input type="text" placeholder="Note">
                <div class="section"></div>
                <center>
                    <input type="submit" value="Submit" class="btn btn-flat yellow darken-3">
                </center>
            </form>
        </div>        
    </div>

    <!-- Cart Modal -->
    <div id="cart" class="modal">
        <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Cart
        </h4>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <td>Product</td>
                        <td>Quantity</td>
                        <td>Price</td>
                        <td>Total</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tr>
                    <td>Product 1</td>
                    <td>1</td>
                    <td>Php 150</td>
                    <td>Php 150</td>
                    <td><a href="#"><i class="material-icons">delete</i></td>
                </tr>
            </table>
            <div class="section"></div>
            <div class="center-align">
                <a href="requestnext.php" class="btn yellow darken-3">Proceed</a>
            </div>
        </div>        
    </div>

    <!-- Report Modal -->
    <div id="reportbusiness" class="modal">
        <div class="modal-content">
            <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                Report
            </h4>
            <p>Tell us why</p>
            <form action="report.php">
                <label for="message">Reason for the report</label>
                <input type="text" required />
                <input type="submit" value="Submit" class="btn yellow darken-3">
            </form>
        </div>
    </div>

    <!-- Events Modal -->
    <div id="events" class="modal">
        <div class="modal-content">
        <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <h4 class="center-align">
                Ongoing Events
            </h4>
            <div class="divider"></div>
            <div class="card">
                <div class="card-image">

                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Event Name<i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-action yellow darken-3 white-text">
                    <p>From 09/28/2018 to 09/29/2018</p>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Event Name<i class="material-icons right">close</i></span>
                    <p>This is the description of the event.</p>
                </div>
            </div>
            
            <div class="section"></div>
            <h4 class="center-align">
                Upcoming Events
            </h4>
            <div class="divider"></div>
            <div class="card">
                <div class="card-image">

                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Event Name<i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-action  yellow darken-3 white-text">
                    <p>From 09/28/2018 to 09/29/2018</p>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Event Name<i class="material-icons right">close</i></span>
                    <p>SEVERE WEATHER FORECAST NO. 2
                    For: Potential Strong Tropical Cyclone #Ompong and Southwest Monsoon
                    September 12-17

                    Pinaghahanda na ngayon ang mga residente sa #ExtremeNorthernLuzon maging sa ilang bahagi ng #NorthernLuzon dahil yan sa posibleng direktang epekto ng inaasahang napakalakas na bagyo na posibleng manalasa sa susunod na linggo (September 15-16) na inaasahang magpapalakas din ng HABAGAT.

                    Inaalerto rin ang mga residente sa iba pang bahagi ng #Luzon kasama ang #MetroManila at sa #Visayas dahil naman sa HABAGAT na paiigtingin ng napakalakas na bagyo next week na posibleng magdulot ng malalakas na pag-ulan at pagbugso ng hangin na posible ring magdulot ng mga pagbaha o pagguho ng lupa.

                    Sa mga taga-Batanes kung saan posibleng dumaan ang sentro ng bagyo maging sa #CagayanValley, #CAR, at sa #IlocosRegion ay inaabisuhang maghanda, ayusin na ang mga dapat ayusin at alamin ang mga pinaka-malapit evacuation center upang maging ligtas sa pagdating sa bagyo.

                    Narito naman ang RAINFALL FORECAST para sa September 15 na inaasahang malalakas na pag-ulan ang posibleng maranasan sa buong LUZON dahil sa epekto ng bagyo at ng HABAGAT.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Service Modal -->
    <div id="homeservice" class="modal">
    <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Home Service
        </h4>
        <div class="divider"></div>
        <div class="card">
            <div class="card-image">

            </div>
            <div class="card-content">
            <form name="hservice" id=hservice class="col s12" action="dashboard_business.php?bname=<?php echo $_GET["bname"]?>" method="post">
                <div class='row'>
                    <div class='input-field col s12 m6 l6'>
                        <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">date_range</i>
                        <input type="date" class="datepicker" id="reqdate" name="reqdate" required/>
                        <label for="reqdate" style="font-size: 20px;">Date of Request</label>
                    </div>
                    <div class='input-field col s12 m6 l6'>
                        <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">location_city</i>
                        <input class='validate' type='text' name='service' id='service' required/>
                        <label for='service'>Type of Service</label>
                    </div>
                </div>
                <div class='row'>
                <div class='input-field col s12 m6 l6'>
                        <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">phone</i>
                        <input class='validate' type='text' name='contact' id='contact' required/>
                        <label for='contact'>Contact No:</label>
                    </div>
                    <div class='input-field col s12 m6 l6'>
                        <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">home</i>
                        <input class='validate' type='text' name='address' id='address' required/>
                        <label for='address'>Address:</label>
                    </div>
                </div>
                
                <div class='row'>
                    <div class='input-field col s12 m12 l12'>
                        <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">edit</i>
                        <textarea class='message materialize-textarea' type='text' name='description' id='description' required/></textarea>
                        <label for='description' style="font-size:15px;">Job Description/Concern</label>
                    </div>
                </div>
            </form>
			<center>
              <div class='' style="float: center; text-align: center;">
				 <a href="#confirmservice" id="confirm" name='confirm' class='col m12 btn btn-large waves-effect grey'>Submit</a>
			  </div>
            </center>            
            </div>
            <div class="card-action yellow darken-3 white-text">
                <p><em>Note: For home services it is advisable to allot 1 day prior to request</em></p>
            </div>
        </div>
    </div>
</div>

<!-- Confirm Service Modal -->
<div id="confirmservice" class="modal">
    <div class="modal-content" style="float: center; text-align: center;">
        <p>Please review the details before confirming</p>
    <div class='row'>
        <div class='col s6 m6'>
            <a href="#!" class="modal-close col s12 btn btn-large waves-effect grey darken-2">Back</a>
        </div>
        <div class='col s6 m6'>
            <button form="hservice" type="submit" name='submitrequest' class='col s12 btn btn-large waves-effect grey darken-2'>Confirm</button></p>
        </div>
    </div>
</div>

<div class="modal" id="notif">
    <ul class="collection with-header">
		<li class="collection-header" style="float: center; text-align: center;"><h4> Notifications </h4></li>
		<li class="collection-item avatar"><img src="../../../assets/images/a.jpg" class="circle">
			<span class="title"><!-- Get business username where the user requested items/services are already approved --><b>Amart Partners</b> approved your requested 
				<!-- item/product/service name --><b>Car Service</b>
			</span>
			<p style="color: grey;">October 21, 2018</p>
		</li>
		<li class="collection-item avatar"><img src="../../../assets/images/a.jpg" class="circle">
			<span class="title"><!-- Get business username where the user requested items/services are already approved --><b>Amart Partners</b> approved your requested 
				<!-- item/product/service name --><b>GTX SABER MAX</b>
			</span>
			<p style="color: grey;">October 21, 2018</p>
		</li>	
		<li class="collection-item avatar"><img src="../../../assets/images/a.jpg" class="circle">
			<span class="title"><!-- Get business username where the user requested items/services are already approved --><b>Amart Partners</b> approved your requested 
				<!-- item/product/service name --><b>Accessories</b>
			</span>
			<p style="color: grey;">October 21, 2018</p>
		</li>
	</ul>
</div>

<script>
    function disablePostButton(){
		var check = document.getElementById("confirm");
        var service = document.getElementById("service");
		var contact = document.getElementById("contact");
		var address = document.getElementById("address");
        var reqdate = document.getElementById("reqdate");
        var desc = document.getElementById("description");
        

        if(service.value != "" && contact.value != "" && address.value != "" && reqdate.value != "" && desc.value != ""){
            check.classList.remove("grey");
            check.className += " modal-trigger yellow darken-3";
            console.log("button is enabled");
            console.log(check.classList);
        }else{

        }
    }

    document.getElementById("service").addEventListener("change", function(){
         disablePostButton();
	});
	
    document.getElementById("contact").addEventListener("change", function(){
         disablePostButton();
	});
 
    document.getElementById("address").addEventListener("change", function(){
         disablePostButton();
	});

	document.getElementById("reqdate").addEventListener("change", function(){
         disablePostButton();
    });

    document.getElementById("description").addEventListener("change", function(){
         disablePostButton();
    });
</script>
<?php require '../../partials/footer.php'; ?>
