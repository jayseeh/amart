<?php require '../../partials/header.php'; ?>
<?php require 'nav_province.php'; 
    $datenow = date('Y-m-d');
?>
<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul class="left">
				<li class="page-title truncate"><h4><?php echo $_GET['province']; ?></h4></li>
			</ul>
            <ul class="right">
                <!-- <li><a href="#notif" class="modal-trigger"><i class="large material-icons">notifications</i></a></li> -->
		  </ul>
        </div>
    </nav>
    <div class="container">
        <div class="row">
        <?php 
            $thisProvince = $_GET['province'];
            $fetchquery= mysqli_query($connect, "SELECT * FROM events
            inner join businesslocation on events.eventBisIDkey = businesslocation.bisLocIDkey
            inner join business on events.eventBisIDkey = business.businessID
            where businesslocation.bisProvince='$thisProvince' 
            AND business.businessSubscriptionEnd >= '$datenow'
            AND events.endDate >= '$datenow'
            OR businessSubscriptionEnd = '0000-00-00'
            AND businesslocation.bisProvince='$thisProvince' 
            AND events.endDate >= '$datenow'
            ORDER BY RAND() limit 6"); 
            $count=mysqli_num_rows($fetchquery);
			if($count !=0){
			while($fetchdata = mysqli_fetch_array($fetchquery)){
                $dateposted = $fetchdata['datePosted'];
                $date = substr($dateposted,0,10);  
                $thisdate = date_create($date);
                $time = substr($dateposted,11,19);  
                $thistime = date_create($time);
                $dateposted = date_format($thisdate,"M d, Y");
                $timeposted = date_format($thistime,"h:i A");   

                $startDate = $fetchdata['startDate'];
                $startDate = date_create($startDate);

                $endDate = $fetchdata['endDate'];
                $endDate = date_create($endDate);
        ?>
            <div class="col s12 m4 l4">
                <div class="card card-dashboard">
                    <div class="card-image">
                        <img src="../../../assets/eventPics/<?php echo $fetchdata['eventPoster'];?>" class="col s12 m12 l12 materialboxed">
                    </div>
                    <div class="card-content">
                    <span class="card-title">
                        <a href="dashboard_business.php?province=<?php echo $fetchdata['bisProvince'];?>&town=<?php echo $fetchdata['bisCityTown'];?>&bID=<?php echo $fetchdata['businessID'];?>&cID=<?php echo $fetchdata['businessCategory'];?>&event=1">
                        <p class="truncate">  <?php echo $fetchdata['eventTitle']; ?></a></p>
                    </span>
                    <small><?php echo date_format($startDate,"M d, Y") . ' - ' . date_format($endDate,"M d, Y");?>
                        <br> By: <?php echo $fetchdata['businessName']; ?>
                    </small>
                    <div class="divider"></div>
                    <p class="truncate"><?php echo $fetchdata['eventDesc']; ?></p>
                    </div>
                </div>
            </div>              
		<?php 
		 	}
		}else{
			echo "<br><center><img src='../../../assets/images/partners.png' width='20%' height='20%'></center><p class='center-align'><em>There is currently no info stored in our database for <strong>$thisProvince</strong>.</em></p>";
		}
		?>
        </div>
    </div>
    

  
</div>


<script>
    var map;
    var pos;
    var lats,lngs;
    var theData;
    $(document).ready(function(){
        $.ajax({
            type: "POST",
            data: {data:$("#location").val()},
            url: "http://localhost/amartv99/views/users/user/getTown.php",
            success: function(data){
            theData = JSON.parse(data);
            console.log(JSON.parse(data));
            }
        });
    });
    function initMap() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            lats= position.coords.latitude;
            lngs= position.coords.longitude;
            document.getElementById('businessLat').value=lats;
            document.getElementById('businessLng').value=lngs;

            }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
            });
        }

        setTimeout(function(){
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "address": document.getElementById('location').value
            }, function(results) {
                //console.log('Coor,'+results[0].geometry.location);
                var coordin=(('Coor,'+results[0].geometry.location).replace(/[()]/g, '')).split(","); //LatLng
                townLat=parseFloat(coordin[1].replace(/\s/g, ''));
                tonwLong=parseFloat(coordin[2].replace(/\s/g, ''));
                var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png';
                map = new google.maps.Map(document.getElementById('mapUser'), {
                center: {lat:townLat, lng:tonwLong},
                zoom: 10,
                mapTypeId: 'satellite',
                streetViewControl: false,
                mapTypeControl: false,
                fullscreenControl:false,
                zoomControl:true,
                draggable:true
                });
                for(a in theData){
                    var marker = new google.maps.Marker({
                    position: {lat: parseFloat(theData[a].lat), lng: parseFloat(theData[a].lng)},
                    map: map,
                    icon: iconBase,
                    draggable:true,

                    });
                    var infowindow = new google.maps.InfoWindow({
                    content: "<div>"+theData[a].name+"</div>"
                    });
                    infowindow.open(map, marker);
                }
            });
            console.log("mapss");
            console.log("Lat: "+lats+ " Lng: "+lngs);
        },1000);
    }
</script>
<?php require '../../partials/footer.php'; ?>
