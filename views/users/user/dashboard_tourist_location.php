<?php require '../../partials/header.php'; ?>
<?php require 'nav_town.php'; ?>
<script>
    $('.carousel').carousel();
    $('.carousel.carousel-slider').carousel({
        fullWidth: true,
        indicators: true
    });
    var map;
var pos;
var lats,lngs;
function initMap() {

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      lats= position.coords.latitude;
      lngs= position.coords.longitude;
      document.getElementById('tourLat').value=lats;
      document.getElementById('tourLong').value=lngs;

    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  }

  setTimeout(function(){
    console.log("mapss");
    console.log("Lat: "+lats+ " Lng: "+lngs);
    var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png';

    mapViewTour = new google.maps.Map(document.getElementById('mapViewTour'), {
      center: {lat:lats, lng:lngs},
      zoom: 12,
      mapTypeId: 'satellite',
      streetViewControl: false,
      mapTypeControl: false,
      fullscreenControl:false,
      zoomControl:true,
      draggable:true
    });
    
    var marker = new google.maps.Marker({
      position: {lat: lats, lng: lngs},
      map: mapViewTour,
      icon: iconBase

    });
    tourLat=document.getElementById("latss").value;
    tourLng=document.getElementById("lngss").value;
    var marker1 = new google.maps.Marker({
      position: {lat: parseFloat(tourLat), lng: parseFloat(tourLng)},
      map: mapViewTour,
      icon: iconBase

    });
    var infowindow = new google.maps.InfoWindow({
      content: "<div>You are here</div>"
    });
    var infowindow1 = new google.maps.InfoWindow({
      content: "<div>Location of Tourist Spot</div>"
    });
    infowindow.open(mapViewTour, marker);
    infowindow1.open(mapViewTour, marker1);
    google.maps.event.addListener(marker, 'dragend', function(evt){
      console.log( 'Marker dropped Tourist: Current Lat: ' + evt.latLng.lat().toFixed(6) + ' Current Lng: ' + evt.latLng.lng().toFixed(6) );
      //alert( 'Marker dropped: Current Lat: ' + evt.latLng.lat() + ' Current Lng: ' + evt.latLng.lng() );
      document.getElementById('tourLat').value=evt.latLng.lat().toFixed(6);
      document.getElementById('tourLong').value=evt.latLng.lng().toFixed(6);
    });
  },1000);

}
</script>
<?php 
    $lat=$_GET['lat'];
    $lng=$_GET['lng'];
?>
<input type="hidden" id="latss" value="<?php echo $lat; ?>">
<input type="hidden" id="lngss" value="<?php echo $lng; ?>">
<div class="main dashboard">
    <nav>
        <div class="nav-wrapper yellow darken-2">
			<ul class="left">
        <li><a href="dashboard_town.php?province=<?php echo $_GET['province']; ?>&town=<?php echo $_GET['town']; ?>" class="large modal-close white-text"><i class="material-icons">arrow_back</i></a></li>
        <li class="page-title truncate"><h4><?php echo $_GET['town']; ?></h4></li>
			</ul>
            <ul class="right">
                <li><a href="" class="left"><i class="material-icons">location_searching</i></a></li>
				<!-- <li><a href="#notif" class="modal-trigger"><i class="large material-icons">notifications</i></a></li> -->
		    </ul>
        </div>
    </nav>
    <div class="map-container">        
        <div class="map">
            <div class="dvmap">                    
                <div id="mapViewTour" style="width: 100%; height: 75vh;"></div>
            </div>
        </div>
        <div class="card div-bottom-card">
            <div class="card-content" style="padding: 15px 10px;">
                <span class="card-title grey-text text-darken-4">
                  <?php echo $_GET['tourspot']; ?>
                  <a href="#viewtourspot" class="modal-trigger">
                    <span class="right"><i style="width: 32px;" class="material-icons">information</i></span>
                  </a>
                </span>
            </div>
        </div>
    </div>
<?php
  $idid = $_GET['tID'];
  $query = mysqli_query($connect, "SELECT * FROM tourspot inner join accounts on tourspot.tourspotUploader=accounts.aID WHERE tourspotID='$idid'"); 
  $datas = mysqli_fetch_array($query);

  $address_arr = explode(" | ", $datas['fullAddress']);
	$filtered_arr = array();
  foreach($address_arr as $val){
    if($val != ""){
      array_push($filtered_arr, $val);
    }
  }
  
  $addresscount = count($filtered_arr);
  $fulladdress = implode(", ", $filtered_arr);

  $last_in = $addresscount - 1;
  // echo $addresscount;

?>
  <div class="modal modal-fixed-footer overflow-modal" id="viewtourspot">
		<div class="modal-content">
			<h4>
				<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
				Tourist Spot Details
			</h4>
			<div class="row">
				<div class="col s12 m6 l6">
					<img src="../../../assets/tourPics/<?php echo $datas['tourspotPic']; ?>" class="col s12 m12 l12 materialboxed" alt="tourspot">
				</div>
				<div class="col s12 m6 l6">

					<small>Name:</small><br> <strong>
            <?php echo $datas['tourSpotTitle']; ?></strong><br>
          <small>Uploader:</small><br> <strong>
						<?php  echo $datas['fname'].' '.$datas['mname'].' '.$datas['lname'];?></strong><br>
          <small>Address:</small><br> <strong>
						<?php echo $fulladdress; ?></strong><br>
					<small>Description:</small><br> <strong>
						<?php echo $datas['tourspotComment']; ?></strong><br>

			</div>
		</div>
	</div>
   
</div>

<?php require '../../partials/footer.php'; ?>
