<?php require '../../partials/header.php'; ?>
<?php require 'nav_town.php'; 
	$datenow = date('Y-m-d'); 
?>

<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
			<ul class="left">
				<li class="page-title truncate"><h4><?php echo $_GET['town']; ?></h4></li>
			</ul>
            <ul class="right">                
				<!-- <li><a href="#notif" class="modal-trigger"><i class="large material-icons">notifications</i></a></li> -->
		    </ul>
        </div>
    </nav>

	<div class="row">
		<div class="row hide-on-small-only">
			<div class="col s12 hide-on-small-only">
				<ul class="tabs">
					<li class="tab col s4 hide-on-small-only"><a href="#ts" class="modal-trigger yellow-text text-darken-3"><h6>Tourist Spot</h6></a></li>
					<li class="tab col s4 hide-on-small-only"><a href="#town_events" class="modal-trigger yellow-text text-darken-3"><h6>Events</h6> <!-- <span class="badge white-text grey darken-2 new" style="width: 30%; height: 90%;">1</span> --></a></li>
					<li class="tab col s4 hide-on-small-only"><a href="#experience" class="modal-trigger yellow-text text-darken-3"><h6>Experience</h6></a></li>
				</ul>
			</div>
		</div>
		
		<div class="row show-on-small-only hide-on-med-and-up">
			<div class="col s12 show-on-small-only hide-on-med-and-up">
				<ul class="tabs show-on-small-only hide-on-med-and-up">
					<li class="tab col s4 show-on-small-only hide-on-med-and-up"><a href="#ts" class="modal-trigger yellow-text text-darken-3"><span class="yellow-text text-darken-3"><i class="small material-icons">location_on</i></span></a></li>
					<li class="tab col s4 show-on-small-only hide-on-med-and-up"><a href="#town_events" class="modal-trigger yellow-text text-darken-3"><span class="yellow-text text-darken-3"><i class="small material-icons">today</i></span> </a></li>
					<li class="tab col s4 show-on-small-only hide-on-med-and-up"><a href="#experience" class="modal-trigger yellow-text text-darken-3"><span class="yellow-text text-darken-3"><i class="small material-icons">notes</i></span></a></li>
				</ul>
			</div>
		</div>
		
<!-- Tourist Spot Tab -->
<div id="ts" class="col s12">
<div class="row">
	<div class="col s12 m12 l12 center-align">
		<h4 class="hide-on-med-and-up"><center>Tourist Spot</center></h4>
	</div>
	<div class="col s12 m12 l12">
		<a href="#post" class="modal-trigger col s12 btn btn-medium waves-effect yellow darken-3" id="postSpot"><b>Post a Spot</b></a>
	</div>
</div> 
    <div class="row">
    	<?php 
    	 include "../../../db.sql/connect_db.php";
		 $town = $_GET['town'];
		 $province = $_GET['province'];

		 $query=mysqli_query($connect, "SELECT * FROM tourspot 
		 INNER JOIN accounts on tourspot.tourspotUploader=accounts.aID 
		 WHERE municipality='$town' and tourspotStatus ='Confirmed'
		 order by tourspotID desc
		 ");
		 $count=mysqli_num_rows($query);
		 
		 if($count!=0){
			while($fetchTourSpot=mysqli_fetch_array($query)){
				$address_arr = explode(" | ", $fetchTourSpot['fullAddress']);
				$filtered_arr = array();

				foreach($address_arr as $val){
					if($val != ""){
						array_push($filtered_arr, $val);
					}
				}
				
				$addresscount = count($filtered_arr);
				$fulladdress = implode(", ", $filtered_arr);

				$last_in = $addresscount - 1;
				// echo $addresscount;

				$data_province = $filtered_arr[$last_in];

				if($province == $data_province){
					$idid = $fetchTourSpot['tourspotID']; 
					$lat = $fetchTourSpot['tourspotLat'];
					$lng = $fetchTourSpot['tourspotLong'];
					
					$checkDesc =  $fetchTourSpot['tourspotComment'];
					if ($checkDesc == '') {
						echo "<em>Not Available</em>";
					} else {
						$desc = $checkDesc;
						$desc = preg_replace("/[\n]/", "<br>\n", $desc);
					}
    	?>
		
		<div class="col s12 m4 l4">
			<div class="card card-dashboard-town">
				<div class="card-image">
					<img src="../../../assets/tourPics/<?php echo $fetchTourSpot['tourspotPic'];?>" width="100%" class="col s12 m12 materialboxed">
				</div>
				<div class="card-content black-text">
					<span class="right btn-floating transparent avatar"><img src="../../../assets/images/<?php echo $fetchTourSpot['uPic']?>">
					</span>
					<span class="card-title">
						<p class="truncate"><?php echo $fetchTourSpot['tourSpotTitle']; ?></p>
					</span>
					<div class="divider"></div>
					<p class="truncate"><?php echo $checkDesc; ?></p>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
					<p>
						<span class="card-title">
							<?php echo $fetchTourSpot['tourSpotTitle']; ?>
						</span>
						<div class="divider"></div>
						<p><?php echo $checkDesc; ?></p>
					</p>
				</div>
				<div class="card-action">
					<span class="activator">
						<i class="material-icons right">more_vert</i>
					</span>
				 	<a href="dashboard_tourist_location.php?province=<?php echo $_GET['province']; ?>&town=<?php echo $_GET['town']; ?>&tourspot=<?php echo $fetchTourSpot['tourSpotTitle']; ?>&lat=<?php echo $lat; ?>&lng=<?php echo $lng; ?>&tID=<?php echo $idid; ?>">View Location</a>
				</div>
			</div>
		</div>  
		<?php
				}  
			}
			//  End Of while 
		}else{
			echo "<center><img src='../../../assets/images/partners.png' width='20%' height='20%'></center><p class='center-align'><em>There is currently no Tourist Spot stored in our database for <strong>$town</strong>.</em></p>";
		}
		?>
	</div>
</div>
<!-- END -->

<!-- Experience Tab -->
<div id="experience" class="col s12">
	<h4 class="hide-on-med-and-up"><center>Experience</center></h4>
    <div class="row">
		<?php 
			$town = $_GET['town'];
			$province_get = $_GET['province'];

			$fetchquery= mysqli_query($connect, "SELECT fname, mname, lname, dateTime, expDesc, expPhoto, business.businessID, business.businessCategory FROM experience 
			inner join business on experience.businessID = business.businessID
			inner join businesslocation on business.businessID = businesslocation.bisLocIDkey 
			inner join accounts on experience.uploaderID = accounts.aID 
			inner join categories on business.businessCategory = categories.categoryID 
			WHERE businesslocation.bisProvince = '$province_get' 
			AND businesslocation.bisCityTown = '$town' 
			and categories.categoryType !='Worship' 
			and business.businessSubscriptionEnd >= '$datenow'
			or business.businessSubscriptionEnd = '0000-00-00'
			and business.businessStatus = 'Active'
			and businesslocation.bisCityTown = '$town' 
			and categories.categoryType !='Worship' 
			order by expID desc limit 30"); 
			$albumcount = mysqli_num_rows($fetchquery);
		 
		    if($albumcount!=0){
			while($fetchdata = mysqli_fetch_array($fetchquery)){
				$fname = $fetchdata['fname'];
				$mname = $fetchdata['mname'];
				$minitial = substr($mname,0, 1);
				$lname = $fetchdata['lname'];
				$fullname = $fname.' '.$minitial.'. '.$lname;
				$date = $fetchdata['dateTime'];
				$thisDate = date_create($date);
				$thisDate = date_format($thisDate, "M d, Y | H:iA");
				$checkDesc =  $fetchdata['expDesc'];
				if ($checkDesc == '') {
					$desc = "Caption is empty";
				} else {
					$desc = $checkDesc;
					$desc = preg_replace("/[\n]/", "<br>\n", $desc);
				}
			
			?>

		<div class="col s12 m4 l4">
			<!-- card album-entry -->
		
			<div class="card card-album-entry">
				<div class="card-image">
				<a href="dashboard_business.php?province=<?php echo $province_get;?>&town=<?php echo $_GET['town'];?>&bID=<?php echo $fetchdata['businessID'];?>&cID=<?php echo $fetchdata['businessCategory']; ?>&album=1"><img src="../../../assets/expPics/<?php echo $fetchdata['expPhoto'];?>" ></a>
				</div>
				<div class="card-content truncate">
					<span class="card-title activator">
						<i class="material-icons right">more_vert</i>
					</span>
					<p>
						<strong>Uploader: </strong><?php echo $fullname; ?><br>
						<strong>Date: </strong><?php echo $thisDate; ?><br>
						<div class="divider"></div>
					</p><p class="truncate">
						<?php echo $checkDesc; ?>
						<br>
						<br>
					</p>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
					<p>
						Uploader:<?php echo $fullname; ?><br>
						Date: <?php echo $thisDate; ?><br>
						<div class="divider"></div>
						<?php echo $desc; ?>
					</p>
				</div>
			</div>
		</div>
        <?php 
		 	}
		}else{
			echo "<center><img src='../../../assets/images/partners.png' width='20%' height='20%'></center><p class='center-align'><em>There is currently no Experience stored in our database for <strong>$town</strong>.</em></p>";
		}
		?>
    </div>   	
</div>

<!--END -->
	
<!-- Town Events Tab-->
<div id="town_events" class="col s12">
	<h4 class="hide-on-med-and-up"><center>Events</center></h4>

	<!-- <div class="search-bar">
		<form action="" method="post">
			<div class="row">
				<div class="col s10 m8 l8 offset-s1 offset-m2 offset-l2 input-field">
					<label for="searchevents">Search</label>
					<input type="text" name="searchevents" id="searchevents" placeholder="Input Keyword">
				</div>
			</div>
		</form>
	</div> -->

	<div class="row">
		
		<?php 
			$datenow = date('Y-m-d');
			$town = $_GET['town'];
			$sql = "SELECT * FROM events INNER JOIN business on events.eventBisIDkey = business.businessID 
			INNER JOIN businesslocation on business.businessID = businesslocation.bisLocIDkey 
			INNER JOIN categories on business.businessCategory = categories.categoryID 
			WHERE businesslocation.bisCityTown = '$town' 
			AND businesslocation.bisProvince = '$province_get' 
			AND categories.categoryType != 'Worship'
			AND business.businessSubscriptionEnd >= '$datenow'
            AND events.endDate >= '$datenow'
			OR businesslocation.bisCityTown = '$town' 
			AND businesslocation.bisProvince = '$province_get' 
			AND businessSubscriptionEnd = '0000-00-00'
            AND events.endDate >= '$datenow'
			order by eventID desc ";
			
			$query = mysqli_query($connect, $sql);
			$count = mysqli_num_rows($query);
			if($count!=0){
			while($fetchEvent = mysqli_fetch_array($query)){
				$checkDesc =  $fetchEvent['eventDesc'];
				if ($checkDesc == '') {
					echo "<em>Not Available</em>";
				} else {
					$desc = $checkDesc;
					$desc = preg_replace("/[\n]/", "<br>\n", $desc);
				}
		?>
		<a href="#viewevents<?php echo  $fetchEvent['eventID']; ?>" class="modal-trigger">
		<div class="col s12 m4 l4">
			<div class="card card-events">
				<div class="card-image">
					<img src="../../../assets/eventPics/<?php echo  $fetchEvent['eventPoster']; ?>">
					<span class="card-title card-title-background">
						<?php echo  $fetchEvent['businessName']; ?>
					</span>
				</div>
				
				<div class="card-content black-text">
					<span class="card-title truncate">
						<?php echo $fetchEvent['eventTitle']; ?>
					</span>
					<div class="divider"></div>
					<p class="truncate" style="max-lines: 1;"><?php echo $checkDesc; ?></p>
				</div>
				
			</div>
		</div>
</a>
		<?php 
		 	}
		}else{
			echo "<center><img src='../../../assets/images/partners.png' width='20%' height='20%'></center><p class='center-align'><em>There is currently no Events stored in our database for <strong>$town</strong>.</em></p>";
		}
		?>
	</div>

</div>

 <!-- EVENTS VIEW MODAL -->
<?php
	$datenow = date('Y-m-d');
	$town = $_GET['town'];
	$sql = "SELECT * FROM events INNER JOIN business on events.eventBisIDkey = business.businessID 
	inner join businesslocation on business.businessID = businesslocation.bisLocIDkey 
	inner join categories on business.businessCategory = categories.categoryID 
	where businesslocation.bisCityTown = '$town' 
	AND business.businessSubscriptionEnd >= '$datenow'
	AND events.endDate >= '$datenow'
	OR businesslocation.bisCityTown = '$town' 
	AND businesslocation.bisProvince = '$province_get' 
	AND businessSubscriptionEnd = '0000-00-00'
	AND events.endDate >= '$datenow'
	-- AND '$datenow' BETWEEN startDate and endDate
	"; 
	$query = mysqli_query($connect, $sql);
	$count = mysqli_num_rows($query);
	while($fetchEvent = mysqli_fetch_array($query)){
		
		$startDate = $fetchEvent['startDate'];
		$startDate = date_create($startDate);

		$endDate = $fetchEvent['endDate'];
		$endDate = date_create($endDate);
?>
<!-- View Events Modal -->
<div class="modal modal-fixed-footer  overflow-modal" id="viewevents<?php echo  $fetchEvent['eventID']; ?>">     
	<div class="modal-content">
		<h4>
			<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
			Details
		</h4>
		<div class="row">
			<div class="col s12 m6 l6">
				<img src="../../../assets/eventPics/<?php echo  $fetchEvent['eventPoster']; ?>" class="col s12 m12 l12" alt="product"></a>
			</div>
			<div class="col s12 m6 l6">
				<small>Business Name:</small><br> <strong><?php echo $fetchEvent['businessName']; ?></strong><br>
				<small>Event Title:</small><br> <strong><?php echo $fetchEvent['eventTitle']; ?></strong><br>
				<small>Event Duration:</small><br><strong><?php echo date_format($startDate,"M d, Y") . ' - ' . date_format($endDate,"M d, Y"); ?></strong><br>
				<small>Description:</small><br> <strong><?php echo $fetchEvent['eventDesc']; ?></strong>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<div class="row center-align">
			<div class="col s12 m12 l12">
				<a href="dashboard_business.php?province=<?php echo $province_get;?>&town=<?php echo $_GET['town'];?>&bID=<?php echo $fetchEvent['businessID'];?>&cID=<?php echo $fetchEvent['businessCategory']; ?>&event" class="modal-trigger btn green darken-3  col s12 m12 l12" >Go To <?php echo $fetchEvent['businessName']; ?> Page </a>
			</div>
		</div>
	</div>
</div>
<?php
}
?>
<!--END-->
<div id="tournotifmodal" class="modal">
	<div class="modal-content">
		<h4>
			<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
			Waiting for Confirmation
		</h4>
		<div class="row">
			<div class="col s12 m12 l12">
				<center>The tourist spot that you've submitted has been received by the administrator for approval. <br> Thank You</center>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	var province, dataProvince="<option value='' disabled selected>SELECT</option>";
	var mun=[],myArray=[], dataMun="<option value='' disabled selected>SELECT</option>";
	var brgy, dataBrgy = "<option value='' disabled selected>SELECT</option>";
	var pCode="", mCode="";
    setTimeout(function() {                
      //GET PROVINCE
      $.ajax({    //create an ajax request to display.php
        type: "GET",
        url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refprovince.json",
        success: function(response){                    
            //$("#responsecontainer").html(response); 
            province=JSON.parse(response);
            console.log(province);
            for (x in province.RECORDS){
            	dataProvince = dataProvince + "<option value="+province.RECORDS[x].provCode+">"+province.RECORDS[x].provDesc+"</option>";
            }
            $("#tourProvince").html(dataProvince);
            $("#tourTown").html(dataMun);
            //$("#tourBrgy").html(dataBrgy);
        }
	  });
	  //GET MUNICIPALITY
      $.ajax({    //create an ajax request to display.php
        type: "GET",
        url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refcitymun.json",
        success: function(response){                    
            //$("#responsecontainer").html(response); 
			mun=JSON.parse(response);
			//capital = mun.RECORDS.citymunDesc.replace(/ *\([^)]*\) */g, "");
			myArray = mun.RECORDS.filter(function( obj ) {
				
				if(obj.citymunDesc.match(/(Capital)/g)!='null'){
					console.log("Find");		
					return obj.citymunDesc.match(/(Capital)/g);
				}
			});
			var myJSONString = JSON.stringify(myArray);
			myJSONString = myJSONString.replace(/ *\([^)]*\) */g, "");
			myArray = JSON.parse(myJSONString);
			console.log(myArray);
            console.log(mun);
        }
	  });
	  //GET BARANGAY
      $.ajax({    //create an ajax request to display.php
        type: "GET",
        url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refbrgy.json",
        success: function(response){                    
            //$("#responsecontainer").html(response); 
            brgy=JSON.parse(response);
            console.log(brgy);
        }
	  });
	},100);
	$("#postSpot").click(function(){ 
		console.log($("#deftown").val());
		theCode = $("#deftown").val().toUpperCase();
		if(theCode.match(/CITY/g)){
			mtCode = myArray.filter( element => element.citymunDesc == String(theCode));
		}else{
			mtCode = mun.RECORDS.filter( element => element.citymunDesc == String(theCode));
		}
		mCode = mtCode[0].citymunCode;
		console.log("Code:"+mCode);
		dataBrgy = "<option>-SELECT BARANGAY-</option>" ;
		filterBrgy = brgy.RECORDS.filter( element => element.citymunCode == String(mCode));
		for (x in filterBrgy){
			dataBrgy= dataBrgy + "<option value="+filterBrgy[x].brgyDesc+">"+filterBrgy[x].brgyDesc+"</option>";
		}
		$("#tourBrgy").html(dataBrgy);
	});
	$("#tourProvince").change(function(){
		console.log("value: "+$(this).val());
		dataMun="<option>-SELECT MUNICIPALITY-</option>";
		pCode = $(this).val();
		filterMun = mun.RECORDS.filter( element => element.provCode == String(pCode));
		for (x in filterMun){
			dataMun= dataMun + "<option value="+filterMun[x].citymunCode+">"+filterMun[x].citymunDesc+"</option>";
		}
		$("#tourTown").html(dataMun);
		$("#tourBrgy").html(dataBrgy);
		console.log(filterMun);
	});
	$("#tourTown").change(function(){
		//mCode = $(this).val();
		theCode = $("#deftown").val();
		mtCode = mun.RECORDS.filter( element => element.citymunDesc == String(theCode));
		mCode = mtCode[0].citymunCode;
		dataBrgy = "<option>-SELECT BARANGAY-</option>" ;
		filterBrgy = brgy.RECORDS.filter( element => element.citymunCode == String(mCode));
		for (x in filterBrgy){
			dataBrgy= dataBrgy + "<option value="+filterBrgy[x].brgyDesc+">"+filterBrgy[x].brgyDesc+"</option>";
		}
		$("#tourBrgy").html(dataBrgy);

	});
	$("#tourBrgy").change(function(){
		p = province.RECORDS.filter( element => element.provCode == String(pCode));
		m = mun.RECORDS.filter( element => element.citymunCode == String(mCode));
		address = $(this).val().toUpperCase() + ", " + m[0].citymunDesc+ ", " + p[0].provDesc;
		$("#disp").html(address);
		$("#tTown").val(m[0].citymunDesc);
		$("#tProvince").val(p[0].provDesc);
		$("#tBrgy").val($(this).val().toUpperCase());

	});
});
</script>
<!-- Tourist Spot Post Modal -->
		<div id="post" class="modal">
			<form class="col s12" action="../../../db.sql/insertTourSpot.php" method="post" enctype="multipart/form-data">	
			<div class="modal-content">
			<h4>
				<a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
				Post a Tourist Spot
			</h4>
			<div class="row">
				<div class="col s12 m12 l12">
					<div id="mapTour" style="width: 100%;height: 50vh;"></div>
				</div>
			</div>
				<div class="row">
					<div class="col s12 m12 l12">
						<input type="hidden" id="deftown" value="<?php echo $_GET['town']; ?>">
						<input type="text" name="tourProvince" value="<?php echo $_GET['province']; ?>" readonly>
						<input type="text" name="tourTown" value="<?php echo $_GET['town']; ?>" readonly>
						<label for="tourBrgy">BARANGAY</label>
						<select name="tourBrgy" id="tourBrgy" class="browser-default" required>			
						</select>
					</div>
				</div>

				<div class="row">
					<div class="col s12 m12 l12 input-field">
						<label for="tourAddress">Street</label>
						<input type="text" name="tourAddress">
					</div>
				</div>
				
				<div class="row">
					<div class="col s12 m12 l12 input-field">
						<label for="title">Tourist Spot Name</label>
						<input type="text" name="title" required>
					</div>
				</div>

				<div class="row">
					<div class="col s12 m12 l12 input-field">
						<label for="tourDesc">Comment/Description</label>
						<textarea class='message materialize-textarea' name='tourDesc' id='tourDesc' required></textarea>
					</div>
				</div>
				
				<div class="row">  
					<div class="file-field input-field">
						<div class="btn btn-flat yellow darken-3 white-text">
							<i class="material-icons">search</i>
							<input type="file" name="photo" accept="image/*">
						</div>
						<div class="file-path-wrapper">
							<input type="text" class="file-path validate" placeholder="Upload Photo" id="photo" required readonly>
						</div>
					</div>
				</div>

				<input type="hidden" name="tourLat" id="tourLat">
				<input type="hidden" name="tourLong" id="tourLong">
				<input type="hidden" name="tourUploader" value="<?php echo $_SESSION['userID'];?>">
				<input type="hidden" name="tTown" id="tTown">
				<input type="hidden" name="tProvince" id="tProvince">
				<input type="hidden" name="tBrgy" id="tBrgy">
				
				<div class="row center-align">
					<input type="submit" name="addpost" class='col m12 btn btn-large yellow darken-3'>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

<?php require '../../partials/footer.php'; ?>

<script>
	$(document).ready(function(){
		// For the notif modal upon success
		var tourResult = '<?php echo $_GET["result"]; ?>';

		if(tourResult == "success"){
			$("#tournotifmodal").modal("open");
		}

		// End of notif modal
	});
</script>
