<?php require '../../partials/header.php'; ?>
<?php require 'nav_worship.php'; 
  $worshiptype = $_GET['wtype'];
  $thisID = $_SESSION['userID'];
?>

<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
          <ul class="left">
            <li class="page-title truncate"><h4><?php echo $worshiptype; ?></h4></li>
          </ul>
          <ul class="right">
            <!-- <li><a href="#notif" class="modal-trigger"><i class="large material-icons">notifications</i></a></li> -->
		      </ul>
        </div>
    </nav>

  <div class="container">
  <div class="row">
    <?php 
        $fetchquery= mysqli_query($connect, "SELECT * FROM favorites 
        INNER JOIN events ON favorites.noteBusinessIDkey = events.eventBisIDkey 
        INNER JOIN business ON events.eventBisIDkey = business.businessID
        INNER JOIN businesslocation ON business.businessID = businesslocation.bisLocIDkey
        WHERE favorites.noteUserID = $thisID AND events.eventStatus='Active' ORDER BY eventID desc"); 
        while($fetchdata = mysqli_fetch_array($fetchquery)){
          $dateposted = $fetchdata['datePosted'];
          $date = substr($dateposted,0,10);  
          $thisdate = date_create($date);
          $time = substr($dateposted,11,19);  
          $thistime = date_create($time);
          $dateposted = date_format($thisdate,"M d, Y");
          $timeposted = date_format($thistime,"h:i A");
      ?>
        <div class="col s12 m4 l4">
          <div class="card card-dashboard">
            <div class="card-image">
                <img src="../../../assets/eventPics/<?php echo $fetchdata['eventPoster'];?>" class="col s12 m12 l12 materialboxed">
            </div>
            <div class="card-content">
              <span class="card-title">
                <a href="dashboard.business.php?town=<?php echo $fetchdata['bisCityTown'];?>&bID=<?php echo $fetchdata['businessID'];?>">
                  <?php echo $fetchdata['eventTitle']; ?></a>
              </span>
              <small><?php echo $dateposted . ' - ' . $timeposted;?>
                By: <?php echo $fetchdata['businessName']; ?>
              </small>
              <div class="divider"></div>
              <p class="truncate"><?php echo $fetchdata['eventDesc']; ?></p>
            </div>
          </div>
        </div>                      
        <?php 
          } 
        ?>
    </div>
  </div>
</div>
<!-- end of main -->
<?php require '../../../views/partials/footer.php' ?>