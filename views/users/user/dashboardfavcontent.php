<div class='ol s12 m4 l4'>
    <div class='card card-dashboard'>
    <div class="card-image">
        <img src='../../../assets/eventPics/<?php echo $fetchdata['eventPoster'];?>' class='col s12 m12 l12 materialboxed'>
    </div>
    <div class='card-content'>
        <span class='card-title'>
        <a href='dashboard.business.php?town=<?php echo $fetchdata['bisCityTown'];?>&bID=<?php echo $fetchdata['businessID'];?>''>
        <p class="truncate"><p class="truncate"> <?php echo $fetchdata['eventTitle']; ?></p></a>
        </span>
        <small><?php echo $dateposted . ' - ' . $timeposted;?>
        By: <?php echo $fetchdata['businessName']; ?>
        </small>
        <div class='divider'></div>
        <p class="truncate"><?php echo $fetchdata['eventDesc']; ?></p>
    </div>
    </div>
</div>