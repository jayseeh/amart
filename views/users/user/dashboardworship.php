<?php require '../../partials/header.php'; ?>
<?php require 'nav_worship.php'; ?>

<?php 
    $event = '';
    if(isset($_GET['event'])){
        $event = 'active';
    }

    $album = '';
    if(isset($_GET['album'])){
        $album = 'active';
    }
    if(isset($_GET['bID'])){
        $bisnesID = $_GET['bID'];
        $sql = "SELECT * from business INNER JOIN businesslocation on businesslocation.bisLocIDkey = business.businessID where businessID = '$bisnesID'";
        $query = mysqli_query($connect, $sql);
        $count = mysqli_num_rows($query);
    }
    $status = '';
    $busName = '';
    $busAddNo = '';
    $busAddStreet = '';
    $busAddBrgy = '';
    $busAddCity = '';
    $busAddProv = '';
    $busDescription = '';
    $busCompAddress = '';

    while ($fetchdata = mysqli_fetch_array($query))  {
        $busName = $fetchdata['businessName'];
        $busAddNo = $fetchdata['bisNumber'];
        $busAddStreet = $fetchdata['bisStreet'];
        $busAddBrgy = $fetchdata['bisBrgy']; 
        $busAddCity = $fetchdata['bisCityTown'];
        $busAddProv = $fetchdata['bisProvince'];
        $busDescription = $fetchdata['businessDescription'];

        $busCompAddress = $busAddNo . ' ' . $busAddStreet . ' ' . $busAddBrgy . ' ' . $busAddCity . ' ' . $busAddProv;
    }
?>
<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul class="right"> 
                <?php 
                $favorite = 1;
                $bid= $_GET['bID'];
                $uid=$_SESSION['userID'];
                $query = mysqli_query($connect, "SELECT * FROM favorites WHERE noteBusinessIDkey='$bid' and noteUserID='$uid'");
                $favorite = mysqli_num_rows($query);
                if($favorite == 1){
                ?>
                <li><a href="#unfavorite" class="modal-trigger"><i class="material-icons red-text">favorite</i></a></li>
                <?php
                    }else{
                ?>
                <li><a href="#favorite" class="modal-trigger"><i class="material-icons red-text">favorite_border</i></a></li>
                <?php
                    }
                ?>
                
                <li><a href="#cart" class="modal-trigger"><i class="material-icons">shopping_cart</i></a></li>
                <!-- <li><a href="#notif" class="modal-trigger"><i class="large material-icons">notifications</i></a></li> -->
            </ul>
        </div>
    </nav>
    <div class="container">
        <h4><span class="grey-text text-darken-4"><?php echo $busName; ?></span><a href="checklocation.php?town=<?php echo $busAddCity; ?>&bID=<?php echo $_GET["bID"]; ?>" class="btn yellow darken-3 right"><span class="hide-on-small-only left">Check Location</span><i class="material-icons">my_location</i></a></h4>
        <div class="row">
            <div class="row hide-on-small-only">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s4 blocks"><a href="#business" class="yellow-text text-darken-3">Business</a></li>
                        <li class="tab col s4 blocks"><a href="#events" class="yellow-text text-darken-3 <?php echo $event; ?>">Events</a></li>
                        <li class="tab col s4 blocks"><a href="#business-album" class="yellow-text text-darken-3 <?php echo $album; ?>">Album</a></li>
                    </ul>
                </div>
            </div>
            <div class="row row-icon-dashboard-business show-on-small hide-on-med-and-up">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s4"><a href="#business" class="yellow-text text-darken-3"><span class="yellow-text text-darken-3"><i class="small material-icons">store</i></span></a></li>
                        <li class="tab col s4"><a href="#events" class="yellow-text text-darken-3 <?php echo $event; ?>"><span class="yellow-text text-darken-3"><i class="small material-icons">today</i></span> <!-- <span class="badge white-text grey darken-2 new">1</span>--></a></li>
                        <li class="tab col s4"><a href="#business-album" class="yellow-text text-darken-3  <?php echo $album; ?>"><span class="yellow-text text-darken-3"><i class="small material-icons">camera</i></span></a></li>
                    </ul>
                </div>
            </div>

            <?php 
                $sql = "SELECT * FROM businessphotos WHERE bisPhotoIDKey = '$bisnesID'";
                $query = mysqli_query($connect, $sql);
                $counter = mysqli_num_rows($query);
            ?>
            <!-- Business Tab -->
            <div id="business" class="col s12">
                <div class="business-photo" style="width: 100%; padding: 0;">
                    <div class="carousel carousel-slider">
                        <?php 
                            if($counter == 0){
                        ?>
                            <a class="carousel-item carousel-business-photo"><img src="../../../assets/images/default-image.png"></a>
                        <?php 
                        }else{ 
                            while($fetchPhotoData = mysqli_fetch_array($query)){ ?>
                            <a class="carousel-item carousel-business-photo"><img src="../../../assets/images/<?php echo $fetchPhotoData['bisPhoto']; ?>" class="materialboxed"></a>
                        <?php 
                            }
                        } ?>
                    </div>
                </div>
                    <p>
                        <div class="row center-align">
                            <div class="col s12 m12 l12">
                                Address: <?php echo $busCompAddress; ?>
                            </div>
                        </div>
                        <?php 
                            $sql = "SELECT * from businesscalendar where bisCalendarIDkey = '$bisnesID'";
                            $query = mysqli_query($connect, $sql);
                            $count = mysqli_num_rows($query);
                        ?>
                        <div class="row">
                            <div class="col s12 m2 l2 valign-wrapper">
                                <strong>Schedule</strong> 
                            </div>
                            <div class="col s12 m10 l10">
                                <table>
                                <?php
                                if($count!=0){
                                    while($fetchdata = mysqli_fetch_array($query)){
                                ?>
                                        <tr>
                                            <td><?php echo $fetchdata['bisSchedDay']?></td>
                                            <td><?php echo $fetchdata['bisSchedTime']?></td>
                                        </tr>
                                <?php 
                                    }
                                }else{
                                ?>
                                    <tr>
                                        <td><em>Schedule is not available</em></td>
                                    </tr>
                                <?php
                                }
                                ?>
                                </table>
                            </div>
                        </div>
                    </p>
                    <p>Contact: <em>Not Available</em></p>
                    <p>Email: <em>Not Available</em></p>
                    <p>Links: <em>Not Available</em></p>

                <h5>Overview</h5>
                <p>
                    <?php 
                        $checkDesc =  $busDescription;
                        if ($checkDesc == '') {
                            echo "<em>Not Available</em>";
                        } else {
                            echo $checkDesc;}
                    ?>
                </p>
                <br />

                <h5>Products and Services</h5>

                <?php 
                    $sql = "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Product'";
                    $fetchquery= mysqli_query($connect, $sql); 
                    $count = mysqli_num_rows($fetchquery);
                ?>

                <div class="products">
                    <h6>Products</h6>
                    <div class="row">
                        <?php
                            if($count!=0){
                                while($fetchproduct = mysqli_fetch_array($fetchquery)){
                        ?>
                                <a href="#details-<?php echo $fetchproduct['businessPSID']; ?>" class="modal-trigger">
                                    <div class="col s6 m4 l4">
                                        <div class="card card-product center-align">
                                            <div class="card-image card-product-photo">
                                                <img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage'] ;?>" alt="<?php echo $fetchproduct['businessPSName']; ?>">
                                                <span class="card-title card-product-title truncate"><?php echo $fetchproduct['businessPSName']; ?></span>
                                            </div>
                                            <div class="card-content card-product-content">
                                                <span class="product-price"><small>Php</small> <?php echo $fetchproduct['businessPSPrice']; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                        <?php 
                                }
                            }else{
                                echo "<p class='center-align'><em>No Product Available</em></p>";
                            }
                        ?>
                    </div>
                </div>

                <?php 
                    $sql = "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Service'";
                    $fetchquery= mysqli_query($connect, $sql); 
                    $count = mysqli_num_rows($fetchquery);
                ?>
                
                <div class="services">
                    <h6>Services</h6>
                    <div class="row">
                        <?php 
                            if($count!=0){
                                while($fetchservice = mysqli_fetch_array($fetchquery)){
                        ?>
                                <a href="#details-<?php echo $fetchservice['businessPSID']; ?>" class="modal-trigger">
                                    <div class="col s6 m4 l4">
                                        <div class="card card-product center-align">
                                            <div class="card-image card-product-photo">
                                                <img src="../../../assets/ps/<?php echo $fetchservice['businessPSImage'] ;?>" alt="<?php echo $fetchservice['businessPSName']; ?>">
                                                <span class="card-title card-product-title truncate"><?php echo $fetchservice['businessPSName']; ?></span>
                                            </div>
                                            <div class="card-content card-product-content">
                                                <span class="product-price"><small>Php</small> <?php echo $fetchservice['businessPSPrice']; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                        <?php 
                            }
                        }else{
                            echo "<p class='center-align'><em>No Service Available</em></p>";
                        }
                        ?>
                    </div>                
                </div>
                <a href="#reportbusiness" class="modal-trigger white-text"><span class="right red" style="padding: 5px; border-radius: 25px;">Report<i class="material-icons right">report</i></span></a>  
            </div>
        <!-- END of Business Overview Tab -->
                    
            <!-- Events Tab -->
            <div id="events" class="col s12 m12 l12">
                <?php
                    $sql = "SELECT * FROM events WHERE eventBisIDKey = '$bisnesID' AND eventStatus = 'Active'";
                    $query = mysqli_query($connect, $sql);
                    $counton = mysqli_num_rows($query);
                    if($counton!=0){
                ?>
                    <div class="row"> 
                        <div class="col s12 m12">
                            <h4>Ongoing Events</h4>
                        </div>
                    </div>
                    <div class="row">
                    <?php
                        while($fetchEvent = mysqli_fetch_array($query)){
                    ?>
                        <div class="col s12 m6 l6">
                            <div class="card card-events">
                                <div class="card-image">
                                    <img src="../../../assets/eventPics/<?php echo  $fetchEvent['eventPoster']; ?>" class="materialboxed">
                                    <span class="card-title">
                                        <?php echo  $busName; ?>
                                    </span>
                                </div>
                                <div class="card-content">
                                    <span class="card-title activator">
                                        <?php echo  $fetchEvent['eventTitle']; ?><i class="material-icons right">more_vert</i><br>
                                        <small><?php echo  $fetchEvent['startDate']; ?> - <?php echo  $fetchEvent['endDate']; ?></small>
                                        
                                    </span>
                                    <div class="divider"></div>
                                    <p class="truncate"><?php echo  $fetchEvent['eventDesc']; ?></p>
                                </div>
                                <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4"><?php echo  $fetchEvent['eventTitle']; ?><i class="material-icons right">close</i></span>
                                <small><?php echo  $fetchEvent['startDate']; ?> - <?php echo  $fetchEvent['endDate']; ?></small>
                                <div class="divider"></div>
                                <p><?php echo  $fetchEvent['eventDesc']; ?></p>
                                </div>
                            </div>
                        </div>
                    <?php        
                        }
                    ?>
                    </div>
                    <?php
                    }
                    ?>
                    
                <?php
                    $sql = "SELECT * FROM events WHERE eventBisIDKey = '$bisnesID' AND eventStatus = 'Inactive';";
                    $query = mysqli_query($connect, $sql);
                    $countup = mysqli_num_rows($query);
                    if($countup!=0){
                ?>
                    <div class="row"> 
                        <div class="col s12 m12">
                            <h4>Upcoming Events</h4>
                        </div>
                    </div>
                    <div class="row">
                    <?php
                        while($fetchEvent = mysqli_fetch_array($query)){
                    ?>
                        <div class="col s12 m6 l6">
                            <div class="card card-events">
                                <div class="card-image">
                                    <img src="../../../assets/eventPics/<?php echo  $fetchEvent['eventPoster']; ?>">
                                    <span class="card-title">
                                        <?php echo  $busName; ?>
                                    </span>
                                </div>
                                <div class="card-content">
                                    <span class="card-title">
                                        <?php echo  $fetchEvent['eventTitle']; ?><br>
                                        <small><?php echo  $fetchEvent['startDate']; ?> - <?php echo  $fetchEvent['endDate']; ?></small>
                                    </span>
                                    <p class="truncate"><?php echo  $fetchEvent['eventDesc']; ?></p>
                                </div>
                            </div>
                        </div>
                    <?php        
                        }
                    ?>
                    </div>
                <?php
                    }
                    if($countup == 0 && $counton == 0){
                        echo '<p class="center-align no-data"><em>There is currently no ongoing or upcoming event</em></p>';
                    }
                ?> 
            </div>
            <!-- END of Events Tab -->

            <!-- Business Experience Tab -->
            <div id="business-album" class="col s12">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <a href="#business-experience" class="btn col s12 m12 l12 yellow darken-3 modal-trigger">Post an Experience</a>
                    </div>
                    <?php 
                            $town = $_GET['town'];
                            $fetchquery= mysqli_query($connect, "SELECT * FROM experience 
                            inner join business on experience.businessID = business.businessID
                            inner join businesslocation on business.businessID = businesslocation.bisLocIDkey 
                            inner join accounts on experience.uploaderID = accounts.aID 
                            where businesslocation.bisCityTown = '$town' order by expID desc"); 
                            $albumcount = mysqli_num_rows($fetchquery);
                            
                            if($albumcount > 0){
                                while($fetchdata = mysqli_fetch_array($fetchquery)){
                                    $fname = $fetchdata['fname'];
                                    $mname = $fetchdata['mname'];
                                    $minitial = substr($mname,0, 1);
                                    $lname = $fetchdata['lname'];
                                    $fullname = $fname.' '.$minitial.'. '.$lname;
                                    $date = $fetchdata['dateTime'];
                                    $thisDate = date_create($date);
                                    $thisDate = date_format($thisDate, "M d, Y | H:iA");
                            ?>
                    <div class="col s12 m6 l6">
                        <!-- card album-entry -->
                    
                        <div class="card card-album-entry">
                            <div class="card-image">
                                <img src="../../../assets/expPics/<?php echo $fetchdata['expPhoto'];?>" class="materialboxed col s12 m12 l12">
                            </div>
                            <div class="card-content">
                                <p>
                                    Uploader:<?php echo $fullname; ?><br>
                                    Date: <?php echo $thisDate; ?><br>
                                    <div class="divider"></div>
                                    Caption: <?php echo $fetchdata['expDesc'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php 
                            } 
                        }else{
                            echo "<p class='center-align no-data'><em>There is Currently no Album Present in this Establishment.</em></p>";
                        }        
                    ?>
                </div>
            </div>
            <!-- END of Experience Tab -->
</div>
<!-- End of Tabs -->
</div>
<!-- End of Container -->

    <!-- Favorite Modal -->
    <div id="favorite" class="modal">
        <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Note to favorite
        </h4>
            <form action="../../../db.sql/insertfavorite.php" method="post">
                <textarea class='message materialize-textarea' name='note' id='note' placeholder="Enter Note" required></textarea>
                <div class="section"></div>
                <center>
                    <input type='hidden' name='businessID' value='<?php echo $_GET['bID']?>'>
                    <input type='hidden' name='userID' value='<?php echo $_SESSION['userID']?>'>
                    <input type='hidden' name='town' value='<?php echo $_GET['town']?>'>
                    <input type="submit" value="Submit" class="btn yellow darken-3">
                </center>
            </form>
        </div>        
    </div>
    
    <!-- unFavorite Modal -->
    <div id="unfavorite" class="modal">
        <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Remove to favorite
        </h4>
            <form action="../../../db.sql/deletefavorite2.php" method="post">
                <p class="center-align">You're about to remove this business in your favorites</p>
                <center>
                    <input type='hidden' name='town' value='<?php echo $_GET['town']?>'>
                    <input type='hidden' name='bID' value='<?php echo $_GET['bID']?>'>
                    <input type='hidden' name='userID' value='<?php echo $_SESSION['userID']?>'>
                    <input type="submit" value="Confirm" class="btn yellow darken-3">
                </center>
            </form>
        </div>        
    </div>

    <!-- Cart Modal -->
    <div id="cart" class="modal">
        <div class="modal-content">
        <h4>
            <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Cart
        </h4>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <td>Product</td>
                        <td>Quantity</td>
                        <td>Price</td>
                        <td>Total</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <?php 
                $user = $_SESSION['userID'];
                $bisnes = $_GET['bID'];
                $fetchquery= mysqli_query($connect, "SELECT * FROM cart WHERE userID = '$user' and cartBisIDkey = '$bisnes' and cartStatus = 'Active' and cartItemType = 'Product'"); 
                while($fetchdata = mysqli_fetch_array($fetchquery)){
                ?>
                <tr>
                    <td><?php echo $fetchdata['itemName']?></td>
                    <td><?php echo $fetchdata['itemQty']?> pcs</td>
                    <td>Php <?php echo $fetchdata['itemPrice']?></td>
                    <td>Php <?php echo $fetchdata['cartTotal']?></td>
                    <td>
                        <form action='../../../db.sql/deletecartitem.php' method='post'>
                            <input type='hidden' name='cartID' value='<?php echo $fetchdata['cartID'];?>'>
                            <input type='hidden' name='town' value='<?php echo $_GET['town']?>'>
                            <input type='hidden' name='bID' value='<?php echo $_GET['bID']?>'>
                            <button type="submit" class="large modal-close yellow-text text-darken-3" ><i class="material-icons">delete</i></button>
                        </form>
                    </td>
                </tr>
                <?php } ?>
                <?php 
                $user = $_SESSION['userID'];
                $bisnes = $_GET['bID'];
                $fetchquery= mysqli_query($connect, "SELECT SUM(cartTotal) as total FROM cart WHERE userID = '$user' and cartBisIDkey = '$bisnes' and cartStatus = 'Active' and cartItemType = 'Product'"); 
                while($fetchdata = mysqli_fetch_array($fetchquery)){
                ?>
                <tr>
                    <td width='20%'>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td colspan='5' >
                        <div class='right-left'>Two-Tall: Php <?php echo $fetchdata['total'];?></div>
                    </td>
                </tr>
                <?php } ?>
            </table>
            <div class="section"></div>
            <div class="center-align">
                <a href="requestnext.php?town=<?php echo $_GET['town']?>&bID=<?php echo $_GET['bID']?>" class="btn yellow darken-3">Proceed</a>
            </div>
        </div>        
    </div>
    <!-- Experience Modal -->
    <div id="business-experience" class="col s12 modal">
        <form action="../../../db.sql/insertexp.php" class="form-horizontal"  role="form" class="col s12"  method="post" enctype="multipart/form-data">
        <div class="modal-content">
            <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                Post an Experience
            </h4>
            <div class='row'>
                <div class='input-field col s12 m12 l12'>
                    <i class="material-icons prefix yellow-text text-darken-3  hide-on-small-only">edit</i>
                    <textarea class='message materialize-textarea' name='myexp' id='myexp' required></textarea>
                    <label for='myexp'>My Experience</label>
                </div>
                </div>
                <div class="row">
                        <input type='hidden' name='uplaoder' value='<?php echo $_SESSION['userID'];?>'>
                        <input type='hidden' name='business' value='<?php echo $_GET['bID'];?>'>
                        <input type='hidden' name='town' value='<?php echo $_GET['town'];?>'>
                        <input type='hidden' name='datetime' value='<?php echo date("Y-m-d h:i: s");?> '>
                        <div class="file-field input-field">
                            <div class="btn btn-flat yellow darken-3 white-text">
                                <i class="material-icons">search</i>
                                <input type="file" name="photo" id='photo' accept="image/*">
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" class="file-path validate" placeholder="Upload Photo" id="photo">
                            </div>
                        </div>
                    </form>
                </div> 

            <div class="modal-footer">
                <input type="submit" value="Post" name="submit" class="btn yellow darken-3">
            </div>
            </form>
        </div>
    </div>
    <!-- END -->


    <!-- Product Details -->
    <?php 
        $fetchquery= mysqli_query($connect, "SELECT * FROM business inner join businessPS on business.businessID = businessps.businessPSIDkey
        WHERE businessPSIdkey='$bisnesID' and businessPSType='Product'"); 
        while($fetchproduct = mysqli_fetch_array($fetchquery)){
        ?>
        <div id="details-<?php echo $fetchproduct['businessPSID']; ?>" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    Product Details
                </h4>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage'] ;?>"  class="col s12 m12 l12 materialboxed" alt="product">
                    </div>
                    <div class="col s12 m6 l6">
                        <small>Name:</small><br> <strong><?php echo $fetchproduct['businessPSName']; ?></strong><br>
                        <small>Description:</small><br> <strong><?php echo $fetchproduct['businessPSDesc']; ?></strong><br>
                        <small>Price:</small><br> <strong>Php <?php echo $fetchproduct['businessPSPrice']; ?></strong><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer modal-footer-ps">
                <form action="../../../db.sql/insertToCart.php" method="POST">
                    <input type="hidden" name="prodID" value=<?php echo $fetchproduct['businessPSID'];?>>
                    <input type="hidden" name="userID" value=<?php echo $_SESSION['userID'];?>>
                    <input type="hidden" name="bisnesID" value=<?php echo $fetchproduct['businessID'];?>>
                    <input type="hidden" name="itemtype" value='Product'>
                    <input type="hidden" name="itemname" value='<?php echo $fetchproduct['businessPSName']; ?>'>
                    <input type="hidden" name="itemprice" value='<?php echo $fetchproduct['businessPSPrice']; ?>'>
                    <input type="hidden" name="town" value='<?php echo $_GET['town']; ?>'>
                    <div class="row right">
                        <div class="col s3 m3 l3 offset-s2 offset-m4 offset-l4 input-field">
                            <label for="qty">Quantity</label>
                            <input type="number" name="prodqty" value="1" class="validate right" autofocus required>
                        </div>
                        <div class="col s4 m4 l4 input-field">
                            <input type="submit" value="Add to Cart" name="submit" class="btn yellow darken-3">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php
        }?>

    <!-- Services Details -->
    <?php 
        $fetchquery= mysqli_query($connect, "SELECT * FROM businessPS WHERE businessPSIdkey='$bisnesID' and businessPSType='Service'"); 
        while($fetchproduct = mysqli_fetch_array($fetchquery)){
        ?>
        <div id="details-<?php echo $fetchproduct['businessPSID']; ?>" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>
                    <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    Service Details
                </h4>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <img src="../../../assets/ps/<?php echo $fetchproduct['businessPSImage'] ;?>"  class="col s12 m12 l12 materialboxed" alt="product">
                    </div>
                    <div class="col s12 m6 l6">
                        <small>Name:</small><br> <strong><?php echo $fetchproduct['businessPSName']; ?></strong><br>
                        <small>Description:</small><br> <strong><?php echo $fetchproduct['businessPSDesc']; ?></strong><br>
                        <small>Price:</small><br> <strong>Php <?php echo $fetchproduct['businessPSPrice']; ?></strong><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer modal-footer-ps">
                <a href="reservation.php?psID=<?php echo $fetchproduct['businessPSID']; ?>&town=<?php echo $_GET['town']; ?>&bID=<?php echo $_GET['bID']; ?>" class="btn yellow darken-3 right">Make an Appointment</a>
            </div>
        </div>
    <?php
        }
    ?>

    <!-- Report Modal -->
    <div id="reportbusiness" class="modal">
        <div class="modal-content">
            <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                Report
            </h4>
            <p><em>Tell us why</em></p>
            <form action="report.php" method="POST">
                <label for="message">Reason for the report</label>
                <textarea class='message materialize-textarea' name='message' required></textarea>
            </form>
        </div>
        <div class="modal-footer">
            <input type="submit" value="Submit Report" class="btn red darken-3">
        </div>
    </div>

    <!-- Confirm Service Modal -->
    <div id="confirmservice" class="modal">
        <div class="modal-content" style="float: center; text-align: center;">
            <p>Please review the details before confirming</p>
            <div class='row'>
                <div class='col s6 m6'>
                    <a href="#!" class="modal-close col s12 btn btn-large waves-effect grey darken-2">Back</a>
                </div>
                <div class='col s6 m6'>
                    <button form="hservice" type="submit" name='submitrequest' class='col s12 btn btn-large waves-effect grey darken-2'>Confirm</button></p>
                </div>
            </div>
        </div>
    </div>
    <?php include('modalnotif.php'); ?>
</div>


<script src="js/prefilled-dropdown.js"></script>
<!-- <script src="jquery/jquery-1.8.3.js"></script> -->
<!-- <script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.material.min.js"></script> -->
<?php require '../../partials/footer.php'; ?>


