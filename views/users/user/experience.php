<?php require '../../partials/header.php'; ?>
<?php require 'nav_town.php'; ?>
<link rel="stylesheet" href="../../../css/datatable.css">
<link rel="stylesheet" href="css/material.min.css">
<link rel="stylesheet" href="css/dataTables.material.min.css">
<script>
    $('.carousel').carousel();
    $('.carousel.carousel-slider').carousel({
        fullWidth: true,
        indicators: true
    });
</script>
<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul class="left">
                <li><a class="waves-effect waves-light"><i class="material-icons red-text">phone</i></a></li>
            </ul>
            <ul class="right">
                <li><a href="#cart" class="modal-trigger"><i class="material-icons">shopping_cart</i></a></li>
				<!-- <li><a href="notif.php"><i class="large material-icons">notifications</i></a></li> -->
		  </ul>
        </div>
    </nav>
	<table>
		<tr>
			<td style="width: 30%;">
				<div style="align:left; text-align:left;">
					<span style="font-size: 30px;">&nbsp;&nbsp;City of San Fernando</span>&nbsp;&nbsp;<a href="#post" class="modal-trigger col m12 btn btn-medium waves-effect yellow'">Post</a>
					</div>
				</td>
			</td>
		</tr>
	</table>
	<nav class="transparent" style="box-shadow: 0;">
         <ul>
            <li><a href="#events" class="modal-trigger yellow-text text-darken-3">Events <span class="badge white-text grey darken-2 new">1</span></a></li>
			<li><a href="dashboard_town.php" class="modal-trigger yellow-text text-darken-3">Tourist Spots</a></li>
            <li style="background-color: orange;"><a href="#experience" class="modal-trigger black-text text-darken-3">Experience</a></li>
		</ul>
    </nav>

	<h4><center>Experience</center></h4>
    <div class="row">
		<div class="col s12 m4 l4">
            <div class="card">
				  <div class="card-image">
						<div class="carousel carousel-slider">
							<a class="carousel-item waves-effect waves-light white darken-3" href=""><img src="../../../assets/images/image-1.jpg" ></a>
					    </div>
						<br>
						<a href="" style="width: 90px; height: 90px;" class="btn-floating left modal-trigger halfway-fab waves-effect waves-light yellow darken-3"><img src="../../../assets/images/a.jpg" class="z-depth-5 circle" style="width: 90px; height: 90px; border: 1px solid orange; border-radius: 100%;"></a>
				  </div>
                    <div class="card-content">
                    <table>
						<tr>
							<td>
								<span class="card-title"><h5>Webster</h5></span>
							</td>
							<td>
								<a href="#description" class="modal-trigger col m12 btn btn-medium waves-effect yellow'">Description</a>
								<a href="#album" class="modal-trigger col m12 dark green btn btn-medium waves-effect yellow'">Album</a>
							</td>
						</tr>
					</table>
                    </div>
						<a href="" class="modal-trigger col m12 btn btn-medium waves-effect yellow'">Business Name</a>
					
		   </div>
        </div>   
	
		<div class="col s12 m4 l4">
            <div class="card">
				  <div class="card-image">
						<div class="carousel carousel-slider">
							<a class="carousel-item waves-effect waves-light white darken-3" href=""><img src="../../../assets/images/image-1.jpg" ></a>
					    </div>
						<br>
						<a href="" style="width: 90px; height: 90px;" class="btn-floating left modal-trigger halfway-fab waves-effect waves-light yellow darken-3"><img src="../../../assets/images/a.jpg" class="z-depth-5 circle" style="width: 90px; height: 90px; border: 1px solid orange; border-radius: 100%;"></a>
				  </div>
                    <div class="card-content">
                    <table>
						<tr>
							<td>
								<span class="card-title"><h5>Raphael Ada</h5></span>
							</td>
							<td>
								<a href="#description" class="modal-trigger col m12 btn btn-medium waves-effect yellow'">Description</a>
								<a href="#album" class="modal-trigger col m12 dark green btn btn-medium waves-effect yellow'">Album</a>
							</td>
						</tr>
					</table>
                    </div>
					
						<a href="" class="modal-trigger col m12 btn btn-medium waves-effect yellow'">Business Name</a>
					
           </div>
        </div>

		<div class="col s12 m4 l4">
            <div class="card">
				  <div class="card-image">
						<div class="carousel carousel-slider">
							<a class="carousel-item waves-effect waves-light white darken-3" href=""><img src="../../../assets/images/image-1.jpg" ></a>
					    </div>
						<br>
						<a href="" style="width: 90px; height: 90px;" class="btn-floating left modal-trigger halfway-fab waves-effect waves-light yellow darken-3"><img src="../../../assets/images/a.jpg" class="z-depth-5 circle" style="width: 90px; height: 90px; border: 1px solid orange; border-radius: 100%;"></a>
				  </div>
                    <div class="card-content">
                    <table>
						<tr>
							<td>
								<span class="card-title"><h5>Thom Patacsil</h5></span>
							</td>
							<td>
								<a href="#description" class="modal-trigger col m12 btn btn-medium waves-effect yellow'">Description</a>
								<a href="#album" class="modal-trigger col m12 dark green btn btn-medium waves-effect yellow'">Album</a>
							</td>
						</tr>
					</table>
                    </div>
					
						<a href="" class="modal-trigger col m12 btn btn-medium waves-effect yellow'">Business Name</a>
					
           </div>
        </div>
		
	</div>

</div>

<!-- Events Modal -->
    <div id="events" class="modal">
        <div class="modal-content">
        <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <h4 class="center-align">
                EVENTS
            </h4>
            <div class="divider"></div>
            <div class="card">
                <div class="card-image">
					<img src="../../../assets/images/image-1.jpg" >
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">EVENTS TITLE<i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-reveal">
					<span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
                    <p>Description</p>
                </div>
            </div>
        </div>
    </div>
	
<!-- Post Modal -->
    <div id="post" class="modal">
        <div class="modal-content">
			<form class="col s12" action="dashboard_town.php" method="post">
				<div class='row'>
				  <div class='input-field col s12'>
					<i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">location_city</i>
					<input class='validate' type='text' name='dtitle' id='dtitle' required/>
					<label for='dtitle'>Experience Title</label>
				  </div>
				</div>
				
				<div class='row'>
				  <div class='input-field col s12'>
					<i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">location_city</i>
					<input class='validate' type='text' name='exp' id='exp' required/>
					<label for='exp'>Experience</label>
				  </div>
				</div>
				
				<div class='row'>
				  <div class='input-field col s12'>
					<i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">location_city</i>
					<input class='validate' type='text' name='photo' id='photo' required/>
					<label for='photo'>Upload Photo</label>
				  </div>
				</div>
				
			</form>
        </div>
    </div>
	
<!-- Description Modal -->
    <div id="description" class="modal">
        <div class="modal-content">
        <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            <h4 class="center-align">
                TOURIST SPOT NAME
            </h4>
			<h6 class="center-align">
				Tourist Spot Address
			</h6>
            <div class="divider"></div>
            <div class="card">
                <div class="card-image">
					<img src="../../../assets/images/image-1.jpg" >
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Family Bonding<i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-reveal">
					<span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i></span>
                    <p>Description</p>
                </div>
            </div>
        </div>
    </div>
	
<!-- Album Modal -->
    <div id="album" class="modal">
        <div class="modal-content">
		<a href="#" style="float: left;" class="large modal-close left yellow-text text-darken-3"><i class="material-icons" style="float: left;">close</i></a>
			  <div class="row">
			<div class="col s12 l12">
                <h4 class="center-align">ALBUMS</h4>
                <div class="">
               <table id="example">
                    <thead style="text-align: center;">
                        <th style="text-align: center;">Uploader</th>
                        <th style="text-align: center;">Posts</th>
                        <th style="text-align: center;">Description</th>
                        <th style="text-align: center;">Time</th>
                    </thead>
					<tbody>
						<tr>
							<td width="20%">Webster</td>
							<td width="35%"><img src="../../../assets/images/image-1.jpg" style="width:100%;" ></td>
							<td width="35%">Having fun time</td>
                            <td width="10%">11:00 AM</td>
                        </tr>
					</tbody>
                </table>
				</div>
            </div>
			
			<script>
            $(document).ready(function() {
                $('#example').DataTable({
					columnDefs: [
						{
							targets: [ 0, 1, 2],
							className: 'mdl-data-table__cell--non-numeric'
						}
					]				
				});
            });

        </script>
			
        </div>
        </div>
    </div>
	<script src="jquery/jquery-1.8.3.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.material.min.js"></script>
<?php require '../../partials/footer.php'; ?>
