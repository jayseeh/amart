<?php require '../../partials/header.php'; ?>
<?php include "../../../db.sql/connect_db.php"; ?>
<?php require 'nav.php'; ?>
    <div class="main">
        <div class="container">
            <h4>Favorites</h4>
                <table>
                    <thead>
                        <tr>
                            <th>Business Name</th>
                            <th>Note</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php 
                    $userID = $_SESSION['userID'];
                    $sql = "SELECT * from accounts 
                    INNER JOIN favorites on accounts.aID = favorites.noteUserID 
                    inner join business on favorites.noteBusinessIDkey = business.businessID 
                    inner join businesslocation on business.businessID = businesslocation.bisLocIDkey
                    where accounts.aID = '$userID' order by faviD desc";
                    $query = mysqli_query($connect, $sql);
                    while ($fetchdata = mysqli_fetch_array($query))  {?>
                    <tr>
                        <td><a href="dashboard_business.php?province=<?php echo $fetchdata['bisProvince'];?>&town=<?php echo $fetchdata['bisCityTown'];?>&bID=<?php echo $fetchdata['noteBusinessIDkey'];?>&cID=<?php echo $fetchdata['businessCategory'];?>" class="black-text"><?php echo $fetchdata['businessName'];?></a></td>
                        <td class="td-fave-note <?php echo $result; ?>"><a href="#edit-<?php echo $fetchdata['favID']; ?>" class="modal-trigger yellow-text text-darken-3"><?php 
                        $text = $fetchdata['favNote'];
                        $textlen = strlen($text);
                        if($textlen >= 30){
                            echo substr($text, 0, 30).'...';
                        }else {
                            echo $text;
                        }
                        ?></a></td>

                        <td class="center-align"><a href="#confirm-unfollow-<?php echo $fetchdata['favID']; ?>" class="modal-trigger"><i class="material-icons red-text">remove_circle_outline</i></a></td>
                    </tr>
                    <?php } ?>
                </table>
        </div>
         <?php 
            $userID = $_SESSION['userID'];
            $sql = "SELECT * from accounts INNER JOIN favorites on accounts.aID = favorites.noteUserID inner join business on 
            favorites.noteBusinessIDkey = business.businessID where accounts.aID = '$userID' order by faviD desc";
            $query = mysqli_query($connect, $sql);
            while ($fetchdata = mysqli_fetch_array($query))  {?>
        <div class="modal" id="edit-<?php echo $fetchdata['favID']?>">
            <div class="modal-content">
                <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    Note to favorite
                </h4>
                <form action="../../../db.sql/updatefavnote.php" method="post">
                    <textarea class='message materialize-textarea' type='text'  name='newnote' id='newnote' required><?php echo $fetchdata['favNote'];?></textarea>
                    <input type="hidden" name='favID' value="<?php echo $fetchdata['favID'];?>">
                    <div class="section"></div>
                    <center>
                        <input type="submit" value="Update" class="btn yellow darken-3">
                    </center>
                </form>
            </div>
        </div>

        <div class="modal" id="confirm-unfollow-<?php echo $fetchdata['favID']?>">
            <div class="modal-content">
                <h4>
                <a href="#" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
                    Remove to Favorites
                </h4>
                <p>Are you sure you want to remove this on your favorites?</p>
                <form action="../../../db.sql/deletefavorite.php" method="post">
                    <center>
                        <input type='hidden' name='businessID' value='<?php echo $_GET['bID']?>'>
                        <input type='hidden' name='userID' value='<?php echo $_SESSION['userID']?>'>
                        <input type="hidden" name='favID' value="<?php echo $fetchdata['favID'];?>">
                        <input type="submit" value="Confirm" class="btn yellow darken-3">
                    </center>
                </form>
            </div>
        </div>
        <?php } ?>
    </div>

<?php require '../../partials/footer.php'; ?>