<!-- Home Service Tab -->
<div id="homeservice" class="col s12">
    <h4>Home Service</h4>
    <p><em>Note: For home services it is advisable to allot 1 day prior to request</em></p>
    <form action="" method="post">
        <div class='row'>
            <div class='input-field col s12 m6 l6'>
                <input type="text" class="datepicker" id="reqdate" name="reqdate" required/>
                <label for="reqdate">Date of Request</label>
            </div>
            <div class='input-field col s12 m6 l6'>
                <input class='validate' type='text' name='service' id='service' required/>
                <label for='service'>Type of Service</label>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m4 l4">
                <label for="province">Province</label>
                <select name="province" id="province" class="browser-default"></select>
            </div>
            <div class="col s12 m4 l4">
                <label for="city">City/Town</label>
                <select name="city" id="municipality" class="browser-default"></select>
            </div>
            <div class="col s12 m4 l4">
                <label for="brgy">Brgy</label>
                <select name="brgy" id="brgy" class="browser-default"></select>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m6 l6 input-field">
                <label for="number">Contact Number</label>
                <input type="text" name="number">
            </div>
            <div class="col s12 m6 l6 input-field">
                <label for="street">Street</label>
                <input type="text" name="street">
            </div>
        </div>
        
        <div class='row'>
            <div class='input-field col s12 m12 l12'>
                <i class="material-icons prefix yellow-text text-darken-3 hide-on-small-only">edit</i>
                <textarea class='message materialize-textarea' type='text' name='description' id='description' required/></textarea>
                <label for='description' style="font-size:15px;">Job Description/Concern</label>
            </div>
        </div>
    </form>
    <center>
        <div>
            <a href="#confirmservice" id="confirm" name='confirm' class='col m12 btn btn-large waves-effect modal-trigger yellow darken-3'>Submit</a>
        </div>
    </center>            
</div>

<!-- END -->