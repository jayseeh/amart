$(document).ready(function() {
    $("#user_editID").click(function(){
        var province, dataProvince="<option value='' selected disabled>-SELECT PROVINCE-</option>";
        var mun, dataMun="<option value='' selected disabled>-SELECT MUNICIPALITY-</option>";
        var brgy, dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>";
        var pCode="", mCode="";
        setTimeout(function() {                
            //GET PROVINCE
            $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refprovince.json",
                success: function(response){                    
                    //$("#responsecontainer").html(response); 
                    province=JSON.parse(response);
                    console.log(province);
                    for (x in province.RECORDS){
                        dataProvince = dataProvince + "<option value="+province.RECORDS[x].provCode+">"+province.RECORDS[x].provDesc+"</option>";
                    }
                    var provCode = '<?php echo $provinceCode; ?>';
                    $("#user_province").html(dataProvince);
                    $("#user_province").val(provCode).attr("selected", "selected");
                    $("#user_municipality").html(dataMun);
                    $("#user_brgy").html(dataBrgy);
                }
            });
            //GET MUNICIPALITY
            $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refcitymun.json",
                success: function(response){                    
                    //$("#responsecontainer").html(response); 
                    mun=JSON.parse(response);
                    console.log(mun);
                }
            });
            //GET BARANGAY
            $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "https://raw.githubusercontent.com/clavearnel/philippines-region-province-citymun-brgy/master/json/refbrgy.json",
                success: function(response){                    
                    //$("#responsecontainer").html(response); 
                    brgy=JSON.parse(response);
                    console.log(brgy);
                }
            });
        },100);

        $("#user_province").change(function(){
            console.log("value: "+$(this).val());
            dataMun="<option value='' selected disabled>-SELECT MUNICIPALITY-</option>";
            dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>";
            pCode = $(this).val();
            filterMun = mun.RECORDS.filter( element => element.provCode == String(pCode));
            for (x in filterMun){
                dataMun= dataMun + "<option value="+filterMun[x].citymunCode+">"+(filterMun[x].citymunDesc).replace(/ *\([^)]*\) */g, "")+"</option>";
            }
            $("#user_municipality").html(dataMun);
            $("#user_brgy").html(dataBrgy);
            
            p = province.RECORDS.filter( element => element.provCode == String(pCode));

            ucprovince = p[0].provDesc;
            ucprovince = ucprovince.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });

            $("#u_province").val(ucprovince);
            $("#u_city").val("");
            $("#u_brgy").val("");

        });

        $("#user_municipality").change(function(){
            mCode = $(this).val();
            dataBrgy = "<option value='' selected disabled>-SELECT BARANGAY-</option>" ;
            filterBrgy = brgy.RECORDS.filter( element => element.citymunCode == String(mCode));
            for (x in filterBrgy){
                dataBrgy= dataBrgy + "<option value='"+filterBrgy[x].brgyCode+"'>"+filterBrgy[x].brgyDesc.toUpperCase() + "</option>";
            }
            $("#user_brgy").html(dataBrgy);

            m = mun.RECORDS.filter( element => element.citymunCode == String(mCode));

            uccity = (m[0].citymunDesc).replace(/ *\([^)]*\) */g, "");
            uccity = uccity.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });

            $("#u_city").val(uccity);
            $("#u_brgy").val("");

        });

        $("#user_brgy").change(function(){
            bCode = $(this).val();
            b = brgy.RECORDS.filter( element => element.brgyCode == String(bCode));
            console.log(b);
            ucbrgy = (b[0].brgyDesc).replace(/ *\([^)]*\) */g, "");
            ucbrgy = ucbrgy.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            
            $("#u_brgy").val(ucbrgy);
            
        });
    
    });
});