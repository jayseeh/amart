<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>

<div class="main">
	<div class="container">
		
	</div>
	
	<div class="row">
		<div class="col s12">
			<div class="card material-table">
			<div class="table-header">
				<span class="table-title">Marketplace</span>
				<div class="actions">
				<a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
				</div>
			</div>
			<table id="marketplace" class="striped">
			<thead>
				<th class="center-align">ID</th>
				<th class="center-align">Type</th>
				<th>Description</th>
				<th>Price</th>
				<th style="text-align: left;" class="hide-on-small-only">Location</th>
				<th class="center-align">Action</th>
			</thead>
			<tbody>
				<?php 
				$fetchquery= mysqli_query($connect, "SELECT * FROM marketplace 
				inner join accounts on marketplace.uploaderID = accounts.aID WHERE marketpostStatus = 'Active' ORDER BY marketpostID DESC"); 
				while($fetchdata = mysqli_fetch_array($fetchquery)){
					$address = explode(" | ", $fetchdata['uAddress']);
				?>
				<tr>
					<td class="center-align"><?php echo $fetchdata['marketpostID']; ?></td>
					<td class="center-align"><?php echo $fetchdata['marketpostType']; ?></td>
					<td><?php echo $fetchdata['marketpostDescription']; ?></td>
					<td>Php <?php echo $fetchdata['marketpostPrice']; ?></td>
					<td class="hide-on-small-only"><?php echo $address[3]; ?></td>
					<td class="center-align"><a href="#viewdetails?<?php echo $fetchdata['marketpostID']; ?>" class="modal-trigger"><i class="material-icons">search</i></a></td>
				</tr>
				<?php 
					} 
				?>
			</tbody>
			</table>
			</div>
		</div>
	</div>
</div>

<!-- Modals -->

<!-- view item details modal -->
<?php 
	//$thisID = $_GET['userid'];
	$fetchquery= mysqli_query($connect, "SELECT * FROM marketplace inner join accounts on marketplace.uploaderID = accounts.aID WHERE marketpostStatus = 'Active' "); 
	while($fetchdata = mysqli_fetch_array($fetchquery)){
		$checkDesc =  $fetchdata['marketpostDescription'];
		if ($checkDesc == '') {
			$desc = "Description is empty";
		} else {
			$desc = $checkDesc;
			$desc = preg_replace("/[\n]/", "<br>\n", $desc);
		}
?>
<div class="modal" id="viewdetails?<?php echo $fetchdata['marketpostID']; ?>">     
	<div class="modal-content">
		<h4>
			<a href="#" class="modal-close"><i class="material-icons yellow-text text-darken-3">arrow_back</i></a>
			Details
		</h4>
		
		<div class="row">
			<div class="col s12 m6 l6">
				<img src="../../../assets/marketPics/<?php echo $fetchdata['marketpostPic'];?>" class="materialboxed col s12 m12 l12" alt="marketplace-photo">
			</div>
			<div class="col s12 m6 l6">
				<small>Owner:</small><br>
				<strong>
				<?php 
				$fname = $fetchdata['fname'];
				$mname = $fetchdata['mname'];
				$minitial = substr($mname,0, 1);
				$lname = $fetchdata['lname'];
				echo $fullname = $fname.' '.$minitial.'. '.$lname;?>
				</strong><br>
				<!-- <small>Location:</small><br> <strong>La Union</strong><br> -->
				<small>Type:</small><br> <strong><?php echo $fetchdata['marketpostType']; ?></strong><br>
				<small>Description:</small><br> <strong><?php echo $desc; ?></strong><br>
			    <small>Contact Number:</small><br> <strong><?php echo $fetchdata['phoneNumber']; ?></strong><br>
				<small>Price:</small><br> <strong>Php <?php echo $fetchdata['marketpostPrice']; ?></strong>
			</div>
		</div>
		
	</div>
</div>
<?php } ?>

<script>
	$(document).ready(function() {
		$('#marketplace').dataTable({
			"columnDefs": [ 
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }],
        "oLanguage": {
            "sStripClasses": "",
            "sSearch": "",
            "sSearchPlaceholder": "Enter Keywords Here",
            "sInfo": "_START_ -_END_ of _TOTAL_",
            "sLengthMenu": '<span>Rows per page:</span><select class="browser-default">' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">All</option>' +
            '</select></div>'
        },
        order : [ 0, 'desc' ],
        bAutoWidth: false,
        resposive: true
        });
	});
</script>

<?php require '../../partials/footer.php' ?>