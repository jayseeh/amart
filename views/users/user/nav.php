<?php include "checkusertype.php"; ?>

<header>
  <nav class="grey darken-3 hide-on-large-only">
      <a href="dashboard.php" class="brand-logo center hide-on-large-only"><span class="yellow-text text-darken-3">A</span><small>MART</small>&nbsp;<span class="yellow-text text-darken-3">D</span><small>IGITAL</small></a>
      <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons  yellow-text text-darken-2">menu</i></a> 
  </nav>
</header>

<ul id="slide-out" class="sidenav sidenav-fixed grey darken-2">
	<nav class="grey darken-3 hide-on-med-and-down">
	<a href="dashboard.php" class="brand-logo center" style="text-decoration: none;"><span class="yellow-text text-darken-3" style="text-decoration: none;">A</span><small style="text-decoration: none;">MART</small>&nbsp;<span class="yellow-text text-darken-3" style="text-decoration: none;">D</span><small style="text-decoration: none;">IGITAL</small></a>
	</nav>

	<?php 
		include('navuserview.php'); 
		include('navitemslist.php')	;
	?>
</ul>

<script>
	$("#search-bar").hide();
	$("#search-trigger").click(function(){
		if($("#search-bar").is(":hidden")){
			$("#search-bar").fadeIn();
		}else{
			$("#search-bar").hide();
		}
	});

	$("#town").hide();
	$("#category").hide();
	$("#backp").hide();
	$("#backpp").hide();
	$("#backppp").hide();
	$("#sarisari").hide();
	$("#market").hide();
	$("#street").hide();
	
	$("#province").click(function(){
		$("ul ul li:first").hide();
		$("#backpp").hide();
		$("#town").fadeIn();
		$("#backp").fadeIn();
	});
	$("#backp").click(function(){
		$("ul ul li:first").fadeIn();
		$("#backp").hide();
		$("#town").hide();
	});
	$("#backpp").click(function(){
		$("ul ul li:first").hide();
		$("#backpp").hide();
		$("#category").hide();
		$("#backp").fadeIn();
		$("#town").fadeIn();
	});
	$("#town").click(function(){
		$("ul ul li:first").hide();
		$("#backp").hide();
		$("#town").hide();
		$("#category").fadeIn();
		$("#backpp").fadeIn();
	});
	$("#category").click(function(){
		$("ul ul li:first").hide();
		$("#backp").hide();
		$("#town").hide();
		$(".category").fadeIn();
	});	
</script>