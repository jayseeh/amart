<?php include "checkusertype.php"; ?>

<header>
  <nav class="grey darken-3 hide-on-large-only">
      <a href="dashboard.php" class="brand-logo center hide-on-med-and-up"><span class="yellow-text text-darken-3">A</span><small>MART</small>&nbsp;<span class="yellow-text text-darken-3">D</span><small>IGITAL</small></a>
			<a data-target="slide-out" class="sidenav-trigger"><i class="material-icons  yellow-text text-darken-2">menu</i></a> 
			<a class="waves-effect waves-light"><i class="material-icons red-text show-on-small">phone</i></a>
  </nav>
</header>

  <ul id="slide-out" class="sidenav sidenav-fixed grey darken-2">
    <nav class="grey darken-2 hide-on-med-and-down">
      <a href="dashboard.php" class="brand-logo center"><span class="yellow-text text-darken-3">A</span><small>MART</small>&nbsp;<span class="yellow-text text-darken-3">D</span><small>IGITAL</small></a>
    </nav>
	<li>
	<?php 
		include('navuserview.php');
		include('navitemslist.php'); 
	?>
  </ul>
  
<script>
	$("#search-bar").hide();
	$("#search-trigger").click(function(){
		if($("#search-bar").is(":hidden")){
			$("#search-bar").fadeIn();
		}else{
			$("#search-bar").hide();
		}
	});

  $("#backppp").fadeIn();
  $("#sarisari").fadeIn();
  $("#province").hide();
	$("#town").hide();
  $("#backp").hide();
	$("#backpp").hide();
	$("#category").hide();
	$("#market").hide();
	$("#street").hide();
  
	$("#province").click(function(){
		$("ul ul li:first").hide();
		$("#backpp").hide();
		$("#town").fadeIn();
		$("#backp").fadeIn();
	});
	$("#backp").click(function(){
		$("ul ul li:first").fadeIn();
		$("#backp").hide();
    $("#town").hide();
    $("#province").fadeIn();
	});
	$("#backpp").click(function(){
		$("ul ul li:first").hide();
		$("#backpp").hide();
		$("#category").hide();
		$("#backp").fadeIn();
		$("#town").fadeIn();
	});
	//back to category
	$("#backppp").click(function(){
		$("ul ul li:first").hide();
		$("#backppp").hide();
		$("#sarisari").hide();
		$("#category").fadeIn();
		$("#backpp").fadeIn();
	});

	$("#town").click(function(){
		$("ul ul li:first").hide();
		$("#backp").hide();
		$("#town").hide();
		$("#category").fadeIn();
		$("#backpp").fadeIn();
	});
	$("#category").click(function(){
		$("ul ul li:first").hide();
		$("#backp").hide();
		$("#town").hide();
		$(".category").fadeIn();
	});

	$("#freestore").click(function(){
		$("ul ul li:first").hide();
		$("#backpp").hide();
		$("#category").hide();
		$("#backppp").fadeIn();
		$("#sarisari").fadeIn();
	});
</script>