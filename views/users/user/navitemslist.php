<?php
    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);

    require_once "../../../db.sql/connect_db.php";

    $datenow = date('Y-m-d');

    if(isset($_GET['town'])){
        $town = $_GET['town']; 
    }

    if(isset($_GET['province'])){
        $province_get = $_GET['province'];
        $townquery = "SELECT businesslocation.bisCityTown, businesslocation.bisLocLat, businesslocation.bisLocLong, business.businessStatus
        FROM businesslocation 
        INNER JOIN business ON business.businessID = businesslocation.bisLocIDkey
        WHERE bisCityTown != '' 
        AND bisProvince = '$province_get' 
        AND bisLocLat != '' 
        AND business.businessStatus = 'Active'
        AND businessSubscriptionEnd >= '$datenow'
        OR businessSubscriptionEnd = '0000-00-00'
        AND bisCityTown != '' 
        AND bisProvince = '$province_get' 
        AND bisLocLat != '' 
        AND business.businessStatus = 'Active'
        GROUP BY bisCityTown ";
    }

    $provincequery = "SELECT businesslocation.bisProvince, businesslocation.bisLocLat, businesslocation.bisLocLong, business.businessStatus
    FROM businesslocation 
    INNER JOIN business ON business.businessID = businesslocation.bisLocIDkey
    WHERE business.businessStatus = 'Active' 
    AND bisProvince != '' 
    AND bisLocLat != '' 
    AND bisLocLong != '' 
    AND businessSubscriptionEnd >= '$datenow'
    OR businessSubscriptionEnd = '0000-00-00'
    AND business.businessStatus = 'Active' 
    AND bisProvince != '' 
    AND bisLocLat != '' 
    AND bisLocLong != '' 
    GROUP BY bisProvince";   
?>

    <li>
        <!-- Back Buttons -->
        <ul id="backp" style="background-color: gray;">
            <li class="bold waves-effect accordion accordnav">
                <a href="dashboard.php?province" class="yellow-text text-darken-3" id="back">Back to Province<i class="material-icons map yellow-text text-darken-3">arrow_left</i></a>
            </li>
        </ul>
        <ul id="backpp" style="background-color: gray;">
            <li class="bold waves-effect accordion accordnav">
                <a class="yellow-text text-darken-3" id="back" href="dashboard.php">Back to Province<i class="material-icons map yellow-text text-darken-3">arrow_left</i></a>
            </li>
        </ul>
        <ul id="backppp" style="background-color: gray;">
            <li class="bold waves-effect accordion accordnav">
                <a class="yellow-text text-darken-3" id="back">Back to Business<i class="material-icons map yellow-text text-darken-3">arrow_left</i></a>
            </li>
        </ul>
    </li>

    <li>
    <!-- Province List -->
    <ul id="province">
        <?php
            $query = mysqli_query($connect, $provincequery);
            while($fetchProvince = mysqli_fetch_array($query)){
        ?>
        <li class="bold waves-effect accordion accordnav" >
            <a href="dashboard_province.php?province=<?php echo $fetchProvince['bisProvince'];?>" class="yellow-text text-darken-3 province"><?php echo ucwords(strtolower($fetchProvince['bisProvince']));?><i class="material-icons my_location yellow-text text-darken-3">my_location</i></a>
        </li>
        <?php 
            }
        ?>
    </ul>

    <?php 
        if(isset($_GET['province'])){
    ?>

    <!-- Town List -->
    <ul id="town">
        <?php
            $queryMun = mysqli_query($connect, $townquery);
            while($fetchTown = mysqli_fetch_array($queryMun)){
        ?>
        <li class="bold waves-effect accordion accordnav">
            <a href="dashboard_town.php?province=<?php echo $province_get; ?>&town=<?php $thistown = $fetchTown['bisCityTown']; echo str_replace('(Capital)','',$thistown); ?>" class="yellow-text text-darken-3 town"><?php $thistown = $fetchTown['bisCityTown']; echo ucwords(strtolower(str_replace('(Capital)','',$thistown))); ?><i class="material-icons map yellow-text text-darken-3">map</i></a>
        </li>
        <?php
            }   
        ?>
    </ul>

    <?php } ?>

    <?php 
        if(isset($_GET['town'])){
    ?>
    <!-- Category List -->
    <ul id="category" class="collapsible">
        <!-- Worship Category -->
        <?php
            $businessQuery1= mysqli_query($connect, "SELECT * FROM  categories  
            INNER JOIN business ON business.businessCategory = categories.categoryID 
            INNER JOIN businesslocation ON business.businessID = businesslocation.bisLocIDkey 
            WHERE businesslocation.bisCityTown != '' 
            AND businesslocation.bisProvince = '$province_get'
            AND businesslocation.bisCityTown = '$town'  
            AND categories.categoryType = 'Worship'
            -- AND business.businessStatus = 'Active'
            GROUP BY categories.categoryName");
            
            $count1 = mysqli_num_rows($businessQuery1);
            while($fetchBusiness= mysqli_fetch_array($businessQuery1)){
                $ref = $fetchBusiness['businessCategory'];
                $businessCount = mysqli_query($connect, "SELECT COUNT(businessCategory) AS thisNumber FROM business 
                INNER JOIN businesslocation on business.businessID = businesslocation.bisLocIDkey WHERE businessCategory='$ref' AND businesslocation.bisProvince = '$province_get' AND businesslocation.bisLocLat!='' AND businesslocation.bisCityTown = '$town'");
                $fetchData = mysqli_fetch_array($businessCount);
                $cat_id = $fetchBusiness['categoryID'];
        ?>

            <li class="bold waves-effect accordion accordnav categorylists" data-id="<?php echo $cat_id; ?>">
                <a href="#" class="collapsible-header yellow-text text-darken-3 category" id="freestore-<?php echo $fetchBusiness['categoryName']; ?>"><?php echo $fetchBusiness['categoryName'].': '.$fetchData['thisNumber']; ?><i class="material-icons yellow-text text-darken-3"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <?php
                            $dropBusiness = mysqli_query($connect,"SELECT * FROM  categories  
                            INNER JOIN business 
                            ON business.businessCategory = categories.categoryID 
                            INNER JOIN businesslocation 
                            ON business.businessID=businesslocation.bisLocIDkey 
                            WHERE businesslocation.bisCityTown != '' 
                            AND businesslocation.bisProvince = '$province_get'
                            AND businesslocation.bisCityTown = '$town'  
                            AND categories.categoryType = 'Worship'
                            AND categories.categoryID = '$cat_id'
                            -- AND business.businessStatus = 'Active'
                            ");
                            while($listBusiness= mysqli_fetch_array($dropBusiness)){
                                $listBID = $listBusiness['businessID'];
                                $listBName = $listBusiness['businessName'];
                                echo "<li class='waves-effect businesslists' data-id='$listBID'><a href='dashboard_business.php?province=$province_get&town=$town&bID=$listBID&cID=$cat_id'>$listBName<i class='material-icons'>map</i></a></li>";                        
                            }
                        ?>
                    </ul>
                </div>
            </li>

        <?php
            }
        ?>

        <!-- Free Business Category -->
        <?php
        if($count1>0) echo "<div class='divider grey darken-1'></div>";
        ?>
        <?php
            $businessQuery2= mysqli_query($connect, "SELECT * FROM  categories  
            INNER JOIN business ON business.businessCategory = categories.categoryID 
            INNER JOIN businesslocation ON business.businessID=businesslocation.bisLocIDkey 
            WHERE businesslocation.bisCityTown != '' 
            AND businesslocation.bisLocLat!=''
            AND businesslocation.bisProvince = '$province_get'
            AND businesslocation.bisCityTown = '$town'  
            AND categories.categoryType = 'Free'
            AND business.businessStatus = 'Active'
            GROUP BY categories.categoryName");
            $count2 = mysqli_num_rows($businessQuery2);
            while($fetchBusiness= mysqli_fetch_array($businessQuery2)){
                $ref = $fetchBusiness['businessCategory'];
                $businessCount = mysqli_query($connect, "SELECT COUNT(businessCategory) AS thisNumber 
                    FROM business 
                    INNER JOIN businesslocation 
                    on business.businessID = businesslocation.bisLocIDkey 
                    WHERE businessCategory='$ref' 
                    AND businesslocation.bisLocLat!='' 
                    AND businesslocation.bisProvince = '$province_get'
                    AND businesslocation.bisCityTown = '$town'
                    AND business.businessStatus = 'Active'");
                $fetchData = mysqli_fetch_array($businessCount);
                $cat_id = $fetchBusiness['categoryID'];
        ?>
        <li class="bold waves-effect accordion accordnav categorylists" data-id="<?php echo $cat_id; ?>">
            <a href="#" class="collapsible-header yellow-text text-darken-3 category" id="freestore-<?php echo $fetchBusiness['categoryName']; ?>"><?php echo $fetchBusiness['categoryName'].': '.$fetchData['thisNumber']; ?><i class="material-icons yellow-text text-darken-3"></i></a>
            <div class="collapsible-body">
            <ul>
                <?php
                    $dropBusiness = mysqli_query($connect,"SELECT * FROM  categories  
                    INNER JOIN business 
                    ON business.businessCategory = categories.categoryID 
                    INNER JOIN businesslocation 
                    ON business.businessID=businesslocation.bisLocIDkey 
                    WHERE businesslocation.bisCityTown != '' 
                    AND businesslocation.bisProvince = '$province_get'
                    AND businesslocation.bisCityTown = '$town'  
                    AND categories.categoryType = 'Free'
                    AND categories.categoryID = '$cat_id'");
                    while($listBusiness= mysqli_fetch_array($dropBusiness)){
                        $listBID = $listBusiness['businessID'];
                        $listBName = $listBusiness['businessName'];
                        echo "<li class='waves-effect businesslists' data-id='$listBID'><a href='dashboard_business.php?province=$province_get&town=$town&bID=$listBID&cID=$cat_id'>$listBName<i class='material-icons'>map</i></a></li>";
                    }
                ?>
            </ul>
            </div>
        </li>

        <?php
            }
        ?>

        <!-- Micro Business Category -->
        <?php
        if($count2>0) echo "<div class='divider grey darken-1'></div>";
        ?>
        <?php
            $businessQuery3= mysqli_query($connect, "SELECT * FROM  categories  
            INNER JOIN business ON business.businessCategory = categories.categoryID 
            INNER JOIN businesslocation ON business.businessID = businesslocation.bisLocIDkey 
            WHERE categories.categoryType = 'Micro' 
            AND businesslocation.bisLocLat!=''
            AND businesslocation.bisProvince = '$province_get'
            AND businesslocation.bisCityTown = '$town'
            AND business.businessStatus = 'Active'
            AND businessSubscriptionEnd >= '$datenow'
            -- OR businessSubscriptionEnd = '0000-00-00'
            -- AND categories.categoryType = 'Micro' 
            -- AND businesslocation.bisLocLat!=''
            -- AND businesslocation.bisCityTown = '$town'
            -- AND business.businessStatus = 'Active'
            GROUP BY categories.categoryName ");
            $count3 = mysqli_num_rows($businessQuery3);
            while($fetchBusiness= mysqli_fetch_array($businessQuery3)){
                $ref = $fetchBusiness['businessCategory'];
                $businessCount = mysqli_query($connect, "SELECT COUNT(businessCategory) AS thisNumber FROM business 
                INNER JOIN businesslocation 
                on business.businessID = businesslocation.bisLocIDkey 
                WHERE businessCategory='$ref' 
                AND businesslocation.bisLocLat!=''
                AND businesslocation.bisProvince = '$province_get' 
                AND businesslocation.bisCityTown = '$town
                AND business.businessStatus = 'Active'
                AND businessSubscriptionEnd >= '$datenow'
                -- OR businessSubscriptionEnd = '0000-00-00'
                -- AND businessCategory='$ref' 
                -- AND businesslocation.bisLocLat!='' 
                -- AND businesslocation.bisCityTown = '$town
                -- AND business.businessStatus = 'Active'");
                $fetchData = mysqli_fetch_array($businessCount);
                $cat_id = $fetchBusiness['categoryID'];

        ?>
        <li class="bold waves-effect accordion accordnav categorylists" data-id="<?php echo $cat_id; ?>">
            <a href="#" class="collapsible-header yellow-text text-darken-3 category" id="freestore-<?php echo $fetchBusiness['categoryName']; ?>"><?php echo $fetchBusiness['categoryName'].': '.$fetchData['thisNumber']; ?><i class="material-icons yellow-text text-darken-3"></i></a>
            <div class="collapsible-body">
            <ul>
                <?php
                    $dropBusiness = mysqli_query($connect,"SELECT * FROM  business  
                    INNER JOIN categories 
                    ON business.businessCategory = categories.categoryID 
                    INNER JOIN businesslocation 
                    ON business.businessID=businesslocation.bisLocIDkey 
                    WHERE businesslocation.bisCityTown != ''
                    AND businesslocation.bisProvince = '$province_get' 
                    AND businesslocation.bisCityTown = '$town'  
                    AND categories.categoryType = 'Micro'
                    AND categories.categoryID = '$cat_id'
                    AND business.businessStatus = 'Active'
                    AND businessSubscriptionEnd >= '$datenow'
                    -- OR businessSubscriptionEnd = '0000-00-00'
                    -- AND businesslocation.bisCityTown != ''
                    -- AND businesslocation.bisProvince = '$province_get' 
                    -- AND businesslocation.bisCityTown = '$town'  
                    -- AND categories.categoryType = 'Micro'
                    -- AND categories.categoryID = '$cat_id'
                    -- AND business.businessStatus = 'Active'");
                    while($listBusiness= mysqli_fetch_array($dropBusiness)){
                        $listBID = $listBusiness['businessID'];
                        $listBName = $listBusiness['businessName'];
                        echo "<li class='waves-effect businesslists' data-id='$listBID'><a href='dashboard_business.php?province=$province_get&town=$town&bID=$listBID&cID=$cat_id'>$listBName<i class='material-icons'>map</i></a></li>";
                    }
                ?>
            </ul>
            </div>
        </li>

        <?php
            }
        ?>

        <!-- Small Business Category -->
        <?php
        if($count3>0) echo "<div class='divider grey darken-1'></div>";
        ?>
        <?php
            $businessQuery4= mysqli_query($connect, "SELECT * FROM  categories  
            INNER JOIN business 
            ON business.businessCategory = categories.categoryID 
            INNER JOIN businesslocation 
            ON business.businessID = businesslocation.bisLocIDkey 
            WHERE categories.categoryType = 'Small'
            AND businesslocation.bisProvince = '$province_get'
            AND businesslocation.bisCityTown = '$town'
            AND business.businessStatus = 'Active' 
            AND businessSubscriptionEnd >= '$datenow'
            -- OR businessSubscriptionEnd = '0000-00-00'
            -- AND categories.categoryType = 'Small'
            -- AND businesslocation.bisCityTown = '$town'
            -- AND business.businessStatus = 'Active' 
            GROUP BY categories.categoryName ");
            $count4 = mysqli_num_rows($businessQuery4);
            while($fetchBusiness= mysqli_fetch_array($businessQuery4)){
                $ref = $fetchBusiness['businessCategory'];
            $businessCount = mysqli_query($connect, "SELECT COUNT(businessCategory) AS thisNumber FROM business 
                    INNER JOIN businesslocation 
                    on business.businessID = businesslocation.bisLocIDkey 
                    WHERE businessCategory='$ref' 
                    AND businesslocation.bisLocLat!=''
                    AND businesslocation.bisProvince = '$province_get' 
                    AND businesslocation.bisCityTown = '$town'
                    AND business.businessStatus = 'Active'
                    AND businessSubscriptionEnd >= '$datenow'
                    -- OR businessSubscriptionEnd = '0000-00-00'
                    -- AND businessCategory='$ref' 
                    -- AND businesslocation.bisLocLat!='' 
                    -- AND businesslocation.bisCityTown = '$town'
                    -- AND business.businessStatus = 'Active'");
                $fetchData = mysqli_fetch_array($businessCount);
                $cat_id = $fetchBusiness['categoryID'];

        ?>
        <li class="bold waves-effect accordion accordnav categorylists" data-id="<?php echo $cat_id; ?>">
            <a href="#" class="collapsible-header yellow-text text-darken-3 category" id="freestore-<?php echo $fetchBusiness['categoryName']; ?>"><?php echo $fetchBusiness['categoryName'].': '.$fetchData['thisNumber']; ?><i class="material-icons yellow-text text-darken-3"></i></a>
            <div class="collapsible-body">
            <ul>
                <?php
                    $dropBusiness = mysqli_query($connect,"SELECT * FROM  categories  
                    INNER JOIN business 
                    ON business.businessCategory = categories.categoryID 
                    INNER JOIN businesslocation 
                    ON business.businessID=businesslocation.bisLocIDkey 
                    WHERE businesslocation.bisCityTown != '' 
                    AND businesslocation.bisProvince = '$province_get'
                    AND businesslocation.bisCityTown = '$town'  
                    AND categories.categoryType = 'Small'
                    AND categories.categoryID = '$cat_id'
                    AND business.businessStatus = 'Active'
                    AND businessSubscriptionEnd >= '$datenow'
                    -- OR businessSubscriptionEnd = '0000-00-00'
                    -- AND businesslocation.bisCityTown != '' 
                    -- AND businesslocation.bisCityTown = '$town'  
                    -- AND categories.categoryType = 'Small'
                    -- AND categories.categoryID = '$cat_id'
                    -- AND business.businessStatus = 'Active'");
                    while($listBusiness= mysqli_fetch_array($dropBusiness)){
                        $listBID = $listBusiness['businessID'];
                        $listBName = $listBusiness['businessName'];
                        echo "<li class='waves-effect businesslists' data-id='$listBID'><a href='dashboard_business.php?province=$province_get&town=$town&bID=$listBID&cID=$cat_id'>$listBName<i class='material-icons'>map</i></a></li>";                        
                    }
                ?>
            </ul>
            </div>
        </li>

        <?php
            }
        ?>

        <!-- Medium Business Category -->
        <?php
        if($count4>0) echo "<div class='divider grey darken-1'></div>";
        ?>
        <?php
            $businessQuery5= mysqli_query($connect, "SELECT * FROM  categories  
            INNER JOIN business 
            ON business.businessCategory = categories.categoryID 
            INNER JOIN businesslocation 
            ON business.businessID = businesslocation.bisLocIDkey 
            WHERE categories.categoryType= 'Medium' 
            AND businesslocation.bisProvince = '$province_get'
            AND businesslocation.bisCityTown = '$town' 
            AND business.businessStatus = 'Active'
            AND businessSubscriptionEnd >= '$datenow'
            -- OR businessSubscriptionEnd = '0000-00-00'
            -- AND categories.categoryType= 'Medium' 
            -- AND businesslocation.bisCityTown = '$town' 
            -- AND business.businessStatus = 'Active'
            GROUP BY categories.categoryName ");
            while($fetchBusiness= mysqli_fetch_array($businessQuery5)){
                $ref = $fetchBusiness['businessCategory'];
                $businessCount = mysqli_query($connect, "SELECT COUNT(businessCategory) AS thisNumber FROM business 
                    INNER JOIN businesslocation 
                    on business.businessID = businesslocation.bisLocIDkey 
                    WHERE businessCategory='$ref' 
                    AND businesslocation.bisLocLat!='' 
                    AND businesslocation.bisProvince = '$province_get'
                    AND businesslocation.bisCityTown = '$town'
                    AND business.businessStatus = 'Active'
                    AND businessSubscriptionEnd >= '$datenow'
                    -- OR businessSubscriptionEnd = '0000-00-00'
                    -- AND businessCategory='$ref' 
                    -- AND businesslocation.bisLocLat!='' 
                    -- AND businesslocation.bisCityTown = '$town'
                    -- AND business.businessStatus = 'Active'");
                $fetchData = mysqli_fetch_array($businessCount);
                $cat_id = $fetchBusiness['categoryID'];
        ?>
        <li class="bold waves-effect accordion accordnav categorylists" data-id="<?php echo $cat_id; ?>">
            <a href="#" class="collapsible-header yellow-text text-darken-3 category" id="freestore-<?php echo $fetchBusiness['categoryName']; ?>"><?php echo $fetchBusiness['categoryName'].': '.$fetchData['thisNumber']; ?><i class="material-icons yellow-text text-darken-3"></i></a>
            <div class="collapsible-body">
            <ul>
                <?php
                    $dropBusiness = mysqli_query($connect,"SELECT * FROM  categories  
                    INNER JOIN business 
                    ON business.businessCategory = categories.categoryID 
                    INNER JOIN businesslocation 
                    ON business.businessID=businesslocation.bisLocIDkey 
                    WHERE businesslocation.bisCityTown != '' 
                    AND businesslocation.bisProvince = '$province_get'
                    AND businesslocation.bisCityTown = '$town'  
                    AND categories.categoryType = 'Medium'
                    AND categories.categoryID = '$cat_id'
                    AND business.businessStatus = 'Active'
                    AND businessSubscriptionEnd >= '$datenow'
                    -- OR businessSubscriptionEnd = '0000-00-00'
                    -- AND businesslocation.bisCityTown != '' 
                    -- AND businesslocation.bisCityTown = '$town'  
                    -- AND categories.categoryType = 'Medium'
                    -- AND categories.categoryID = '$cat_id'
                    -- AND business.businessStatus = 'Active'");
                    while($listBusiness= mysqli_fetch_array($dropBusiness)){
                        $listBID = $listBusiness['businessID'];
                        $listBName = $listBusiness['businessName'];
                        echo "<li class='waves-effect businesslists' data-id='$listBID'><a href='dashboard_business.php?province=$province_get&town=$town&bID=$listBID&cID=$cat_id'>$listBName<i class='material-icons'>map</i></a></li>";
                    }
                ?>
            </ul>
            </div>
        </li>            
        <?php
            }
        ?>

        </ul>
    <?php } ?>
</li>

<script>
    <?php 
        if( isset($_GET['cID']) ){
            $categoryID = $_GET['cID'];
        } else {
            $categoryID = '';
        }

        if( isset($_GET['bID']) ){
            $businessID = $_GET['bID'];
        } else {
            $businessID = '';
        }
    ?>
    $(document).ready(function(){
        var categoryID = '<?php echo $categoryID ?>';
        $(".categorylists[data-id='" + categoryID + "']").addClass("active");

        var businessID = '<?php echo $businessID ?>';
        $(".businesslists[data-id='" + businessID + "']").addClass("active").addClass("yellow").addClass("darken-1");
        $(".businesslists[data-id='" + businessID + "'] a").addClass("grey-text").addClass("text-darken-3");
    });
</script>