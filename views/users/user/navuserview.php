<?php 
	$ID = $_SESSION['userID'];
	$fetchquery= mysqli_query($connect, "SELECT uPic, fname, mname, lname FROM accounts WHERE aID = '$ID'"); 
	while($fetchdata = mysqli_fetch_array($fetchquery)){
		$uPic = $fetchdata['uPic'];
		$fname = $fetchdata['fname'];
		$mname = $fetchdata['mname'];
		$minitial = substr($mname,0, 1);
		$lname = $fetchdata['lname'];

		$fullname = $fname.' '.$minitial.'. '.$lname;
	}

	$fetchquery = mysqli_query($connect, "SELECT count(DISTINCT cartRefCode) as x,  userID, cartRefCode, cartStatus
	FROM cart WHERE userID = '$ID' and cartStatus = 'Pending'"); 
	while($fetchdata = mysqli_fetch_array($fetchquery)){
		$cart_count = $fetchdata['x'];
	}
?>
	<li>
		<div class="user-view">
			<div class="background">
				<img src="../../../assets/images/bg.png">
			</div>
			<a href="profile.php">
				<img class="circle" src="../../../assets/images/<?php echo $uPic; ?>">
			</a>
			<span class="white-text name">
				<?php echo $fullname; ?>
				<a href="../../../db.sql/logout.php" class="btn btn-flat right transparent" id="options"><i class="material-icons yellow-text text-darken-3">exit_to_app</i><span style="position: absolute; bottom: -10px; right: 10px;" class="logout-label yellow-text text-darken-3"><small>Logout</small></span></a>
			</span>
		</div>
	</li>

	<!-- <li> -->
		<div class="row sidebar-item-container">
			<a href="cart.php" class="yellow-text text-darken-3">
				<div class="sidebar-item col s3 m3 l3 center-align waves-effect">
					<i class="material-icons yellow-text text-darken-3">shopping_cart</i>
					<?php echo $cart_count; ?>
				</div>
			</a>
			<a href="favorites.php" class="yellow-text text-darken-3">
				<div class="sidebar-item col s3 m3 l3 center-align waves-effect">
					<i class="material-icons yellow-text text-darken-3">favorite</i></a>
				</div>
			</a>
			<a href="marketplace.php" class="yellow-text text-darken-3">
				<div class="sidebar-item col s3 m3 l3 center-align waves-effect">
					<i class="material-icons yellow-text text-darken-3">shopping_basket</i></a>
				</div>
			</a>
			<a href="#" id="search-trigger" class="yellow-text text-darken-3">
				<div class="sidebar-item col s3 m3 l3 center-align waves-effect">
					<i class="material-icons yellow-text text-darken-3">search</i>
				</div>
			</a>
		</div>
	<!-- </li> -->

    <li id="search-bar">
   		<form action="searchresults.php" method="POST">
			<div class="input-field">
				<input id="searchbar" name="search-keyword" type="search" required>
				<label class="label-icon" for="searchbar"><i class="material-icons">search</i></label>
				<i class="material-icons">close</i>
			</div>
    	</form>
    </li>

	<script>
		var is_mobile = 0;
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		// some code..
			is_mobile = 1;
		} else {
			is_mobile = 0;
		}

		$(document).ready(function(){
			if(!is_mobile){
				$('.logout-label').css('display', 'none');
			}
		
			$("#options").mouseenter(function () {
				$(".logout-label").show();
			});

			$("#options").mouseleave(function () {
				$(".logout-label").hide();
			});
		});
	</script>