<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>

<div class="main">
<div class="container">
<br>
<a href="dashboard_province.php" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
	<ul class="collection with-header">
		<li class="collection-header" style="float: center; text-align: center;"><h4> Notifications </h4></li>
		<li class="collection-item avatar"><img src="../../../assets/images/a.jpg" class="circle">
			<span class="title"><!-- Get business username where the user requested items/services are already approved --><b>Amart Partners</b> approved your requested 
				<!-- item/product/service name --><b>Car Service</b>
			</span>
			<p style="color: grey;">October 21, 2018</p>
		</li>
		<li class="collection-item avatar"><img src="../../../assets/images/a.jpg" class="circle">
			<span class="title"><!-- Get business username where the user requested items/services are already approved --><b>Amart Partners</b> approved your requested 
				<!-- item/product/service name --><b>GTX SABER MAX</b>
			</span>
			<p style="color: grey;">October 21, 2018</p>
		</li>	
		<li class="collection-item avatar"><img src="../../../assets/images/a.jpg" class="circle">
			<span class="title"><!-- Get business username where the user requested items/services are already approved --><b>Amart Partners</b> approved your requested 
				<!-- item/product/service name --><b>Accessories</b>
			</span>
			<p style="color: grey;">October 21, 2018</p>
		</li>
	</ul>
	
</div>
</div>


<?php require '../../partials/footer.php'; ?>