<?php require '../../partials/header.php'; ?>
<?php require 'nav_business.php'; ?>

<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul class="left">
                <li><a href="dashboard_business.php?bname=Business%20Name"><i class="material-icons">arrow_back</i></a></li>
                <li><a class="waves-effect waves-light"><img src="../../../assets/images/emergency.png" class="hide-on-small-only valign-wrapper" style="height: 30px; width: 30px;"></a></a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <h4>Make a Request</h4>
        <div class="row">
            <ul class="tabs">
                <li class="tab"><a href="#products" class="active">Products</a></li>
                <li class="tab"><a href="#services" class="active">Services</a></li>
            </ul>
            <div id="products">Products
                <div class="row">
                    <div class="col s12 m4 l4">
                        <div class="card product-card center-align">
                            <div class="card-image">
                                <img src="../../../assets/images/prod 1.png" alt="product">
                            </div>
                            <div class="card-content">
                                <span class="card-title">Product Name</span>
                                <span class="product-price">Php 150</span><br />
                                <span class="product-status green-text">in stock: 3 remaining</span>
                            </div>
                            <div class="card-action">
                                <form action="#" method="get">
                                    <input type="number" name="qty" placeholder="qty" />
                                    <button type="submit" class="btn btn-flat yellow darken-3">Add to Cart</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 l4">
                        <div class="card product-card center-align">
                            <div class="card-image">
                                <img src="../../../assets/images/prod 1.png" alt="product">
                            </div>
                            <div class="card-content">
                                <span class="card-title">Product Name</span>
                                <span class="product-price">Php 150</span><br />
                                <span class="product-status red-text">out of stock</span>
                            </div>
                            <div class="card-action">
                                <form action="#" method="get">
                                    <input type="number" name="qty" placeholder="qty" disabled/>
                                    <button type="submit" class="btn btn-flat yellow darken-3 disabled">Add to Cart</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 l4">
                        <div class="card product-card center-align">
                            <div class="card-image">
                                <img src="../../../assets/images/prod 1.png" alt="product">
                            </div>
                            <div class="card-content">
                                <span class="card-title">Product Name</span>
                                <span class="product-price">Php 150</span><br />
                                <span class="product-status green-text">in stock: 1 remaining</span>
                            </div>
                            <div class="card-action">
                                <form action="#" method="get">
                                    <input type="number" name="qty" placeholder="qty" />
                                    <button type="submit" class="btn btn-flat yellow darken-3">Add to Cart</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="services">Services
                <div class="row">
                    <div class="col s12 m4 l4">
                        <div class="card product-card center-align">
                            <div class="card-image">
                                <img src="../../../assets/images/prod 1.png" alt="product">
                            </div>
                            <div class="card-content">
                                <span class="card-title">Service Name</span>
                                <span class="service-price">Php 150</span><br />
                                
                            </div>
                            <div class="card-action">
                                <form action="#" method="get">
                                    <input type="number" name="pax" placeholder="pax" />
                                    <button type="submit" class="btn btn-flat yellow darken-3">Get Service</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col s12 m4 l4">
                        <div class="card product-card center-align">
                            <div class="card-image">
                                <img src="../../../assets/images/prod 1.png" alt="product">
                            </div>
                            <div class="card-content">
                                <span class="card-title">Service Name</span>
                                <span class="service-price">Php 150</span><br />
                                
                            </div>
                            <div class="card-action">
                                <form action="#" method="get">
                                    <input type="number" name="pax" placeholder="pax" />
                                    <button type="submit" class="btn btn-flat yellow darken-3">Get Service</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col s12 m4 l4">
                        <div class="card product-card center-align">
                            <div class="card-image">
                                <img src="../../../assets/images/prod 1.png" alt="product">
                            </div>
                            <div class="card-content">
                                <span class="card-title">Service Name</span>
                                <span class="service-price">Php 150</span><br />
                                
                            </div>
                            <div class="card-action">
                                <form action="#" method="get">
                                    <input type="number" name="pax" placeholder="pax" />
                                    <button type="submit" class="btn btn-flat yellow darken-3">Get Service</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <h4>Cart</h4>
        <table class="table table-responsive">
            <thead>
                <tr>
                    <td>Name</td>
                    <td>Pax</td>
                    <td>Price</td>
                    <td>total</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tr>
                <td>Service Name</td>
                <td>1</td>
                <td>150</td>
                <td>150</td>
                <td><a href="#"><i class="material-icons">edit</i></a><a href="#"><i class="material-icons">close</i></a></td>
            </tr>
        </table>
        <div class="section"></div>
        <span class="right"><a href="requestnext.php" class="btn btn-flat yellow darken-3">Proceed</a></span>
    </div>
</div>


    
<!-- Confirm Service Modal -->
<div id="confirmservice" class="modal">
    <div class="modal-content" style="float: center; text-align: center;">
        <p>Please review the details before confirming</p>
    <div class='row'>
        <div class='col s6 m6'>
            <a href="#!" class="modal-close col s12 btn btn-large waves-effect grey darken-2">Back</a>
        </div>
        <div class='col s6 m6'>
            <button form="hservice" type="submit" name='submitrequest' class='col s12 btn btn-large waves-effect grey darken-2'>Confirm</button></p>
        </div>
    </div>
</div>

<script>
    function disablePostButton(){
		var check = document.getElementById("confirm");
        var service = document.getElementById("service");
		var contact = document.getElementById("contact");
		var address = document.getElementById("address");
        var reqdate = document.getElementById("reqdate");
        var desc = document.getElementById("description");
        

        if(service.value != "" && contact.value != "" && address.value != "" && reqdate.value != "" && desc.value != ""){
            check.classList.remove("grey");
            check.className += " modal-trigger yellow darken-3";
            console.log("button is enabled");
            console.log(check.classList);
        }else{

        }
    }

    document.getElementById("service").addEventListener("change", function(){
         disablePostButton();
	});
	
    document.getElementById("contact").addEventListener("change", function(){
         disablePostButton();
	});
 
    document.getElementById("address").addEventListener("change", function(){
         disablePostButton();
	});

	document.getElementById("reqdate").addEventListener("change", function(){
         disablePostButton();
    });

    document.getElementById("description").addEventListener("change", function(){
         disablePostButton();
    });
</script>
<?php require '../../partials/footer.php'; ?>
