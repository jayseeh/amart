<?php require '../../partials/header.php'; ?>
<?php require 'nav_business.php'; ?>

<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul class="left">
                <li><a href="requestnext.php"><i class="material-icons">arrow_back</i></a></li>
                <li><a href="#" class="waves-effect waves-light"><img src="../../../assets/images/emergency.png" class="hide-on-small-only valign-wrapper" style="height: 30px; width: 30px;"></a></a></li>
            </ul>
        </div>
    </nav>
    <div class="container center-align">
        <h4>Request Sent to Business Name</h4>
        
        <h4>Details</h4>
        <div>
            <p>
                <span><strong>Juan Del Rosario</strong></span><br>
                <span>Contact Number: 09959886992</span><br>
                <span>Address: San Fernando, La Union</span><br>
                <span>Date: 10/8/2018</span><br>
                <span>Time: 10:00 AM</span><br>
                <span>Message: Thank You</span>
            </p>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <td>Name</td>
                        <td>Qty</td>
                        <td>Price</td>
                        <td>Total</td>
                    </tr>
                </thead>
                <tr>
                    <td>Product Name</td>
                    <td>1</td>
                    <td>150</td>
                    <td>150</td>
                </tr>
            </table>
            <div class="section"></div>
            <a href="dashboard_business.php?bname=Business Name" class="btn yellow darken-3">Return</a>
        </div>
    </div>
</div>

<?php require '../../partials/footer.php'; ?>
