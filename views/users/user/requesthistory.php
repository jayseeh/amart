<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>

<div class="main">
<nav>
    <div class="nav-wrapper yellow darken-2">
        <ul class="left">
            <li><a href="dashboard.php"><i class="material-icons">arrow_back</i></a></li>
        </ul>
        <ul class="right">
            
        </ul>
    </div>
</nav>

    <div class="container">
        <h4>Request History</h4>
        <div class="row">
            <ul class="tabs">
                <li class="tab"><a href="#ongoing">Ongoing</a></li>
                <li class="tab"><a href="#finished">Finished</a></li>
            </ul>
        <div id="ongoing">
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <td>Request No.</td>
                        <td>Business Name</td>
                        <td>Date of Request</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tr>
                    <td>2</td>
                    <td>Aguila</td>
                    <td>10/10/2018</td>
                    <td><a href="viewrequest.php"><i class="material-icons">search</i></td>
                </tr>
            </table>
        </div>

        <div id="finished">
        <table class="table table-responsive">
                <thead>
                    <tr>
                        <td>Request No.</td>
                        <td>Business Name</td>
                        <td>Date of Request</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tr>
                    <td>1</td>
                    <td>Perez Aluminum Glass</td>
                    <td>09/28/2018</td>
                    <td><a href="viewrequest.php"><i class="material-icons">search</i></a></td>
                </tr>
            </table>
        </div>
        </div>
    </div>
</div>
<?php require '../../partials/footer.php'; ?>
