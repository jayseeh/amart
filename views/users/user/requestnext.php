<?php require '../../partials/header.php'; ?>
<?php require 'nav_business.php'; ?>

<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul class="left">
                <li><a href="dashboard_business.php?province=<?php echo $_GET['province']?>&town=<?php echo $_GET['town']?>&bID=<?php echo $_GET['bID']?>&cID=<?php echo $_GET['cID']?>"><i class="material-icons">arrow_back</i></a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <h4>Finalizing Request</h4>
        <div class="row">
            <div class="col s12 m12 l12">
            
                <h4>Request Details</h4>
                
                <form action="../../../db.sql/requestconfirm.php" method="post">
                        <input type='hidden' name='town' value='<?php echo $_GET['town']?>'>
                        <input type="hidden" name="province" value='<?php echo $_GET['province']; ?>'>
                        <input type="hidden" name="cID" value='<?php echo $_GET['cID']; ?>'>
                        <input type='hidden' name='bID' value='<?php echo $_GET['bID']?>'>
                        <input type='hidden' name='userID' value='<?php echo $_SESSION['userID']?>'>
                        <input type='hidden' name='datereq' value='<?php date_default_timezone_set('Asia/Manila'); echo date('Y-m-d');?>'>
                        
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <label for="appt-date">Pick-up Date</label>
                            <input type="text" class="datepicker" name="pickupdate" required />
                        </div>
                        <div class="col s12 m6 l6">
                            <label for="appt-time">Pick-up Time</label>
                            <input type="text" class="timepicker" name="pickuptime" required />
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <label for="message">Message</label>
                            <input type="text" name="message" />
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" class="btn yellow darken-3 right col s12 m12 l12">
                    </div>
            </div>
        </div>

        <h4>Cart</h4>
        <table class="table table-responsive">
            <thead>
                <tr>
                    <td>Name</td>
                    <td>Qty</td>
                    <td class="center-align">Price</td>
                    <td class="center-align">Total</td>
                </tr>
            </thead>
            <?php 
            $user = $_SESSION['userID'];
            $bisnes = $_GET['bID'];
            $fetchquery= mysqli_query($connect, "SELECT * FROM cart WHERE userID = '$user' and cartBisIDkey = '$bisnes' and cartStatus = 'Active' and cartItemType = 'Product'"); 
            while($fetchdata = mysqli_fetch_array($fetchquery)){
            ?>
            <tr>
                <td><?php echo $fetchdata['itemName']?></td>
                <td><?php echo $fetchdata['itemQty']?> pcs</td>
                <td class="center-align">Php <?php echo $fetchdata['itemPrice']?></td>
                <td class="center-align">Php <?php echo $fetchdata['cartTotal']?></td>
            </tr>
            <?php } ?>
            <?php 
                $user = $_SESSION['userID'];
                $bisnes = $_GET['bID'];
                $fetchquery= mysqli_query($connect, "SELECT SUM(cartTotal) as total FROM cart WHERE userID = '$user' and cartBisIDkey = '$bisnes' and cartStatus = 'Active' and cartItemType = 'Product'"); 
                while($fetchdata = mysqli_fetch_array($fetchquery)){
            ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <div class="center-align"><strong>Cart Total: Php <?php echo $fetchdata['total'];?></strong></div>
                        <input type='hidden' name='cartTotal' value='<?php echo $fetchdata['total'];?>'>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <div class="section"></div>
    </div>
    </form>
</div>

<?php require '../../partials/footer.php'; ?>
