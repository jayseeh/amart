<?php include('../../partials/header.php'); ?>
<?php require 'nav_town.php'; 
    $psID = $_GET['psID'];
    $town = $_GET['town'];
    $province = $_GET['province'];
    $bID = $_GET['bID'];
    $fetchquery= mysqli_query($connect, "SELECT * FROM businessPS WHERE businessPSID='$psID'"); 
    while($fetchproduct = mysqli_fetch_array($fetchquery)){
        $businessPSName = $fetchproduct['businessPSName'];
        $businessPSDesc = $fetchproduct['businessPSDesc'];
        $businessPSPrice = $fetchproduct['businessPSPrice'];
    }

?>

<div class="main">
    <div class="container">
    <form action="../../../db.sql/insertToCart.php" method="POST">
        <h4>
            <a href="dashboard_business.php?province=<?php echo $province; ?>&town=<?php echo $town; ?>&bID=<?php echo $bID; ?>" class="large modal-close yellow-text text-darken-3"><i class="material-icons">arrow_back</i></a>
            Make an Appointment
        </h4>
        <div class="row">
            <div class="row">
                <div class="col s12 m12 l12">
                    <ul class="tabs">
                        <li class="tab s6 m6 l6"><a href="#reservation">Reservation</a></li>
                        <li class="tab s6 m6 l6"><a href="#homeservice">Home Service</a></li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col s12 m4 l4">
                        <small>Name:</small><br> <strong><?php echo $businessPSName; ?></strong><br>
                    </div>
                    <div class="col s12 m4 l4">
                        <small>Description:</small><br> <strong><?php echo $businessPSDesc; ?></strong><br>
                    </div>
                    <div class="col s12 m4 l4">
                        <small>Price:</small><br> <strong>Php <?php echo $businessPSPrice; ?></strong><br>
                    </div>
                </div>
                <!-- Reservation -->
                <div id="reservation" class="col s12 m12 l12">
                <form action="../../../db.sql/insertToCart.php" method="POST">
                    <div class="row">
                        <div class='input-field col s12 m6 l6'>
                            <input type="text" class="datepicker" id="aptdate" name="aptdate" required/>
                            <label for="aptdate">Appointment Date</label>
                        </div>
                        <div class='input-field col s12 m6 l6'>
                            <input type="text" class="timepicker" id="apttime" name="apttime" required/>
                            <label for="apttime">Appointment Time</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class='input-field col s12 m12 l12'>
                            <label for="aptnote">Note</label>
                            <textarea class='message materialize-textarea' name='aptnote' required></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="prodID" value=<?php echo $psID; ?>>
                    <input type="hidden" name="serviceType" id="serviceType" value="reservation">
                    <input type="submit" value="Submit Request" name="submit" class="btn yellow darken-3 right">
                </form>
                </div>
                <!-- end of reservation -->

                <!-- Home Service -->
                <div id="homeservice" class="col s12 m12 l12">
                <form action="../../../db.sql/insertToCart.php" method="POST">
                    <div class="row">
                        <div class='input-field col s12 m6 l6'>
                            <input type="text" class="datepicker" id="aptdate" name="aptdate" required/>
                            <label for="aptdate">Appointment Date</label>
                        </div>
                        <div class='input-field col s12 m6 l6'>
                            <input type="text" class="timepicker" id="apttime" name="apttime" required/>
                            <label for="apttime">Appointment Time</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class='input-field col s12 m12 l12'>
                            <textarea class='message materialize-textarea' name='aptnote' required></textarea>
                            <label for="aptnote">Note</label>
                        </div>
                    </div>
                    <input type="hidden" name="prodID" value=<?php echo $psID; ?>>
                    <input type="hidden" name="serviceType" id="serviceType" value="homeservice">
                    <input type="submit" value="Submit Request" name="submit" class="btn yellow darken-3 right">
                </form>
                </div>
                <!-- end of Home Service -->
            </div>
            <!-- end of tabs row -->
        </div>
    </div>
</div>
<?php include('../../partials/footer.php'); ?>