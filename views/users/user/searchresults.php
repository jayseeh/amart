<?php 
    require '../../partials/header.php'; 
    include('nav.php');

    $keyword = mysqli_real_escape_string($connect, $_POST['search-keyword']);
    $searchedword = $_POST['search-keyword'];
    $datenow = date('Y-m-d');

    $query = mysqli_query($connect, "SELECT * FROM businessps 
    INNER JOIN business 
    ON businessps.businessPSIDkey=business.businessID 
    INNER JOIN businesslocation 
    ON business.businessID=businesslocation.bisLocIDkey 

    WHERE 
    businessps.businessPSDesc LIKE '%$keyword%' AND business.businessStatus = 'Active' AND businessSubscriptionEnd >= '$datenow'
    OR business.businessName LIKE '%$keyword%' AND business.businessStatus = 'Active' AND businessSubscriptionEnd >= '$datenow'
    OR business.businessDescription LIKE '%$keyword%' AND business.businessStatus = 'Active' AND businessSubscriptionEnd >= '$datenow'
    OR businessps.businessPSName LIKE '%$keyword%' AND business.businessStatus = 'Active' AND businessSubscriptionEnd >= '$datenow'
    GROUP BY businessName
    ");
    $count = mysqli_num_rows($query);

?>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col s12 hide-on-med-and-up">
                <form action="searchresults.php" method="POST">
                    <div class="input-field">
                        <input id="searchbar" name="search-keyword" type="search" required>
                        <label class="label-icon" for="searchbar"><i class="material-icons">search</i></label>
                        <i class="material-icons">close</i>
                    </div>
                </form>
            </div>
        </div>
        <h4>Search Results: <?php echo $count; ?> Item Found</h4>
        <p>Keyword: "<?php echo $searchedword;?>"</p>
        <div class="separator"></div>
        <ul class="collection collection-search-results">
        <?php 
            $address_arr = array();
            while ($fetch=mysqli_fetch_array($query)){
                array_push($address_arr, $fetch['bisNumber']);
                array_push($address_arr, $fetch['bisStreet']);
                array_push($address_arr, $fetch['bisBrgy']);
                array_push($address_arr, $fetch['bisCityTown']);
                array_push($address_arr, $fetch['bisProvince']);

                $address = implode(', ', $address_arr);

        ?>
  
        <li class="collection-item avatar">
            <img src="../../../assets/images/<?php echo $fetch['bisPhoto'];?>" alt="" class="circle">
            <b> <?php echo $fetch['businessPSName']; ?></b><br>
            <span class="title"><b><a href="dashboard_business.php?province=<?php echo $fetch['bisProvince']; ?>&town=<?php echo $fetch['bisCityTown'];?>&bID=<?php echo $fetch['businessID']; ?>&cID=<?php echo $fetch['businessCategory']; ?>"><?php echo $fetch['businessName'];?></a></b></span>
          
            <p>
                <?php echo $address ;?><br>
            </p>
            </li>
            <?php 
            }
            ?>
            <!-- <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a> -->
            <!--<li class="collection-item avatar">
            <img src="../../../assets/images/default-image.png" alt="" class="circle">
            <span class="title"><b>Business Title Here</b></span>
            <p>
                Address: 169 P. Burgos St, Tanqui, City of San Fernando, La Union<br>
            </p>
            <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
            </li>-->
        </ul>
    </div>
    <div id="back-to-top" class="right">
        <a href="#top" class="btn btn-floating transparent"><i class="material-icons yellow-text text-darken-3">arrow_upward</i></a>
    </div>
</div>

<?php include('../../partials/footer.php'); ?>