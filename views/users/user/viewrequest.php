<?php require '../../partials/header.php'; ?>
<?php require 'nav.php'; ?>

<div class="main">
    <nav>
        <div class="nav-wrapper yellow darken-2">
            <ul class="left">
                <li><a href="requesthistory.php"><i class="material-icons">arrow_back</i></a></li>                
            </ul>
        </div>
    </nav>
    <div class="container">
        <h4>Request No.</h4>
        
        <h4>Details</h4>
        <div>
            <p>
                <span><strong>Business Name</strong></span><br>
                <span>Date: 10/8/2018</span><br>
                <span>Time: 10:00 AM</span><br>
                <span>Message: Thank You</span>
            </p>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <td>Name</td>
                        <td>Qty</td>
                        <td>Price</td>
                        <td>Total</td>
                    </tr>
                </thead>
                <tr>
                    <td>Product Name</td>
                    <td>1</td>
                    <td>150</td>
                    <td>150</td>
                </tr>
            </table>
            
        </div>
    </div>
</div>

<?php require '../../partials/footer.php'; ?>
